{!! view_render_event('bagisto.shop.products.buy_now.before', ['product' => $product]) !!}

@php
    $width = ($product->type != 'game') ? '49' : '100';
@endphp

<button type="submit" class="btn btn-lg btn-primary buynow" {{ ! $product->isSaleable(1) ? 'disabled' : '' }}
style="width: <?php echo $width.'%';?>;">
    {{($product->type == 'game') ?  __('shop::app.products.rent-now') :  __('shop::app.products.buy-now') }}
</button>

{!! view_render_event('bagisto.shop.products.buy_now.after', ['product' => $product]) !!}