<?php

Route::group([
        'prefix'        => 'admin/gameproduct',
        'middleware'    => ['web', 'admin']
    ], function () {

        Route::get('', 'GameStore\GameProduct\Http\Controllers\Admin\GameProductController@index')->defaults('_config', [
            'view' => 'gameproduct::admin.index',
        ])->name('admin.gameproduct.index');

});