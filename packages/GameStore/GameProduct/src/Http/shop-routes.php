<?php

Route::group([
        'prefix'     => 'gameproduct',
        'middleware' => ['web', 'theme', 'locale', 'currency']
    ], function () {

        Route::get('/', 'GameStore\GameProduct\Http\Controllers\Shop\GameProductController@index')->defaults('_config', [
            'view' => 'gameproduct::shop.index',
        ])->name('shop.gameproduct.index');

});