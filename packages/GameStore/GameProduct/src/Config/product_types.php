<?php

return [
    'game' => [
        'key' => 'game',
        'name' => 'Game',
        'class' => 'GameStore\GameProduct\Type\Game',
        'sort' => 8
    ],
];