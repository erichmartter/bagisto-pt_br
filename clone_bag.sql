-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 07-Abr-2021 às 17:16
-- Versão do servidor: 10.4.16-MariaDB
-- versão do PHP: 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `clone_bag`
--

DELIMITER $$
--
-- Funções
--
CREATE DEFINER=`root`@`localhost` FUNCTION `get_url_path_of_category` (`categoryId` INT, `localeCode` VARCHAR(255)) RETURNS VARCHAR(255) CHARSET utf8mb4 BEGIN

                DECLARE urlPath VARCHAR(255);

                IF NOT EXISTS (
                    SELECT id
                    FROM categories
                    WHERE
                        id = categoryId
                        AND parent_id IS NULL
                )
                THEN
                    SELECT
                        GROUP_CONCAT(parent_translations.slug SEPARATOR '/') INTO urlPath
                    FROM
                        categories AS node,
                        categories AS parent
                        JOIN category_translations AS parent_translations ON parent.id = parent_translations.category_id
                    WHERE
                        node._lft >= parent._lft
                        AND node._rgt <= parent._rgt
                        AND node.id = categoryId
                        AND node.parent_id IS NOT NULL
                        AND parent.parent_id IS NOT NULL
                        AND parent_translations.locale = localeCode
                    GROUP BY
                        node.id;

                    IF urlPath IS NULL
                    THEN
                        SET urlPath = (SELECT slug FROM category_translations WHERE category_translations.category_id = categoryId);
                    END IF;
                 ELSE
                    SET urlPath = '';
                 END IF;

                 RETURN urlPath;
            END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `addresses`
--

CREATE TABLE `addresses` (
  `id` int(10) UNSIGNED NOT NULL,
  `address_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'null if guest checkout',
  `cart_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'only for cart_addresses',
  `order_id` int(10) UNSIGNED DEFAULT NULL COMMENT 'only for order_addresses',
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postcode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vat_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default_address` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'only for customer_addresses',
  `additional` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`additional`)),
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `addresses`
--

INSERT INTO `addresses` (`id`, `address_type`, `customer_id`, `cart_id`, `order_id`, `first_name`, `last_name`, `gender`, `company_name`, `address1`, `address2`, `postcode`, `city`, `state`, `country`, `email`, `phone`, `vat_id`, `default_address`, `additional`, `created_at`, `updated_at`) VALUES
(1, 'cart_billing', NULL, 3, NULL, 'Erich', 'Martter', NULL, NULL, 'Avenida Antonio Peres 2750, Interior 5 -, D Ap 102', NULL, '96081300', 'Pelotas', 'RS', 'BR', 'erichmartter@gmail.com', '53984459543', NULL, 0, NULL, '2021-04-07 14:08:03', '2021-04-07 14:08:03'),
(2, 'cart_shipping', NULL, 3, NULL, 'Erich', 'Martter', NULL, NULL, 'Avenida Antonio Peres 2750, Interior 5 -, D Ap 102', NULL, '96081300', 'Pelotas', 'RS', 'BR', 'erichmartter@gmail.com', '53984459543', NULL, 0, NULL, '2021-04-07 14:08:03', '2021-04-07 14:08:03'),
(3, 'order_shipping', NULL, NULL, 1, 'Erich', 'Martter', NULL, NULL, 'Avenida Antonio Peres 2750, Interior 5 -, D Ap 102', NULL, '96081300', 'Pelotas', 'RS', 'BR', 'erichmartter@gmail.com', '53984459543', NULL, 0, NULL, '2021-04-07 14:08:34', '2021-04-07 14:08:34'),
(4, 'order_billing', NULL, NULL, 1, 'Erich', 'Martter', NULL, NULL, 'Avenida Antonio Peres 2750, Interior 5 -, D Ap 102', NULL, '96081300', 'Pelotas', 'RS', 'BR', 'erichmartter@gmail.com', '53984459543', NULL, 0, NULL, '2021-04-07 14:08:34', '2021-04-07 14:08:34'),
(5, 'customer', 1, NULL, NULL, 'Erich', 'Martter', NULL, NULL, 'Avenida Antonio Peres 2750, Interior 5 -, D Ap 102', NULL, '96081300', 'Pelotas', 'RS', 'BR', 'erichmartter@gmail.com', '53984459543', NULL, 0, NULL, '2021-04-07 14:12:57', '2021-04-07 14:12:57'),
(6, 'cart_billing', 1, 2, NULL, 'Erich', 'Martter', NULL, NULL, 'Avenida Antonio Peres 2750, Interior 5 -, D Ap 102', NULL, '96081300', 'Pelotas', 'RS', 'BR', 'erichmartter@gmail.com', '53984459543', NULL, 0, NULL, '2021-04-07 14:12:57', '2021-04-07 14:12:57'),
(7, 'cart_shipping', 1, 2, NULL, 'Erich', 'Martter', NULL, NULL, 'Avenida Antonio Peres 2750, Interior 5 -, D Ap 102', NULL, '96081300', 'Pelotas', 'RS', 'BR', 'erichmartter@gmail.com', '53984459543', NULL, 0, NULL, '2021-04-07 14:12:57', '2021-04-07 14:12:57'),
(8, 'order_shipping', 1, NULL, 2, 'Erich', 'Martter', NULL, NULL, 'Avenida Antonio Peres 2750, Interior 5 -, D Ap 102', NULL, '96081300', 'Pelotas', 'RS', 'BR', 'erichmartter@gmail.com', '53984459543', NULL, 0, NULL, '2021-04-07 14:13:15', '2021-04-07 14:13:15'),
(9, 'order_billing', 1, NULL, 2, 'Erich', 'Martter', NULL, NULL, 'Avenida Antonio Peres 2750, Interior 5 -, D Ap 102', NULL, '96081300', 'Pelotas', 'RS', 'BR', 'erichmartter@gmail.com', '53984459543', NULL, 0, NULL, '2021-04-07 14:13:15', '2021-04-07 14:13:15'),
(10, 'cart_billing', 1, 4, NULL, 'Erich', 'Martter', NULL, NULL, 'Avenida Antonio Peres 2750, Interior 5 -, D Ap 102', NULL, '96081300', 'Pelotas', 'RS', 'BR', 'user@example.com', '53984459543', NULL, 0, NULL, '2021-04-07 14:32:06', '2021-04-07 14:32:06'),
(11, 'cart_shipping', 1, 4, NULL, 'Erich', 'Martter', NULL, NULL, 'Avenida Antonio Peres 2750, Interior 5 -, D Ap 102', NULL, '96081300', 'Pelotas', 'RS', 'BR', 'user@example.com', '53984459543', NULL, 0, NULL, '2021-04-07 14:32:06', '2021-04-07 14:32:06'),
(12, 'order_shipping', 1, NULL, 3, 'Erich', 'Martter', NULL, NULL, 'Avenida Antonio Peres 2750, Interior 5 -, D Ap 102', NULL, '96081300', 'Pelotas', 'RS', 'BR', 'user@example.com', '53984459543', NULL, 0, NULL, '2021-04-07 14:34:08', '2021-04-07 14:34:08'),
(13, 'order_billing', 1, NULL, 3, 'Erich', 'Martter', NULL, NULL, 'Avenida Antonio Peres 2750, Interior 5 -, D Ap 102', NULL, '96081300', 'Pelotas', 'RS', 'BR', 'user@example.com', '53984459543', NULL, 0, NULL, '2021-04-07 14:34:08', '2021-04-07 14:34:08');

-- --------------------------------------------------------

--
-- Estrutura da tabela `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `api_token` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `role_id` int(10) UNSIGNED NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `api_token`, `status`, `role_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Example', 'admin@example.com', '$2y$10$BnatD3CwFYkig0HUeITsoeUGRTpamDnovaMonlxtowm2JTm5b7M6S', 'ia8DAmZwxrad4dnTF5kjeJgde6PMjvYBNLU5iF0SwxMoCAzXwqGZWErnF7B1YxILMdOHigMpP7mTYoSP', 1, 1, NULL, '2021-04-07 18:58:51', '2021-04-07 18:58:51');

-- --------------------------------------------------------

--
-- Estrutura da tabela `admin_password_resets`
--

CREATE TABLE `admin_password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `attributes`
--

CREATE TABLE `attributes` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `validation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `is_required` tinyint(1) NOT NULL DEFAULT 0,
  `is_unique` tinyint(1) NOT NULL DEFAULT 0,
  `value_per_locale` tinyint(1) NOT NULL DEFAULT 0,
  `value_per_channel` tinyint(1) NOT NULL DEFAULT 0,
  `is_filterable` tinyint(1) NOT NULL DEFAULT 0,
  `is_configurable` tinyint(1) NOT NULL DEFAULT 0,
  `is_user_defined` tinyint(1) NOT NULL DEFAULT 1,
  `is_visible_on_front` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `swatch_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `use_in_flat` tinyint(1) NOT NULL DEFAULT 1,
  `is_comparable` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `attributes`
--

INSERT INTO `attributes` (`id`, `code`, `admin_name`, `type`, `validation`, `position`, `is_required`, `is_unique`, `value_per_locale`, `value_per_channel`, `is_filterable`, `is_configurable`, `is_user_defined`, `is_visible_on_front`, `created_at`, `updated_at`, `swatch_type`, `use_in_flat`, `is_comparable`) VALUES
(1, 'sku', 'SKU', 'text', NULL, 1, 1, 1, 0, 0, 0, 0, 0, 0, '2021-04-07 18:58:51', '2021-04-07 18:58:51', NULL, 1, 0),
(2, 'name', 'Name', 'text', NULL, 3, 1, 0, 1, 1, 0, 0, 0, 0, '2021-04-07 18:58:51', '2021-04-07 18:58:51', NULL, 1, 1),
(3, 'url_key', 'URL Key', 'text', NULL, 4, 1, 1, 0, 0, 0, 0, 0, 0, '2021-04-07 18:58:51', '2021-04-07 18:58:51', NULL, 1, 0),
(4, 'tax_category_id', 'Tax Category', 'select', NULL, 5, 0, 0, 0, 1, 0, 0, 0, 0, '2021-04-07 18:58:51', '2021-04-07 18:58:51', NULL, 1, 0),
(5, 'new', 'New', 'boolean', NULL, 6, 0, 0, 0, 0, 0, 0, 0, 0, '2021-04-07 18:58:51', '2021-04-07 18:58:51', NULL, 1, 0),
(6, 'featured', 'Featured', 'boolean', NULL, 7, 0, 0, 0, 0, 0, 0, 0, 0, '2021-04-07 18:58:51', '2021-04-07 18:58:51', NULL, 1, 0),
(7, 'visible_individually', 'Visible Individually', 'boolean', NULL, 9, 1, 0, 0, 0, 0, 0, 0, 0, '2021-04-07 18:58:51', '2021-04-07 18:58:51', NULL, 1, 0),
(8, 'status', 'Status', 'boolean', NULL, 10, 1, 0, 0, 0, 0, 0, 0, 0, '2021-04-07 18:58:51', '2021-04-07 18:58:51', NULL, 1, 0),
(9, 'short_description', 'Short Description', 'textarea', NULL, 11, 1, 0, 1, 1, 0, 0, 0, 0, '2021-04-07 18:58:51', '2021-04-07 18:58:51', NULL, 1, 0),
(10, 'description', 'Description', 'textarea', NULL, 12, 1, 0, 1, 1, 0, 0, 0, 0, '2021-04-07 18:58:51', '2021-04-07 18:58:51', NULL, 1, 1),
(11, 'price', 'Price', 'price', 'decimal', 13, 1, 0, 0, 0, 1, 0, 0, 0, '2021-04-07 18:58:51', '2021-04-07 18:58:51', NULL, 1, 1),
(12, 'cost', 'Cost', 'price', 'decimal', 14, 0, 0, 0, 1, 0, 0, 1, 0, '2021-04-07 18:58:51', '2021-04-07 18:58:51', NULL, 1, 0),
(13, 'special_price', 'Special Price', 'price', 'decimal', 15, 0, 0, 0, 0, 0, 0, 0, 0, '2021-04-07 18:58:51', '2021-04-07 18:58:51', NULL, 1, 0),
(14, 'special_price_from', 'Special Price From', 'date', NULL, 16, 0, 0, 0, 1, 0, 0, 0, 0, '2021-04-07 18:58:51', '2021-04-07 18:58:51', NULL, 1, 0),
(15, 'special_price_to', 'Special Price To', 'date', NULL, 17, 0, 0, 0, 1, 0, 0, 0, 0, '2021-04-07 18:58:51', '2021-04-07 18:58:51', NULL, 1, 0),
(16, 'meta_title', 'Meta Title', 'textarea', NULL, 18, 0, 0, 1, 1, 0, 0, 0, 0, '2021-04-07 18:58:51', '2021-04-07 18:58:51', NULL, 1, 0),
(17, 'meta_keywords', 'Meta Keywords', 'textarea', NULL, 20, 0, 0, 1, 1, 0, 0, 0, 0, '2021-04-07 18:58:51', '2021-04-07 18:58:51', NULL, 1, 0),
(18, 'meta_description', 'Meta Description', 'textarea', NULL, 21, 0, 0, 1, 1, 0, 0, 1, 0, '2021-04-07 18:58:51', '2021-04-07 18:58:51', NULL, 1, 0),
(19, 'width', 'Width', 'text', 'decimal', 22, 0, 0, 0, 0, 0, 0, 1, 0, '2021-04-07 18:58:51', '2021-04-07 18:58:51', NULL, 1, 0),
(20, 'height', 'Height', 'text', 'decimal', 23, 0, 0, 0, 0, 0, 0, 1, 0, '2021-04-07 18:58:51', '2021-04-07 18:58:51', NULL, 1, 0),
(21, 'depth', 'Depth', 'text', 'decimal', 24, 0, 0, 0, 0, 0, 0, 1, 0, '2021-04-07 18:58:51', '2021-04-07 18:58:51', NULL, 1, 0),
(22, 'weight', 'Weight', 'text', 'decimal', 25, 1, 0, 0, 0, 0, 0, 0, 0, '2021-04-07 18:58:51', '2021-04-07 18:58:51', NULL, 1, 0),
(23, 'color', 'Color', 'select', NULL, 26, 0, 0, 0, 0, 1, 1, 1, 0, '2021-04-07 18:58:51', '2021-04-07 18:58:51', NULL, 1, 0),
(24, 'size', 'Size', 'select', NULL, 27, 0, 0, 0, 0, 1, 1, 1, 0, '2021-04-07 18:58:51', '2021-04-07 18:58:51', NULL, 1, 0),
(25, 'brand', 'Brand', 'select', NULL, 28, 0, 0, 0, 0, 1, 0, 1, 1, '2021-04-07 18:58:51', '2021-04-07 18:58:51', NULL, 1, 0),
(26, 'guest_checkout', 'Guest Checkout', 'boolean', NULL, 8, 1, 0, 0, 0, 0, 0, 0, 0, '2021-04-07 18:58:51', '2021-04-07 18:58:51', NULL, 1, 0),
(27, 'product_number', 'Product Number', 'text', NULL, 2, 0, 1, 0, 0, 0, 0, 0, 0, '2021-04-07 18:58:51', '2021-04-07 18:58:51', NULL, 1, 0),
(29, 'tipo_conta', 'Tipo Conta', 'select', '', NULL, 0, 0, 0, 0, 0, 1, 1, 0, '2021-04-07 13:57:16', '2021-04-07 14:02:13', 'text', 0, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `attribute_families`
--

CREATE TABLE `attribute_families` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `is_user_defined` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `attribute_families`
--

INSERT INTO `attribute_families` (`id`, `code`, `name`, `status`, `is_user_defined`) VALUES
(1, 'default', 'Default', 0, 1),
(3, 'jogo', 'Jogo', 0, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `attribute_groups`
--

CREATE TABLE `attribute_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` int(11) NOT NULL,
  `is_user_defined` tinyint(1) NOT NULL DEFAULT 1,
  `attribute_family_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `attribute_groups`
--

INSERT INTO `attribute_groups` (`id`, `name`, `position`, `is_user_defined`, `attribute_family_id`) VALUES
(1, 'General', 1, 0, 1),
(2, 'Description', 2, 0, 1),
(3, 'Meta Description', 3, 0, 1),
(4, 'Price', 4, 0, 1),
(5, 'Shipping', 5, 0, 1),
(11, 'General', 1, 0, 3),
(12, 'Description', 2, 0, 3),
(13, 'Meta Description', 3, 0, 3),
(14, 'Price', 4, 0, 3),
(15, 'Shipping', 5, 0, 3);

-- --------------------------------------------------------

--
-- Estrutura da tabela `attribute_group_mappings`
--

CREATE TABLE `attribute_group_mappings` (
  `attribute_id` int(10) UNSIGNED NOT NULL,
  `attribute_group_id` int(10) UNSIGNED NOT NULL,
  `position` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `attribute_group_mappings`
--

INSERT INTO `attribute_group_mappings` (`attribute_id`, `attribute_group_id`, `position`) VALUES
(1, 1, 1),
(1, 11, 1),
(2, 1, 3),
(2, 11, 3),
(3, 1, 4),
(3, 11, 4),
(4, 1, 5),
(4, 11, 5),
(5, 1, 6),
(5, 11, 6),
(6, 1, 7),
(6, 11, 7),
(7, 1, 8),
(7, 11, 8),
(8, 1, 10),
(8, 11, 10),
(9, 2, 1),
(9, 12, 1),
(10, 2, 2),
(10, 12, 2),
(11, 4, 1),
(11, 14, 1),
(12, 4, 2),
(13, 4, 3),
(13, 14, 2),
(14, 4, 4),
(14, 14, 3),
(15, 4, 5),
(15, 14, 4),
(16, 3, 1),
(16, 13, 1),
(17, 3, 2),
(17, 13, 2),
(18, 3, 3),
(18, 13, 3),
(19, 5, 1),
(20, 5, 2),
(21, 5, 3),
(22, 5, 4),
(22, 15, 1),
(23, 1, 11),
(24, 1, 12),
(25, 1, 13),
(26, 1, 9),
(26, 11, 9),
(27, 1, 2),
(27, 11, 2),
(29, 11, 11);

-- --------------------------------------------------------

--
-- Estrutura da tabela `attribute_options`
--

CREATE TABLE `attribute_options` (
  `id` int(10) UNSIGNED NOT NULL,
  `admin_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `attribute_id` int(10) UNSIGNED NOT NULL,
  `swatch_value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `attribute_options`
--

INSERT INTO `attribute_options` (`id`, `admin_name`, `sort_order`, `attribute_id`, `swatch_value`) VALUES
(1, 'Red', 1, 23, NULL),
(2, 'Green', 2, 23, NULL),
(3, 'Yellow', 3, 23, NULL),
(4, 'Black', 4, 23, NULL),
(5, 'White', 5, 23, NULL),
(6, 'S', 1, 24, NULL),
(7, 'M', 2, 24, NULL),
(8, 'L', 3, 24, NULL),
(9, 'XL', 4, 24, NULL),
(14, 'Primária', 1, 29, NULL),
(15, 'Secundária', 2, 29, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `attribute_option_translations`
--

CREATE TABLE `attribute_option_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attribute_option_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `attribute_option_translations`
--

INSERT INTO `attribute_option_translations` (`id`, `locale`, `label`, `attribute_option_id`) VALUES
(1, 'en', 'Red', 1),
(2, 'en', 'Green', 2),
(3, 'en', 'Yellow', 3),
(4, 'en', 'Black', 4),
(5, 'en', 'White', 5),
(6, 'en', 'S', 6),
(7, 'en', 'M', 7),
(8, 'en', 'L', 8),
(9, 'en', 'XL', 9),
(12, 'en', 'First', 14),
(13, 'fr', '', 14),
(14, 'nl', '', 14),
(15, 'tr', '', 14),
(16, 'es', '', 14),
(17, 'pt_BR', 'Primária', 14),
(18, 'en', 'Second', 15),
(19, 'fr', '', 15),
(20, 'nl', '', 15),
(21, 'tr', '', 15),
(22, 'es', '', 15),
(23, 'pt_BR', 'Secundária', 15);

-- --------------------------------------------------------

--
-- Estrutura da tabela `attribute_translations`
--

CREATE TABLE `attribute_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attribute_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `attribute_translations`
--

INSERT INTO `attribute_translations` (`id`, `locale`, `name`, `attribute_id`) VALUES
(1, 'en', 'SKU', 1),
(2, 'en', 'Name', 2),
(3, 'en', 'URL Key', 3),
(4, 'en', 'Tax Category', 4),
(5, 'en', 'New', 5),
(6, 'en', 'Featured', 6),
(7, 'en', 'Visible Individually', 7),
(8, 'en', 'Status', 8),
(9, 'en', 'Short Description', 9),
(10, 'en', 'Description', 10),
(11, 'en', 'Price', 11),
(12, 'en', 'Cost', 12),
(13, 'en', 'Special Price', 13),
(14, 'en', 'Special Price From', 14),
(15, 'en', 'Special Price To', 15),
(16, 'en', 'Meta Description', 16),
(17, 'en', 'Meta Keywords', 17),
(18, 'en', 'Meta Description', 18),
(19, 'en', 'Width', 19),
(20, 'en', 'Height', 20),
(21, 'en', 'Depth', 21),
(22, 'en', 'Weight', 22),
(23, 'en', 'Color', 23),
(24, 'en', 'Size', 24),
(25, 'en', 'Brand', 25),
(26, 'en', 'Allow Guest Checkout', 26),
(27, 'en', 'Product Number', 27),
(34, 'en', 'Type Count', 29),
(35, 'fr', '', 29),
(36, 'nl', '', 29),
(37, 'tr', '', 29),
(38, 'es', '', 29),
(39, 'pt_BR', 'Tipo Conta', 29);

-- --------------------------------------------------------

--
-- Estrutura da tabela `bookings`
--

CREATE TABLE `bookings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `qty` int(11) DEFAULT 0,
  `from` int(11) DEFAULT NULL,
  `to` int(11) DEFAULT NULL,
  `order_item_id` int(10) UNSIGNED DEFAULT NULL,
  `booking_product_event_ticket_id` int(10) UNSIGNED DEFAULT NULL,
  `order_id` int(10) UNSIGNED DEFAULT NULL,
  `product_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `booking_products`
--

CREATE TABLE `booking_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty` int(11) DEFAULT 0,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `show_location` tinyint(1) NOT NULL DEFAULT 0,
  `available_every_week` tinyint(1) DEFAULT NULL,
  `available_from` datetime DEFAULT NULL,
  `available_to` datetime DEFAULT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `booking_product_appointment_slots`
--

CREATE TABLE `booking_product_appointment_slots` (
  `id` int(10) UNSIGNED NOT NULL,
  `duration` int(11) DEFAULT NULL,
  `break_time` int(11) DEFAULT NULL,
  `same_slot_all_days` tinyint(1) DEFAULT NULL,
  `slots` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`slots`)),
  `booking_product_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `booking_product_default_slots`
--

CREATE TABLE `booking_product_default_slots` (
  `id` int(10) UNSIGNED NOT NULL,
  `booking_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `duration` int(11) DEFAULT NULL,
  `break_time` int(11) DEFAULT NULL,
  `slots` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`slots`)),
  `booking_product_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `booking_product_event_tickets`
--

CREATE TABLE `booking_product_event_tickets` (
  `id` int(10) UNSIGNED NOT NULL,
  `price` decimal(12,4) DEFAULT 0.0000,
  `qty` int(11) DEFAULT 0,
  `special_price` decimal(12,4) DEFAULT NULL,
  `special_price_from` datetime DEFAULT NULL,
  `special_price_to` datetime DEFAULT NULL,
  `booking_product_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `booking_product_event_ticket_translations`
--

CREATE TABLE `booking_product_event_ticket_translations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `booking_product_event_ticket_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `booking_product_rental_slots`
--

CREATE TABLE `booking_product_rental_slots` (
  `id` int(10) UNSIGNED NOT NULL,
  `renting_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `daily_price` decimal(12,4) DEFAULT 0.0000,
  `hourly_price` decimal(12,4) DEFAULT 0.0000,
  `same_slot_all_days` tinyint(1) DEFAULT NULL,
  `slots` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`slots`)),
  `booking_product_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `booking_product_table_slots`
--

CREATE TABLE `booking_product_table_slots` (
  `id` int(10) UNSIGNED NOT NULL,
  `price_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guest_limit` int(11) NOT NULL DEFAULT 0,
  `duration` int(11) NOT NULL,
  `break_time` int(11) NOT NULL,
  `prevent_scheduling_before` int(11) NOT NULL,
  `same_slot_all_days` tinyint(1) DEFAULT NULL,
  `slots` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`slots`)),
  `booking_product_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cart`
--

CREATE TABLE `cart` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_method` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coupon_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_gift` tinyint(1) NOT NULL DEFAULT 0,
  `items_count` int(11) DEFAULT NULL,
  `items_qty` decimal(12,4) DEFAULT NULL,
  `exchange_rate` decimal(12,4) DEFAULT NULL,
  `global_currency_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `base_currency_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `channel_currency_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cart_currency_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `grand_total` decimal(12,4) DEFAULT 0.0000,
  `base_grand_total` decimal(12,4) DEFAULT 0.0000,
  `sub_total` decimal(12,4) DEFAULT 0.0000,
  `base_sub_total` decimal(12,4) DEFAULT 0.0000,
  `tax_total` decimal(12,4) DEFAULT 0.0000,
  `base_tax_total` decimal(12,4) DEFAULT 0.0000,
  `discount_amount` decimal(12,4) DEFAULT 0.0000,
  `base_discount_amount` decimal(12,4) DEFAULT 0.0000,
  `checkout_method` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_guest` tinyint(1) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT 1,
  `conversion_time` datetime DEFAULT NULL,
  `customer_id` int(10) UNSIGNED DEFAULT NULL,
  `channel_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `applied_cart_rule_ids` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `cart`
--

INSERT INTO `cart` (`id`, `customer_email`, `customer_first_name`, `customer_last_name`, `shipping_method`, `coupon_code`, `is_gift`, `items_count`, `items_qty`, `exchange_rate`, `global_currency_code`, `base_currency_code`, `channel_currency_code`, `cart_currency_code`, `grand_total`, `base_grand_total`, `sub_total`, `base_sub_total`, `tax_total`, `base_tax_total`, `discount_amount`, `base_discount_amount`, `checkout_method`, `is_guest`, `is_active`, `conversion_time`, `customer_id`, `channel_id`, `created_at`, `updated_at`, `applied_cart_rule_ids`) VALUES
(2, 'user@example.com', 'Erich', 'Martter', 'free_free', NULL, 0, 1, '1.0000', NULL, 'BRL', 'BRL', 'BRL', 'BRL', '15.0000', '15.0000', '15.0000', '15.0000', '0.0000', '0.0000', '0.0000', '0.0000', NULL, 0, 0, NULL, 1, 1, '2021-04-07 10:57:23', '2021-04-07 14:13:16', ''),
(3, 'erichmartter@gmail.com', 'Erich', 'Martter', 'flatrate_flatrate', NULL, 0, 1, '1.0000', NULL, 'BRL', 'BRL', 'BRL', 'BRL', '25.0000', '25.0000', '15.0000', '15.0000', '0.0000', '0.0000', '0.0000', '0.0000', NULL, 1, 0, NULL, NULL, 1, '2021-04-07 14:06:33', '2021-04-07 14:08:34', ''),
(4, 'user@example.com', 'Erich', 'Martter', 'free_free', NULL, 0, 1, '1.0000', NULL, 'BRL', 'BRL', 'BRL', 'USD', '3.0000', '15.0000', '3.0000', '15.0000', '0.0000', '0.0000', '0.0000', '0.0000', NULL, 0, 0, NULL, 1, 1, '2021-04-07 14:31:00', '2021-04-07 14:34:08', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cart_items`
--

CREATE TABLE `cart_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `quantity` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `sku` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coupon_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `weight` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `total_weight` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `base_total_weight` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `price` decimal(12,4) NOT NULL DEFAULT 1.0000,
  `base_price` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `total` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `base_total` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `tax_percent` decimal(12,4) DEFAULT 0.0000,
  `tax_amount` decimal(12,4) DEFAULT 0.0000,
  `base_tax_amount` decimal(12,4) DEFAULT 0.0000,
  `discount_percent` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `discount_amount` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `base_discount_amount` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `additional` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`additional`)),
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `cart_id` int(10) UNSIGNED NOT NULL,
  `tax_category_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `custom_price` decimal(12,4) DEFAULT NULL,
  `applied_cart_rule_ids` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `cart_items`
--

INSERT INTO `cart_items` (`id`, `quantity`, `sku`, `type`, `name`, `coupon_code`, `weight`, `total_weight`, `base_total_weight`, `price`, `base_price`, `total`, `base_total`, `tax_percent`, `tax_amount`, `base_tax_amount`, `discount_percent`, `discount_amount`, `base_discount_amount`, `additional`, `parent_id`, `product_id`, `cart_id`, `tax_category_id`, `created_at`, `updated_at`, `custom_price`, `applied_cart_rule_ids`) VALUES
(2, 1, 'tony-hawks-xbox', 'game', 'Tony Hawks XBOX', NULL, '0.0000', '0.0000', '0.0000', '15.0000', '15.0000', '15.0000', '15.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '{\"is_buy_now\":\"0\",\"_token\":\"98Z1uckkOpR6qzyPaYaumIWh1ugP3kSLSpwvoO7H\",\"product_id\":\"3\",\"quantity\":\"1\",\"selected_configurable_option\":\"4\",\"super_attribute\":{\"29\":\"14\"},\"attributes\":{\"tipo_conta\":{\"attribute_name\":\"Tipo Conta\",\"option_id\":14,\"option_label\":\"Prim\\u00e1ria\"}}}', NULL, 3, 3, NULL, '2021-04-07 14:06:33', '2021-04-07 14:08:33', NULL, ''),
(3, 0, 'tony-hawks-xbox-variant-14', 'virtual', 'Primária', NULL, '0.0000', '0.0000', '0.0000', '1.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '{\"product_id\":4,\"parent_id\":3}', 2, 4, 3, NULL, '2021-04-07 14:06:33', '2021-04-07 14:06:33', NULL, NULL),
(4, 1, 'tony-hawks-xbox', 'game', 'Tony Hawks XBOX', NULL, '0.0000', '0.0000', '0.0000', '15.0000', '15.0000', '15.0000', '15.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '{\"is_buy_now\":\"0\",\"_token\":\"98Z1uckkOpR6qzyPaYaumIWh1ugP3kSLSpwvoO7H\",\"product_id\":\"3\",\"quantity\":\"1\",\"selected_configurable_option\":\"4\",\"super_attribute\":{\"29\":\"14\"},\"attributes\":{\"tipo_conta\":{\"attribute_name\":\"Tipo Conta\",\"option_id\":14,\"option_label\":\"Prim\\u00e1ria\"}}}', NULL, 3, 2, NULL, '2021-04-07 14:12:03', '2021-04-07 14:13:15', NULL, ''),
(5, 0, 'tony-hawks-xbox-variant-14', 'virtual', 'Primária', NULL, '0.0000', '0.0000', '0.0000', '1.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '{\"product_id\":4,\"parent_id\":3}', 4, 4, 2, NULL, '2021-04-07 14:12:03', '2021-04-07 14:12:03', NULL, NULL),
(6, 1, 'tony-hawks-xbox', 'game', 'Tony Hawks XBOX', NULL, '0.0000', '0.0000', '0.0000', '3.0000', '15.0000', '3.0000', '15.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '{\"is_buy_now\":\"0\",\"_token\":\"98Z1uckkOpR6qzyPaYaumIWh1ugP3kSLSpwvoO7H\",\"product_id\":\"3\",\"quantity\":\"1\",\"selected_configurable_option\":\"4\",\"super_attribute\":{\"29\":\"14\"},\"attributes\":{\"tipo_conta\":{\"attribute_name\":\"Tipo Conta\",\"option_id\":14,\"option_label\":\"Prim\\u00e1ria\"}}}', NULL, 3, 4, NULL, '2021-04-07 14:31:00', '2021-04-07 14:34:07', NULL, ''),
(7, 0, 'tony-hawks-xbox-variant-14', 'virtual', 'Primária', NULL, '0.0000', '0.0000', '0.0000', '1.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '{\"product_id\":4,\"parent_id\":3}', 6, 4, 4, NULL, '2021-04-07 14:31:00', '2021-04-07 14:31:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cart_item_inventories`
--

CREATE TABLE `cart_item_inventories` (
  `id` int(10) UNSIGNED NOT NULL,
  `qty` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `inventory_source_id` int(10) UNSIGNED DEFAULT NULL,
  `cart_item_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cart_payment`
--

CREATE TABLE `cart_payment` (
  `id` int(10) UNSIGNED NOT NULL,
  `method` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cart_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `cart_payment`
--

INSERT INTO `cart_payment` (`id`, `method`, `method_title`, `cart_id`, `created_at`, `updated_at`) VALUES
(1, 'cashondelivery', NULL, 3, '2021-04-07 14:08:21', '2021-04-07 14:08:21'),
(2, 'moneytransfer', NULL, 2, '2021-04-07 14:13:10', '2021-04-07 14:13:10'),
(5, 'moneytransfer', NULL, 4, '2021-04-07 14:34:00', '2021-04-07 14:34:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cart_rules`
--

CREATE TABLE `cart_rules` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `starts_from` datetime DEFAULT NULL,
  `ends_till` datetime DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `coupon_type` int(11) NOT NULL DEFAULT 1,
  `use_auto_generation` tinyint(1) NOT NULL DEFAULT 0,
  `usage_per_customer` int(11) NOT NULL DEFAULT 0,
  `uses_per_coupon` int(11) NOT NULL DEFAULT 0,
  `times_used` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `condition_type` tinyint(1) NOT NULL DEFAULT 1,
  `conditions` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`conditions`)),
  `end_other_rules` tinyint(1) NOT NULL DEFAULT 0,
  `uses_attribute_conditions` tinyint(1) NOT NULL DEFAULT 0,
  `action_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount_amount` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `discount_quantity` int(11) NOT NULL DEFAULT 1,
  `discount_step` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `apply_to_shipping` tinyint(1) NOT NULL DEFAULT 0,
  `free_shipping` tinyint(1) NOT NULL DEFAULT 0,
  `sort_order` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cart_rule_channels`
--

CREATE TABLE `cart_rule_channels` (
  `cart_rule_id` int(10) UNSIGNED NOT NULL,
  `channel_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cart_rule_coupons`
--

CREATE TABLE `cart_rule_coupons` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `usage_limit` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `usage_per_customer` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `times_used` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `type` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `is_primary` tinyint(1) NOT NULL DEFAULT 0,
  `expired_at` date DEFAULT NULL,
  `cart_rule_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cart_rule_coupon_usage`
--

CREATE TABLE `cart_rule_coupon_usage` (
  `id` int(10) UNSIGNED NOT NULL,
  `times_used` int(11) NOT NULL DEFAULT 0,
  `cart_rule_coupon_id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cart_rule_customers`
--

CREATE TABLE `cart_rule_customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `times_used` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `cart_rule_id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cart_rule_customer_groups`
--

CREATE TABLE `cart_rule_customer_groups` (
  `cart_rule_id` int(10) UNSIGNED NOT NULL,
  `customer_group_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cart_rule_translations`
--

CREATE TABLE `cart_rule_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cart_rule_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cart_shipping_rates`
--

CREATE TABLE `cart_shipping_rates` (
  `id` int(10) UNSIGNED NOT NULL,
  `carrier` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `carrier_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` double DEFAULT 0,
  `base_price` double DEFAULT 0,
  `cart_address_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `discount_amount` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `base_discount_amount` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `is_calculate_tax` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `cart_shipping_rates`
--

INSERT INTO `cart_shipping_rates` (`id`, `carrier`, `carrier_title`, `method`, `method_title`, `method_description`, `price`, `base_price`, `cart_address_id`, `created_at`, `updated_at`, `discount_amount`, `base_discount_amount`, `is_calculate_tax`) VALUES
(1, 'flatrate', 'Flat Rate', 'flatrate_flatrate', 'Flat Rate', 'Flat Rate Shipping', 10, 10, 2, '2021-04-07 14:08:04', '2021-04-07 14:08:33', '0.0000', '0.0000', 1),
(2, 'free', 'Free Shipping', 'free_free', 'Free Shipping', 'Free Shipping', 0, 0, 2, '2021-04-07 14:08:04', '2021-04-07 14:08:04', '0.0000', '0.0000', 1),
(3, 'flatrate', 'Flat Rate', 'flatrate_flatrate', 'Flat Rate', 'Flat Rate Shipping', 10, 10, 7, '2021-04-07 14:12:58', '2021-04-07 14:12:58', '0.0000', '0.0000', 1),
(4, 'free', 'Free Shipping', 'free_free', 'Free Shipping', 'Free Shipping', 0, 0, 7, '2021-04-07 14:12:58', '2021-04-07 14:13:15', '0.0000', '0.0000', 1),
(11, 'flatrate', 'Flat Rate', 'flatrate_flatrate', 'Flat Rate', 'Flat Rate Shipping', 2, 10, 11, '2021-04-07 14:33:52', '2021-04-07 14:33:52', '0.0000', '0.0000', 1),
(12, 'free', 'Free Shipping', 'free_free', 'Free Shipping', 'Free Shipping', 0, 0, 11, '2021-04-07 14:33:52', '2021-04-07 14:34:07', '0.0000', '0.0000', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `catalog_rules`
--

CREATE TABLE `catalog_rules` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `starts_from` date DEFAULT NULL,
  `ends_till` date DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `condition_type` tinyint(1) NOT NULL DEFAULT 1,
  `conditions` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`conditions`)),
  `end_other_rules` tinyint(1) NOT NULL DEFAULT 0,
  `action_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount_amount` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `sort_order` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `catalog_rule_channels`
--

CREATE TABLE `catalog_rule_channels` (
  `catalog_rule_id` int(10) UNSIGNED NOT NULL,
  `channel_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `catalog_rule_customer_groups`
--

CREATE TABLE `catalog_rule_customer_groups` (
  `catalog_rule_id` int(10) UNSIGNED NOT NULL,
  `customer_group_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `catalog_rule_products`
--

CREATE TABLE `catalog_rule_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `starts_from` datetime DEFAULT NULL,
  `ends_till` datetime DEFAULT NULL,
  `end_other_rules` tinyint(1) NOT NULL DEFAULT 0,
  `action_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discount_amount` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `sort_order` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `product_id` int(10) UNSIGNED NOT NULL,
  `customer_group_id` int(10) UNSIGNED NOT NULL,
  `catalog_rule_id` int(10) UNSIGNED NOT NULL,
  `channel_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `catalog_rule_product_prices`
--

CREATE TABLE `catalog_rule_product_prices` (
  `id` int(10) UNSIGNED NOT NULL,
  `price` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `rule_date` date NOT NULL,
  `starts_from` datetime DEFAULT NULL,
  `ends_till` datetime DEFAULT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `customer_group_id` int(10) UNSIGNED NOT NULL,
  `catalog_rule_id` int(10) UNSIGNED NOT NULL,
  `channel_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `position` int(11) NOT NULL DEFAULT 0,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `_lft` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `_rgt` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `display_mode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'products_and_description',
  `category_icon_path` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `additional` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`additional`))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `categories`
--

INSERT INTO `categories` (`id`, `position`, `image`, `status`, `_lft`, `_rgt`, `parent_id`, `created_at`, `updated_at`, `display_mode`, `category_icon_path`, `additional`) VALUES
(1, 1, NULL, 1, 1, 14, NULL, '2021-04-07 18:58:50', '2021-04-07 18:58:50', 'products_and_description', NULL, NULL);

--
-- Acionadores `categories`
--
DELIMITER $$
CREATE TRIGGER `trig_categories_insert` AFTER INSERT ON `categories` FOR EACH ROW BEGIN
                            DECLARE urlPath VARCHAR(255);
            DECLARE localeCode VARCHAR(255);
            DECLARE done INT;
            DECLARE curs CURSOR FOR (SELECT category_translations.locale
                    FROM category_translations
                    WHERE category_id = NEW.id);
            DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;


            IF EXISTS (
                SELECT *
                FROM category_translations
                WHERE category_id = NEW.id
            )
            THEN

                OPEN curs;

            	SET done = 0;
                REPEAT
                	FETCH curs INTO localeCode;

                    SELECT get_url_path_of_category(NEW.id, localeCode) INTO urlPath;

                    IF NEW.parent_id IS NULL
                    THEN
                        SET urlPath = '';
                    END IF;

                    UPDATE category_translations
                    SET url_path = urlPath
                    WHERE
                        category_translations.category_id = NEW.id
                        AND category_translations.locale = localeCode;

                UNTIL done END REPEAT;

                CLOSE curs;

            END IF;
            END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `trig_categories_update` AFTER UPDATE ON `categories` FOR EACH ROW BEGIN
                            DECLARE urlPath VARCHAR(255);
            DECLARE localeCode VARCHAR(255);
            DECLARE done INT;
            DECLARE curs CURSOR FOR (SELECT category_translations.locale
                    FROM category_translations
                    WHERE category_id = NEW.id);
            DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;


            IF EXISTS (
                SELECT *
                FROM category_translations
                WHERE category_id = NEW.id
            )
            THEN

                OPEN curs;

            	SET done = 0;
                REPEAT
                	FETCH curs INTO localeCode;

                    SELECT get_url_path_of_category(NEW.id, localeCode) INTO urlPath;

                    IF NEW.parent_id IS NULL
                    THEN
                        SET urlPath = '';
                    END IF;

                    UPDATE category_translations
                    SET url_path = urlPath
                    WHERE
                        category_translations.category_id = NEW.id
                        AND category_translations.locale = localeCode;

                UNTIL done END REPEAT;

                CLOSE curs;

            END IF;
            END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `category_filterable_attributes`
--

CREATE TABLE `category_filterable_attributes` (
  `category_id` int(10) UNSIGNED NOT NULL,
  `attribute_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `category_translations`
--

CREATE TABLE `category_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `locale_id` int(10) UNSIGNED DEFAULT NULL,
  `url_path` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'maintained by database triggers'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `category_translations`
--

INSERT INTO `category_translations` (`id`, `name`, `slug`, `description`, `meta_title`, `meta_description`, `meta_keywords`, `category_id`, `locale`, `locale_id`, `url_path`) VALUES
(1, 'Root', 'root', 'Root', '', '', '', 1, 'en', NULL, '');

--
-- Acionadores `category_translations`
--
DELIMITER $$
CREATE TRIGGER `trig_category_translations_insert` BEFORE INSERT ON `category_translations` FOR EACH ROW BEGIN
                            DECLARE parentUrlPath varchar(255);
            DECLARE urlPath varchar(255);

            IF NOT EXISTS (
                SELECT id
                FROM categories
                WHERE
                    id = NEW.category_id
                    AND parent_id IS NULL
            )
            THEN

                SELECT
                    GROUP_CONCAT(parent_translations.slug SEPARATOR '/') INTO parentUrlPath
                FROM
                    categories AS node,
                    categories AS parent
                    JOIN category_translations AS parent_translations ON parent.id = parent_translations.category_id
                WHERE
                    node._lft >= parent._lft
                    AND node._rgt <= parent._rgt
                    AND node.id = (SELECT parent_id FROM categories WHERE id = NEW.category_id)
                    AND node.parent_id IS NOT NULL
                    AND parent.parent_id IS NOT NULL
                    AND parent_translations.locale = NEW.locale
                GROUP BY
                    node.id;

                IF parentUrlPath IS NULL
                THEN
                    SET urlPath = NEW.slug;
                ELSE
                    SET urlPath = concat(parentUrlPath, '/', NEW.slug);
                END IF;

                SET NEW.url_path = urlPath;

            END IF;
            END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `trig_category_translations_update` BEFORE UPDATE ON `category_translations` FOR EACH ROW BEGIN
                            DECLARE parentUrlPath varchar(255);
            DECLARE urlPath varchar(255);

            IF NOT EXISTS (
                SELECT id
                FROM categories
                WHERE
                    id = NEW.category_id
                    AND parent_id IS NULL
            )
            THEN

                SELECT
                    GROUP_CONCAT(parent_translations.slug SEPARATOR '/') INTO parentUrlPath
                FROM
                    categories AS node,
                    categories AS parent
                    JOIN category_translations AS parent_translations ON parent.id = parent_translations.category_id
                WHERE
                    node._lft >= parent._lft
                    AND node._rgt <= parent._rgt
                    AND node.id = (SELECT parent_id FROM categories WHERE id = NEW.category_id)
                    AND node.parent_id IS NOT NULL
                    AND parent.parent_id IS NOT NULL
                    AND parent_translations.locale = NEW.locale
                GROUP BY
                    node.id;

                IF parentUrlPath IS NULL
                THEN
                    SET urlPath = NEW.slug;
                ELSE
                    SET urlPath = concat(parentUrlPath, '/', NEW.slug);
                END IF;

                SET NEW.url_path = urlPath;

            END IF;
            END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `channels`
--

CREATE TABLE `channels` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `timezone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `theme` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hostname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `favicon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_maintenance_on` tinyint(1) NOT NULL DEFAULT 0,
  `allowed_ips` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default_locale_id` int(10) UNSIGNED NOT NULL,
  `base_currency_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `root_category_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `channels`
--

INSERT INTO `channels` (`id`, `code`, `timezone`, `theme`, `hostname`, `logo`, `favicon`, `is_maintenance_on`, `allowed_ips`, `default_locale_id`, `base_currency_id`, `created_at`, `updated_at`, `root_category_id`) VALUES
(1, 'default', NULL, 'default', 'http://localhost:8000/bagisto/public', NULL, NULL, 0, '', 6, 3, NULL, '2021-04-07 10:53:59', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `channel_currencies`
--

CREATE TABLE `channel_currencies` (
  `channel_id` int(10) UNSIGNED NOT NULL,
  `currency_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `channel_currencies`
--

INSERT INTO `channel_currencies` (`channel_id`, `currency_id`) VALUES
(1, 1),
(1, 3);

-- --------------------------------------------------------

--
-- Estrutura da tabela `channel_inventory_sources`
--

CREATE TABLE `channel_inventory_sources` (
  `channel_id` int(10) UNSIGNED NOT NULL,
  `inventory_source_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `channel_inventory_sources`
--

INSERT INTO `channel_inventory_sources` (`channel_id`, `inventory_source_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `channel_locales`
--

CREATE TABLE `channel_locales` (
  `channel_id` int(10) UNSIGNED NOT NULL,
  `locale_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `channel_locales`
--

INSERT INTO `channel_locales` (`channel_id`, `locale_id`) VALUES
(1, 1),
(1, 6);

-- --------------------------------------------------------

--
-- Estrutura da tabela `channel_translations`
--

CREATE TABLE `channel_translations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `channel_id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `home_page_content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer_content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `maintenance_mode_text` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `home_seo` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`home_seo`)),
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `channel_translations`
--

INSERT INTO `channel_translations` (`id`, `channel_id`, `locale`, `name`, `description`, `home_page_content`, `footer_content`, `maintenance_mode_text`, `home_seo`, `created_at`, `updated_at`) VALUES
(1, 1, 'en', 'Default', NULL, '\r\n                    <p>@include(\"shop::home.slider\") @include(\"shop::home.featured-products\") @include(\"shop::home.new-products\")</p>\r\n                        <div class=\"banner-container\">\r\n                        <div class=\"left-banner\">\r\n                            <img src=http://localhost:8000/bagisto/public/themes/default/assets/images/1.webp data-src=http://localhost:8000/bagisto/public/themes/default/assets/images/1.webp class=\"lazyload\" alt=\"test\" width=\"720\" height=\"720\" />\r\n                        </div>\r\n                        <div class=\"right-banner\">\r\n                            <img src=http://localhost:8000/bagisto/public/themes/default/assets/images/2.webp data-src=http://localhost:8000/bagisto/public/themes/default/assets/images/2.webp class=\"lazyload\" alt=\"test\" width=\"460\" height=\"330\" />\r\n                            <img src=http://localhost:8000/bagisto/public/themes/default/assets/images/3.webp data-src=http://localhost:8000/bagisto/public/themes/default/assets/images/3.webp  class=\"lazyload\" alt=\"test\" width=\"460\" height=\"330\" />\r\n                        </div>\r\n                    </div>\r\n                ', '\r\n                    <div class=\"list-container\">\r\n                        <span class=\"list-heading\">Quick Links</span>\r\n                        <ul class=\"list-group\">\r\n                            <li><a href=\"@php echo route(\'shop.cms.page\', \'about-us\') @endphp\">About Us</a></li>\r\n                            <li><a href=\"@php echo route(\'shop.cms.page\', \'return-policy\') @endphp\">Return Policy</a></li>\r\n                            <li><a href=\"@php echo route(\'shop.cms.page\', \'refund-policy\') @endphp\">Refund Policy</a></li>\r\n                            <li><a href=\"@php echo route(\'shop.cms.page\', \'terms-conditions\') @endphp\">Terms and conditions</a></li>\r\n                            <li><a href=\"@php echo route(\'shop.cms.page\', \'terms-of-use\') @endphp\">Terms of Use</a></li><li><a href=\"@php echo route(\'shop.cms.page\', \'contact-us\') @endphp\">Contact Us</a></li>\r\n                        </ul>\r\n                    </div>\r\n                    <div class=\"list-container\">\r\n                        <span class=\"list-heading\">Connect With Us</span>\r\n                            <ul class=\"list-group\">\r\n                                <li><a href=\"#\"><span class=\"icon icon-facebook\"></span>Facebook </a></li>\r\n                                <li><a href=\"#\"><span class=\"icon icon-twitter\"></span> Twitter </a></li>\r\n                                <li><a href=\"#\"><span class=\"icon icon-instagram\"></span> Instagram </a></li>\r\n                                <li><a href=\"#\"> <span class=\"icon icon-google-plus\"></span>Google+ </a></li>\r\n                                <li><a href=\"#\"> <span class=\"icon icon-linkedin\"></span>LinkedIn </a></li>\r\n                            </ul>\r\n                        </div>\r\n                ', NULL, '{\"meta_title\": \"Demo store\", \"meta_keywords\": \"Demo store meta keyword\", \"meta_description\": \"Demo store meta description\"}', NULL, NULL),
(2, 1, 'fr', 'Default', NULL, '\r\n                    <p>@include(\"shop::home.slider\") @include(\"shop::home.featured-products\") @include(\"shop::home.new-products\")</p>\r\n                        <div class=\"banner-container\">\r\n                        <div class=\"left-banner\">\r\n                            <img src=http://localhost:8000/bagisto/public/themes/default/assets/images/1.webp data-src=http://localhost:8000/bagisto/public/themes/default/assets/images/1.webp class=\"lazyload\" alt=\"test\" width=\"720\" height=\"720\" />\r\n                        </div>\r\n                        <div class=\"right-banner\">\r\n                            <img src=http://localhost:8000/bagisto/public/themes/default/assets/images/2.webp data-src=http://localhost:8000/bagisto/public/themes/default/assets/images/2.webp class=\"lazyload\" alt=\"test\" width=\"460\" height=\"330\" />\r\n                            <img src=http://localhost:8000/bagisto/public/themes/default/assets/images/3.webp data-src=http://localhost:8000/bagisto/public/themes/default/assets/images/3.webp  class=\"lazyload\" alt=\"test\" width=\"460\" height=\"330\" />\r\n                        </div>\r\n                    </div>\r\n                ', '\r\n                    <div class=\"list-container\">\r\n                        <span class=\"list-heading\">Quick Links</span>\r\n                        <ul class=\"list-group\">\r\n                            <li><a href=\"@php echo route(\'shop.cms.page\', \'about-us\') @endphp\">About Us</a></li>\r\n                            <li><a href=\"@php echo route(\'shop.cms.page\', \'return-policy\') @endphp\">Return Policy</a></li>\r\n                            <li><a href=\"@php echo route(\'shop.cms.page\', \'refund-policy\') @endphp\">Refund Policy</a></li>\r\n                            <li><a href=\"@php echo route(\'shop.cms.page\', \'terms-conditions\') @endphp\">Terms and conditions</a></li>\r\n                            <li><a href=\"@php echo route(\'shop.cms.page\', \'terms-of-use\') @endphp\">Terms of Use</a></li><li><a href=\"@php echo route(\'shop.cms.page\', \'contact-us\') @endphp\">Contact Us</a></li>\r\n                        </ul>\r\n                    </div>\r\n                    <div class=\"list-container\">\r\n                        <span class=\"list-heading\">Connect With Us</span>\r\n                            <ul class=\"list-group\">\r\n                                <li><a href=\"#\"><span class=\"icon icon-facebook\"></span>Facebook </a></li>\r\n                                <li><a href=\"#\"><span class=\"icon icon-twitter\"></span> Twitter </a></li>\r\n                                <li><a href=\"#\"><span class=\"icon icon-instagram\"></span> Instagram </a></li>\r\n                                <li><a href=\"#\"> <span class=\"icon icon-google-plus\"></span>Google+ </a></li>\r\n                                <li><a href=\"#\"> <span class=\"icon icon-linkedin\"></span>LinkedIn </a></li>\r\n                            </ul>\r\n                        </div>\r\n                ', NULL, '{\"meta_title\": \"Demo store\", \"meta_keywords\": \"Demo store meta keyword\", \"meta_description\": \"Demo store meta description\"}', NULL, NULL),
(3, 1, 'nl', 'Default', NULL, '\r\n                    <p>@include(\"shop::home.slider\") @include(\"shop::home.featured-products\") @include(\"shop::home.new-products\")</p>\r\n                        <div class=\"banner-container\">\r\n                        <div class=\"left-banner\">\r\n                            <img src=http://localhost:8000/bagisto/public/themes/default/assets/images/1.webp data-src=http://localhost:8000/bagisto/public/themes/default/assets/images/1.webp class=\"lazyload\" alt=\"test\" width=\"720\" height=\"720\" />\r\n                        </div>\r\n                        <div class=\"right-banner\">\r\n                            <img src=http://localhost:8000/bagisto/public/themes/default/assets/images/2.webp data-src=http://localhost:8000/bagisto/public/themes/default/assets/images/2.webp class=\"lazyload\" alt=\"test\" width=\"460\" height=\"330\" />\r\n                            <img src=http://localhost:8000/bagisto/public/themes/default/assets/images/3.webp data-src=http://localhost:8000/bagisto/public/themes/default/assets/images/3.webp  class=\"lazyload\" alt=\"test\" width=\"460\" height=\"330\" />\r\n                        </div>\r\n                    </div>\r\n                ', '\r\n                    <div class=\"list-container\">\r\n                        <span class=\"list-heading\">Quick Links</span>\r\n                        <ul class=\"list-group\">\r\n                            <li><a href=\"@php echo route(\'shop.cms.page\', \'about-us\') @endphp\">About Us</a></li>\r\n                            <li><a href=\"@php echo route(\'shop.cms.page\', \'return-policy\') @endphp\">Return Policy</a></li>\r\n                            <li><a href=\"@php echo route(\'shop.cms.page\', \'refund-policy\') @endphp\">Refund Policy</a></li>\r\n                            <li><a href=\"@php echo route(\'shop.cms.page\', \'terms-conditions\') @endphp\">Terms and conditions</a></li>\r\n                            <li><a href=\"@php echo route(\'shop.cms.page\', \'terms-of-use\') @endphp\">Terms of Use</a></li><li><a href=\"@php echo route(\'shop.cms.page\', \'contact-us\') @endphp\">Contact Us</a></li>\r\n                        </ul>\r\n                    </div>\r\n                    <div class=\"list-container\">\r\n                        <span class=\"list-heading\">Connect With Us</span>\r\n                            <ul class=\"list-group\">\r\n                                <li><a href=\"#\"><span class=\"icon icon-facebook\"></span>Facebook </a></li>\r\n                                <li><a href=\"#\"><span class=\"icon icon-twitter\"></span> Twitter </a></li>\r\n                                <li><a href=\"#\"><span class=\"icon icon-instagram\"></span> Instagram </a></li>\r\n                                <li><a href=\"#\"> <span class=\"icon icon-google-plus\"></span>Google+ </a></li>\r\n                                <li><a href=\"#\"> <span class=\"icon icon-linkedin\"></span>LinkedIn </a></li>\r\n                            </ul>\r\n                        </div>\r\n                ', NULL, '{\"meta_title\": \"Demo store\", \"meta_keywords\": \"Demo store meta keyword\", \"meta_description\": \"Demo store meta description\"}', NULL, NULL),
(4, 1, 'tr', 'Default', NULL, '\r\n                    <p>@include(\"shop::home.slider\") @include(\"shop::home.featured-products\") @include(\"shop::home.new-products\")</p>\r\n                        <div class=\"banner-container\">\r\n                        <div class=\"left-banner\">\r\n                            <img src=http://localhost:8000/bagisto/public/themes/default/assets/images/1.webp data-src=http://localhost:8000/bagisto/public/themes/default/assets/images/1.webp class=\"lazyload\" alt=\"test\" width=\"720\" height=\"720\" />\r\n                        </div>\r\n                        <div class=\"right-banner\">\r\n                            <img src=http://localhost:8000/bagisto/public/themes/default/assets/images/2.webp data-src=http://localhost:8000/bagisto/public/themes/default/assets/images/2.webp class=\"lazyload\" alt=\"test\" width=\"460\" height=\"330\" />\r\n                            <img src=http://localhost:8000/bagisto/public/themes/default/assets/images/3.webp data-src=http://localhost:8000/bagisto/public/themes/default/assets/images/3.webp  class=\"lazyload\" alt=\"test\" width=\"460\" height=\"330\" />\r\n                        </div>\r\n                    </div>\r\n                ', '\r\n                    <div class=\"list-container\">\r\n                        <span class=\"list-heading\">Quick Links</span>\r\n                        <ul class=\"list-group\">\r\n                            <li><a href=\"@php echo route(\'shop.cms.page\', \'about-us\') @endphp\">About Us</a></li>\r\n                            <li><a href=\"@php echo route(\'shop.cms.page\', \'return-policy\') @endphp\">Return Policy</a></li>\r\n                            <li><a href=\"@php echo route(\'shop.cms.page\', \'refund-policy\') @endphp\">Refund Policy</a></li>\r\n                            <li><a href=\"@php echo route(\'shop.cms.page\', \'terms-conditions\') @endphp\">Terms and conditions</a></li>\r\n                            <li><a href=\"@php echo route(\'shop.cms.page\', \'terms-of-use\') @endphp\">Terms of Use</a></li><li><a href=\"@php echo route(\'shop.cms.page\', \'contact-us\') @endphp\">Contact Us</a></li>\r\n                        </ul>\r\n                    </div>\r\n                    <div class=\"list-container\">\r\n                        <span class=\"list-heading\">Connect With Us</span>\r\n                            <ul class=\"list-group\">\r\n                                <li><a href=\"#\"><span class=\"icon icon-facebook\"></span>Facebook </a></li>\r\n                                <li><a href=\"#\"><span class=\"icon icon-twitter\"></span> Twitter </a></li>\r\n                                <li><a href=\"#\"><span class=\"icon icon-instagram\"></span> Instagram </a></li>\r\n                                <li><a href=\"#\"> <span class=\"icon icon-google-plus\"></span>Google+ </a></li>\r\n                                <li><a href=\"#\"> <span class=\"icon icon-linkedin\"></span>LinkedIn </a></li>\r\n                            </ul>\r\n                        </div>\r\n                ', NULL, '{\"meta_title\": \"Demo store\", \"meta_keywords\": \"Demo store meta keyword\", \"meta_description\": \"Demo store meta description\"}', NULL, NULL),
(5, 1, 'es', 'Default', NULL, '\r\n                    <p>@include(\"shop::home.slider\") @include(\"shop::home.featured-products\") @include(\"shop::home.new-products\")</p>\r\n                        <div class=\"banner-container\">\r\n                        <div class=\"left-banner\">\r\n                            <img src=http://localhost:8000/bagisto/public/themes/default/assets/images/1.webp data-src=http://localhost:8000/bagisto/public/themes/default/assets/images/1.webp class=\"lazyload\" alt=\"test\" width=\"720\" height=\"720\" />\r\n                        </div>\r\n                        <div class=\"right-banner\">\r\n                            <img src=http://localhost:8000/bagisto/public/themes/default/assets/images/2.webp data-src=http://localhost:8000/bagisto/public/themes/default/assets/images/2.webp class=\"lazyload\" alt=\"test\" width=\"460\" height=\"330\" />\r\n                            <img src=http://localhost:8000/bagisto/public/themes/default/assets/images/3.webp data-src=http://localhost:8000/bagisto/public/themes/default/assets/images/3.webp  class=\"lazyload\" alt=\"test\" width=\"460\" height=\"330\" />\r\n                        </div>\r\n                    </div>\r\n                ', '\r\n                    <div class=\"list-container\">\r\n                        <span class=\"list-heading\">Quick Links</span>\r\n                        <ul class=\"list-group\">\r\n                            <li><a href=\"@php echo route(\'shop.cms.page\', \'about-us\') @endphp\">About Us</a></li>\r\n                            <li><a href=\"@php echo route(\'shop.cms.page\', \'return-policy\') @endphp\">Return Policy</a></li>\r\n                            <li><a href=\"@php echo route(\'shop.cms.page\', \'refund-policy\') @endphp\">Refund Policy</a></li>\r\n                            <li><a href=\"@php echo route(\'shop.cms.page\', \'terms-conditions\') @endphp\">Terms and conditions</a></li>\r\n                            <li><a href=\"@php echo route(\'shop.cms.page\', \'terms-of-use\') @endphp\">Terms of Use</a></li><li><a href=\"@php echo route(\'shop.cms.page\', \'contact-us\') @endphp\">Contact Us</a></li>\r\n                        </ul>\r\n                    </div>\r\n                    <div class=\"list-container\">\r\n                        <span class=\"list-heading\">Connect With Us</span>\r\n                            <ul class=\"list-group\">\r\n                                <li><a href=\"#\"><span class=\"icon icon-facebook\"></span>Facebook </a></li>\r\n                                <li><a href=\"#\"><span class=\"icon icon-twitter\"></span> Twitter </a></li>\r\n                                <li><a href=\"#\"><span class=\"icon icon-instagram\"></span> Instagram </a></li>\r\n                                <li><a href=\"#\"> <span class=\"icon icon-google-plus\"></span>Google+ </a></li>\r\n                                <li><a href=\"#\"> <span class=\"icon icon-linkedin\"></span>LinkedIn </a></li>\r\n                            </ul>\r\n                        </div>\r\n                ', NULL, '{\"meta_title\": \"Demo store\", \"meta_keywords\": \"Demo store meta keyword\", \"meta_description\": \"Demo store meta description\"}', NULL, NULL),
(6, 1, 'pt_BR', 'Onclick', '', '<p>@include(\"shop::home.slider\") @include(\"shop::home.featured-products\") @include(\"shop::home.new-products\")</p>\r\n<div class=\"banner-container\">\r\n<div class=\"left-banner\"><img src=\"../../../bagisto/public/themes/default/assets/images/1.webp\" data-src=\"http://localhost:8000/bagisto/public/themes/default/assets/images/1.webp\" class=\"lazyload\" alt=\"test\" width=\"720\" height=\"720\" /></div>\r\n<div class=\"right-banner\"><img src=\"../../../bagisto/public/themes/default/assets/images/2.webp\" data-src=\"http://localhost:8000/bagisto/public/themes/default/assets/images/2.webp\" class=\"lazyload\" alt=\"test\" width=\"460\" height=\"330\" />&nbsp;<img src=\"../../../bagisto/public/themes/default/assets/images/3.webp\" data-src=\"http://localhost:8000/bagisto/public/themes/default/assets/images/3.webp\" class=\"lazyload\" alt=\"test\" width=\"460\" height=\"330\" /></div>\r\n</div>', '<div class=\"list-container\"><span class=\"list-heading\">Quick Links</span>\r\n<ul class=\"list-group\">\r\n<li><a href=\"@php echo route(\'shop.cms.page\', \'about-us\') @endphp\">About Us</a></li>\r\n<li><a href=\"@php echo route(\'shop.cms.page\', \'return-policy\') @endphp\">Return Policy</a></li>\r\n<li><a href=\"@php echo route(\'shop.cms.page\', \'refund-policy\') @endphp\">Refund Policy</a></li>\r\n<li><a href=\"@php echo route(\'shop.cms.page\', \'terms-conditions\') @endphp\">Terms and conditions</a></li>\r\n<li><a href=\"@php echo route(\'shop.cms.page\', \'terms-of-use\') @endphp\">Terms of Use</a></li>\r\n<li><a href=\"@php echo route(\'shop.cms.page\', \'contact-us\') @endphp\">Contact Us</a></li>\r\n</ul>\r\n</div>\r\n<div class=\"list-container\"><span class=\"list-heading\">Connect With Us</span>\r\n<ul class=\"list-group\">\r\n<li><a href=\"#\"><span class=\"icon icon-facebook\"></span>Facebook</a></li>\r\n<li><a href=\"#\"><span class=\"icon icon-twitter\"></span>Twitter</a></li>\r\n<li><a href=\"#\"><span class=\"icon icon-instagram\"></span>Instagram</a></li>\r\n<li><a href=\"#\"><span class=\"icon icon-google-plus\"></span>Google+</a></li>\r\n<li><a href=\"#\"><span class=\"icon icon-linkedin\"></span>LinkedIn</a></li>\r\n</ul>\r\n</div>', '', '{\"meta_title\":\"Jogos\",\"meta_description\":\"Jogos\",\"meta_keywords\":\"Jogos\"}', '2021-04-07 10:39:24', '2021-04-07 10:53:59');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cms_pages`
--

CREATE TABLE `cms_pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `layout` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `cms_pages`
--

INSERT INTO `cms_pages` (`id`, `layout`, `created_at`, `updated_at`) VALUES
(1, NULL, '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(2, NULL, '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(3, NULL, '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(4, NULL, '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(5, NULL, '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(6, NULL, '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(7, NULL, '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(8, NULL, '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(9, NULL, '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(10, NULL, '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(11, NULL, '2021-04-07 18:58:51', '2021-04-07 18:58:51');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cms_page_channels`
--

CREATE TABLE `cms_page_channels` (
  `cms_page_id` int(10) UNSIGNED NOT NULL,
  `channel_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cms_page_translations`
--

CREATE TABLE `cms_page_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `page_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url_key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `html_content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cms_page_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `cms_page_translations`
--

INSERT INTO `cms_page_translations` (`id`, `page_title`, `url_key`, `html_content`, `meta_title`, `meta_description`, `meta_keywords`, `locale`, `cms_page_id`) VALUES
(12, 'About Us', 'about-us', '<div class=\"static-container\"><div class=\"mb-5\">About us page content</div></div>', 'about us', '', 'aboutus', 'en', 1),
(13, 'Return Policy', 'return-policy', '<div class=\"static-container\"><div class=\"mb-5\">Return policy page content</div></div>', 'return policy', '', 'return, policy', 'en', 2),
(14, 'Refund Policy', 'refund-policy', '<div class=\"static-container\"><div class=\"mb-5\">Refund policy page content</div></div>', 'Refund policy', '', 'refund, policy', 'en', 3),
(15, 'Terms & Conditions', 'terms-conditions', '<div class=\"static-container\"><div class=\"mb-5\">Terms & conditions page content</div></div>', 'Terms & Conditions', '', 'term, conditions', 'en', 4),
(16, 'Terms of use', 'terms-of-use', '<div class=\"static-container\"><div class=\"mb-5\">Terms of use page content</div></div>', 'Terms of use', '', 'term, use', 'en', 5),
(17, 'Contact Us', 'contact-us', '<div class=\"static-container\"><div class=\"mb-5\">Contact us page content</div></div>', 'Contact Us', '', 'contact, us', 'en', 6),
(18, 'Customer Service', 'cutomer-service', '<div class=\"static-container\"><div class=\"mb-5\">Customer service  page content</div></div>', 'Customer Service', '', 'customer, service', 'en', 7),
(19, 'What\'s New', 'whats-new', '<div class=\"static-container\"><div class=\"mb-5\">What\'s New page content</div></div>', 'What\'s New', '', 'new', 'en', 8),
(20, 'Payment Policy', 'payment-policy', '<div class=\"static-container\"><div class=\"mb-5\">Payment Policy page content</div></div>', 'Payment Policy', '', 'payment, policy', 'en', 9),
(21, 'Shipping Policy', 'shipping-policy', '<div class=\"static-container\"><div class=\"mb-5\">Shipping Policy  page content</div></div>', 'Shipping Policy', '', 'shipping, policy', 'en', 10),
(22, 'Privacy Policy', 'privacy-policy', '<div class=\"static-container\"><div class=\"mb-5\">Privacy Policy  page content</div></div>', 'Privacy Policy', '', 'privacy, policy', 'en', 11);

-- --------------------------------------------------------

--
-- Estrutura da tabela `core_config`
--

CREATE TABLE `core_config` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `channel_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `locale_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `core_config`
--

INSERT INTO `core_config` (`id`, `code`, `value`, `channel_code`, `locale_code`, `created_at`, `updated_at`) VALUES
(1, 'catalog.products.guest-checkout.allow-guest-checkout', '1', NULL, NULL, '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(2, 'emails.general.notifications.emails.general.notifications.verification', '1', NULL, NULL, '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(3, 'emails.general.notifications.emails.general.notifications.registration', '1', NULL, NULL, '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(4, 'emails.general.notifications.emails.general.notifications.customer', '1', NULL, NULL, '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(5, 'emails.general.notifications.emails.general.notifications.new-order', '1', NULL, NULL, '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(6, 'emails.general.notifications.emails.general.notifications.new-admin', '1', NULL, NULL, '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(7, 'emails.general.notifications.emails.general.notifications.new-invoice', '1', NULL, NULL, '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(8, 'emails.general.notifications.emails.general.notifications.new-refund', '1', NULL, NULL, '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(9, 'emails.general.notifications.emails.general.notifications.new-shipment', '1', NULL, NULL, '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(10, 'emails.general.notifications.emails.general.notifications.new-inventory-source', '1', NULL, NULL, '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(11, 'emails.general.notifications.emails.general.notifications.cancel-order', '1', NULL, NULL, '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(12, 'catalog.products.homepage.out_of_stock_items', '1', NULL, NULL, '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(54, 'customer.settings.social_login.enable_facebook', '1', 'default', NULL, '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(55, 'customer.settings.social_login.enable_twitter', '1', 'default', NULL, '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(56, 'customer.settings.social_login.enable_google', '1', 'default', NULL, '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(57, 'customer.settings.social_login.enable_linkedin', '1', 'default', NULL, '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(58, 'customer.settings.social_login.enable_github', '1', 'default', NULL, '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(59, 'general.content.shop.compare_option', '1', 'default', 'en', '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(60, 'general.content.shop.compare_option', '1', 'default', 'fr', '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(61, 'general.content.shop.compare_option', '1', 'default', 'ar', '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(62, 'general.content.shop.compare_option', '1', 'default', 'de', '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(63, 'general.content.shop.compare_option', '1', 'default', 'es', '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(64, 'general.content.shop.compare_option', '1', 'default', 'fa', '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(65, 'general.content.shop.compare_option', '1', 'default', 'it', '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(66, 'general.content.shop.compare_option', '1', 'default', 'ja', '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(67, 'general.content.shop.compare_option', '1', 'default', 'nl', '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(68, 'general.content.shop.compare_option', '1', 'default', 'pl', '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(69, 'general.content.shop.compare_option', '1', 'default', 'pt_BR', '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(70, 'general.content.shop.compare_option', '1', 'default', 'tr', '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(71, 'general.content.shop.wishlist_option', '1', 'default', 'en', '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(72, 'general.content.shop.wishlist_option', '1', 'default', 'fr', '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(73, 'general.content.shop.wishlist_option', '1', 'default', 'ar', '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(74, 'general.content.shop.wishlist_option', '1', 'default', 'de', '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(75, 'general.content.shop.wishlist_option', '1', 'default', 'es', '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(76, 'general.content.shop.wishlist_option', '1', 'default', 'fa', '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(77, 'general.content.shop.wishlist_option', '1', 'default', 'it', '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(78, 'general.content.shop.wishlist_option', '1', 'default', 'ja', '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(79, 'general.content.shop.wishlist_option', '1', 'default', 'nl', '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(80, 'general.content.shop.wishlist_option', '1', 'default', 'pl', '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(81, 'general.content.shop.wishlist_option', '1', 'default', 'pt_BR', '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(82, 'general.content.shop.wishlist_option', '1', 'default', 'tr', '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(83, 'general.content.shop.image_search', '1', 'default', 'en', '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(84, 'general.content.shop.image_search', '1', 'default', 'fr', '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(85, 'general.content.shop.image_search', '1', 'default', 'ar', '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(86, 'general.content.shop.image_search', '1', 'default', 'de', '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(87, 'general.content.shop.image_search', '1', 'default', 'es', '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(88, 'general.content.shop.image_search', '1', 'default', 'fa', '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(89, 'general.content.shop.image_search', '1', 'default', 'it', '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(90, 'general.content.shop.image_search', '1', 'default', 'ja', '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(91, 'general.content.shop.image_search', '1', 'default', 'nl', '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(92, 'general.content.shop.image_search', '1', 'default', 'pl', '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(93, 'general.content.shop.image_search', '1', 'default', 'pt_BR', '2021-04-07 18:58:51', '2021-04-07 18:58:51'),
(94, 'general.content.shop.image_search', '1', 'default', 'tr', '2021-04-07 18:58:51', '2021-04-07 18:58:51');

-- --------------------------------------------------------

--
-- Estrutura da tabela `countries`
--

CREATE TABLE `countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `countries`
--

INSERT INTO `countries` (`id`, `code`, `name`) VALUES
(1, 'AF', 'Afghanistan'),
(2, 'AX', 'Åland Islands'),
(3, 'AL', 'Albania'),
(4, 'DZ', 'Algeria'),
(5, 'AS', 'American Samoa'),
(6, 'AD', 'Andorra'),
(7, 'AO', 'Angola'),
(8, 'AI', 'Anguilla'),
(9, 'AQ', 'Antarctica'),
(10, 'AG', 'Antigua & Barbuda'),
(11, 'AR', 'Argentina'),
(12, 'AM', 'Armenia'),
(13, 'AW', 'Aruba'),
(14, 'AC', 'Ascension Island'),
(15, 'AU', 'Australia'),
(16, 'AT', 'Austria'),
(17, 'AZ', 'Azerbaijan'),
(18, 'BS', 'Bahamas'),
(19, 'BH', 'Bahrain'),
(20, 'BD', 'Bangladesh'),
(21, 'BB', 'Barbados'),
(22, 'BY', 'Belarus'),
(23, 'BE', 'Belgium'),
(24, 'BZ', 'Belize'),
(25, 'BJ', 'Benin'),
(26, 'BM', 'Bermuda'),
(27, 'BT', 'Bhutan'),
(28, 'BO', 'Bolivia'),
(29, 'BA', 'Bosnia & Herzegovina'),
(30, 'BW', 'Botswana'),
(31, 'BR', 'Brazil'),
(32, 'IO', 'British Indian Ocean Territory'),
(33, 'VG', 'British Virgin Islands'),
(34, 'BN', 'Brunei'),
(35, 'BG', 'Bulgaria'),
(36, 'BF', 'Burkina Faso'),
(37, 'BI', 'Burundi'),
(38, 'KH', 'Cambodia'),
(39, 'CM', 'Cameroon'),
(40, 'CA', 'Canada'),
(41, 'IC', 'Canary Islands'),
(42, 'CV', 'Cape Verde'),
(43, 'BQ', 'Caribbean Netherlands'),
(44, 'KY', 'Cayman Islands'),
(45, 'CF', 'Central African Republic'),
(46, 'EA', 'Ceuta & Melilla'),
(47, 'TD', 'Chad'),
(48, 'CL', 'Chile'),
(49, 'CN', 'China'),
(50, 'CX', 'Christmas Island'),
(51, 'CC', 'Cocos (Keeling) Islands'),
(52, 'CO', 'Colombia'),
(53, 'KM', 'Comoros'),
(54, 'CG', 'Congo - Brazzaville'),
(55, 'CD', 'Congo - Kinshasa'),
(56, 'CK', 'Cook Islands'),
(57, 'CR', 'Costa Rica'),
(58, 'CI', 'Côte d’Ivoire'),
(59, 'HR', 'Croatia'),
(60, 'CU', 'Cuba'),
(61, 'CW', 'Curaçao'),
(62, 'CY', 'Cyprus'),
(63, 'CZ', 'Czechia'),
(64, 'DK', 'Denmark'),
(65, 'DG', 'Diego Garcia'),
(66, 'DJ', 'Djibouti'),
(67, 'DM', 'Dominica'),
(68, 'DO', 'Dominican Republic'),
(69, 'EC', 'Ecuador'),
(70, 'EG', 'Egypt'),
(71, 'SV', 'El Salvador'),
(72, 'GQ', 'Equatorial Guinea'),
(73, 'ER', 'Eritrea'),
(74, 'EE', 'Estonia'),
(75, 'ET', 'Ethiopia'),
(76, 'EZ', 'Eurozone'),
(77, 'FK', 'Falkland Islands'),
(78, 'FO', 'Faroe Islands'),
(79, 'FJ', 'Fiji'),
(80, 'FI', 'Finland'),
(81, 'FR', 'France'),
(82, 'GF', 'French Guiana'),
(83, 'PF', 'French Polynesia'),
(84, 'TF', 'French Southern Territories'),
(85, 'GA', 'Gabon'),
(86, 'GM', 'Gambia'),
(87, 'GE', 'Georgia'),
(88, 'DE', 'Germany'),
(89, 'GH', 'Ghana'),
(90, 'GI', 'Gibraltar'),
(91, 'GR', 'Greece'),
(92, 'GL', 'Greenland'),
(93, 'GD', 'Grenada'),
(94, 'GP', 'Guadeloupe'),
(95, 'GU', 'Guam'),
(96, 'GT', 'Guatemala'),
(97, 'GG', 'Guernsey'),
(98, 'GN', 'Guinea'),
(99, 'GW', 'Guinea-Bissau'),
(100, 'GY', 'Guyana'),
(101, 'HT', 'Haiti'),
(102, 'HN', 'Honduras'),
(103, 'HK', 'Hong Kong SAR China'),
(104, 'HU', 'Hungary'),
(105, 'IS', 'Iceland'),
(106, 'IN', 'India'),
(107, 'ID', 'Indonesia'),
(108, 'IR', 'Iran'),
(109, 'IQ', 'Iraq'),
(110, 'IE', 'Ireland'),
(111, 'IM', 'Isle of Man'),
(112, 'IL', 'Israel'),
(113, 'IT', 'Italy'),
(114, 'JM', 'Jamaica'),
(115, 'JP', 'Japan'),
(116, 'JE', 'Jersey'),
(117, 'JO', 'Jordan'),
(118, 'KZ', 'Kazakhstan'),
(119, 'KE', 'Kenya'),
(120, 'KI', 'Kiribati'),
(121, 'XK', 'Kosovo'),
(122, 'KW', 'Kuwait'),
(123, 'KG', 'Kyrgyzstan'),
(124, 'LA', 'Laos'),
(125, 'LV', 'Latvia'),
(126, 'LB', 'Lebanon'),
(127, 'LS', 'Lesotho'),
(128, 'LR', 'Liberia'),
(129, 'LY', 'Libya'),
(130, 'LI', 'Liechtenstein'),
(131, 'LT', 'Lithuania'),
(132, 'LU', 'Luxembourg'),
(133, 'MO', 'Macau SAR China'),
(134, 'MK', 'Macedonia'),
(135, 'MG', 'Madagascar'),
(136, 'MW', 'Malawi'),
(137, 'MY', 'Malaysia'),
(138, 'MV', 'Maldives'),
(139, 'ML', 'Mali'),
(140, 'MT', 'Malta'),
(141, 'MH', 'Marshall Islands'),
(142, 'MQ', 'Martinique'),
(143, 'MR', 'Mauritania'),
(144, 'MU', 'Mauritius'),
(145, 'YT', 'Mayotte'),
(146, 'MX', 'Mexico'),
(147, 'FM', 'Micronesia'),
(148, 'MD', 'Moldova'),
(149, 'MC', 'Monaco'),
(150, 'MN', 'Mongolia'),
(151, 'ME', 'Montenegro'),
(152, 'MS', 'Montserrat'),
(153, 'MA', 'Morocco'),
(154, 'MZ', 'Mozambique'),
(155, 'MM', 'Myanmar (Burma)'),
(156, 'NA', 'Namibia'),
(157, 'NR', 'Nauru'),
(158, 'NP', 'Nepal'),
(159, 'NL', 'Netherlands'),
(160, 'NC', 'New Caledonia'),
(161, 'NZ', 'New Zealand'),
(162, 'NI', 'Nicaragua'),
(163, 'NE', 'Niger'),
(164, 'NG', 'Nigeria'),
(165, 'NU', 'Niue'),
(166, 'NF', 'Norfolk Island'),
(167, 'KP', 'North Korea'),
(168, 'MP', 'Northern Mariana Islands'),
(169, 'NO', 'Norway'),
(170, 'OM', 'Oman'),
(171, 'PK', 'Pakistan'),
(172, 'PW', 'Palau'),
(173, 'PS', 'Palestinian Territories'),
(174, 'PA', 'Panama'),
(175, 'PG', 'Papua New Guinea'),
(176, 'PY', 'Paraguay'),
(177, 'PE', 'Peru'),
(178, 'PH', 'Philippines'),
(179, 'PN', 'Pitcairn Islands'),
(180, 'PL', 'Poland'),
(181, 'PT', 'Portugal'),
(182, 'PR', 'Puerto Rico'),
(183, 'QA', 'Qatar'),
(184, 'RE', 'Réunion'),
(185, 'RO', 'Romania'),
(186, 'RU', 'Russia'),
(187, 'RW', 'Rwanda'),
(188, 'WS', 'Samoa'),
(189, 'SM', 'San Marino'),
(190, 'ST', 'São Tomé & Príncipe'),
(191, 'SA', 'Saudi Arabia'),
(192, 'SN', 'Senegal'),
(193, 'RS', 'Serbia'),
(194, 'SC', 'Seychelles'),
(195, 'SL', 'Sierra Leone'),
(196, 'SG', 'Singapore'),
(197, 'SX', 'Sint Maarten'),
(198, 'SK', 'Slovakia'),
(199, 'SI', 'Slovenia'),
(200, 'SB', 'Solomon Islands'),
(201, 'SO', 'Somalia'),
(202, 'ZA', 'South Africa'),
(203, 'GS', 'South Georgia & South Sandwich Islands'),
(204, 'KR', 'South Korea'),
(205, 'SS', 'South Sudan'),
(206, 'ES', 'Spain'),
(207, 'LK', 'Sri Lanka'),
(208, 'BL', 'St. Barthélemy'),
(209, 'SH', 'St. Helena'),
(210, 'KN', 'St. Kitts & Nevis'),
(211, 'LC', 'St. Lucia'),
(212, 'MF', 'St. Martin'),
(213, 'PM', 'St. Pierre & Miquelon'),
(214, 'VC', 'St. Vincent & Grenadines'),
(215, 'SD', 'Sudan'),
(216, 'SR', 'Suriname'),
(217, 'SJ', 'Svalbard & Jan Mayen'),
(218, 'SZ', 'Swaziland'),
(219, 'SE', 'Sweden'),
(220, 'CH', 'Switzerland'),
(221, 'SY', 'Syria'),
(222, 'TW', 'Taiwan'),
(223, 'TJ', 'Tajikistan'),
(224, 'TZ', 'Tanzania'),
(225, 'TH', 'Thailand'),
(226, 'TL', 'Timor-Leste'),
(227, 'TG', 'Togo'),
(228, 'TK', 'Tokelau'),
(229, 'TO', 'Tonga'),
(230, 'TT', 'Trinidad & Tobago'),
(231, 'TA', 'Tristan da Cunha'),
(232, 'TN', 'Tunisia'),
(233, 'TR', 'Turkey'),
(234, 'TM', 'Turkmenistan'),
(235, 'TC', 'Turks & Caicos Islands'),
(236, 'TV', 'Tuvalu'),
(237, 'UM', 'U.S. Outlying Islands'),
(238, 'VI', 'U.S. Virgin Islands'),
(239, 'UG', 'Uganda'),
(240, 'UA', 'Ukraine'),
(241, 'AE', 'United Arab Emirates'),
(242, 'GB', 'United Kingdom'),
(243, 'UN', 'United Nations'),
(244, 'US', 'United States'),
(245, 'UY', 'Uruguay'),
(246, 'UZ', 'Uzbekistan'),
(247, 'VU', 'Vanuatu'),
(248, 'VA', 'Vatican City'),
(249, 'VE', 'Venezuela'),
(250, 'VN', 'Vietnam'),
(251, 'WF', 'Wallis & Futuna'),
(252, 'EH', 'Western Sahara'),
(253, 'YE', 'Yemen'),
(254, 'ZM', 'Zambia'),
(255, 'ZW', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Estrutura da tabela `country_states`
--

CREATE TABLE `country_states` (
  `id` int(10) UNSIGNED NOT NULL,
  `country_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `country_states`
--

INSERT INTO `country_states` (`id`, `country_code`, `code`, `default_name`, `country_id`) VALUES
(1, 'US', 'AL', 'Alabama', 244),
(2, 'US', 'AK', 'Alaska', 244),
(3, 'US', 'AS', 'American Samoa', 244),
(4, 'US', 'AZ', 'Arizona', 244),
(5, 'US', 'AR', 'Arkansas', 244),
(6, 'US', 'AE', 'Armed Forces Africa', 244),
(7, 'US', 'AA', 'Armed Forces Americas', 244),
(8, 'US', 'AE', 'Armed Forces Canada', 244),
(9, 'US', 'AE', 'Armed Forces Europe', 244),
(10, 'US', 'AE', 'Armed Forces Middle East', 244),
(11, 'US', 'AP', 'Armed Forces Pacific', 244),
(12, 'US', 'CA', 'California', 244),
(13, 'US', 'CO', 'Colorado', 244),
(14, 'US', 'CT', 'Connecticut', 244),
(15, 'US', 'DE', 'Delaware', 244),
(16, 'US', 'DC', 'District of Columbia', 244),
(17, 'US', 'FM', 'Federated States Of Micronesia', 244),
(18, 'US', 'FL', 'Florida', 244),
(19, 'US', 'GA', 'Georgia', 244),
(20, 'US', 'GU', 'Guam', 244),
(21, 'US', 'HI', 'Hawaii', 244),
(22, 'US', 'ID', 'Idaho', 244),
(23, 'US', 'IL', 'Illinois', 244),
(24, 'US', 'IN', 'Indiana', 244),
(25, 'US', 'IA', 'Iowa', 244),
(26, 'US', 'KS', 'Kansas', 244),
(27, 'US', 'KY', 'Kentucky', 244),
(28, 'US', 'LA', 'Louisiana', 244),
(29, 'US', 'ME', 'Maine', 244),
(30, 'US', 'MH', 'Marshall Islands', 244),
(31, 'US', 'MD', 'Maryland', 244),
(32, 'US', 'MA', 'Massachusetts', 244),
(33, 'US', 'MI', 'Michigan', 244),
(34, 'US', 'MN', 'Minnesota', 244),
(35, 'US', 'MS', 'Mississippi', 244),
(36, 'US', 'MO', 'Missouri', 244),
(37, 'US', 'MT', 'Montana', 244),
(38, 'US', 'NE', 'Nebraska', 244),
(39, 'US', 'NV', 'Nevada', 244),
(40, 'US', 'NH', 'New Hampshire', 244),
(41, 'US', 'NJ', 'New Jersey', 244),
(42, 'US', 'NM', 'New Mexico', 244),
(43, 'US', 'NY', 'New York', 244),
(44, 'US', 'NC', 'North Carolina', 244),
(45, 'US', 'ND', 'North Dakota', 244),
(46, 'US', 'MP', 'Northern Mariana Islands', 244),
(47, 'US', 'OH', 'Ohio', 244),
(48, 'US', 'OK', 'Oklahoma', 244),
(49, 'US', 'OR', 'Oregon', 244),
(50, 'US', 'PW', 'Palau', 244),
(51, 'US', 'PA', 'Pennsylvania', 244),
(52, 'US', 'PR', 'Puerto Rico', 244),
(53, 'US', 'RI', 'Rhode Island', 244),
(54, 'US', 'SC', 'South Carolina', 244),
(55, 'US', 'SD', 'South Dakota', 244),
(56, 'US', 'TN', 'Tennessee', 244),
(57, 'US', 'TX', 'Texas', 244),
(58, 'US', 'UT', 'Utah', 244),
(59, 'US', 'VT', 'Vermont', 244),
(60, 'US', 'VI', 'Virgin Islands', 244),
(61, 'US', 'VA', 'Virginia', 244),
(62, 'US', 'WA', 'Washington', 244),
(63, 'US', 'WV', 'West Virginia', 244),
(64, 'US', 'WI', 'Wisconsin', 244),
(65, 'US', 'WY', 'Wyoming', 244),
(66, 'CA', 'AB', 'Alberta', 40),
(67, 'CA', 'BC', 'British Columbia', 40),
(68, 'CA', 'MB', 'Manitoba', 40),
(69, 'CA', 'NL', 'Newfoundland and Labrador', 40),
(70, 'CA', 'NB', 'New Brunswick', 40),
(71, 'CA', 'NS', 'Nova Scotia', 40),
(72, 'CA', 'NT', 'Northwest Territories', 40),
(73, 'CA', 'NU', 'Nunavut', 40),
(74, 'CA', 'ON', 'Ontario', 40),
(75, 'CA', 'PE', 'Prince Edward Island', 40),
(76, 'CA', 'QC', 'Quebec', 40),
(77, 'CA', 'SK', 'Saskatchewan', 40),
(78, 'CA', 'YT', 'Yukon Territory', 40),
(79, 'DE', 'NDS', 'Niedersachsen', 88),
(80, 'DE', 'BAW', 'Baden-Württemberg', 88),
(81, 'DE', 'BAY', 'Bayern', 88),
(82, 'DE', 'BER', 'Berlin', 88),
(83, 'DE', 'BRG', 'Brandenburg', 88),
(84, 'DE', 'BRE', 'Bremen', 88),
(85, 'DE', 'HAM', 'Hamburg', 88),
(86, 'DE', 'HES', 'Hessen', 88),
(87, 'DE', 'MEC', 'Mecklenburg-Vorpommern', 88),
(88, 'DE', 'NRW', 'Nordrhein-Westfalen', 88),
(89, 'DE', 'RHE', 'Rheinland-Pfalz', 88),
(90, 'DE', 'SAR', 'Saarland', 88),
(91, 'DE', 'SAS', 'Sachsen', 88),
(92, 'DE', 'SAC', 'Sachsen-Anhalt', 88),
(93, 'DE', 'SCN', 'Schleswig-Holstein', 88),
(94, 'DE', 'THE', 'Thüringen', 88),
(95, 'AT', 'WI', 'Wien', 16),
(96, 'AT', 'NO', 'Niederösterreich', 16),
(97, 'AT', 'OO', 'Oberösterreich', 16),
(98, 'AT', 'SB', 'Salzburg', 16),
(99, 'AT', 'KN', 'Kärnten', 16),
(100, 'AT', 'ST', 'Steiermark', 16),
(101, 'AT', 'TI', 'Tirol', 16),
(102, 'AT', 'BL', 'Burgenland', 16),
(103, 'AT', 'VB', 'Vorarlberg', 16),
(104, 'CH', 'AG', 'Aargau', 220),
(105, 'CH', 'AI', 'Appenzell Innerrhoden', 220),
(106, 'CH', 'AR', 'Appenzell Ausserrhoden', 220),
(107, 'CH', 'BE', 'Bern', 220),
(108, 'CH', 'BL', 'Basel-Landschaft', 220),
(109, 'CH', 'BS', 'Basel-Stadt', 220),
(110, 'CH', 'FR', 'Freiburg', 220),
(111, 'CH', 'GE', 'Genf', 220),
(112, 'CH', 'GL', 'Glarus', 220),
(113, 'CH', 'GR', 'Graubünden', 220),
(114, 'CH', 'JU', 'Jura', 220),
(115, 'CH', 'LU', 'Luzern', 220),
(116, 'CH', 'NE', 'Neuenburg', 220),
(117, 'CH', 'NW', 'Nidwalden', 220),
(118, 'CH', 'OW', 'Obwalden', 220),
(119, 'CH', 'SG', 'St. Gallen', 220),
(120, 'CH', 'SH', 'Schaffhausen', 220),
(121, 'CH', 'SO', 'Solothurn', 220),
(122, 'CH', 'SZ', 'Schwyz', 220),
(123, 'CH', 'TG', 'Thurgau', 220),
(124, 'CH', 'TI', 'Tessin', 220),
(125, 'CH', 'UR', 'Uri', 220),
(126, 'CH', 'VD', 'Waadt', 220),
(127, 'CH', 'VS', 'Wallis', 220),
(128, 'CH', 'ZG', 'Zug', 220),
(129, 'CH', 'ZH', 'Zürich', 220),
(130, 'ES', 'A Coruсa', 'A Coruña', 206),
(131, 'ES', 'Alava', 'Alava', 206),
(132, 'ES', 'Albacete', 'Albacete', 206),
(133, 'ES', 'Alicante', 'Alicante', 206),
(134, 'ES', 'Almeria', 'Almeria', 206),
(135, 'ES', 'Asturias', 'Asturias', 206),
(136, 'ES', 'Avila', 'Avila', 206),
(137, 'ES', 'Badajoz', 'Badajoz', 206),
(138, 'ES', 'Baleares', 'Baleares', 206),
(139, 'ES', 'Barcelona', 'Barcelona', 206),
(140, 'ES', 'Burgos', 'Burgos', 206),
(141, 'ES', 'Caceres', 'Caceres', 206),
(142, 'ES', 'Cadiz', 'Cadiz', 206),
(143, 'ES', 'Cantabria', 'Cantabria', 206),
(144, 'ES', 'Castellon', 'Castellon', 206),
(145, 'ES', 'Ceuta', 'Ceuta', 206),
(146, 'ES', 'Ciudad Real', 'Ciudad Real', 206),
(147, 'ES', 'Cordoba', 'Cordoba', 206),
(148, 'ES', 'Cuenca', 'Cuenca', 206),
(149, 'ES', 'Girona', 'Girona', 206),
(150, 'ES', 'Granada', 'Granada', 206),
(151, 'ES', 'Guadalajara', 'Guadalajara', 206),
(152, 'ES', 'Guipuzcoa', 'Guipuzcoa', 206),
(153, 'ES', 'Huelva', 'Huelva', 206),
(154, 'ES', 'Huesca', 'Huesca', 206),
(155, 'ES', 'Jaen', 'Jaen', 206),
(156, 'ES', 'La Rioja', 'La Rioja', 206),
(157, 'ES', 'Las Palmas', 'Las Palmas', 206),
(158, 'ES', 'Leon', 'Leon', 206),
(159, 'ES', 'Lleida', 'Lleida', 206),
(160, 'ES', 'Lugo', 'Lugo', 206),
(161, 'ES', 'Madrid', 'Madrid', 206),
(162, 'ES', 'Malaga', 'Malaga', 206),
(163, 'ES', 'Melilla', 'Melilla', 206),
(164, 'ES', 'Murcia', 'Murcia', 206),
(165, 'ES', 'Navarra', 'Navarra', 206),
(166, 'ES', 'Ourense', 'Ourense', 206),
(167, 'ES', 'Palencia', 'Palencia', 206),
(168, 'ES', 'Pontevedra', 'Pontevedra', 206),
(169, 'ES', 'Salamanca', 'Salamanca', 206),
(170, 'ES', 'Santa Cruz de Tenerife', 'Santa Cruz de Tenerife', 206),
(171, 'ES', 'Segovia', 'Segovia', 206),
(172, 'ES', 'Sevilla', 'Sevilla', 206),
(173, 'ES', 'Soria', 'Soria', 206),
(174, 'ES', 'Tarragona', 'Tarragona', 206),
(175, 'ES', 'Teruel', 'Teruel', 206),
(176, 'ES', 'Toledo', 'Toledo', 206),
(177, 'ES', 'Valencia', 'Valencia', 206),
(178, 'ES', 'Valladolid', 'Valladolid', 206),
(179, 'ES', 'Vizcaya', 'Vizcaya', 206),
(180, 'ES', 'Zamora', 'Zamora', 206),
(181, 'ES', 'Zaragoza', 'Zaragoza', 206),
(182, 'FR', '1', 'Ain', 81),
(183, 'FR', '2', 'Aisne', 81),
(184, 'FR', '3', 'Allier', 81),
(185, 'FR', '4', 'Alpes-de-Haute-Provence', 81),
(186, 'FR', '5', 'Hautes-Alpes', 81),
(187, 'FR', '6', 'Alpes-Maritimes', 81),
(188, 'FR', '7', 'Ardèche', 81),
(189, 'FR', '8', 'Ardennes', 81),
(190, 'FR', '9', 'Ariège', 81),
(191, 'FR', '10', 'Aube', 81),
(192, 'FR', '11', 'Aude', 81),
(193, 'FR', '12', 'Aveyron', 81),
(194, 'FR', '13', 'Bouches-du-Rhône', 81),
(195, 'FR', '14', 'Calvados', 81),
(196, 'FR', '15', 'Cantal', 81),
(197, 'FR', '16', 'Charente', 81),
(198, 'FR', '17', 'Charente-Maritime', 81),
(199, 'FR', '18', 'Cher', 81),
(200, 'FR', '19', 'Corrèze', 81),
(201, 'FR', '2A', 'Corse-du-Sud', 81),
(202, 'FR', '2B', 'Haute-Corse', 81),
(203, 'FR', '21', 'Côte-d\'Or', 81),
(204, 'FR', '22', 'Côtes-d\'Armor', 81),
(205, 'FR', '23', 'Creuse', 81),
(206, 'FR', '24', 'Dordogne', 81),
(207, 'FR', '25', 'Doubs', 81),
(208, 'FR', '26', 'Drôme', 81),
(209, 'FR', '27', 'Eure', 81),
(210, 'FR', '28', 'Eure-et-Loir', 81),
(211, 'FR', '29', 'Finistère', 81),
(212, 'FR', '30', 'Gard', 81),
(213, 'FR', '31', 'Haute-Garonne', 81),
(214, 'FR', '32', 'Gers', 81),
(215, 'FR', '33', 'Gironde', 81),
(216, 'FR', '34', 'Hérault', 81),
(217, 'FR', '35', 'Ille-et-Vilaine', 81),
(218, 'FR', '36', 'Indre', 81),
(219, 'FR', '37', 'Indre-et-Loire', 81),
(220, 'FR', '38', 'Isère', 81),
(221, 'FR', '39', 'Jura', 81),
(222, 'FR', '40', 'Landes', 81),
(223, 'FR', '41', 'Loir-et-Cher', 81),
(224, 'FR', '42', 'Loire', 81),
(225, 'FR', '43', 'Haute-Loire', 81),
(226, 'FR', '44', 'Loire-Atlantique', 81),
(227, 'FR', '45', 'Loiret', 81),
(228, 'FR', '46', 'Lot', 81),
(229, 'FR', '47', 'Lot-et-Garonne', 81),
(230, 'FR', '48', 'Lozère', 81),
(231, 'FR', '49', 'Maine-et-Loire', 81),
(232, 'FR', '50', 'Manche', 81),
(233, 'FR', '51', 'Marne', 81),
(234, 'FR', '52', 'Haute-Marne', 81),
(235, 'FR', '53', 'Mayenne', 81),
(236, 'FR', '54', 'Meurthe-et-Moselle', 81),
(237, 'FR', '55', 'Meuse', 81),
(238, 'FR', '56', 'Morbihan', 81),
(239, 'FR', '57', 'Moselle', 81),
(240, 'FR', '58', 'Nièvre', 81),
(241, 'FR', '59', 'Nord', 81),
(242, 'FR', '60', 'Oise', 81),
(243, 'FR', '61', 'Orne', 81),
(244, 'FR', '62', 'Pas-de-Calais', 81),
(245, 'FR', '63', 'Puy-de-Dôme', 81),
(246, 'FR', '64', 'Pyrénées-Atlantiques', 81),
(247, 'FR', '65', 'Hautes-Pyrénées', 81),
(248, 'FR', '66', 'Pyrénées-Orientales', 81),
(249, 'FR', '67', 'Bas-Rhin', 81),
(250, 'FR', '68', 'Haut-Rhin', 81),
(251, 'FR', '69', 'Rhône', 81),
(252, 'FR', '70', 'Haute-Saône', 81),
(253, 'FR', '71', 'Saône-et-Loire', 81),
(254, 'FR', '72', 'Sarthe', 81),
(255, 'FR', '73', 'Savoie', 81),
(256, 'FR', '74', 'Haute-Savoie', 81),
(257, 'FR', '75', 'Paris', 81),
(258, 'FR', '76', 'Seine-Maritime', 81),
(259, 'FR', '77', 'Seine-et-Marne', 81),
(260, 'FR', '78', 'Yvelines', 81),
(261, 'FR', '79', 'Deux-Sèvres', 81),
(262, 'FR', '80', 'Somme', 81),
(263, 'FR', '81', 'Tarn', 81),
(264, 'FR', '82', 'Tarn-et-Garonne', 81),
(265, 'FR', '83', 'Var', 81),
(266, 'FR', '84', 'Vaucluse', 81),
(267, 'FR', '85', 'Vendée', 81),
(268, 'FR', '86', 'Vienne', 81),
(269, 'FR', '87', 'Haute-Vienne', 81),
(270, 'FR', '88', 'Vosges', 81),
(271, 'FR', '89', 'Yonne', 81),
(272, 'FR', '90', 'Territoire-de-Belfort', 81),
(273, 'FR', '91', 'Essonne', 81),
(274, 'FR', '92', 'Hauts-de-Seine', 81),
(275, 'FR', '93', 'Seine-Saint-Denis', 81),
(276, 'FR', '94', 'Val-de-Marne', 81),
(277, 'FR', '95', 'Val-d\'Oise', 81),
(278, 'RO', 'AB', 'Alba', 185),
(279, 'RO', 'AR', 'Arad', 185),
(280, 'RO', 'AG', 'Argeş', 185),
(281, 'RO', 'BC', 'Bacău', 185),
(282, 'RO', 'BH', 'Bihor', 185),
(283, 'RO', 'BN', 'Bistriţa-Năsăud', 185),
(284, 'RO', 'BT', 'Botoşani', 185),
(285, 'RO', 'BV', 'Braşov', 185),
(286, 'RO', 'BR', 'Brăila', 185),
(287, 'RO', 'B', 'Bucureşti', 185),
(288, 'RO', 'BZ', 'Buzău', 185),
(289, 'RO', 'CS', 'Caraş-Severin', 185),
(290, 'RO', 'CL', 'Călăraşi', 185),
(291, 'RO', 'CJ', 'Cluj', 185),
(292, 'RO', 'CT', 'Constanţa', 185),
(293, 'RO', 'CV', 'Covasna', 185),
(294, 'RO', 'DB', 'Dâmboviţa', 185),
(295, 'RO', 'DJ', 'Dolj', 185),
(296, 'RO', 'GL', 'Galaţi', 185),
(297, 'RO', 'GR', 'Giurgiu', 185),
(298, 'RO', 'GJ', 'Gorj', 185),
(299, 'RO', 'HR', 'Harghita', 185),
(300, 'RO', 'HD', 'Hunedoara', 185),
(301, 'RO', 'IL', 'Ialomiţa', 185),
(302, 'RO', 'IS', 'Iaşi', 185),
(303, 'RO', 'IF', 'Ilfov', 185),
(304, 'RO', 'MM', 'Maramureş', 185),
(305, 'RO', 'MH', 'Mehedinţi', 185),
(306, 'RO', 'MS', 'Mureş', 185),
(307, 'RO', 'NT', 'Neamţ', 185),
(308, 'RO', 'OT', 'Olt', 185),
(309, 'RO', 'PH', 'Prahova', 185),
(310, 'RO', 'SM', 'Satu-Mare', 185),
(311, 'RO', 'SJ', 'Sălaj', 185),
(312, 'RO', 'SB', 'Sibiu', 185),
(313, 'RO', 'SV', 'Suceava', 185),
(314, 'RO', 'TR', 'Teleorman', 185),
(315, 'RO', 'TM', 'Timiş', 185),
(316, 'RO', 'TL', 'Tulcea', 185),
(317, 'RO', 'VS', 'Vaslui', 185),
(318, 'RO', 'VL', 'Vâlcea', 185),
(319, 'RO', 'VN', 'Vrancea', 185),
(320, 'FI', 'Lappi', 'Lappi', 80),
(321, 'FI', 'Pohjois-Pohjanmaa', 'Pohjois-Pohjanmaa', 80),
(322, 'FI', 'Kainuu', 'Kainuu', 80),
(323, 'FI', 'Pohjois-Karjala', 'Pohjois-Karjala', 80),
(324, 'FI', 'Pohjois-Savo', 'Pohjois-Savo', 80),
(325, 'FI', 'Etelä-Savo', 'Etelä-Savo', 80),
(326, 'FI', 'Etelä-Pohjanmaa', 'Etelä-Pohjanmaa', 80),
(327, 'FI', 'Pohjanmaa', 'Pohjanmaa', 80),
(328, 'FI', 'Pirkanmaa', 'Pirkanmaa', 80),
(329, 'FI', 'Satakunta', 'Satakunta', 80),
(330, 'FI', 'Keski-Pohjanmaa', 'Keski-Pohjanmaa', 80),
(331, 'FI', 'Keski-Suomi', 'Keski-Suomi', 80),
(332, 'FI', 'Varsinais-Suomi', 'Varsinais-Suomi', 80),
(333, 'FI', 'Etelä-Karjala', 'Etelä-Karjala', 80),
(334, 'FI', 'Päijät-Häme', 'Päijät-Häme', 80),
(335, 'FI', 'Kanta-Häme', 'Kanta-Häme', 80),
(336, 'FI', 'Uusimaa', 'Uusimaa', 80),
(337, 'FI', 'Itä-Uusimaa', 'Itä-Uusimaa', 80),
(338, 'FI', 'Kymenlaakso', 'Kymenlaakso', 80),
(339, 'FI', 'Ahvenanmaa', 'Ahvenanmaa', 80),
(340, 'EE', 'EE-37', 'Harjumaa', 74),
(341, 'EE', 'EE-39', 'Hiiumaa', 74),
(342, 'EE', 'EE-44', 'Ida-Virumaa', 74),
(343, 'EE', 'EE-49', 'Jõgevamaa', 74),
(344, 'EE', 'EE-51', 'Järvamaa', 74),
(345, 'EE', 'EE-57', 'Läänemaa', 74),
(346, 'EE', 'EE-59', 'Lääne-Virumaa', 74),
(347, 'EE', 'EE-65', 'Põlvamaa', 74),
(348, 'EE', 'EE-67', 'Pärnumaa', 74),
(349, 'EE', 'EE-70', 'Raplamaa', 74),
(350, 'EE', 'EE-74', 'Saaremaa', 74),
(351, 'EE', 'EE-78', 'Tartumaa', 74),
(352, 'EE', 'EE-82', 'Valgamaa', 74),
(353, 'EE', 'EE-84', 'Viljandimaa', 74),
(354, 'EE', 'EE-86', 'Võrumaa', 74),
(355, 'LV', 'LV-DGV', 'Daugavpils', 125),
(356, 'LV', 'LV-JEL', 'Jelgava', 125),
(357, 'LV', 'Jēkabpils', 'Jēkabpils', 125),
(358, 'LV', 'LV-JUR', 'Jūrmala', 125),
(359, 'LV', 'LV-LPX', 'Liepāja', 125),
(360, 'LV', 'LV-LE', 'Liepājas novads', 125),
(361, 'LV', 'LV-REZ', 'Rēzekne', 125),
(362, 'LV', 'LV-RIX', 'Rīga', 125),
(363, 'LV', 'LV-RI', 'Rīgas novads', 125),
(364, 'LV', 'Valmiera', 'Valmiera', 125),
(365, 'LV', 'LV-VEN', 'Ventspils', 125),
(366, 'LV', 'Aglonas novads', 'Aglonas novads', 125),
(367, 'LV', 'LV-AI', 'Aizkraukles novads', 125),
(368, 'LV', 'Aizputes novads', 'Aizputes novads', 125),
(369, 'LV', 'Aknīstes novads', 'Aknīstes novads', 125),
(370, 'LV', 'Alojas novads', 'Alojas novads', 125),
(371, 'LV', 'Alsungas novads', 'Alsungas novads', 125),
(372, 'LV', 'LV-AL', 'Alūksnes novads', 125),
(373, 'LV', 'Amatas novads', 'Amatas novads', 125),
(374, 'LV', 'Apes novads', 'Apes novads', 125),
(375, 'LV', 'Auces novads', 'Auces novads', 125),
(376, 'LV', 'Babītes novads', 'Babītes novads', 125),
(377, 'LV', 'Baldones novads', 'Baldones novads', 125),
(378, 'LV', 'Baltinavas novads', 'Baltinavas novads', 125),
(379, 'LV', 'LV-BL', 'Balvu novads', 125),
(380, 'LV', 'LV-BU', 'Bauskas novads', 125),
(381, 'LV', 'Beverīnas novads', 'Beverīnas novads', 125),
(382, 'LV', 'Brocēnu novads', 'Brocēnu novads', 125),
(383, 'LV', 'Burtnieku novads', 'Burtnieku novads', 125),
(384, 'LV', 'Carnikavas novads', 'Carnikavas novads', 125),
(385, 'LV', 'Cesvaines novads', 'Cesvaines novads', 125),
(386, 'LV', 'Ciblas novads', 'Ciblas novads', 125),
(387, 'LV', 'LV-CE', 'Cēsu novads', 125),
(388, 'LV', 'Dagdas novads', 'Dagdas novads', 125),
(389, 'LV', 'LV-DA', 'Daugavpils novads', 125),
(390, 'LV', 'LV-DO', 'Dobeles novads', 125),
(391, 'LV', 'Dundagas novads', 'Dundagas novads', 125),
(392, 'LV', 'Durbes novads', 'Durbes novads', 125),
(393, 'LV', 'Engures novads', 'Engures novads', 125),
(394, 'LV', 'Garkalnes novads', 'Garkalnes novads', 125),
(395, 'LV', 'Grobiņas novads', 'Grobiņas novads', 125),
(396, 'LV', 'LV-GU', 'Gulbenes novads', 125),
(397, 'LV', 'Iecavas novads', 'Iecavas novads', 125),
(398, 'LV', 'Ikšķiles novads', 'Ikšķiles novads', 125),
(399, 'LV', 'Ilūkstes novads', 'Ilūkstes novads', 125),
(400, 'LV', 'Inčukalna novads', 'Inčukalna novads', 125),
(401, 'LV', 'Jaunjelgavas novads', 'Jaunjelgavas novads', 125),
(402, 'LV', 'Jaunpiebalgas novads', 'Jaunpiebalgas novads', 125),
(403, 'LV', 'Jaunpils novads', 'Jaunpils novads', 125),
(404, 'LV', 'LV-JL', 'Jelgavas novads', 125),
(405, 'LV', 'LV-JK', 'Jēkabpils novads', 125),
(406, 'LV', 'Kandavas novads', 'Kandavas novads', 125),
(407, 'LV', 'Kokneses novads', 'Kokneses novads', 125),
(408, 'LV', 'Krimuldas novads', 'Krimuldas novads', 125),
(409, 'LV', 'Krustpils novads', 'Krustpils novads', 125),
(410, 'LV', 'LV-KR', 'Krāslavas novads', 125),
(411, 'LV', 'LV-KU', 'Kuldīgas novads', 125),
(412, 'LV', 'Kārsavas novads', 'Kārsavas novads', 125),
(413, 'LV', 'Lielvārdes novads', 'Lielvārdes novads', 125),
(414, 'LV', 'LV-LM', 'Limbažu novads', 125),
(415, 'LV', 'Lubānas novads', 'Lubānas novads', 125),
(416, 'LV', 'LV-LU', 'Ludzas novads', 125),
(417, 'LV', 'Līgatnes novads', 'Līgatnes novads', 125),
(418, 'LV', 'Līvānu novads', 'Līvānu novads', 125),
(419, 'LV', 'LV-MA', 'Madonas novads', 125),
(420, 'LV', 'Mazsalacas novads', 'Mazsalacas novads', 125),
(421, 'LV', 'Mālpils novads', 'Mālpils novads', 125),
(422, 'LV', 'Mārupes novads', 'Mārupes novads', 125),
(423, 'LV', 'Naukšēnu novads', 'Naukšēnu novads', 125),
(424, 'LV', 'Neretas novads', 'Neretas novads', 125),
(425, 'LV', 'Nīcas novads', 'Nīcas novads', 125),
(426, 'LV', 'LV-OG', 'Ogres novads', 125),
(427, 'LV', 'Olaines novads', 'Olaines novads', 125),
(428, 'LV', 'Ozolnieku novads', 'Ozolnieku novads', 125),
(429, 'LV', 'LV-PR', 'Preiļu novads', 125),
(430, 'LV', 'Priekules novads', 'Priekules novads', 125),
(431, 'LV', 'Priekuļu novads', 'Priekuļu novads', 125),
(432, 'LV', 'Pārgaujas novads', 'Pārgaujas novads', 125),
(433, 'LV', 'Pāvilostas novads', 'Pāvilostas novads', 125),
(434, 'LV', 'Pļaviņu novads', 'Pļaviņu novads', 125),
(435, 'LV', 'Raunas novads', 'Raunas novads', 125),
(436, 'LV', 'Riebiņu novads', 'Riebiņu novads', 125),
(437, 'LV', 'Rojas novads', 'Rojas novads', 125),
(438, 'LV', 'Ropažu novads', 'Ropažu novads', 125),
(439, 'LV', 'Rucavas novads', 'Rucavas novads', 125),
(440, 'LV', 'Rugāju novads', 'Rugāju novads', 125),
(441, 'LV', 'Rundāles novads', 'Rundāles novads', 125),
(442, 'LV', 'LV-RE', 'Rēzeknes novads', 125),
(443, 'LV', 'Rūjienas novads', 'Rūjienas novads', 125),
(444, 'LV', 'Salacgrīvas novads', 'Salacgrīvas novads', 125),
(445, 'LV', 'Salas novads', 'Salas novads', 125),
(446, 'LV', 'Salaspils novads', 'Salaspils novads', 125),
(447, 'LV', 'LV-SA', 'Saldus novads', 125),
(448, 'LV', 'Saulkrastu novads', 'Saulkrastu novads', 125),
(449, 'LV', 'Siguldas novads', 'Siguldas novads', 125),
(450, 'LV', 'Skrundas novads', 'Skrundas novads', 125),
(451, 'LV', 'Skrīveru novads', 'Skrīveru novads', 125),
(452, 'LV', 'Smiltenes novads', 'Smiltenes novads', 125),
(453, 'LV', 'Stopiņu novads', 'Stopiņu novads', 125),
(454, 'LV', 'Strenču novads', 'Strenču novads', 125),
(455, 'LV', 'Sējas novads', 'Sējas novads', 125),
(456, 'LV', 'LV-TA', 'Talsu novads', 125),
(457, 'LV', 'LV-TU', 'Tukuma novads', 125),
(458, 'LV', 'Tērvetes novads', 'Tērvetes novads', 125),
(459, 'LV', 'Vaiņodes novads', 'Vaiņodes novads', 125),
(460, 'LV', 'LV-VK', 'Valkas novads', 125),
(461, 'LV', 'LV-VM', 'Valmieras novads', 125),
(462, 'LV', 'Varakļānu novads', 'Varakļānu novads', 125),
(463, 'LV', 'Vecpiebalgas novads', 'Vecpiebalgas novads', 125),
(464, 'LV', 'Vecumnieku novads', 'Vecumnieku novads', 125),
(465, 'LV', 'LV-VE', 'Ventspils novads', 125),
(466, 'LV', 'Viesītes novads', 'Viesītes novads', 125),
(467, 'LV', 'Viļakas novads', 'Viļakas novads', 125),
(468, 'LV', 'Viļānu novads', 'Viļānu novads', 125),
(469, 'LV', 'Vārkavas novads', 'Vārkavas novads', 125),
(470, 'LV', 'Zilupes novads', 'Zilupes novads', 125),
(471, 'LV', 'Ādažu novads', 'Ādažu novads', 125),
(472, 'LV', 'Ērgļu novads', 'Ērgļu novads', 125),
(473, 'LV', 'Ķeguma novads', 'Ķeguma novads', 125),
(474, 'LV', 'Ķekavas novads', 'Ķekavas novads', 125),
(475, 'LT', 'LT-AL', 'Alytaus Apskritis', 131),
(476, 'LT', 'LT-KU', 'Kauno Apskritis', 131),
(477, 'LT', 'LT-KL', 'Klaipėdos Apskritis', 131),
(478, 'LT', 'LT-MR', 'Marijampolės Apskritis', 131),
(479, 'LT', 'LT-PN', 'Panevėžio Apskritis', 131),
(480, 'LT', 'LT-SA', 'Šiaulių Apskritis', 131),
(481, 'LT', 'LT-TA', 'Tauragės Apskritis', 131),
(482, 'LT', 'LT-TE', 'Telšių Apskritis', 131),
(483, 'LT', 'LT-UT', 'Utenos Apskritis', 131),
(484, 'LT', 'LT-VL', 'Vilniaus Apskritis', 131),
(485, 'BR', 'AC', 'Acre', 31),
(486, 'BR', 'AL', 'Alagoas', 31),
(487, 'BR', 'AP', 'Amapá', 31),
(488, 'BR', 'AM', 'Amazonas', 31),
(489, 'BR', 'BA', 'Bahia', 31),
(490, 'BR', 'CE', 'Ceará', 31),
(491, 'BR', 'ES', 'Espírito Santo', 31),
(492, 'BR', 'GO', 'Goiás', 31),
(493, 'BR', 'MA', 'Maranhão', 31),
(494, 'BR', 'MT', 'Mato Grosso', 31),
(495, 'BR', 'MS', 'Mato Grosso do Sul', 31),
(496, 'BR', 'MG', 'Minas Gerais', 31),
(497, 'BR', 'PA', 'Pará', 31),
(498, 'BR', 'PB', 'Paraíba', 31),
(499, 'BR', 'PR', 'Paraná', 31),
(500, 'BR', 'PE', 'Pernambuco', 31),
(501, 'BR', 'PI', 'Piauí', 31),
(502, 'BR', 'RJ', 'Rio de Janeiro', 31),
(503, 'BR', 'RN', 'Rio Grande do Norte', 31),
(504, 'BR', 'RS', 'Rio Grande do Sul', 31),
(505, 'BR', 'RO', 'Rondônia', 31),
(506, 'BR', 'RR', 'Roraima', 31),
(507, 'BR', 'SC', 'Santa Catarina', 31),
(508, 'BR', 'SP', 'São Paulo', 31),
(509, 'BR', 'SE', 'Sergipe', 31),
(510, 'BR', 'TO', 'Tocantins', 31),
(511, 'BR', 'DF', 'Distrito Federal', 31),
(512, 'HR', 'HR-01', 'Zagrebačka županija', 59),
(513, 'HR', 'HR-02', 'Krapinsko-zagorska županija', 59),
(514, 'HR', 'HR-03', 'Sisačko-moslavačka županija', 59),
(515, 'HR', 'HR-04', 'Karlovačka županija', 59),
(516, 'HR', 'HR-05', 'Varaždinska županija', 59),
(517, 'HR', 'HR-06', 'Koprivničko-križevačka županija', 59),
(518, 'HR', 'HR-07', 'Bjelovarsko-bilogorska županija', 59),
(519, 'HR', 'HR-08', 'Primorsko-goranska županija', 59),
(520, 'HR', 'HR-09', 'Ličko-senjska županija', 59),
(521, 'HR', 'HR-10', 'Virovitičko-podravska županija', 59),
(522, 'HR', 'HR-11', 'Požeško-slavonska županija', 59),
(523, 'HR', 'HR-12', 'Brodsko-posavska županija', 59),
(524, 'HR', 'HR-13', 'Zadarska županija', 59),
(525, 'HR', 'HR-14', 'Osječko-baranjska županija', 59),
(526, 'HR', 'HR-15', 'Šibensko-kninska županija', 59),
(527, 'HR', 'HR-16', 'Vukovarsko-srijemska županija', 59),
(528, 'HR', 'HR-17', 'Splitsko-dalmatinska županija', 59),
(529, 'HR', 'HR-18', 'Istarska županija', 59),
(530, 'HR', 'HR-19', 'Dubrovačko-neretvanska županija', 59),
(531, 'HR', 'HR-20', 'Međimurska županija', 59),
(532, 'HR', 'HR-21', 'Grad Zagreb', 59),
(533, 'IN', 'AN', 'Andaman and Nicobar Islands', 106),
(534, 'IN', 'AP', 'Andhra Pradesh', 106),
(535, 'IN', 'AR', 'Arunachal Pradesh', 106),
(536, 'IN', 'AS', 'Assam', 106),
(537, 'IN', 'BR', 'Bihar', 106),
(538, 'IN', 'CH', 'Chandigarh', 106),
(539, 'IN', 'CT', 'Chhattisgarh', 106),
(540, 'IN', 'DN', 'Dadra and Nagar Haveli', 106),
(541, 'IN', 'DD', 'Daman and Diu', 106),
(542, 'IN', 'DL', 'Delhi', 106),
(543, 'IN', 'GA', 'Goa', 106),
(544, 'IN', 'GJ', 'Gujarat', 106),
(545, 'IN', 'HR', 'Haryana', 106),
(546, 'IN', 'HP', 'Himachal Pradesh', 106),
(547, 'IN', 'JK', 'Jammu and Kashmir', 106),
(548, 'IN', 'JH', 'Jharkhand', 106),
(549, 'IN', 'KA', 'Karnataka', 106),
(550, 'IN', 'KL', 'Kerala', 106),
(551, 'IN', 'LD', 'Lakshadweep', 106),
(552, 'IN', 'MP', 'Madhya Pradesh', 106),
(553, 'IN', 'MH', 'Maharashtra', 106),
(554, 'IN', 'MN', 'Manipur', 106),
(555, 'IN', 'ML', 'Meghalaya', 106),
(556, 'IN', 'MZ', 'Mizoram', 106),
(557, 'IN', 'NL', 'Nagaland', 106),
(558, 'IN', 'OR', 'Odisha', 106),
(559, 'IN', 'PY', 'Puducherry', 106),
(560, 'IN', 'PB', 'Punjab', 106),
(561, 'IN', 'RJ', 'Rajasthan', 106),
(562, 'IN', 'SK', 'Sikkim', 106),
(563, 'IN', 'TN', 'Tamil Nadu', 106),
(564, 'IN', 'TG', 'Telangana', 106),
(565, 'IN', 'TR', 'Tripura', 106),
(566, 'IN', 'UP', 'Uttar Pradesh', 106),
(567, 'IN', 'UT', 'Uttarakhand', 106),
(568, 'IN', 'WB', 'West Bengal', 106),
(569, 'PY', 'PY-16', 'Alto Paraguay', 176),
(570, 'PY', 'PY-10', 'Alto Paraná', 176),
(571, 'PY', 'PY-13', 'Amambay', 176),
(572, 'PY', 'PY-ASU', 'Asunción', 176),
(573, 'PY', 'PY-19', 'Boquerón', 176),
(574, 'PY', 'PY-5', 'Caaguazú', 176),
(575, 'PY', 'PY-6', 'Caazapá', 176),
(576, 'PY', 'PY-14', 'Canindeyú', 176),
(577, 'PY', 'PY-11', 'Central', 176),
(578, 'PY', 'PY-1', 'Concepción', 176),
(579, 'PY', 'PY-3', 'Cordillera', 176),
(580, 'PY', 'PY-4', 'Guairá', 176),
(581, 'PY', 'PY-7', 'Itapúa', 176),
(582, 'PY', 'PY-8', 'Misiones', 176),
(583, 'PY', 'PY-9', 'Paraguarí', 176),
(584, 'PY', 'PY-15', 'Presidente Hayes', 176),
(585, 'PY', 'PY-2', 'San Pedro', 176),
(586, 'PY', 'PY-12', 'Ñeembucú', 176);

-- --------------------------------------------------------

--
-- Estrutura da tabela `country_state_translations`
--

CREATE TABLE `country_state_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `default_name` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_state_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `country_state_translations`
--

INSERT INTO `country_state_translations` (`id`, `locale`, `default_name`, `country_state_id`) VALUES
(1705, 'ar', 'ألاباما', 1),
(1706, 'ar', 'ألاسكا', 2),
(1707, 'ar', 'ساموا الأمريكية', 3),
(1708, 'ar', 'أريزونا', 4),
(1709, 'ar', 'أركنساس', 5),
(1710, 'ar', 'القوات المسلحة أفريقيا', 6),
(1711, 'ar', 'القوات المسلحة الأمريكية', 7),
(1712, 'ar', 'القوات المسلحة الكندية', 8),
(1713, 'ar', 'القوات المسلحة أوروبا', 9),
(1714, 'ar', 'القوات المسلحة الشرق الأوسط', 10),
(1715, 'ar', 'القوات المسلحة في المحيط الهادئ', 11),
(1716, 'ar', 'كاليفورنيا', 12),
(1717, 'ar', 'كولورادو', 13),
(1718, 'ar', 'كونيتيكت', 14),
(1719, 'ar', 'ديلاوير', 15),
(1720, 'ar', 'مقاطعة كولومبيا', 16),
(1721, 'ar', 'ولايات ميكرونيزيا الموحدة', 17),
(1722, 'ar', 'فلوريدا', 18),
(1723, 'ar', 'جورجيا', 19),
(1724, 'ar', 'غوام', 20),
(1725, 'ar', 'هاواي', 21),
(1726, 'ar', 'ايداهو', 22),
(1727, 'ar', 'إلينوي', 23),
(1728, 'ar', 'إنديانا', 24),
(1729, 'ar', 'أيوا', 25),
(1730, 'ar', 'كانساس', 26),
(1731, 'ar', 'كنتاكي', 27),
(1732, 'ar', 'لويزيانا', 28),
(1733, 'ar', 'مين', 29),
(1734, 'ar', 'جزر مارشال', 30),
(1735, 'ar', 'ماريلاند', 31),
(1736, 'ar', 'ماساتشوستس', 32),
(1737, 'ar', 'ميشيغان', 33),
(1738, 'ar', 'مينيسوتا', 34),
(1739, 'ar', 'ميسيسيبي', 35),
(1740, 'ar', 'ميسوري', 36),
(1741, 'ar', 'مونتانا', 37),
(1742, 'ar', 'نبراسكا', 38),
(1743, 'ar', 'نيفادا', 39),
(1744, 'ar', 'نيو هامبشاير', 40),
(1745, 'ar', 'نيو جيرسي', 41),
(1746, 'ar', 'المكسيك جديدة', 42),
(1747, 'ar', 'نيويورك', 43),
(1748, 'ar', 'شمال كارولينا', 44),
(1749, 'ar', 'شمال داكوتا', 45),
(1750, 'ar', 'جزر مريانا الشمالية', 46),
(1751, 'ar', 'أوهايو', 47),
(1752, 'ar', 'أوكلاهوما', 48),
(1753, 'ar', 'ولاية أوريغون', 49),
(1754, 'ar', 'بالاو', 50),
(1755, 'ar', 'بنسلفانيا', 51),
(1756, 'ar', 'بورتوريكو', 52),
(1757, 'ar', 'جزيرة رود', 53),
(1758, 'ar', 'كارولينا الجنوبية', 54),
(1759, 'ar', 'جنوب داكوتا', 55),
(1760, 'ar', 'تينيسي', 56),
(1761, 'ar', 'تكساس', 57),
(1762, 'ar', 'يوتا', 58),
(1763, 'ar', 'فيرمونت', 59),
(1764, 'ar', 'جزر فيرجن', 60),
(1765, 'ar', 'فرجينيا', 61),
(1766, 'ar', 'واشنطن', 62),
(1767, 'ar', 'فرجينيا الغربية', 63),
(1768, 'ar', 'ولاية ويسكونسن', 64),
(1769, 'ar', 'وايومنغ', 65),
(1770, 'ar', 'ألبرتا', 66),
(1771, 'ar', 'كولومبيا البريطانية', 67),
(1772, 'ar', 'مانيتوبا', 68),
(1773, 'ar', 'نيوفاوندلاند ولابرادور', 69),
(1774, 'ar', 'برونزيك جديد', 70),
(1775, 'ar', 'مقاطعة نفوفا سكوشيا', 71),
(1776, 'ar', 'الاقاليم الشمالية الغربية', 72),
(1777, 'ar', 'نونافوت', 73),
(1778, 'ar', 'أونتاريو', 74),
(1779, 'ar', 'جزيرة الأمير ادوارد', 75),
(1780, 'ar', 'كيبيك', 76),
(1781, 'ar', 'ساسكاتشوان', 77),
(1782, 'ar', 'إقليم يوكون', 78),
(1783, 'ar', 'Niedersachsen', 79),
(1784, 'ar', 'بادن فورتمبيرغ', 80),
(1785, 'ar', 'بايرن ميونيخ', 81),
(1786, 'ar', 'برلين', 82),
(1787, 'ar', 'براندنبورغ', 83),
(1788, 'ar', 'بريمن', 84),
(1789, 'ar', 'هامبورغ', 85),
(1790, 'ar', 'هيسن', 86),
(1791, 'ar', 'مكلنبورغ-فوربومرن', 87),
(1792, 'ar', 'نوردراين فيستفالن', 88),
(1793, 'ar', 'راينلاند-بفالز', 89),
(1794, 'ar', 'سارلاند', 90),
(1795, 'ar', 'ساكسن', 91),
(1796, 'ar', 'سكسونيا أنهالت', 92),
(1797, 'ar', 'شليسفيغ هولشتاين', 93),
(1798, 'ar', 'تورنغن', 94),
(1799, 'ar', 'فيينا', 95),
(1800, 'ar', 'النمسا السفلى', 96),
(1801, 'ar', 'النمسا العليا', 97),
(1802, 'ar', 'سالزبورغ', 98),
(1803, 'ar', 'Каринтия', 99),
(1804, 'ar', 'STEIERMARK', 100),
(1805, 'ar', 'تيرول', 101),
(1806, 'ar', 'بورغنلاند', 102),
(1807, 'ar', 'فورارلبرغ', 103),
(1808, 'ar', 'أرجاو', 104),
(1809, 'ar', 'Appenzell Innerrhoden', 105),
(1810, 'ar', 'أبنزل أوسيرهودن', 106),
(1811, 'ar', 'برن', 107),
(1812, 'ar', 'كانتون ريف بازل', 108),
(1813, 'ar', 'بازل شتات', 109),
(1814, 'ar', 'فرايبورغ', 110),
(1815, 'ar', 'Genf', 111),
(1816, 'ar', 'جلاروس', 112),
(1817, 'ar', 'غراوبوندن', 113),
(1818, 'ar', 'العصر الجوارسي أو الجوري', 114),
(1819, 'ar', 'لوزيرن', 115),
(1820, 'ar', 'في Neuenburg', 116),
(1821, 'ar', 'نيدوالدن', 117),
(1822, 'ar', 'أوبوالدن', 118),
(1823, 'ar', 'سانت غالن', 119),
(1824, 'ar', 'شافهاوزن', 120),
(1825, 'ar', 'سولوتورن', 121),
(1826, 'ar', 'شفيتس', 122),
(1827, 'ar', 'ثورجو', 123),
(1828, 'ar', 'تيتشينو', 124),
(1829, 'ar', 'أوري', 125),
(1830, 'ar', 'وادت', 126),
(1831, 'ar', 'اليس', 127),
(1832, 'ar', 'زوغ', 128),
(1833, 'ar', 'زيورخ', 129),
(1834, 'ar', 'Corunha', 130),
(1835, 'ar', 'ألافا', 131),
(1836, 'ar', 'الباسيتي', 132),
(1837, 'ar', 'اليكانتي', 133),
(1838, 'ar', 'الميريا', 134),
(1839, 'ar', 'أستورياس', 135),
(1840, 'ar', 'أفيلا', 136),
(1841, 'ar', 'بطليوس', 137),
(1842, 'ar', 'البليار', 138),
(1843, 'ar', 'برشلونة', 139),
(1844, 'ar', 'برغش', 140),
(1845, 'ar', 'كاسيريس', 141),
(1846, 'ar', 'كاديز', 142),
(1847, 'ar', 'كانتابريا', 143),
(1848, 'ar', 'كاستيلون', 144),
(1849, 'ar', 'سبتة', 145),
(1850, 'ar', 'سيوداد ريال', 146),
(1851, 'ar', 'قرطبة', 147),
(1852, 'ar', 'كوينكا', 148),
(1853, 'ar', 'جيرونا', 149),
(1854, 'ar', 'غرناطة', 150),
(1855, 'ar', 'غوادالاخارا', 151),
(1856, 'ar', 'بجويبوزكوا', 152),
(1857, 'ar', 'هويلفا', 153),
(1858, 'ar', 'هويسكا', 154),
(1859, 'ar', 'خاين', 155),
(1860, 'ar', 'لاريوخا', 156),
(1861, 'ar', 'لاس بالماس', 157),
(1862, 'ar', 'ليون', 158),
(1863, 'ar', 'يدا', 159),
(1864, 'ar', 'لوغو', 160),
(1865, 'ar', 'مدريد', 161),
(1866, 'ar', 'ملقة', 162),
(1867, 'ar', 'مليلية', 163),
(1868, 'ar', 'مورسيا', 164),
(1869, 'ar', 'نافارا', 165),
(1870, 'ar', 'أورينس', 166),
(1871, 'ar', 'بلنسية', 167),
(1872, 'ar', 'بونتيفيدرا', 168),
(1873, 'ar', 'سالامانكا', 169),
(1874, 'ar', 'سانتا كروز دي تينيريفي', 170),
(1875, 'ar', 'سيغوفيا', 171),
(1876, 'ar', 'اشبيلية', 172),
(1877, 'ar', 'سوريا', 173),
(1878, 'ar', 'تاراغونا', 174),
(1879, 'ar', 'تيرويل', 175),
(1880, 'ar', 'توليدو', 176),
(1881, 'ar', 'فالنسيا', 177),
(1882, 'ar', 'بلد الوليد', 178),
(1883, 'ar', 'فيزكايا', 179),
(1884, 'ar', 'زامورا', 180),
(1885, 'ar', 'سرقسطة', 181),
(1886, 'ar', 'عين', 182),
(1887, 'ar', 'أيسن', 183),
(1888, 'ar', 'اليي', 184),
(1889, 'ar', 'ألب البروفنس العليا', 185),
(1890, 'ar', 'أوتس ألب', 186),
(1891, 'ar', 'ألب ماريتيم', 187),
(1892, 'ar', 'ARDECHE', 188),
(1893, 'ar', 'Ardennes', 189),
(1894, 'ar', 'آردن', 190),
(1895, 'ar', 'أوب', 191),
(1896, 'ar', 'اود', 192),
(1897, 'ar', 'أفيرون', 193),
(1898, 'ar', 'بوكاس دو رون', 194),
(1899, 'ar', 'كالفادوس', 195),
(1900, 'ar', 'كانتال', 196),
(1901, 'ar', 'شارانت', 197),
(1902, 'ar', 'سيين إت مارن', 198),
(1903, 'ar', 'شير', 199),
(1904, 'ar', 'كوريز', 200),
(1905, 'ar', 'سود كورس-دو-', 201),
(1906, 'ar', 'هوت كورس', 202),
(1907, 'ar', 'كوستا دوركوريز', 203),
(1908, 'ar', 'كوتس دورمور', 204),
(1909, 'ar', 'كروز', 205),
(1910, 'ar', 'دوردوني', 206),
(1911, 'ar', 'دوبس', 207),
(1912, 'ar', 'DrômeFinistère', 208),
(1913, 'ar', 'أور', 209),
(1914, 'ar', 'أور ولوار', 210),
(1915, 'ar', 'فينيستير', 211),
(1916, 'ar', 'جارد', 212),
(1917, 'ar', 'هوت غارون', 213),
(1918, 'ar', 'الخيام', 214),
(1919, 'ar', 'جيروند', 215),
(1920, 'ar', 'هيرولت', 216),
(1921, 'ar', 'إيل وفيلان', 217),
(1922, 'ar', 'إندر', 218),
(1923, 'ar', 'أندر ولوار', 219),
(1924, 'ar', 'إيسر', 220),
(1925, 'ar', 'العصر الجوارسي أو الجوري', 221),
(1926, 'ar', 'اندز', 222),
(1927, 'ar', 'لوار وشير', 223),
(1928, 'ar', 'لوار', 224),
(1929, 'ar', 'هوت-لوار', 225),
(1930, 'ar', 'وار أتلانتيك', 226),
(1931, 'ar', 'لورا', 227),
(1932, 'ar', 'كثيرا', 228),
(1933, 'ar', 'الكثير غارون', 229),
(1934, 'ar', 'لوزر', 230),
(1935, 'ar', 'مين-إي-لوار', 231),
(1936, 'ar', 'المانش', 232),
(1937, 'ar', 'مارن', 233),
(1938, 'ar', 'هوت مارن', 234),
(1939, 'ar', 'مايين', 235),
(1940, 'ar', 'مورت وموزيل', 236),
(1941, 'ar', 'ميوز', 237),
(1942, 'ar', 'موربيهان', 238),
(1943, 'ar', 'موسيل', 239),
(1944, 'ar', 'نيفر', 240),
(1945, 'ar', 'نورد', 241),
(1946, 'ar', 'إيل دو فرانس', 242),
(1947, 'ar', 'أورن', 243),
(1948, 'ar', 'با-دو-كاليه', 244),
(1949, 'ar', 'بوي دي دوم', 245),
(1950, 'ar', 'البرانيس ​​الأطلسية', 246),
(1951, 'ar', 'أوتس-بيرينيهs', 247),
(1952, 'ar', 'بيرينيه-أورينتال', 248),
(1953, 'ar', 'بس رين', 249),
(1954, 'ar', 'أوت رين', 250),
(1955, 'ar', 'رون [3]', 251),
(1956, 'ar', 'هوت-سون', 252),
(1957, 'ar', 'سون ولوار', 253),
(1958, 'ar', 'سارت', 254),
(1959, 'ar', 'سافوا', 255),
(1960, 'ar', 'هاوت سافوي', 256),
(1961, 'ar', 'باريس', 257),
(1962, 'ar', 'سين البحرية', 258),
(1963, 'ar', 'سيين إت مارن', 259),
(1964, 'ar', 'إيفلين', 260),
(1965, 'ar', 'دوكس سفرس', 261),
(1966, 'ar', 'السوم', 262),
(1967, 'ar', 'تارن', 263),
(1968, 'ar', 'تارن وغارون', 264),
(1969, 'ar', 'فار', 265),
(1970, 'ar', 'فوكلوز', 266),
(1971, 'ar', 'تارن', 267),
(1972, 'ar', 'فيين', 268),
(1973, 'ar', 'هوت فيين', 269),
(1974, 'ar', 'الفوج', 270),
(1975, 'ar', 'يون', 271),
(1976, 'ar', 'تيريتوير-دي-بلفور', 272),
(1977, 'ar', 'إيسون', 273),
(1978, 'ar', 'هوت دو سين', 274),
(1979, 'ar', 'سين سان دوني', 275),
(1980, 'ar', 'فال دو مارن', 276),
(1981, 'ar', 'فال دواز', 277),
(1982, 'ar', 'ألبا', 278),
(1983, 'ar', 'اراد', 279),
(1984, 'ar', 'ARGES', 280),
(1985, 'ar', 'باكاو', 281),
(1986, 'ar', 'بيهور', 282),
(1987, 'ar', 'بيستريتا ناسود', 283),
(1988, 'ar', 'بوتوساني', 284),
(1989, 'ar', 'براشوف', 285),
(1990, 'ar', 'برايلا', 286),
(1991, 'ar', 'بوخارست', 287),
(1992, 'ar', 'بوزاو', 288),
(1993, 'ar', 'كاراس سيفيرين', 289),
(1994, 'ar', 'كالاراسي', 290),
(1995, 'ar', 'كلوج', 291),
(1996, 'ar', 'كونستانتا', 292),
(1997, 'ar', 'كوفاسنا', 293),
(1998, 'ar', 'دامبوفيتا', 294),
(1999, 'ar', 'دولج', 295),
(2000, 'ar', 'جالاتي', 296),
(2001, 'ar', 'Giurgiu', 297),
(2002, 'ar', 'غيورغيو', 298),
(2003, 'ar', 'هارغيتا', 299),
(2004, 'ar', 'هونيدوارا', 300),
(2005, 'ar', 'ايالوميتا', 301),
(2006, 'ar', 'ياشي', 302),
(2007, 'ar', 'إيلفوف', 303),
(2008, 'ar', 'مارامريس', 304),
(2009, 'ar', 'MEHEDINTI', 305),
(2010, 'ar', 'موريس', 306),
(2011, 'ar', 'نيامتس', 307),
(2012, 'ar', 'أولت', 308),
(2013, 'ar', 'براهوفا', 309),
(2014, 'ar', 'ساتو ماري', 310),
(2015, 'ar', 'سالاج', 311),
(2016, 'ar', 'سيبيو', 312),
(2017, 'ar', 'سوسيفا', 313),
(2018, 'ar', 'تيليورمان', 314),
(2019, 'ar', 'تيم هو', 315),
(2020, 'ar', 'تولسيا', 316),
(2021, 'ar', 'فاسلوي', 317),
(2022, 'ar', 'فالسيا', 318),
(2023, 'ar', 'فرانتشا', 319),
(2024, 'ar', 'Lappi', 320),
(2025, 'ar', 'Pohjois-Pohjanmaa', 321),
(2026, 'ar', 'كاينو', 322),
(2027, 'ar', 'Pohjois-كارجالا', 323),
(2028, 'ar', 'Pohjois-سافو', 324),
(2029, 'ar', 'Etelä-سافو', 325),
(2030, 'ar', 'Etelä-Pohjanmaa', 326),
(2031, 'ar', 'Pohjanmaa', 327),
(2032, 'ar', 'بيركنما', 328),
(2033, 'ar', 'ساتا كونتا', 329),
(2034, 'ar', 'كسكي-Pohjanmaa', 330),
(2035, 'ar', 'كسكي-سومي', 331),
(2036, 'ar', 'Varsinais-سومي', 332),
(2037, 'ar', 'Etelä-كارجالا', 333),
(2038, 'ar', 'Päijät-Häme', 334),
(2039, 'ar', 'كانتا-HAME', 335),
(2040, 'ar', 'أوسيما', 336),
(2041, 'ar', 'أوسيما', 337),
(2042, 'ar', 'كومنلاكسو', 338),
(2043, 'ar', 'Ahvenanmaa', 339),
(2044, 'ar', 'Harjumaa', 340),
(2045, 'ar', 'هيوما', 341),
(2046, 'ar', 'المؤسسة الدولية للتنمية فيروما', 342),
(2047, 'ar', 'جوغفما', 343),
(2048, 'ar', 'يارفا', 344),
(2049, 'ar', 'انيما', 345),
(2050, 'ar', 'اني فيريوما', 346),
(2051, 'ar', 'بولفاما', 347),
(2052, 'ar', 'بارنوما', 348),
(2053, 'ar', 'Raplamaa', 349),
(2054, 'ar', 'Saaremaa', 350),
(2055, 'ar', 'Tartumaa', 351),
(2056, 'ar', 'Valgamaa', 352),
(2057, 'ar', 'Viljandimaa', 353),
(2058, 'ar', 'روايات Salacgr novvas', 354),
(2059, 'ar', 'داوجافبيلس', 355),
(2060, 'ar', 'يلغافا', 356),
(2061, 'ar', 'يكاب', 357),
(2062, 'ar', 'يورمال', 358),
(2063, 'ar', 'يابايا', 359),
(2064, 'ar', 'ليباج أبريس', 360),
(2065, 'ar', 'ريزكن', 361),
(2066, 'ar', 'ريغا', 362),
(2067, 'ar', 'مقاطعة ريغا', 363),
(2068, 'ar', 'فالميرا', 364),
(2069, 'ar', 'فنتسبيلز', 365),
(2070, 'ar', 'روايات Aglonas', 366),
(2071, 'ar', 'Aizkraukles novads', 367),
(2072, 'ar', 'Aizkraukles novads', 368),
(2073, 'ar', 'Aknīstes novads', 369),
(2074, 'ar', 'Alojas novads', 370),
(2075, 'ar', 'روايات Alsungas', 371),
(2076, 'ar', 'ألكسنس أبريز', 372),
(2077, 'ar', 'روايات أماتاس', 373),
(2078, 'ar', 'قرود الروايات', 374),
(2079, 'ar', 'روايات أوسيس', 375),
(2080, 'ar', 'بابيت الروايات', 376),
(2081, 'ar', 'Baldones الروايات', 377),
(2082, 'ar', 'بالتينافاس الروايات', 378),
(2083, 'ar', 'روايات بالفو', 379),
(2084, 'ar', 'Bauskas الروايات', 380),
(2085, 'ar', 'Beverīnas novads', 381),
(2086, 'ar', 'Novads Brocēnu', 382),
(2087, 'ar', 'Novads Burtnieku', 383),
(2088, 'ar', 'Carnikavas novads', 384),
(2089, 'ar', 'Cesvaines novads', 385),
(2090, 'ar', 'Ciblas novads', 386),
(2091, 'ar', 'تسو أبريس', 387),
(2092, 'ar', 'Dagdas novads', 388),
(2093, 'ar', 'Daugavpils novads', 389),
(2094, 'ar', 'روايات دوبيليس', 390),
(2095, 'ar', 'ديربيس الروايات', 391),
(2096, 'ar', 'ديربيس الروايات', 392),
(2097, 'ar', 'يشرك الروايات', 393),
(2098, 'ar', 'Garkalnes novads', 394),
(2099, 'ar', 'Grobiņas novads', 395),
(2100, 'ar', 'غولبينيس الروايات', 396),
(2101, 'ar', 'إيكافاس روايات', 397),
(2102, 'ar', 'Ikškiles novads', 398),
(2103, 'ar', 'Ilūkstes novads', 399),
(2104, 'ar', 'روايات Inčukalna', 400),
(2105, 'ar', 'Jaunjelgavas novads', 401),
(2106, 'ar', 'Jaunpiebalgas novads', 402),
(2107, 'ar', 'روايات Jaunpiebalgas', 403),
(2108, 'ar', 'Jelgavas novads', 404),
(2109, 'ar', 'جيكابيلس أبريز', 405),
(2110, 'ar', 'روايات كاندافاس', 406),
(2111, 'ar', 'Kokneses الروايات', 407),
(2112, 'ar', 'Krimuldas novads', 408),
(2113, 'ar', 'Krustpils الروايات', 409),
(2114, 'ar', 'Krāslavas Apriņķis', 410),
(2115, 'ar', 'كولدوغاس أبريز', 411),
(2116, 'ar', 'Kārsavas novads', 412),
(2117, 'ar', 'روايات ييلفاريس', 413),
(2118, 'ar', 'ليمباو أبريز', 414),
(2119, 'ar', 'روايات لباناس', 415),
(2120, 'ar', 'روايات لودزاس', 416),
(2121, 'ar', 'مقاطعة ليجاتني', 417),
(2122, 'ar', 'مقاطعة ليفاني', 418),
(2123, 'ar', 'مادونا روايات', 419),
(2124, 'ar', 'Mazsalacas novads', 420),
(2125, 'ar', 'روايات مالبلز', 421),
(2126, 'ar', 'Mārupes novads', 422),
(2127, 'ar', 'نوفاو نوكشنو', 423),
(2128, 'ar', 'روايات نيريتاس', 424),
(2129, 'ar', 'روايات نيكاس', 425),
(2130, 'ar', 'أغنام الروايات', 426),
(2131, 'ar', 'أولينيس الروايات', 427),
(2132, 'ar', 'روايات Ozolnieku', 428),
(2133, 'ar', 'بريسيو أبرييس', 429),
(2134, 'ar', 'Priekules الروايات', 430),
(2135, 'ar', 'كوندادو دي بريكوي', 431),
(2136, 'ar', 'Pärgaujas novads', 432),
(2137, 'ar', 'روايات بافيلوستاس', 433),
(2138, 'ar', 'بلافيناس مقاطعة', 434),
(2139, 'ar', 'روناس روايات', 435),
(2140, 'ar', 'Riebiņu novads', 436),
(2141, 'ar', 'روجاس روايات', 437),
(2142, 'ar', 'Novads روباو', 438),
(2143, 'ar', 'روكافاس روايات', 439),
(2144, 'ar', 'روغاجو روايات', 440),
(2145, 'ar', 'رندلس الروايات', 441),
(2146, 'ar', 'Radzeknes novads', 442),
(2147, 'ar', 'Rūjienas novads', 443),
(2148, 'ar', 'بلدية سالاسغريفا', 444),
(2149, 'ar', 'روايات سالاس', 445),
(2150, 'ar', 'Salaspils novads', 446),
(2151, 'ar', 'روايات سالدوس', 447),
(2152, 'ar', 'Novuls Saulkrastu', 448),
(2153, 'ar', 'سيغولداس روايات', 449),
(2154, 'ar', 'Skrundas novads', 450),
(2155, 'ar', 'مقاطعة Skrīveri', 451),
(2156, 'ar', 'يبتسم الروايات', 452),
(2157, 'ar', 'روايات Stopiņu', 453),
(2158, 'ar', 'روايات Stren novu', 454),
(2159, 'ar', 'سجاس روايات', 455),
(2160, 'ar', 'روايات تالسو', 456),
(2161, 'ar', 'توكوما الروايات', 457),
(2162, 'ar', 'Tērvetes novads', 458),
(2163, 'ar', 'Vaiņodes novads', 459),
(2164, 'ar', 'فالكاس الروايات', 460),
(2165, 'ar', 'فالميراس الروايات', 461),
(2166, 'ar', 'مقاطعة فاكلاني', 462),
(2167, 'ar', 'Vecpiebalgas novads', 463),
(2168, 'ar', 'روايات Vecumnieku', 464),
(2169, 'ar', 'فنتسبيلس الروايات', 465),
(2170, 'ar', 'Viesītes Novads', 466),
(2171, 'ar', 'Viļakas novads', 467),
(2172, 'ar', 'روايات فيناو', 468),
(2173, 'ar', 'Vārkavas novads', 469),
(2174, 'ar', 'روايات زيلوبس', 470),
(2175, 'ar', 'مقاطعة أدازي', 471),
(2176, 'ar', 'مقاطعة Erglu', 472),
(2177, 'ar', 'مقاطعة كيغمس', 473),
(2178, 'ar', 'مقاطعة كيكافا', 474),
(2179, 'ar', 'Alytaus Apskritis', 475),
(2180, 'ar', 'كاونو ابكريتيس', 476),
(2181, 'ar', 'Klaipėdos apskritis', 477),
(2182, 'ar', 'Marijampol\'s apskritis', 478),
(2183, 'ar', 'Panevėžio apskritis', 479),
(2184, 'ar', 'uliaulių apskritis', 480),
(2185, 'ar', 'Taurag\'s apskritis', 481),
(2186, 'ar', 'Telšių apskritis', 482),
(2187, 'ar', 'Utenos apskritis', 483),
(2188, 'ar', 'فيلنياوس ابكريتيس', 484),
(2189, 'ar', 'فدان', 485),
(2190, 'ar', 'ألاغواس', 486),
(2191, 'ar', 'أمابا', 487),
(2192, 'ar', 'أمازوناس', 488),
(2193, 'ar', 'باهيا', 489),
(2194, 'ar', 'سيارا', 490),
(2195, 'ar', 'إسبيريتو سانتو', 491),
(2196, 'ar', 'غوياس', 492),
(2197, 'ar', 'مارانهاو', 493),
(2198, 'ar', 'ماتو جروسو', 494),
(2199, 'ar', 'ماتو جروسو دو سول', 495),
(2200, 'ar', 'ميناس جريس', 496),
(2201, 'ar', 'بارا', 497),
(2202, 'ar', 'بارايبا', 498),
(2203, 'ar', 'بارانا', 499),
(2204, 'ar', 'بيرنامبوكو', 500),
(2205, 'ar', 'بياوي', 501),
(2206, 'ar', 'ريو دي جانيرو', 502),
(2207, 'ar', 'ريو غراندي دو نورتي', 503),
(2208, 'ar', 'ريو غراندي دو سول', 504),
(2209, 'ar', 'روندونيا', 505),
(2210, 'ar', 'رورايما', 506),
(2211, 'ar', 'سانتا كاتارينا', 507),
(2212, 'ar', 'ساو باولو', 508),
(2213, 'ar', 'سيرغيبي', 509),
(2214, 'ar', 'توكانتينز', 510),
(2215, 'ar', 'وفي مقاطعة الاتحادية', 511),
(2216, 'ar', 'Zagrebačka زوبانيا', 512),
(2217, 'ar', 'Krapinsko-zagorska زوبانيا', 513),
(2218, 'ar', 'Sisačko-moslavačka زوبانيا', 514),
(2219, 'ar', 'كارلوفيتش شوبانيا', 515),
(2220, 'ar', 'فارادينسكا زوبانيجا', 516),
(2221, 'ar', 'Koprivničko-križevačka زوبانيجا', 517),
(2222, 'ar', 'بيلوفارسكو-بيلوجورسكا', 518),
(2223, 'ar', 'بريمورسكو غورانسكا سوبانيا', 519),
(2224, 'ar', 'ليكو سينيسكا زوبانيا', 520),
(2225, 'ar', 'Virovitičko-podravska زوبانيا', 521),
(2226, 'ar', 'Požeško-slavonska županija', 522),
(2227, 'ar', 'Brodsko-posavska županija', 523),
(2228, 'ar', 'زادارسكا زوبانيجا', 524),
(2229, 'ar', 'Osječko-baranjska županija', 525),
(2230, 'ar', 'شيبنسكو-كنينسكا سوبانيا', 526),
(2231, 'ar', 'Virovitičko-podravska زوبانيا', 527),
(2232, 'ar', 'Splitsko-dalmatinska زوبانيا', 528),
(2233, 'ar', 'Istarska زوبانيا', 529),
(2234, 'ar', 'Dubrovačko-neretvanska زوبانيا', 530),
(2235, 'ar', 'Međimurska زوبانيا', 531),
(2236, 'ar', 'غراد زغرب', 532),
(2237, 'ar', 'جزر أندامان ونيكوبار', 533),
(2238, 'ar', 'ولاية اندرا براديش', 534),
(2239, 'ar', 'اروناتشال براديش', 535),
(2240, 'ar', 'أسام', 536),
(2241, 'ar', 'بيهار', 537),
(2242, 'ar', 'شانديغار', 538),
(2243, 'ar', 'تشهاتيسجاره', 539),
(2244, 'ar', 'دادرا ونجار هافيلي', 540),
(2245, 'ar', 'دامان وديو', 541),
(2246, 'ar', 'دلهي', 542),
(2247, 'ar', 'غوا', 543),
(2248, 'ar', 'غوجارات', 544),
(2249, 'ar', 'هاريانا', 545),
(2250, 'ar', 'هيماشال براديش', 546),
(2251, 'ar', 'جامو وكشمير', 547),
(2252, 'ar', 'جهارخاند', 548),
(2253, 'ar', 'كارناتاكا', 549),
(2254, 'ar', 'ولاية كيرالا', 550),
(2255, 'ar', 'اكشادويب', 551),
(2256, 'ar', 'ماديا براديش', 552),
(2257, 'ar', 'ماهاراشترا', 553),
(2258, 'ar', 'مانيبور', 554),
(2259, 'ar', 'ميغالايا', 555),
(2260, 'ar', 'ميزورام', 556),
(2261, 'ar', 'ناجالاند', 557),
(2262, 'ar', 'أوديشا', 558),
(2263, 'ar', 'بودوتشيري', 559),
(2264, 'ar', 'البنجاب', 560),
(2265, 'ar', 'راجستان', 561),
(2266, 'ar', 'سيكيم', 562),
(2267, 'ar', 'تاميل نادو', 563),
(2268, 'ar', 'تيلانجانا', 564),
(2269, 'ar', 'تريبورا', 565),
(2270, 'ar', 'ولاية اوتار براديش', 566),
(2271, 'ar', 'أوتارانتشال', 567),
(2272, 'ar', 'البنغال الغربية', 568),
(2273, 'es', 'Alabama', 1),
(2274, 'es', 'Alaska', 2),
(2275, 'es', 'American Samoa', 3),
(2276, 'es', 'Arizona', 4),
(2277, 'es', 'Arkansas', 5),
(2278, 'es', 'Armed Forces Africa', 6),
(2279, 'es', 'Armed Forces Americas', 7),
(2280, 'es', 'Armed Forces Canada', 8),
(2281, 'es', 'Armed Forces Europe', 9),
(2282, 'es', 'Armed Forces Middle East', 10),
(2283, 'es', 'Armed Forces Pacific', 11),
(2284, 'es', 'California', 12),
(2285, 'es', 'Colorado', 13),
(2286, 'es', 'Connecticut', 14),
(2287, 'es', 'Delaware', 15),
(2288, 'es', 'District of Columbia', 16),
(2289, 'es', 'Federated States Of Micronesia', 17),
(2290, 'es', 'Florida', 18),
(2291, 'es', 'Georgia', 19),
(2292, 'es', 'Guam', 20),
(2293, 'es', 'Hawaii', 21),
(2294, 'es', 'Idaho', 22),
(2295, 'es', 'Illinois', 23),
(2296, 'es', 'Indiana', 24),
(2297, 'es', 'Iowa', 25),
(2298, 'es', 'Kansas', 26),
(2299, 'es', 'Kentucky', 27),
(2300, 'es', 'Louisiana', 28),
(2301, 'es', 'Maine', 29),
(2302, 'es', 'Marshall Islands', 30),
(2303, 'es', 'Maryland', 31),
(2304, 'es', 'Massachusetts', 32),
(2305, 'es', 'Michigan', 33),
(2306, 'es', 'Minnesota', 34),
(2307, 'es', 'Mississippi', 35),
(2308, 'es', 'Missouri', 36),
(2309, 'es', 'Montana', 37),
(2310, 'es', 'Nebraska', 38),
(2311, 'es', 'Nevada', 39),
(2312, 'es', 'New Hampshire', 40),
(2313, 'es', 'New Jersey', 41),
(2314, 'es', 'New Mexico', 42),
(2315, 'es', 'New York', 43),
(2316, 'es', 'North Carolina', 44),
(2317, 'es', 'North Dakota', 45),
(2318, 'es', 'Northern Mariana Islands', 46),
(2319, 'es', 'Ohio', 47),
(2320, 'es', 'Oklahoma', 48),
(2321, 'es', 'Oregon', 49),
(2322, 'es', 'Palau', 50),
(2323, 'es', 'Pennsylvania', 51),
(2324, 'es', 'Puerto Rico', 52),
(2325, 'es', 'Rhode Island', 53),
(2326, 'es', 'South Carolina', 54),
(2327, 'es', 'South Dakota', 55),
(2328, 'es', 'Tennessee', 56),
(2329, 'es', 'Texas', 57),
(2330, 'es', 'Utah', 58),
(2331, 'es', 'Vermont', 59),
(2332, 'es', 'Virgin Islands', 60),
(2333, 'es', 'Virginia', 61),
(2334, 'es', 'Washington', 62),
(2335, 'es', 'West Virginia', 63),
(2336, 'es', 'Wisconsin', 64),
(2337, 'es', 'Wyoming', 65),
(2338, 'es', 'Alberta', 66),
(2339, 'es', 'British Columbia', 67),
(2340, 'es', 'Manitoba', 68),
(2341, 'es', 'Newfoundland and Labrador', 69),
(2342, 'es', 'New Brunswick', 70),
(2343, 'es', 'Nova Scotia', 71),
(2344, 'es', 'Northwest Territories', 72),
(2345, 'es', 'Nunavut', 73),
(2346, 'es', 'Ontario', 74),
(2347, 'es', 'Prince Edward Island', 75),
(2348, 'es', 'Quebec', 76),
(2349, 'es', 'Saskatchewan', 77),
(2350, 'es', 'Yukon Territory', 78),
(2351, 'es', 'Niedersachsen', 79),
(2352, 'es', 'Baden-Württemberg', 80),
(2353, 'es', 'Bayern', 81),
(2354, 'es', 'Berlin', 82),
(2355, 'es', 'Brandenburg', 83),
(2356, 'es', 'Bremen', 84),
(2357, 'es', 'Hamburg', 85),
(2358, 'es', 'Hessen', 86),
(2359, 'es', 'Mecklenburg-Vorpommern', 87),
(2360, 'es', 'Nordrhein-Westfalen', 88),
(2361, 'es', 'Rheinland-Pfalz', 89),
(2362, 'es', 'Saarland', 90),
(2363, 'es', 'Sachsen', 91),
(2364, 'es', 'Sachsen-Anhalt', 92),
(2365, 'es', 'Schleswig-Holstein', 93),
(2366, 'es', 'Thüringen', 94),
(2367, 'es', 'Wien', 95),
(2368, 'es', 'Niederösterreich', 96),
(2369, 'es', 'Oberösterreich', 97),
(2370, 'es', 'Salzburg', 98),
(2371, 'es', 'Kärnten', 99),
(2372, 'es', 'Steiermark', 100),
(2373, 'es', 'Tirol', 101),
(2374, 'es', 'Burgenland', 102),
(2375, 'es', 'Vorarlberg', 103),
(2376, 'es', 'Aargau', 104),
(2377, 'es', 'Appenzell Innerrhoden', 105),
(2378, 'es', 'Appenzell Ausserrhoden', 106),
(2379, 'es', 'Bern', 107),
(2380, 'es', 'Basel-Landschaft', 108),
(2381, 'es', 'Basel-Stadt', 109),
(2382, 'es', 'Freiburg', 110),
(2383, 'es', 'Genf', 111),
(2384, 'es', 'Glarus', 112),
(2385, 'es', 'Graubünden', 113),
(2386, 'es', 'Jura', 114),
(2387, 'es', 'Luzern', 115),
(2388, 'es', 'Neuenburg', 116),
(2389, 'es', 'Nidwalden', 117),
(2390, 'es', 'Obwalden', 118),
(2391, 'es', 'St. Gallen', 119),
(2392, 'es', 'Schaffhausen', 120),
(2393, 'es', 'Solothurn', 121),
(2394, 'es', 'Schwyz', 122),
(2395, 'es', 'Thurgau', 123),
(2396, 'es', 'Tessin', 124),
(2397, 'es', 'Uri', 125),
(2398, 'es', 'Waadt', 126),
(2399, 'es', 'Wallis', 127),
(2400, 'es', 'Zug', 128),
(2401, 'es', 'Zürich', 129),
(2402, 'es', 'La Coruña', 130),
(2403, 'es', 'Álava', 131),
(2404, 'es', 'Albacete', 132),
(2405, 'es', 'Alicante', 133),
(2406, 'es', 'Almería', 134),
(2407, 'es', 'Asturias', 135),
(2408, 'es', 'Ávila', 136),
(2409, 'es', 'Badajoz', 137),
(2410, 'es', 'Baleares', 138),
(2411, 'es', 'Barcelona', 139),
(2412, 'es', 'Burgos', 140),
(2413, 'es', 'Cáceres', 141),
(2414, 'es', 'Cádiz', 142),
(2415, 'es', 'Cantabria', 143),
(2416, 'es', 'Castellón', 144),
(2417, 'es', 'Ceuta', 145),
(2418, 'es', 'Ciudad Real', 146),
(2419, 'es', 'Córdoba', 147),
(2420, 'es', 'Cuenca', 148),
(2421, 'es', 'Gerona', 149),
(2422, 'es', 'Granada', 150),
(2423, 'es', 'Guadalajara', 151),
(2424, 'es', 'Guipúzcoa', 152),
(2425, 'es', 'Huelva', 153),
(2426, 'es', 'Huesca', 154),
(2427, 'es', 'Jaén', 155),
(2428, 'es', 'La Rioja', 156),
(2429, 'es', 'Las Palmas', 157),
(2430, 'es', 'León', 158),
(2431, 'es', 'Lérida', 159),
(2432, 'es', 'Lugo', 160),
(2433, 'es', 'Madrid', 161),
(2434, 'es', 'Málaga', 162),
(2435, 'es', 'Melilla', 163),
(2436, 'es', 'Murcia', 164),
(2437, 'es', 'Navarra', 165),
(2438, 'es', 'Orense', 166),
(2439, 'es', 'Palencia', 167),
(2440, 'es', 'Pontevedra', 168),
(2441, 'es', 'Salamanca', 169),
(2442, 'es', 'Santa Cruz de Tenerife', 170),
(2443, 'es', 'Segovia', 171),
(2444, 'es', 'Sevilla', 172),
(2445, 'es', 'Soria', 173),
(2446, 'es', 'Tarragona', 174),
(2447, 'es', 'Teruel', 175),
(2448, 'es', 'Toledo', 176),
(2449, 'es', 'Valencia', 177),
(2450, 'es', 'Valladolid', 178),
(2451, 'es', 'Vizcaya', 179),
(2452, 'es', 'Zamora', 180),
(2453, 'es', 'Zaragoza', 181),
(2454, 'es', 'Ain', 182),
(2455, 'es', 'Aisne', 183),
(2456, 'es', 'Allier', 184),
(2457, 'es', 'Alpes-de-Haute-Provence', 185),
(2458, 'es', 'Hautes-Alpes', 186),
(2459, 'es', 'Alpes-Maritimes', 187),
(2460, 'es', 'Ardèche', 188),
(2461, 'es', 'Ardennes', 189),
(2462, 'es', 'Ariège', 190),
(2463, 'es', 'Aube', 191),
(2464, 'es', 'Aude', 192),
(2465, 'es', 'Aveyron', 193),
(2466, 'es', 'Bouches-du-Rhône', 194),
(2467, 'es', 'Calvados', 195),
(2468, 'es', 'Cantal', 196),
(2469, 'es', 'Charente', 197),
(2470, 'es', 'Charente-Maritime', 198),
(2471, 'es', 'Cher', 199),
(2472, 'es', 'Corrèze', 200),
(2473, 'es', 'Corse-du-Sud', 201),
(2474, 'es', 'Haute-Corse', 202),
(2475, 'es', 'Côte-d\'Or', 203),
(2476, 'es', 'Côtes-d\'Armor', 204),
(2477, 'es', 'Creuse', 205),
(2478, 'es', 'Dordogne', 206),
(2479, 'es', 'Doubs', 207),
(2480, 'es', 'Drôme', 208),
(2481, 'es', 'Eure', 209),
(2482, 'es', 'Eure-et-Loir', 210),
(2483, 'es', 'Finistère', 211),
(2484, 'es', 'Gard', 212),
(2485, 'es', 'Haute-Garonne', 213),
(2486, 'es', 'Gers', 214),
(2487, 'es', 'Gironde', 215),
(2488, 'es', 'Hérault', 216),
(2489, 'es', 'Ille-et-Vilaine', 217),
(2490, 'es', 'Indre', 218),
(2491, 'es', 'Indre-et-Loire', 219),
(2492, 'es', 'Isère', 220),
(2493, 'es', 'Jura', 221),
(2494, 'es', 'Landes', 222),
(2495, 'es', 'Loir-et-Cher', 223),
(2496, 'es', 'Loire', 224),
(2497, 'es', 'Haute-Loire', 225),
(2498, 'es', 'Loire-Atlantique', 226),
(2499, 'es', 'Loiret', 227),
(2500, 'es', 'Lot', 228),
(2501, 'es', 'Lot-et-Garonne', 229),
(2502, 'es', 'Lozère', 230),
(2503, 'es', 'Maine-et-Loire', 231),
(2504, 'es', 'Manche', 232),
(2505, 'es', 'Marne', 233),
(2506, 'es', 'Haute-Marne', 234),
(2507, 'es', 'Mayenne', 235),
(2508, 'es', 'Meurthe-et-Moselle', 236),
(2509, 'es', 'Meuse', 237),
(2510, 'es', 'Morbihan', 238),
(2511, 'es', 'Moselle', 239),
(2512, 'es', 'Nièvre', 240),
(2513, 'es', 'Nord', 241),
(2514, 'es', 'Oise', 242),
(2515, 'es', 'Orne', 243),
(2516, 'es', 'Pas-de-Calais', 244),
(2517, 'es', 'Puy-de-Dôme', 245),
(2518, 'es', 'Pyrénées-Atlantiques', 246),
(2519, 'es', 'Hautes-Pyrénées', 247),
(2520, 'es', 'Pyrénées-Orientales', 248),
(2521, 'es', 'Bas-Rhin', 249),
(2522, 'es', 'Haut-Rhin', 250),
(2523, 'es', 'Rhône', 251),
(2524, 'es', 'Haute-Saône', 252),
(2525, 'es', 'Saône-et-Loire', 253),
(2526, 'es', 'Sarthe', 254),
(2527, 'es', 'Savoie', 255),
(2528, 'es', 'Haute-Savoie', 256),
(2529, 'es', 'Paris', 257),
(2530, 'es', 'Seine-Maritime', 258),
(2531, 'es', 'Seine-et-Marne', 259),
(2532, 'es', 'Yvelines', 260),
(2533, 'es', 'Deux-Sèvres', 261),
(2534, 'es', 'Somme', 262),
(2535, 'es', 'Tarn', 263),
(2536, 'es', 'Tarn-et-Garonne', 264),
(2537, 'es', 'Var', 265),
(2538, 'es', 'Vaucluse', 266),
(2539, 'es', 'Vendée', 267),
(2540, 'es', 'Vienne', 268),
(2541, 'es', 'Haute-Vienne', 269),
(2542, 'es', 'Vosges', 270),
(2543, 'es', 'Yonne', 271),
(2544, 'es', 'Territoire-de-Belfort', 272),
(2545, 'es', 'Essonne', 273),
(2546, 'es', 'Hauts-de-Seine', 274),
(2547, 'es', 'Seine-Saint-Denis', 275),
(2548, 'es', 'Val-de-Marne', 276),
(2549, 'es', 'Val-d\'Oise', 277),
(2550, 'es', 'Alba', 278),
(2551, 'es', 'Arad', 279),
(2552, 'es', 'Argeş', 280),
(2553, 'es', 'Bacău', 281),
(2554, 'es', 'Bihor', 282),
(2555, 'es', 'Bistriţa-Năsăud', 283),
(2556, 'es', 'Botoşani', 284),
(2557, 'es', 'Braşov', 285),
(2558, 'es', 'Brăila', 286),
(2559, 'es', 'Bucureşti', 287),
(2560, 'es', 'Buzău', 288),
(2561, 'es', 'Caraş-Severin', 289),
(2562, 'es', 'Călăraşi', 290),
(2563, 'es', 'Cluj', 291),
(2564, 'es', 'Constanţa', 292),
(2565, 'es', 'Covasna', 293),
(2566, 'es', 'Dâmboviţa', 294),
(2567, 'es', 'Dolj', 295),
(2568, 'es', 'Galaţi', 296),
(2569, 'es', 'Giurgiu', 297),
(2570, 'es', 'Gorj', 298),
(2571, 'es', 'Harghita', 299),
(2572, 'es', 'Hunedoara', 300),
(2573, 'es', 'Ialomiţa', 301),
(2574, 'es', 'Iaşi', 302),
(2575, 'es', 'Ilfov', 303),
(2576, 'es', 'Maramureş', 304),
(2577, 'es', 'Mehedinţi', 305),
(2578, 'es', 'Mureş', 306),
(2579, 'es', 'Neamţ', 307),
(2580, 'es', 'Olt', 308),
(2581, 'es', 'Prahova', 309),
(2582, 'es', 'Satu-Mare', 310),
(2583, 'es', 'Sălaj', 311),
(2584, 'es', 'Sibiu', 312),
(2585, 'es', 'Suceava', 313),
(2586, 'es', 'Teleorman', 314),
(2587, 'es', 'Timiş', 315),
(2588, 'es', 'Tulcea', 316),
(2589, 'es', 'Vaslui', 317),
(2590, 'es', 'Vâlcea', 318),
(2591, 'es', 'Vrancea', 319),
(2592, 'es', 'Lappi', 320),
(2593, 'es', 'Pohjois-Pohjanmaa', 321),
(2594, 'es', 'Kainuu', 322),
(2595, 'es', 'Pohjois-Karjala', 323),
(2596, 'es', 'Pohjois-Savo', 324),
(2597, 'es', 'Etelä-Savo', 325),
(2598, 'es', 'Etelä-Pohjanmaa', 326),
(2599, 'es', 'Pohjanmaa', 327),
(2600, 'es', 'Pirkanmaa', 328),
(2601, 'es', 'Satakunta', 329),
(2602, 'es', 'Keski-Pohjanmaa', 330),
(2603, 'es', 'Keski-Suomi', 331),
(2604, 'es', 'Varsinais-Suomi', 332),
(2605, 'es', 'Etelä-Karjala', 333),
(2606, 'es', 'Päijät-Häme', 334),
(2607, 'es', 'Kanta-Häme', 335),
(2608, 'es', 'Uusimaa', 336),
(2609, 'es', 'Itä-Uusimaa', 337),
(2610, 'es', 'Kymenlaakso', 338),
(2611, 'es', 'Ahvenanmaa', 339),
(2612, 'es', 'Harjumaa', 340),
(2613, 'es', 'Hiiumaa', 341),
(2614, 'es', 'country_state_ida-Virumaa', 342),
(2615, 'es', 'Jõgevamaa', 343),
(2616, 'es', 'Järvamaa', 344),
(2617, 'es', 'Läänemaa', 345),
(2618, 'es', 'Lääne-Virumaa', 346),
(2619, 'es', 'Põlvamaa', 347),
(2620, 'es', 'Pärnumaa', 348),
(2621, 'es', 'Raplamaa', 349),
(2622, 'es', 'Saaremaa', 350),
(2623, 'es', 'Tartumaa', 351),
(2624, 'es', 'Valgamaa', 352),
(2625, 'es', 'Viljandimaa', 353),
(2626, 'es', 'Võrumaa', 354),
(2627, 'es', 'Daugavpils', 355),
(2628, 'es', 'Jelgava', 356),
(2629, 'es', 'Jēkabpils', 357),
(2630, 'es', 'Jūrmala', 358),
(2631, 'es', 'Liepāja', 359),
(2632, 'es', 'Liepājas novads', 360),
(2633, 'es', 'Rēzekne', 361),
(2634, 'es', 'Rīga', 362),
(2635, 'es', 'Rīgas novads', 363),
(2636, 'es', 'Valmiera', 364),
(2637, 'es', 'Ventspils', 365),
(2638, 'es', 'Aglonas novads', 366),
(2639, 'es', 'Aizkraukles novads', 367),
(2640, 'es', 'Aizputes novads', 368),
(2641, 'es', 'Aknīstes novads', 369),
(2642, 'es', 'Alojas novads', 370),
(2643, 'es', 'Alsungas novads', 371),
(2644, 'es', 'Alūksnes novads', 372),
(2645, 'es', 'Amatas novads', 373),
(2646, 'es', 'Apes novads', 374),
(2647, 'es', 'Auces novads', 375),
(2648, 'es', 'Babītes novads', 376),
(2649, 'es', 'Baldones novads', 377),
(2650, 'es', 'Baltinavas novads', 378),
(2651, 'es', 'Balvu novads', 379),
(2652, 'es', 'Bauskas novads', 380),
(2653, 'es', 'Beverīnas novads', 381),
(2654, 'es', 'Brocēnu novads', 382),
(2655, 'es', 'Burtnieku novads', 383),
(2656, 'es', 'Carnikavas novads', 384),
(2657, 'es', 'Cesvaines novads', 385),
(2658, 'es', 'Ciblas novads', 386),
(2659, 'es', 'Cēsu novads', 387),
(2660, 'es', 'Dagdas novads', 388),
(2661, 'es', 'Daugavpils novads', 389),
(2662, 'es', 'Dobeles novads', 390),
(2663, 'es', 'Dundagas novads', 391),
(2664, 'es', 'Durbes novads', 392),
(2665, 'es', 'Engures novads', 393),
(2666, 'es', 'Garkalnes novads', 394),
(2667, 'es', 'Grobiņas novads', 395),
(2668, 'es', 'Gulbenes novads', 396),
(2669, 'es', 'Iecavas novads', 397),
(2670, 'es', 'Ikšķiles novads', 398),
(2671, 'es', 'Ilūkstes novads', 399),
(2672, 'es', 'Inčukalna novads', 400),
(2673, 'es', 'Jaunjelgavas novads', 401),
(2674, 'es', 'Jaunpiebalgas novads', 402),
(2675, 'es', 'Jaunpils novads', 403),
(2676, 'es', 'Jelgavas novads', 404),
(2677, 'es', 'Jēkabpils novads', 405),
(2678, 'es', 'Kandavas novads', 406),
(2679, 'es', 'Kokneses novads', 407),
(2680, 'es', 'Krimuldas novads', 408),
(2681, 'es', 'Krustpils novads', 409),
(2682, 'es', 'Krāslavas novads', 410),
(2683, 'es', 'Kuldīgas novads', 411),
(2684, 'es', 'Kārsavas novads', 412),
(2685, 'es', 'Lielvārdes novads', 413),
(2686, 'es', 'Limbažu novads', 414),
(2687, 'es', 'Lubānas novads', 415),
(2688, 'es', 'Ludzas novads', 416),
(2689, 'es', 'Līgatnes novads', 417),
(2690, 'es', 'Līvānu novads', 418),
(2691, 'es', 'Madonas novads', 419),
(2692, 'es', 'Mazsalacas novads', 420),
(2693, 'es', 'Mālpils novads', 421),
(2694, 'es', 'Mārupes novads', 422),
(2695, 'es', 'Naukšēnu novads', 423),
(2696, 'es', 'Neretas novads', 424),
(2697, 'es', 'Nīcas novads', 425),
(2698, 'es', 'Ogres novads', 426),
(2699, 'es', 'Olaines novads', 427),
(2700, 'es', 'Ozolnieku novads', 428),
(2701, 'es', 'Preiļu novads', 429),
(2702, 'es', 'Priekules novads', 430),
(2703, 'es', 'Priekuļu novads', 431),
(2704, 'es', 'Pārgaujas novads', 432),
(2705, 'es', 'Pāvilostas novads', 433),
(2706, 'es', 'Pļaviņu novads', 434),
(2707, 'es', 'Raunas novads', 435),
(2708, 'es', 'Riebiņu novads', 436),
(2709, 'es', 'Rojas novads', 437),
(2710, 'es', 'Ropažu novads', 438),
(2711, 'es', 'Rucavas novads', 439),
(2712, 'es', 'Rugāju novads', 440),
(2713, 'es', 'Rundāles novads', 441),
(2714, 'es', 'Rēzeknes novads', 442),
(2715, 'es', 'Rūjienas novads', 443),
(2716, 'es', 'Salacgrīvas novads', 444),
(2717, 'es', 'Salas novads', 445),
(2718, 'es', 'Salaspils novads', 446),
(2719, 'es', 'Saldus novads', 447),
(2720, 'es', 'Saulkrastu novads', 448),
(2721, 'es', 'Siguldas novads', 449),
(2722, 'es', 'Skrundas novads', 450),
(2723, 'es', 'Skrīveru novads', 451),
(2724, 'es', 'Smiltenes novads', 452),
(2725, 'es', 'Stopiņu novads', 453),
(2726, 'es', 'Strenču novads', 454),
(2727, 'es', 'Sējas novads', 455),
(2728, 'es', 'Talsu novads', 456),
(2729, 'es', 'Tukuma novads', 457),
(2730, 'es', 'Tērvetes novads', 458),
(2731, 'es', 'Vaiņodes novads', 459),
(2732, 'es', 'Valkas novads', 460),
(2733, 'es', 'Valmieras novads', 461),
(2734, 'es', 'Varakļānu novads', 462),
(2735, 'es', 'Vecpiebalgas novads', 463),
(2736, 'es', 'Vecumnieku novads', 464),
(2737, 'es', 'Ventspils novads', 465),
(2738, 'es', 'Viesītes novads', 466),
(2739, 'es', 'Viļakas novads', 467),
(2740, 'es', 'Viļānu novads', 468),
(2741, 'es', 'Vārkavas novads', 469),
(2742, 'es', 'Zilupes novads', 470),
(2743, 'es', 'Ādažu novads', 471),
(2744, 'es', 'Ērgļu novads', 472),
(2745, 'es', 'Ķeguma novads', 473),
(2746, 'es', 'Ķekavas novads', 474),
(2747, 'es', 'Alytaus Apskritis', 475),
(2748, 'es', 'Kauno Apskritis', 476),
(2749, 'es', 'Klaipėdos Apskritis', 477),
(2750, 'es', 'Marijampolės Apskritis', 478),
(2751, 'es', 'Panevėžio Apskritis', 479),
(2752, 'es', 'Šiaulių Apskritis', 480),
(2753, 'es', 'Tauragės Apskritis', 481),
(2754, 'es', 'Telšių Apskritis', 482),
(2755, 'es', 'Utenos Apskritis', 483),
(2756, 'es', 'Vilniaus Apskritis', 484),
(2757, 'es', 'Acre', 485),
(2758, 'es', 'Alagoas', 486),
(2759, 'es', 'Amapá', 487),
(2760, 'es', 'Amazonas', 488),
(2761, 'es', 'Bahía', 489),
(2762, 'es', 'Ceará', 490),
(2763, 'es', 'Espíritu Santo', 491),
(2764, 'es', 'Goiás', 492),
(2765, 'es', 'Maranhão', 493),
(2766, 'es', 'Mato Grosso', 494),
(2767, 'es', 'Mato Grosso del Sur', 495),
(2768, 'es', 'Minas Gerais', 496),
(2769, 'es', 'Pará', 497),
(2770, 'es', 'Paraíba', 498),
(2771, 'es', 'Paraná', 499),
(2772, 'es', 'Pernambuco', 500),
(2773, 'es', 'Piauí', 501),
(2774, 'es', 'Río de Janeiro', 502),
(2775, 'es', 'Río Grande del Norte', 503),
(2776, 'es', 'Río Grande del Sur', 504),
(2777, 'es', 'Rondônia', 505),
(2778, 'es', 'Roraima', 506),
(2779, 'es', 'Santa Catarina', 507),
(2780, 'es', 'São Paulo', 508),
(2781, 'es', 'Sergipe', 509),
(2782, 'es', 'Tocantins', 510),
(2783, 'es', 'Distrito Federal', 511),
(2784, 'es', 'Zagrebačka županija', 512),
(2785, 'es', 'Krapinsko-zagorska županija', 513),
(2786, 'es', 'Sisačko-moslavačka županija', 514),
(2787, 'es', 'Karlovačka županija', 515),
(2788, 'es', 'Varaždinska županija', 516),
(2789, 'es', 'Koprivničko-križevačka županija', 517),
(2790, 'es', 'Bjelovarsko-bilogorska županija', 518),
(2791, 'es', 'Primorsko-goranska županija', 519),
(2792, 'es', 'Ličko-senjska županija', 520),
(2793, 'es', 'Virovitičko-podravska županija', 521),
(2794, 'es', 'Požeško-slavonska županija', 522),
(2795, 'es', 'Brodsko-posavska županija', 523),
(2796, 'es', 'Zadarska županija', 524),
(2797, 'es', 'Osječko-baranjska županija', 525),
(2798, 'es', 'Šibensko-kninska županija', 526),
(2799, 'es', 'Vukovarsko-srijemska županija', 527),
(2800, 'es', 'Splitsko-dalmatinska županija', 528),
(2801, 'es', 'Istarska županija', 529),
(2802, 'es', 'Dubrovačko-neretvanska županija', 530),
(2803, 'es', 'Međimurska županija', 531),
(2804, 'es', 'Grad Zagreb', 532),
(2805, 'es', 'Andaman and Nicobar Islands', 533),
(2806, 'es', 'Andhra Pradesh', 534),
(2807, 'es', 'Arunachal Pradesh', 535),
(2808, 'es', 'Assam', 536),
(2809, 'es', 'Bihar', 537),
(2810, 'es', 'Chandigarh', 538),
(2811, 'es', 'Chhattisgarh', 539),
(2812, 'es', 'Dadra and Nagar Haveli', 540),
(2813, 'es', 'Daman and Diu', 541),
(2814, 'es', 'Delhi', 542),
(2815, 'es', 'Goa', 543),
(2816, 'es', 'Gujarat', 544),
(2817, 'es', 'Haryana', 545),
(2818, 'es', 'Himachal Pradesh', 546),
(2819, 'es', 'Jammu and Kashmir', 547),
(2820, 'es', 'Jharkhand', 548),
(2821, 'es', 'Karnataka', 549),
(2822, 'es', 'Kerala', 550),
(2823, 'es', 'Lakshadweep', 551),
(2824, 'es', 'Madhya Pradesh', 552),
(2825, 'es', 'Maharashtra', 553),
(2826, 'es', 'Manipur', 554),
(2827, 'es', 'Meghalaya', 555),
(2828, 'es', 'Mizoram', 556),
(2829, 'es', 'Nagaland', 557),
(2830, 'es', 'Odisha', 558),
(2831, 'es', 'Puducherry', 559),
(2832, 'es', 'Punjab', 560),
(2833, 'es', 'Rajasthan', 561),
(2834, 'es', 'Sikkim', 562),
(2835, 'es', 'Tamil Nadu', 563),
(2836, 'es', 'Telangana', 564),
(2837, 'es', 'Tripura', 565),
(2838, 'es', 'Uttar Pradesh', 566),
(2839, 'es', 'Uttarakhand', 567),
(2840, 'es', 'West Bengal', 568),
(2841, 'es', 'Alto Paraguay', 569),
(2842, 'es', 'Alto Paraná', 570),
(2843, 'es', 'Amambay', 571),
(2844, 'es', 'Asunción', 572),
(2845, 'es', 'Boquerón', 573),
(2846, 'es', 'Caaguazú', 574),
(2847, 'es', 'Caazapá', 575),
(2848, 'es', 'Canindeyú', 576),
(2849, 'es', 'Central', 577),
(2850, 'es', 'Concepción', 578),
(2851, 'es', 'Cordillera', 579),
(2852, 'es', 'Guairá', 580),
(2853, 'es', 'Itapúa', 581),
(2854, 'es', 'Misiones', 582),
(2855, 'es', 'Paraguarí', 583),
(2856, 'es', 'Presidente Hayes', 584),
(2857, 'es', 'San Pedro', 585),
(2858, 'es', 'Ñeembucú', 586),
(2859, 'fa', 'آلاباما', 1),
(2860, 'fa', 'آلاسکا', 2),
(2861, 'fa', 'ساموآ آمریکایی', 3),
(2862, 'fa', 'آریزونا', 4),
(2863, 'fa', 'آرکانزاس', 5),
(2864, 'fa', 'نیروهای مسلح آفریقا', 6),
(2865, 'fa', 'Armed Forces America', 7),
(2866, 'fa', 'نیروهای مسلح کانادا', 8),
(2867, 'fa', 'نیروهای مسلح اروپا', 9),
(2868, 'fa', 'نیروهای مسلح خاورمیانه', 10),
(2869, 'fa', 'نیروهای مسلح اقیانوس آرام', 11),
(2870, 'fa', 'کالیفرنیا', 12),
(2871, 'fa', 'کلرادو', 13),
(2872, 'fa', 'کانکتیکات', 14),
(2873, 'fa', 'دلاور', 15),
(2874, 'fa', 'منطقه کلمبیا', 16),
(2875, 'fa', 'ایالات فدرال میکرونزی', 17),
(2876, 'fa', 'فلوریدا', 18),
(2877, 'fa', 'جورجیا', 19),
(2878, 'fa', 'گوام', 20),
(2879, 'fa', 'هاوایی', 21),
(2880, 'fa', 'آیداهو', 22),
(2881, 'fa', 'ایلینویز', 23),
(2882, 'fa', 'ایندیانا', 24),
(2883, 'fa', 'آیووا', 25),
(2884, 'fa', 'کانزاس', 26),
(2885, 'fa', 'کنتاکی', 27),
(2886, 'fa', 'لوئیزیانا', 28),
(2887, 'fa', 'ماین', 29),
(2888, 'fa', 'مای', 30),
(2889, 'fa', 'مریلند', 31),
(2890, 'fa', ' ', 32),
(2891, 'fa', 'میشیگان', 33),
(2892, 'fa', 'مینه سوتا', 34),
(2893, 'fa', 'می سی سی پی', 35),
(2894, 'fa', 'میسوری', 36),
(2895, 'fa', 'مونتانا', 37),
(2896, 'fa', 'نبراسکا', 38),
(2897, 'fa', 'نواد', 39),
(2898, 'fa', 'نیوهمپشایر', 40),
(2899, 'fa', 'نیوجرسی', 41),
(2900, 'fa', 'نیومکزیکو', 42),
(2901, 'fa', 'نیویورک', 43),
(2902, 'fa', 'کارولینای شمالی', 44),
(2903, 'fa', 'داکوتای شمالی', 45),
(2904, 'fa', 'جزایر ماریانای شمالی', 46),
(2905, 'fa', 'اوهایو', 47),
(2906, 'fa', 'اوکلاهما', 48),
(2907, 'fa', 'اورگان', 49),
(2908, 'fa', 'پالائو', 50),
(2909, 'fa', 'پنسیلوانیا', 51),
(2910, 'fa', 'پورتوریکو', 52),
(2911, 'fa', 'رود آیلند', 53),
(2912, 'fa', 'کارولینای جنوبی', 54),
(2913, 'fa', 'داکوتای جنوبی', 55),
(2914, 'fa', 'تنسی', 56),
(2915, 'fa', 'تگزاس', 57),
(2916, 'fa', 'یوتا', 58),
(2917, 'fa', 'ورمونت', 59),
(2918, 'fa', 'جزایر ویرجین', 60),
(2919, 'fa', 'ویرجینیا', 61),
(2920, 'fa', 'واشنگتن', 62),
(2921, 'fa', 'ویرجینیای غربی', 63),
(2922, 'fa', 'ویسکانسین', 64),
(2923, 'fa', 'وایومینگ', 65),
(2924, 'fa', 'آلبرتا', 66),
(2925, 'fa', 'بریتیش کلمبیا', 67),
(2926, 'fa', 'مانیتوبا', 68),
(2927, 'fa', 'نیوفاندلند و لابرادور', 69),
(2928, 'fa', 'نیوبرانزویک', 70),
(2929, 'fa', 'نوا اسکوشیا', 71),
(2930, 'fa', 'سرزمینهای شمال غربی', 72),
(2931, 'fa', 'نوناووت', 73),
(2932, 'fa', 'انتاریو', 74),
(2933, 'fa', 'جزیره پرنس ادوارد', 75),
(2934, 'fa', 'کبک', 76),
(2935, 'fa', 'ساسکاتچوان', 77),
(2936, 'fa', 'قلمرو یوکان', 78),
(2937, 'fa', 'نیدرزاکسن', 79),
(2938, 'fa', 'بادن-وورتمبرگ', 80),
(2939, 'fa', 'بایرن', 81),
(2940, 'fa', 'برلین', 82),
(2941, 'fa', 'براندنبورگ', 83),
(2942, 'fa', 'برمن', 84),
(2943, 'fa', 'هامبور', 85),
(2944, 'fa', 'هسن', 86),
(2945, 'fa', 'مکلنبورگ-وورپومرن', 87),
(2946, 'fa', 'نوردراین-وستفالن', 88),
(2947, 'fa', 'راینلاند-پلاتینات', 89),
(2948, 'fa', 'سارلند', 90),
(2949, 'fa', 'ساچسن', 91),
(2950, 'fa', 'ساچسن-آنهالت', 92),
(2951, 'fa', 'شلسویگ-هولشتاین', 93),
(2952, 'fa', 'تورینگی', 94),
(2953, 'fa', 'وین', 95),
(2954, 'fa', 'اتریش پایین', 96),
(2955, 'fa', 'اتریش فوقانی', 97),
(2956, 'fa', 'سالزبورگ', 98),
(2957, 'fa', 'کارنتا', 99),
(2958, 'fa', 'Steiermar', 100),
(2959, 'fa', 'تیرول', 101),
(2960, 'fa', 'بورگنلن', 102),
(2961, 'fa', 'Vorarlber', 103),
(2962, 'fa', 'آرگ', 104),
(2963, 'fa', '', 105),
(2964, 'fa', 'اپنزلسرهودن', 106),
(2965, 'fa', 'بر', 107),
(2966, 'fa', 'بازل-لندشفت', 108),
(2967, 'fa', 'بازل استاد', 109),
(2968, 'fa', 'فرایبورگ', 110),
(2969, 'fa', 'گنف', 111),
(2970, 'fa', 'گلاروس', 112),
(2971, 'fa', 'Graubünde', 113),
(2972, 'fa', 'ژورا', 114),
(2973, 'fa', 'لوزرن', 115),
(2974, 'fa', 'نوینبور', 116),
(2975, 'fa', 'نیدالد', 117),
(2976, 'fa', 'اوبولدن', 118),
(2977, 'fa', 'سنت گالن', 119),
(2978, 'fa', 'شافهاوز', 120),
(2979, 'fa', 'سولوتور', 121),
(2980, 'fa', 'شووی', 122),
(2981, 'fa', 'تورگاو', 123),
(2982, 'fa', 'تسسی', 124),
(2983, 'fa', 'اوری', 125),
(2984, 'fa', 'وادت', 126),
(2985, 'fa', 'والی', 127),
(2986, 'fa', 'ز', 128),
(2987, 'fa', 'زوریخ', 129),
(2988, 'fa', 'کورونا', 130),
(2989, 'fa', 'آلاوا', 131),
(2990, 'fa', 'آلبوم', 132),
(2991, 'fa', 'آلیکانت', 133),
(2992, 'fa', 'آلمریا', 134),
(2993, 'fa', 'آستوریا', 135),
(2994, 'fa', 'آویلا', 136),
(2995, 'fa', 'باداژوز', 137),
(2996, 'fa', 'ضرب و شتم', 138),
(2997, 'fa', 'بارسلون', 139),
(2998, 'fa', 'بورگو', 140),
(2999, 'fa', 'کاسر', 141),
(3000, 'fa', 'کادی', 142),
(3001, 'fa', 'کانتابریا', 143),
(3002, 'fa', 'کاستلون', 144),
(3003, 'fa', 'سوت', 145),
(3004, 'fa', 'سیوداد واقعی', 146),
(3005, 'fa', 'کوردوب', 147),
(3006, 'fa', 'Cuenc', 148),
(3007, 'fa', 'جیرون', 149),
(3008, 'fa', 'گراناد', 150),
(3009, 'fa', 'گوادالاجار', 151),
(3010, 'fa', 'Guipuzcoa', 152),
(3011, 'fa', 'هولوا', 153),
(3012, 'fa', 'هوسک', 154),
(3013, 'fa', 'جی', 155),
(3014, 'fa', 'لا ریوجا', 156),
(3015, 'fa', 'لاس پالماس', 157),
(3016, 'fa', 'لئو', 158),
(3017, 'fa', 'Lleid', 159),
(3018, 'fa', 'لوگ', 160),
(3019, 'fa', 'مادری', 161),
(3020, 'fa', 'مالاگ', 162),
(3021, 'fa', 'ملیلی', 163),
(3022, 'fa', 'مورسیا', 164),
(3023, 'fa', 'ناوار', 165),
(3024, 'fa', 'اورنس', 166),
(3025, 'fa', 'پالنسی', 167),
(3026, 'fa', 'پونتوودر', 168),
(3027, 'fa', 'سالامانک', 169),
(3028, 'fa', 'سانتا کروز د تنریفه', 170),
(3029, 'fa', 'سوگویا', 171),
(3030, 'fa', 'سوی', 172),
(3031, 'fa', 'سوریا', 173),
(3032, 'fa', 'تاراگونا', 174),
(3033, 'fa', 'ترئو', 175),
(3034, 'fa', 'تولدو', 176),
(3035, 'fa', 'والنسیا', 177),
(3036, 'fa', 'والادولی', 178),
(3037, 'fa', 'ویزکایا', 179),
(3038, 'fa', 'زامور', 180),
(3039, 'fa', 'ساراگوز', 181),
(3040, 'fa', 'عی', 182),
(3041, 'fa', 'آیز', 183),
(3042, 'fa', 'آلی', 184),
(3043, 'fa', 'آلپ-دو-هاوت-پرووانس', 185),
(3044, 'fa', 'هاوتس آلپ', 186),
(3045, 'fa', 'Alpes-Maritime', 187),
(3046, 'fa', 'اردچه', 188),
(3047, 'fa', 'آرد', 189),
(3048, 'fa', 'محاصر', 190),
(3049, 'fa', 'آبه', 191),
(3050, 'fa', 'Aud', 192),
(3051, 'fa', 'آویرون', 193),
(3052, 'fa', 'BOCAS DO Rhône', 194),
(3053, 'fa', 'نوعی عرق', 195),
(3054, 'fa', 'کانتینال', 196),
(3055, 'fa', 'چارنت', 197),
(3056, 'fa', 'چارنت-دریایی', 198),
(3057, 'fa', 'چ', 199),
(3058, 'fa', 'کور', 200),
(3059, 'fa', 'کرس دو ساد', 201),
(3060, 'fa', 'هاوت کورس', 202),
(3061, 'fa', 'کوستا دورکرز', 203),
(3062, 'fa', 'تخت دارمور', 204),
(3063, 'fa', 'درهم', 205),
(3064, 'fa', 'دوردگن', 206),
(3065, 'fa', 'دوب', 207),
(3066, 'fa', 'تعریف اول', 208),
(3067, 'fa', 'یور', 209),
(3068, 'fa', 'Eure-et-Loi', 210),
(3069, 'fa', 'فمینیست', 211),
(3070, 'fa', 'باغ', 212),
(3071, 'fa', 'اوت-گارون', 213),
(3072, 'fa', 'گر', 214),
(3073, 'fa', 'جیروند', 215),
(3074, 'fa', 'هیر', 216),
(3075, 'fa', 'هشدار داده می شود', 217),
(3076, 'fa', 'ایندور', 218),
(3077, 'fa', 'Indre-et-Loir', 219),
(3078, 'fa', 'ایزر', 220),
(3079, 'fa', 'یور', 221),
(3080, 'fa', 'لندز', 222),
(3081, 'fa', 'Loir-et-Che', 223),
(3082, 'fa', 'وام گرفتن', 224),
(3083, 'fa', 'Haute-Loir', 225),
(3084, 'fa', 'Loire-Atlantiqu', 226),
(3085, 'fa', 'لیرت', 227),
(3086, 'fa', 'لوط', 228),
(3087, 'fa', 'لوت و گارون', 229),
(3088, 'fa', 'لوزر', 230),
(3089, 'fa', 'ماین et-Loire', 231),
(3090, 'fa', 'مانچ', 232),
(3091, 'fa', 'مارن', 233),
(3092, 'fa', 'هاوت-مارن', 234),
(3093, 'fa', 'مایین', 235),
(3094, 'fa', 'مورته-et-Moselle', 236),
(3095, 'fa', 'مسخره کردن', 237),
(3096, 'fa', 'موربیان', 238),
(3097, 'fa', 'موزل', 239),
(3098, 'fa', 'Nièvr', 240),
(3099, 'fa', 'نورد', 241),
(3100, 'fa', 'اوی', 242),
(3101, 'fa', 'ارن', 243),
(3102, 'fa', 'پاس-کاله', 244),
(3103, 'fa', 'Puy-de-Dôm', 245),
(3104, 'fa', 'Pyrénées-Atlantiques', 246),
(3105, 'fa', 'Hautes-Pyrénée', 247),
(3106, 'fa', 'Pyrénées-Orientales', 248),
(3107, 'fa', 'بس راین', 249),
(3108, 'fa', 'هاوت-رین', 250),
(3109, 'fa', 'رو', 251),
(3110, 'fa', 'Haute-Saône', 252),
(3111, 'fa', 'Saône-et-Loire', 253),
(3112, 'fa', 'سارته', 254),
(3113, 'fa', 'ساووی', 255),
(3114, 'fa', 'هاو-ساووی', 256),
(3115, 'fa', 'پاری', 257),
(3116, 'fa', 'Seine-Maritime', 258),
(3117, 'fa', 'Seine-et-Marn', 259),
(3118, 'fa', 'ایولینز', 260),
(3119, 'fa', 'Deux-Sèvres', 261),
(3120, 'fa', 'سمی', 262),
(3121, 'fa', 'ضعف', 263),
(3122, 'fa', 'Tarn-et-Garonne', 264),
(3123, 'fa', 'وار', 265),
(3124, 'fa', 'ووکلوز', 266),
(3125, 'fa', 'وندیه', 267),
(3126, 'fa', 'وین', 268),
(3127, 'fa', 'هاوت-وین', 269),
(3128, 'fa', 'رأی دادن', 270),
(3129, 'fa', 'یون', 271),
(3130, 'fa', 'سرزمین-دو-بلفورت', 272),
(3131, 'fa', 'اسون', 273),
(3132, 'fa', 'هاوتز دی سی', 274),
(3133, 'fa', 'Seine-Saint-Deni', 275),
(3134, 'fa', 'والد مارن', 276),
(3135, 'fa', 'Val-d\'Ois', 277),
(3136, 'fa', 'آلبا', 278),
(3137, 'fa', 'آرا', 279),
(3138, 'fa', 'Argeș', 280),
(3139, 'fa', 'باکو', 281),
(3140, 'fa', 'بیهور', 282),
(3141, 'fa', 'بیستریا-نسوود', 283),
(3142, 'fa', 'بوتانی', 284),
(3143, 'fa', 'برازوف', 285),
(3144, 'fa', 'Brăila', 286),
(3145, 'fa', 'București', 287),
(3146, 'fa', 'بوز', 288),
(3147, 'fa', 'کارا- Severin', 289),
(3148, 'fa', 'کالیراسی', 290),
(3149, 'fa', 'كلوژ', 291),
(3150, 'fa', 'کنستانس', 292),
(3151, 'fa', 'کواسنا', 293),
(3152, 'fa', 'Dâmbovița', 294),
(3153, 'fa', 'دال', 295),
(3154, 'fa', 'گالشی', 296),
(3155, 'fa', 'جورجیو', 297),
(3156, 'fa', 'گور', 298),
(3157, 'fa', 'هارگیتا', 299),
(3158, 'fa', 'هوندهار', 300),
(3159, 'fa', 'ایالومیشا', 301),
(3160, 'fa', 'Iași', 302),
(3161, 'fa', 'Ilfo', 303),
(3162, 'fa', 'Maramureș', 304),
(3163, 'fa', 'Mehedinți', 305),
(3164, 'fa', 'Mureș', 306),
(3165, 'fa', 'Neamț', 307),
(3166, 'fa', 'اولت', 308),
(3167, 'fa', 'پرهوا', 309),
(3168, 'fa', 'ستو ماره', 310),
(3169, 'fa', 'سلاج', 311),
(3170, 'fa', 'سیبیو', 312),
(3171, 'fa', 'سوساو', 313),
(3172, 'fa', 'تلورمان', 314),
(3173, 'fa', 'تیمیچ', 315),
(3174, 'fa', 'تولسا', 316),
(3175, 'fa', 'واسلوئی', 317),
(3176, 'fa', 'Vâlcea', 318),
(3177, 'fa', 'ورانسا', 319),
(3178, 'fa', 'لاپی', 320),
(3179, 'fa', 'Pohjois-Pohjanmaa', 321),
(3180, 'fa', 'کائینو', 322),
(3181, 'fa', 'Pohjois-Karjala', 323),
(3182, 'fa', 'Pohjois-Savo', 324),
(3183, 'fa', 'اتل-ساوو', 325),
(3184, 'fa', 'کسکی-پوهانما', 326),
(3185, 'fa', 'Pohjanmaa', 327),
(3186, 'fa', 'پیرکانما', 328),
(3187, 'fa', 'ساتاکونتا', 329),
(3188, 'fa', 'کسکی-پوهانما', 330),
(3189, 'fa', 'کسکی-سوومی', 331),
(3190, 'fa', 'Varsinais-Suomi', 332),
(3191, 'fa', 'اتلی کرجالا', 333),
(3192, 'fa', 'Päijät-HAM', 334),
(3193, 'fa', 'کانتا-هوم', 335),
(3194, 'fa', 'یوسیما', 336),
(3195, 'fa', 'اوسیم', 337),
(3196, 'fa', 'کیمنلاکو', 338),
(3197, 'fa', 'آونوانما', 339),
(3198, 'fa', 'هارژوم', 340),
(3199, 'fa', 'سلا', 341),
(3200, 'fa', 'آیدا-ویروما', 342),
(3201, 'fa', 'Jõgevamaa', 343),
(3202, 'fa', 'جوروماا', 344),
(3203, 'fa', 'لونما', 345),
(3204, 'fa', 'لون-ویروما', 346),
(3205, 'fa', 'پالوماا', 347),
(3206, 'fa', 'پورنوما', 348),
(3207, 'fa', 'Raplama', 349),
(3208, 'fa', 'ساارما', 350),
(3209, 'fa', 'تارتوما', 351),
(3210, 'fa', 'والگام', 352),
(3211, 'fa', 'ویلجاندیم', 353),
(3212, 'fa', 'Võrumaa', 354),
(3213, 'fa', 'داگاوپیل', 355),
(3214, 'fa', 'جلگاو', 356),
(3215, 'fa', 'جکابیل', 357),
(3216, 'fa', 'جرمل', 358),
(3217, 'fa', 'لیپجا', 359),
(3218, 'fa', 'شهرستان لیپاج', 360),
(3219, 'fa', 'روژن', 361),
(3220, 'fa', 'راگ', 362),
(3221, 'fa', 'شهرستان ریگ', 363),
(3222, 'fa', 'والمییرا', 364),
(3223, 'fa', 'Ventspils', 365),
(3224, 'fa', 'آگلوناس نوادا', 366),
(3225, 'fa', 'تازه کاران آیزکرایکلس', 367),
(3226, 'fa', 'تازه واردان', 368),
(3227, 'fa', 'شهرستا', 369),
(3228, 'fa', 'نوازندگان آلوجاس', 370),
(3229, 'fa', 'تازه های آلسونگاس', 371),
(3230, 'fa', 'شهرستان آلوکس', 372),
(3231, 'fa', 'تازه کاران آماتاس', 373),
(3232, 'fa', 'میمون های تازه', 374),
(3233, 'fa', 'نوادا را آویز می کند', 375),
(3234, 'fa', 'شهرستان بابی', 376),
(3235, 'fa', 'Baldones novad', 377),
(3236, 'fa', 'نوین های بالتیناوا', 378),
(3237, 'fa', 'Balvu novad', 379),
(3238, 'fa', 'نوازندگان باسکاس', 380),
(3239, 'fa', 'شهرستان بورین', 381),
(3240, 'fa', 'شهرستان بروچن', 382),
(3241, 'fa', 'بوردنیکو نوآوران', 383),
(3242, 'fa', 'تازه کارنیکاوا', 384),
(3243, 'fa', 'نوازان سزوینس', 385),
(3244, 'fa', 'نوادگان Cibla', 386),
(3245, 'fa', 'شهرستان Cesis', 387),
(3246, 'fa', 'تازه های داگدا', 388),
(3247, 'fa', 'داوگاوپیلز نوادا', 389),
(3248, 'fa', 'دابل نوادی', 390),
(3249, 'fa', 'تازه کارهای دنداگاس', 391),
(3250, 'fa', 'نوباد دوربس', 392),
(3251, 'fa', 'مشغول تازه کارها است', 393),
(3252, 'fa', 'گرکالنس نواد', 394),
(3253, 'fa', 'یا شهرستان گروبی', 395),
(3254, 'fa', 'تازه های گلبنس', 396),
(3255, 'fa', 'Iecavas novads', 397),
(3256, 'fa', 'شهرستان ایسکل', 398),
(3257, 'fa', 'ایالت ایلکست', 399),
(3258, 'fa', 'کنددو د اینچوکالن', 400),
(3259, 'fa', 'نوجواد Jaunjelgavas', 401),
(3260, 'fa', 'تازه های Jaunpiebalgas', 402),
(3261, 'fa', 'شهرستان جونپیلس', 403),
(3262, 'fa', 'شهرستان جگلو', 404),
(3263, 'fa', 'شهرستان جکابیل', 405),
(3264, 'fa', 'شهرستان کنداوا', 406),
(3265, 'fa', 'شهرستان کوکنز', 407),
(3266, 'fa', 'شهرستان کریمولد', 408),
(3267, 'fa', 'شهرستان کرستپیل', 409),
(3268, 'fa', 'شهرستان کراسلاو', 410),
(3269, 'fa', 'کاندادو د کلدیگا', 411),
(3270, 'fa', 'کاندادو د کارساوا', 412),
(3271, 'fa', 'شهرستان لیولوارد', 413),
(3272, 'fa', 'شهرستان لیمباشی', 414),
(3273, 'fa', 'ای ولسوالی لوبون', 415),
(3274, 'fa', 'شهرستان لودزا', 416),
(3275, 'fa', 'شهرستان لیگات', 417),
(3276, 'fa', 'شهرستان لیوانی', 418),
(3277, 'fa', 'شهرستان مادونا', 419),
(3278, 'fa', 'شهرستان مازسال', 420),
(3279, 'fa', 'شهرستان مالپیلس', 421),
(3280, 'fa', 'شهرستان Mārupe', 422),
(3281, 'fa', 'ا کنددو د نوکشنی', 423),
(3282, 'fa', 'کاملاً یک شهرستان', 424),
(3283, 'fa', 'شهرستان نیکا', 425),
(3284, 'fa', 'شهرستان اوگر', 426),
(3285, 'fa', 'شهرستان اولین', 427),
(3286, 'fa', 'شهرستان اوزولنیکی', 428),
(3287, 'fa', 'شهرستان پرلیلی', 429),
(3288, 'fa', 'شهرستان Priekule', 430),
(3289, 'fa', 'Condado de Priekuļi', 431),
(3290, 'fa', 'شهرستان در حال حرکت', 432),
(3291, 'fa', 'شهرستان پاویلوستا', 433),
(3292, 'fa', 'شهرستان Plavinas', 4),
(3293, 'fa', 'شهرستان راونا', 435),
(3294, 'fa', 'شهرستان ریبیشی', 436),
(3295, 'fa', 'شهرستان روجا', 437),
(3296, 'fa', 'شهرستان روپازی', 438),
(3297, 'fa', 'شهرستان روساوا', 439),
(3298, 'fa', 'شهرستان روگی', 440),
(3299, 'fa', 'شهرستان راندل', 441),
(3300, 'fa', 'شهرستان ریزکن', 442),
(3301, 'fa', 'شهرستان روژینا', 443),
(3302, 'fa', 'شهرداری Salacgriva', 444),
(3303, 'fa', 'منطقه جزیره', 445),
(3304, 'fa', 'شهرستان Salaspils', 446),
(3305, 'fa', 'شهرستان سالدوس', 447),
(3306, 'fa', 'شهرستان ساولکرستی', 448),
(3307, 'fa', 'شهرستان سیگولدا', 449),
(3308, 'fa', 'شهرستان Skrunda', 450),
(3309, 'fa', 'شهرستان Skrīveri', 451),
(3310, 'fa', 'شهرستان Smiltene', 452),
(3311, 'fa', 'شهرستان ایستینی', 453);
INSERT INTO `country_state_translations` (`id`, `locale`, `default_name`, `country_state_id`) VALUES
(3312, 'fa', 'شهرستان استرنشی', 454),
(3313, 'fa', 'منطقه کاشت', 455),
(3314, 'fa', 'شهرستان تالسی', 456),
(3315, 'fa', 'توکومس', 457),
(3316, 'fa', 'شهرستان تورت', 458),
(3317, 'fa', 'یا شهرستان وایودود', 459),
(3318, 'fa', 'شهرستان والکا', 460),
(3319, 'fa', 'شهرستان Valmiera', 461),
(3320, 'fa', 'شهرستان وارکانی', 462),
(3321, 'fa', 'شهرستان Vecpiebalga', 463),
(3322, 'fa', 'شهرستان وکومنیکی', 464),
(3323, 'fa', 'شهرستان ونتسپیل', 465),
(3324, 'fa', 'کنددو د بازدید', 466),
(3325, 'fa', 'شهرستان ویلاکا', 467),
(3326, 'fa', 'شهرستان ویلانی', 468),
(3327, 'fa', 'شهرستان واركاوا', 469),
(3328, 'fa', 'شهرستان زیلوپ', 470),
(3329, 'fa', 'شهرستان آدازی', 471),
(3330, 'fa', 'شهرستان ارگلو', 472),
(3331, 'fa', 'شهرستان کگومس', 473),
(3332, 'fa', 'شهرستان ککاوا', 474),
(3333, 'fa', 'شهرستان Alytus', 475),
(3334, 'fa', 'شهرستان Kaunas', 476),
(3335, 'fa', 'شهرستان کلایپدا', 477),
(3336, 'fa', 'شهرستان ماریجامپولی', 478),
(3337, 'fa', 'شهرستان پانویسیز', 479),
(3338, 'fa', 'شهرستان سیاولیا', 480),
(3339, 'fa', 'شهرستان تاجیج', 481),
(3340, 'fa', 'شهرستان تلشیا', 482),
(3341, 'fa', 'شهرستان اوتنا', 483),
(3342, 'fa', 'شهرستان ویلنیوس', 484),
(3343, 'fa', 'جریب', 485),
(3344, 'fa', 'حالت', 486),
(3345, 'fa', 'آمپá', 487),
(3346, 'fa', 'آمازون', 488),
(3347, 'fa', 'باهی', 489),
(3348, 'fa', 'سارا', 490),
(3349, 'fa', 'روح القدس', 491),
(3350, 'fa', 'برو', 492),
(3351, 'fa', 'مارانهائ', 493),
(3352, 'fa', 'ماتو گروسو', 494),
(3353, 'fa', 'Mato Grosso do Sul', 495),
(3354, 'fa', 'ایالت میناس گرایس', 496),
(3355, 'fa', 'پار', 497),
(3356, 'fa', 'حالت', 498),
(3357, 'fa', 'پارانا', 499),
(3358, 'fa', 'حال', 500),
(3359, 'fa', 'پیازو', 501),
(3360, 'fa', 'ریو دوژانیرو', 502),
(3361, 'fa', 'ریو گراند دو نورته', 503),
(3362, 'fa', 'ریو گراند دو سول', 504),
(3363, 'fa', 'Rondôni', 505),
(3364, 'fa', 'Roraim', 506),
(3365, 'fa', 'سانتا کاتارینا', 507),
(3366, 'fa', 'پ', 508),
(3367, 'fa', 'Sergip', 509),
(3368, 'fa', 'توکانتین', 510),
(3369, 'fa', 'منطقه فدرال', 511),
(3370, 'fa', 'شهرستان زاگرب', 512),
(3371, 'fa', 'Condado de Krapina-Zagorj', 513),
(3372, 'fa', 'شهرستان سیساک-موسلاوینا', 514),
(3373, 'fa', 'شهرستان کارلوواک', 515),
(3374, 'fa', 'شهرداری واراžدین', 516),
(3375, 'fa', 'Condo de Koprivnica-Križevci', 517),
(3376, 'fa', 'محل سکونت د بیلوار-بلوگورا', 518),
(3377, 'fa', 'Condado de Primorje-Gorski kotar', 519),
(3378, 'fa', 'شهرستان لیکا-سنج', 520),
(3379, 'fa', 'Condado de Virovitica-Podravina', 521),
(3380, 'fa', 'شهرستان پوژگا-اسلاونیا', 522),
(3381, 'fa', 'Condado de Brod-Posavina', 523),
(3382, 'fa', 'شهرستان زجر', 524),
(3383, 'fa', 'Condado de Osijek-Baranja', 525),
(3384, 'fa', 'Condo de Sibenik-Knin', 526),
(3385, 'fa', 'Condado de Vukovar-Srijem', 527),
(3386, 'fa', 'شهرستان اسپلیت-Dalmatia', 528),
(3387, 'fa', 'شهرستان ایستیا', 529),
(3388, 'fa', 'Condado de Dubrovnik-Neretva', 530),
(3389, 'fa', 'شهرستان Međimurje', 531),
(3390, 'fa', 'شهر زاگرب', 532),
(3391, 'fa', 'جزایر آندامان و نیکوبار', 533),
(3392, 'fa', 'آندرا پرادش', 534),
(3393, 'fa', 'آروناچال پرادش', 535),
(3394, 'fa', 'آسام', 536),
(3395, 'fa', 'Biha', 537),
(3396, 'fa', 'چاندیگار', 538),
(3397, 'fa', 'چاتیسگار', 539),
(3398, 'fa', 'دادرا و نگار هاولی', 540),
(3399, 'fa', 'دامان و دیو', 541),
(3400, 'fa', 'دهلی', 542),
(3401, 'fa', 'گوا', 543),
(3402, 'fa', 'گجرات', 544),
(3403, 'fa', 'هاریانا', 545),
(3404, 'fa', 'هیماچال پرادش', 546),
(3405, 'fa', 'جامو و کشمیر', 547),
(3406, 'fa', 'جهخند', 548),
(3407, 'fa', 'کارناتاکا', 549),
(3408, 'fa', 'کرال', 550),
(3409, 'fa', 'لاکشادوپ', 551),
(3410, 'fa', 'مادیا پرادش', 552),
(3411, 'fa', 'ماهاراشترا', 553),
(3412, 'fa', 'مانی پور', 554),
(3413, 'fa', 'مگالایا', 555),
(3414, 'fa', 'مزورام', 556),
(3415, 'fa', 'ناگلند', 557),
(3416, 'fa', 'ادیشا', 558),
(3417, 'fa', 'میناکاری', 559),
(3418, 'fa', 'پنجا', 560),
(3419, 'fa', 'راجستان', 561),
(3420, 'fa', 'سیکیم', 562),
(3421, 'fa', 'تامیل نادو', 563),
(3422, 'fa', 'تلنگانا', 564),
(3423, 'fa', 'تریپورا', 565),
(3424, 'fa', 'اوتار پرادش', 566),
(3425, 'fa', 'اوتاراکند', 567),
(3426, 'fa', 'بنگال غرب', 568),
(3427, 'pt_BR', 'Alabama', 1),
(3428, 'pt_BR', 'Alaska', 2),
(3429, 'pt_BR', 'Samoa Americana', 3),
(3430, 'pt_BR', 'Arizona', 4),
(3431, 'pt_BR', 'Arkansas', 5),
(3432, 'pt_BR', 'Forças Armadas da África', 6),
(3433, 'pt_BR', 'Forças Armadas das Américas', 7),
(3434, 'pt_BR', 'Forças Armadas do Canadá', 8),
(3435, 'pt_BR', 'Forças Armadas da Europa', 9),
(3436, 'pt_BR', 'Forças Armadas do Oriente Médio', 10),
(3437, 'pt_BR', 'Forças Armadas do Pacífico', 11),
(3438, 'pt_BR', 'California', 12),
(3439, 'pt_BR', 'Colorado', 13),
(3440, 'pt_BR', 'Connecticut', 14),
(3441, 'pt_BR', 'Delaware', 15),
(3442, 'pt_BR', 'Distrito de Columbia', 16),
(3443, 'pt_BR', 'Estados Federados da Micronésia', 17),
(3444, 'pt_BR', 'Florida', 18),
(3445, 'pt_BR', 'Geórgia', 19),
(3446, 'pt_BR', 'Guam', 20),
(3447, 'pt_BR', 'Havaí', 21),
(3448, 'pt_BR', 'Idaho', 22),
(3449, 'pt_BR', 'Illinois', 23),
(3450, 'pt_BR', 'Indiana', 24),
(3451, 'pt_BR', 'Iowa', 25),
(3452, 'pt_BR', 'Kansas', 26),
(3453, 'pt_BR', 'Kentucky', 27),
(3454, 'pt_BR', 'Louisiana', 28),
(3455, 'pt_BR', 'Maine', 29),
(3456, 'pt_BR', 'Ilhas Marshall', 30),
(3457, 'pt_BR', 'Maryland', 31),
(3458, 'pt_BR', 'Massachusetts', 32),
(3459, 'pt_BR', 'Michigan', 33),
(3460, 'pt_BR', 'Minnesota', 34),
(3461, 'pt_BR', 'Mississippi', 35),
(3462, 'pt_BR', 'Missouri', 36),
(3463, 'pt_BR', 'Montana', 37),
(3464, 'pt_BR', 'Nebraska', 38),
(3465, 'pt_BR', 'Nevada', 39),
(3466, 'pt_BR', 'New Hampshire', 40),
(3467, 'pt_BR', 'Nova Jersey', 41),
(3468, 'pt_BR', 'Novo México', 42),
(3469, 'pt_BR', 'Nova York', 43),
(3470, 'pt_BR', 'Carolina do Norte', 44),
(3471, 'pt_BR', 'Dakota do Norte', 45),
(3472, 'pt_BR', 'Ilhas Marianas do Norte', 46),
(3473, 'pt_BR', 'Ohio', 47),
(3474, 'pt_BR', 'Oklahoma', 48),
(3475, 'pt_BR', 'Oregon', 4),
(3476, 'pt_BR', 'Palau', 50),
(3477, 'pt_BR', 'Pensilvânia', 51),
(3478, 'pt_BR', 'Porto Rico', 52),
(3479, 'pt_BR', 'Rhode Island', 53),
(3480, 'pt_BR', 'Carolina do Sul', 54),
(3481, 'pt_BR', 'Dakota do Sul', 55),
(3482, 'pt_BR', 'Tennessee', 56),
(3483, 'pt_BR', 'Texas', 57),
(3484, 'pt_BR', 'Utah', 58),
(3485, 'pt_BR', 'Vermont', 59),
(3486, 'pt_BR', 'Ilhas Virgens', 60),
(3487, 'pt_BR', 'Virginia', 61),
(3488, 'pt_BR', 'Washington', 62),
(3489, 'pt_BR', 'West Virginia', 63),
(3490, 'pt_BR', 'Wisconsin', 64),
(3491, 'pt_BR', 'Wyoming', 65),
(3492, 'pt_BR', 'Alberta', 66),
(3493, 'pt_BR', 'Colúmbia Britânica', 67),
(3494, 'pt_BR', 'Manitoba', 68),
(3495, 'pt_BR', 'Terra Nova e Labrador', 69),
(3496, 'pt_BR', 'New Brunswick', 70),
(3497, 'pt_BR', 'Nova Escócia', 7),
(3498, 'pt_BR', 'Territórios do Noroeste', 72),
(3499, 'pt_BR', 'Nunavut', 73),
(3500, 'pt_BR', 'Ontario', 74),
(3501, 'pt_BR', 'Ilha do Príncipe Eduardo', 75),
(3502, 'pt_BR', 'Quebec', 76),
(3503, 'pt_BR', 'Saskatchewan', 77),
(3504, 'pt_BR', 'Território yukon', 78),
(3505, 'pt_BR', 'Niedersachsen', 79),
(3506, 'pt_BR', 'Baden-Wurttemberg', 80),
(3507, 'pt_BR', 'Bayern', 81),
(3508, 'pt_BR', 'Berlim', 82),
(3509, 'pt_BR', 'Brandenburg', 83),
(3510, 'pt_BR', 'Bremen', 84),
(3511, 'pt_BR', 'Hamburgo', 85),
(3512, 'pt_BR', 'Hessen', 86),
(3513, 'pt_BR', 'Mecklenburg-Vorpommern', 87),
(3514, 'pt_BR', 'Nordrhein-Westfalen', 88),
(3515, 'pt_BR', 'Renânia-Palatinado', 8),
(3516, 'pt_BR', 'Sarre', 90),
(3517, 'pt_BR', 'Sachsen', 91),
(3518, 'pt_BR', 'Sachsen-Anhalt', 92),
(3519, 'pt_BR', 'Schleswig-Holstein', 93),
(3520, 'pt_BR', 'Turíngia', 94),
(3521, 'pt_BR', 'Viena', 95),
(3522, 'pt_BR', 'Baixa Áustria', 96),
(3523, 'pt_BR', 'Oberösterreich', 97),
(3524, 'pt_BR', 'Salzburg', 98),
(3525, 'pt_BR', 'Caríntia', 99),
(3526, 'pt_BR', 'Steiermark', 100),
(3527, 'pt_BR', 'Tirol', 101),
(3528, 'pt_BR', 'Burgenland', 102),
(3529, 'pt_BR', 'Vorarlberg', 103),
(3530, 'pt_BR', 'Aargau', 104),
(3531, 'pt_BR', 'Appenzell Innerrhoden', 105),
(3532, 'pt_BR', 'Appenzell Ausserrhoden', 106),
(3533, 'pt_BR', 'Bern', 107),
(3534, 'pt_BR', 'Basel-Landschaft', 108),
(3535, 'pt_BR', 'Basel-Stadt', 109),
(3536, 'pt_BR', 'Freiburg', 110),
(3537, 'pt_BR', 'Genf', 111),
(3538, 'pt_BR', 'Glarus', 112),
(3539, 'pt_BR', 'Grisons', 113),
(3540, 'pt_BR', 'Jura', 114),
(3541, 'pt_BR', 'Luzern', 115),
(3542, 'pt_BR', 'Neuenburg', 116),
(3543, 'pt_BR', 'Nidwalden', 117),
(3544, 'pt_BR', 'Obwalden', 118),
(3545, 'pt_BR', 'St. Gallen', 119),
(3546, 'pt_BR', 'Schaffhausen', 120),
(3547, 'pt_BR', 'Solothurn', 121),
(3548, 'pt_BR', 'Schwyz', 122),
(3549, 'pt_BR', 'Thurgau', 123),
(3550, 'pt_BR', 'Tessin', 124),
(3551, 'pt_BR', 'Uri', 125),
(3552, 'pt_BR', 'Waadt', 126),
(3553, 'pt_BR', 'Wallis', 127),
(3554, 'pt_BR', 'Zug', 128),
(3555, 'pt_BR', 'Zurique', 129),
(3556, 'pt_BR', 'Corunha', 130),
(3557, 'pt_BR', 'Álava', 131),
(3558, 'pt_BR', 'Albacete', 132),
(3559, 'pt_BR', 'Alicante', 133),
(3560, 'pt_BR', 'Almeria', 134),
(3561, 'pt_BR', 'Astúrias', 135),
(3562, 'pt_BR', 'Avila', 136),
(3563, 'pt_BR', 'Badajoz', 137),
(3564, 'pt_BR', 'Baleares', 138),
(3565, 'pt_BR', 'Barcelona', 139),
(3566, 'pt_BR', 'Burgos', 140),
(3567, 'pt_BR', 'Caceres', 141),
(3568, 'pt_BR', 'Cadiz', 142),
(3569, 'pt_BR', 'Cantábria', 143),
(3570, 'pt_BR', 'Castellon', 144),
(3571, 'pt_BR', 'Ceuta', 145),
(3572, 'pt_BR', 'Ciudad Real', 146),
(3573, 'pt_BR', 'Cordoba', 147),
(3574, 'pt_BR', 'Cuenca', 148),
(3575, 'pt_BR', 'Girona', 149),
(3576, 'pt_BR', 'Granada', 150),
(3577, 'pt_BR', 'Guadalajara', 151),
(3578, 'pt_BR', 'Guipuzcoa', 152),
(3579, 'pt_BR', 'Huelva', 153),
(3580, 'pt_BR', 'Huesca', 154),
(3581, 'pt_BR', 'Jaen', 155),
(3582, 'pt_BR', 'La Rioja', 156),
(3583, 'pt_BR', 'Las Palmas', 157),
(3584, 'pt_BR', 'Leon', 158),
(3585, 'pt_BR', 'Lleida', 159),
(3586, 'pt_BR', 'Lugo', 160),
(3587, 'pt_BR', 'Madri', 161),
(3588, 'pt_BR', 'Málaga', 162),
(3589, 'pt_BR', 'Melilla', 163),
(3590, 'pt_BR', 'Murcia', 164),
(3591, 'pt_BR', 'Navarra', 165),
(3592, 'pt_BR', 'Ourense', 166),
(3593, 'pt_BR', 'Palencia', 167),
(3594, 'pt_BR', 'Pontevedra', 168),
(3595, 'pt_BR', 'Salamanca', 169),
(3596, 'pt_BR', 'Santa Cruz de Tenerife', 170),
(3597, 'pt_BR', 'Segovia', 171),
(3598, 'pt_BR', 'Sevilla', 172),
(3599, 'pt_BR', 'Soria', 173),
(3600, 'pt_BR', 'Tarragona', 174),
(3601, 'pt_BR', 'Teruel', 175),
(3602, 'pt_BR', 'Toledo', 176),
(3603, 'pt_BR', 'Valencia', 177),
(3604, 'pt_BR', 'Valladolid', 178),
(3605, 'pt_BR', 'Vizcaya', 179),
(3606, 'pt_BR', 'Zamora', 180),
(3607, 'pt_BR', 'Zaragoza', 181),
(3608, 'pt_BR', 'Ain', 182),
(3609, 'pt_BR', 'Aisne', 183),
(3610, 'pt_BR', 'Allier', 184),
(3611, 'pt_BR', 'Alpes da Alta Provença', 185),
(3612, 'pt_BR', 'Altos Alpes', 186),
(3613, 'pt_BR', 'Alpes-Maritimes', 187),
(3614, 'pt_BR', 'Ardèche', 188),
(3615, 'pt_BR', 'Ardennes', 189),
(3616, 'pt_BR', 'Ariege', 190),
(3617, 'pt_BR', 'Aube', 191),
(3618, 'pt_BR', 'Aude', 192),
(3619, 'pt_BR', 'Aveyron', 193),
(3620, 'pt_BR', 'BOCAS DO Rhône', 194),
(3621, 'pt_BR', 'Calvados', 195),
(3622, 'pt_BR', 'Cantal', 196),
(3623, 'pt_BR', 'Charente', 197),
(3624, 'pt_BR', 'Charente-Maritime', 198),
(3625, 'pt_BR', 'Cher', 199),
(3626, 'pt_BR', 'Corrèze', 200),
(3627, 'pt_BR', 'Corse-du-Sud', 201),
(3628, 'pt_BR', 'Alta Córsega', 202),
(3629, 'pt_BR', 'Costa d\'OrCorrèze', 203),
(3630, 'pt_BR', 'Cotes d\'Armor', 204),
(3631, 'pt_BR', 'Creuse', 205),
(3632, 'pt_BR', 'Dordogne', 206),
(3633, 'pt_BR', 'Doubs', 207),
(3634, 'pt_BR', 'DrômeFinistère', 208),
(3635, 'pt_BR', 'Eure', 209),
(3636, 'pt_BR', 'Eure-et-Loir', 210),
(3637, 'pt_BR', 'Finistère', 211),
(3638, 'pt_BR', 'Gard', 212),
(3639, 'pt_BR', 'Haute-Garonne', 213),
(3640, 'pt_BR', 'Gers', 214),
(3641, 'pt_BR', 'Gironde', 215),
(3642, 'pt_BR', 'Hérault', 216),
(3643, 'pt_BR', 'Ille-et-Vilaine', 217),
(3644, 'pt_BR', 'Indre', 218),
(3645, 'pt_BR', 'Indre-et-Loire', 219),
(3646, 'pt_BR', 'Isère', 220),
(3647, 'pt_BR', 'Jura', 221),
(3648, 'pt_BR', 'Landes', 222),
(3649, 'pt_BR', 'Loir-et-Cher', 223),
(3650, 'pt_BR', 'Loire', 224),
(3651, 'pt_BR', 'Haute-Loire', 22),
(3652, 'pt_BR', 'Loire-Atlantique', 226),
(3653, 'pt_BR', 'Loiret', 227),
(3654, 'pt_BR', 'Lot', 228),
(3655, 'pt_BR', 'Lot e Garona', 229),
(3656, 'pt_BR', 'Lozère', 230),
(3657, 'pt_BR', 'Maine-et-Loire', 231),
(3658, 'pt_BR', 'Manche', 232),
(3659, 'pt_BR', 'Marne', 233),
(3660, 'pt_BR', 'Haute-Marne', 234),
(3661, 'pt_BR', 'Mayenne', 235),
(3662, 'pt_BR', 'Meurthe-et-Moselle', 236),
(3663, 'pt_BR', 'Meuse', 237),
(3664, 'pt_BR', 'Morbihan', 238),
(3665, 'pt_BR', 'Moselle', 239),
(3666, 'pt_BR', 'Nièvre', 240),
(3667, 'pt_BR', 'Nord', 241),
(3668, 'pt_BR', 'Oise', 242),
(3669, 'pt_BR', 'Orne', 243),
(3670, 'pt_BR', 'Pas-de-Calais', 244),
(3671, 'pt_BR', 'Puy-de-Dôme', 24),
(3672, 'pt_BR', 'Pirineus Atlânticos', 246),
(3673, 'pt_BR', 'Hautes-Pyrénées', 247),
(3674, 'pt_BR', 'Pirineus Orientais', 248),
(3675, 'pt_BR', 'Bas-Rhin', 249),
(3676, 'pt_BR', 'Alto Reno', 250),
(3677, 'pt_BR', 'Rhône', 251),
(3678, 'pt_BR', 'Haute-Saône', 252),
(3679, 'pt_BR', 'Saône-et-Loire', 253),
(3680, 'pt_BR', 'Sarthe', 25),
(3681, 'pt_BR', 'Savoie', 255),
(3682, 'pt_BR', 'Alta Sabóia', 256),
(3683, 'pt_BR', 'Paris', 257),
(3684, 'pt_BR', 'Seine-Maritime', 258),
(3685, 'pt_BR', 'Seine-et-Marne', 259),
(3686, 'pt_BR', 'Yvelines', 260),
(3687, 'pt_BR', 'Deux-Sèvres', 261),
(3688, 'pt_BR', 'Somme', 262),
(3689, 'pt_BR', 'Tarn', 263),
(3690, 'pt_BR', 'Tarn-et-Garonne', 264),
(3691, 'pt_BR', 'Var', 265),
(3692, 'pt_BR', 'Vaucluse', 266),
(3693, 'pt_BR', 'Compradora', 267),
(3694, 'pt_BR', 'Vienne', 268),
(3695, 'pt_BR', 'Haute-Vienne', 269),
(3696, 'pt_BR', 'Vosges', 270),
(3697, 'pt_BR', 'Yonne', 271),
(3698, 'pt_BR', 'Território de Belfort', 272),
(3699, 'pt_BR', 'Essonne', 273),
(3700, 'pt_BR', 'Altos do Sena', 274),
(3701, 'pt_BR', 'Seine-Saint-Denis', 275),
(3702, 'pt_BR', 'Val-de-Marne', 276),
(3703, 'pt_BR', 'Val-d\'Oise', 277),
(3704, 'pt_BR', 'Alba', 278),
(3705, 'pt_BR', 'Arad', 279),
(3706, 'pt_BR', 'Arges', 280),
(3707, 'pt_BR', 'Bacau', 281),
(3708, 'pt_BR', 'Bihor', 282),
(3709, 'pt_BR', 'Bistrita-Nasaud', 283),
(3710, 'pt_BR', 'Botosani', 284),
(3711, 'pt_BR', 'Brașov', 285),
(3712, 'pt_BR', 'Braila', 286),
(3713, 'pt_BR', 'Bucareste', 287),
(3714, 'pt_BR', 'Buzau', 288),
(3715, 'pt_BR', 'Caras-Severin', 289),
(3716, 'pt_BR', 'Călărași', 290),
(3717, 'pt_BR', 'Cluj', 291),
(3718, 'pt_BR', 'Constanta', 292),
(3719, 'pt_BR', 'Covasna', 29),
(3720, 'pt_BR', 'Dambovita', 294),
(3721, 'pt_BR', 'Dolj', 295),
(3722, 'pt_BR', 'Galati', 296),
(3723, 'pt_BR', 'Giurgiu', 297),
(3724, 'pt_BR', 'Gorj', 298),
(3725, 'pt_BR', 'Harghita', 299),
(3726, 'pt_BR', 'Hunedoara', 300),
(3727, 'pt_BR', 'Ialomita', 301),
(3728, 'pt_BR', 'Iasi', 302),
(3729, 'pt_BR', 'Ilfov', 303),
(3730, 'pt_BR', 'Maramures', 304),
(3731, 'pt_BR', 'Maramures', 305),
(3732, 'pt_BR', 'Mures', 306),
(3733, 'pt_BR', 'alemão', 307),
(3734, 'pt_BR', 'Olt', 308),
(3735, 'pt_BR', 'Prahova', 309),
(3736, 'pt_BR', 'Satu-Mare', 310),
(3737, 'pt_BR', 'Salaj', 311),
(3738, 'pt_BR', 'Sibiu', 312),
(3739, 'pt_BR', 'Suceava', 313),
(3740, 'pt_BR', 'Teleorman', 314),
(3741, 'pt_BR', 'Timis', 315),
(3742, 'pt_BR', 'Tulcea', 316),
(3743, 'pt_BR', 'Vaslui', 317),
(3744, 'pt_BR', 'dale', 318),
(3745, 'pt_BR', 'Vrancea', 319),
(3746, 'pt_BR', 'Lappi', 320),
(3747, 'pt_BR', 'Pohjois-Pohjanmaa', 321),
(3748, 'pt_BR', 'Kainuu', 322),
(3749, 'pt_BR', 'Pohjois-Karjala', 323),
(3750, 'pt_BR', 'Pohjois-Savo', 324),
(3751, 'pt_BR', 'Sul Savo', 325),
(3752, 'pt_BR', 'Ostrobothnia do sul', 326),
(3753, 'pt_BR', 'Pohjanmaa', 327),
(3754, 'pt_BR', 'Pirkanmaa', 328),
(3755, 'pt_BR', 'Satakunta', 329),
(3756, 'pt_BR', 'Keski-Pohjanmaa', 330),
(3757, 'pt_BR', 'Keski-Suomi', 331),
(3758, 'pt_BR', 'Varsinais-Suomi', 332),
(3759, 'pt_BR', 'Carélia do Sul', 333),
(3760, 'pt_BR', 'Päijät-Häme', 334),
(3761, 'pt_BR', 'Kanta-Häme', 335),
(3762, 'pt_BR', 'Uusimaa', 336),
(3763, 'pt_BR', 'Uusimaa', 337),
(3764, 'pt_BR', 'Kymenlaakso', 338),
(3765, 'pt_BR', 'Ahvenanmaa', 339),
(3766, 'pt_BR', 'Harjumaa', 340),
(3767, 'pt_BR', 'Hiiumaa', 341),
(3768, 'pt_BR', 'Ida-Virumaa', 342),
(3769, 'pt_BR', 'Condado de Jõgeva', 343),
(3770, 'pt_BR', 'Condado de Järva', 344),
(3771, 'pt_BR', 'Läänemaa', 345),
(3772, 'pt_BR', 'Condado de Lääne-Viru', 346),
(3773, 'pt_BR', 'Condado de Põlva', 347),
(3774, 'pt_BR', 'Condado de Pärnu', 348),
(3775, 'pt_BR', 'Raplamaa', 349),
(3776, 'pt_BR', 'Saaremaa', 350),
(3777, 'pt_BR', 'Tartumaa', 351),
(3778, 'pt_BR', 'Valgamaa', 352),
(3779, 'pt_BR', 'Viljandimaa', 353),
(3780, 'pt_BR', 'Võrumaa', 354),
(3781, 'pt_BR', 'Daugavpils', 355),
(3782, 'pt_BR', 'Jelgava', 356),
(3783, 'pt_BR', 'Jekabpils', 357),
(3784, 'pt_BR', 'Jurmala', 358),
(3785, 'pt_BR', 'Liepaja', 359),
(3786, 'pt_BR', 'Liepaja County', 360),
(3787, 'pt_BR', 'Rezekne', 361),
(3788, 'pt_BR', 'Riga', 362),
(3789, 'pt_BR', 'Condado de Riga', 363),
(3790, 'pt_BR', 'Valmiera', 364),
(3791, 'pt_BR', 'Ventspils', 365),
(3792, 'pt_BR', 'Aglonas novads', 366),
(3793, 'pt_BR', 'Aizkraukles novads', 367),
(3794, 'pt_BR', 'Aizputes novads', 368),
(3795, 'pt_BR', 'Condado de Akniste', 369),
(3796, 'pt_BR', 'Alojas novads', 370),
(3797, 'pt_BR', 'Alsungas novads', 371),
(3798, 'pt_BR', 'Aluksne County', 372),
(3799, 'pt_BR', 'Amatas novads', 373),
(3800, 'pt_BR', 'Macacos novads', 374),
(3801, 'pt_BR', 'Auces novads', 375),
(3802, 'pt_BR', 'Babītes novads', 376),
(3803, 'pt_BR', 'Baldones novads', 377),
(3804, 'pt_BR', 'Baltinavas novads', 378),
(3805, 'pt_BR', 'Balvu novads', 379),
(3806, 'pt_BR', 'Bauskas novads', 380),
(3807, 'pt_BR', 'Condado de Beverina', 381),
(3808, 'pt_BR', 'Condado de Broceni', 382),
(3809, 'pt_BR', 'Burtnieku novads', 383),
(3810, 'pt_BR', 'Carnikavas novads', 384),
(3811, 'pt_BR', 'Cesvaines novads', 385),
(3812, 'pt_BR', 'Ciblas novads', 386),
(3813, 'pt_BR', 'Cesis county', 387),
(3814, 'pt_BR', 'Dagdas novads', 388),
(3815, 'pt_BR', 'Daugavpils novads', 389),
(3816, 'pt_BR', 'Dobeles novads', 390),
(3817, 'pt_BR', 'Dundagas novads', 391),
(3818, 'pt_BR', 'Durbes novads', 392),
(3819, 'pt_BR', 'Engad novads', 393),
(3820, 'pt_BR', 'Garkalnes novads', 394),
(3821, 'pt_BR', 'O condado de Grobiņa', 395),
(3822, 'pt_BR', 'Gulbenes novads', 396),
(3823, 'pt_BR', 'Iecavas novads', 397),
(3824, 'pt_BR', 'Ikskile county', 398),
(3825, 'pt_BR', 'Ilūkste county', 399),
(3826, 'pt_BR', 'Condado de Inčukalns', 400),
(3827, 'pt_BR', 'Jaunjelgavas novads', 401),
(3828, 'pt_BR', 'Jaunpiebalgas novads', 402),
(3829, 'pt_BR', 'Jaunpils novads', 403),
(3830, 'pt_BR', 'Jelgavas novads', 404),
(3831, 'pt_BR', 'Jekabpils county', 405),
(3832, 'pt_BR', 'Kandavas novads', 406),
(3833, 'pt_BR', 'Kokneses novads', 407),
(3834, 'pt_BR', 'Krimuldas novads', 408),
(3835, 'pt_BR', 'Krustpils novads', 409),
(3836, 'pt_BR', 'Condado de Kraslava', 410),
(3837, 'pt_BR', 'Condado de Kuldīga', 411),
(3838, 'pt_BR', 'Condado de Kārsava', 412),
(3839, 'pt_BR', 'Condado de Lielvarde', 413),
(3840, 'pt_BR', 'Condado de Limbaži', 414),
(3841, 'pt_BR', 'O distrito de Lubāna', 415),
(3842, 'pt_BR', 'Ludzas novads', 416),
(3843, 'pt_BR', 'Ligatne county', 417),
(3844, 'pt_BR', 'Livani county', 418),
(3845, 'pt_BR', 'Madonas novads', 419),
(3846, 'pt_BR', 'Mazsalacas novads', 420),
(3847, 'pt_BR', 'Mālpils county', 421),
(3848, 'pt_BR', 'Mārupe county', 422),
(3849, 'pt_BR', 'O condado de Naukšēni', 423),
(3850, 'pt_BR', 'Neretas novads', 424),
(3851, 'pt_BR', 'Nīca county', 425),
(3852, 'pt_BR', 'Ogres novads', 426),
(3853, 'pt_BR', 'Olaines novads', 427),
(3854, 'pt_BR', 'Ozolnieku novads', 428),
(3855, 'pt_BR', 'Preiļi county', 429),
(3856, 'pt_BR', 'Priekules novads', 430),
(3857, 'pt_BR', 'Condado de Priekuļi', 431),
(3858, 'pt_BR', 'Moving county', 432),
(3859, 'pt_BR', 'Condado de Pavilosta', 433),
(3860, 'pt_BR', 'Condado de Plavinas', 434),
(3861, 'pt_BR', 'Raunas novads', 435),
(3862, 'pt_BR', 'Condado de Riebiņi', 436),
(3863, 'pt_BR', 'Rojas novads', 437),
(3864, 'pt_BR', 'Ropazi county', 438),
(3865, 'pt_BR', 'Rucavas novads', 439),
(3866, 'pt_BR', 'Rugāji county', 440),
(3867, 'pt_BR', 'Rundāle county', 441),
(3868, 'pt_BR', 'Rezekne county', 442),
(3869, 'pt_BR', 'Rūjiena county', 443),
(3870, 'pt_BR', 'O município de Salacgriva', 444),
(3871, 'pt_BR', 'Salas novads', 445),
(3872, 'pt_BR', 'Salaspils novads', 446),
(3873, 'pt_BR', 'Saldus novads', 447),
(3874, 'pt_BR', 'Saulkrastu novads', 448),
(3875, 'pt_BR', 'Siguldas novads', 449),
(3876, 'pt_BR', 'Skrundas novads', 450),
(3877, 'pt_BR', 'Skrīveri county', 451),
(3878, 'pt_BR', 'Smiltenes novads', 452),
(3879, 'pt_BR', 'Condado de Stopini', 453),
(3880, 'pt_BR', 'Condado de Strenči', 454),
(3881, 'pt_BR', 'Região de semeadura', 455),
(3882, 'pt_BR', 'Talsu novads', 456),
(3883, 'pt_BR', 'Tukuma novads', 457),
(3884, 'pt_BR', 'Condado de Tērvete', 458),
(3885, 'pt_BR', 'O condado de Vaiņode', 459),
(3886, 'pt_BR', 'Valkas novads', 460),
(3887, 'pt_BR', 'Valmieras novads', 461),
(3888, 'pt_BR', 'Varaklani county', 462),
(3889, 'pt_BR', 'Vecpiebalgas novads', 463),
(3890, 'pt_BR', 'Vecumnieku novads', 464),
(3891, 'pt_BR', 'Ventspils novads', 465),
(3892, 'pt_BR', 'Condado de Viesite', 466),
(3893, 'pt_BR', 'Condado de Vilaka', 467),
(3894, 'pt_BR', 'Vilani county', 468),
(3895, 'pt_BR', 'Condado de Varkava', 469),
(3896, 'pt_BR', 'Zilupes novads', 470),
(3897, 'pt_BR', 'Adazi county', 471),
(3898, 'pt_BR', 'Erglu county', 472),
(3899, 'pt_BR', 'Kegums county', 473),
(3900, 'pt_BR', 'Kekava county', 474),
(3901, 'pt_BR', 'Alytaus Apskritis', 475),
(3902, 'pt_BR', 'Kauno Apskritis', 476),
(3903, 'pt_BR', 'Condado de Klaipeda', 477),
(3904, 'pt_BR', 'Marijampolė county', 478),
(3905, 'pt_BR', 'Panevezys county', 479),
(3906, 'pt_BR', 'Siauliai county', 480),
(3907, 'pt_BR', 'Taurage county', 481),
(3908, 'pt_BR', 'Telšiai county', 482),
(3909, 'pt_BR', 'Utenos Apskritis', 483),
(3910, 'pt_BR', 'Vilniaus Apskritis', 484),
(3911, 'pt_BR', 'Acre', 485),
(3912, 'pt_BR', 'Alagoas', 486),
(3913, 'pt_BR', 'Amapá', 487),
(3914, 'pt_BR', 'Amazonas', 488),
(3915, 'pt_BR', 'Bahia', 489),
(3916, 'pt_BR', 'Ceará', 490),
(3917, 'pt_BR', 'Espírito Santo', 491),
(3918, 'pt_BR', 'Goiás', 492),
(3919, 'pt_BR', 'Maranhão', 493),
(3920, 'pt_BR', 'Mato Grosso', 494),
(3921, 'pt_BR', 'Mato Grosso do Sul', 495),
(3922, 'pt_BR', 'Minas Gerais', 496),
(3923, 'pt_BR', 'Pará', 497),
(3924, 'pt_BR', 'Paraíba', 498),
(3925, 'pt_BR', 'Paraná', 499),
(3926, 'pt_BR', 'Pernambuco', 500),
(3927, 'pt_BR', 'Piauí', 501),
(3928, 'pt_BR', 'Rio de Janeiro', 502),
(3929, 'pt_BR', 'Rio Grande do Norte', 503),
(3930, 'pt_BR', 'Rio Grande do Sul', 504),
(3931, 'pt_BR', 'Rondônia', 505),
(3932, 'pt_BR', 'Roraima', 506),
(3933, 'pt_BR', 'Santa Catarina', 507),
(3934, 'pt_BR', 'São Paulo', 508),
(3935, 'pt_BR', 'Sergipe', 509),
(3936, 'pt_BR', 'Tocantins', 510),
(3937, 'pt_BR', 'Distrito Federal', 511),
(3938, 'pt_BR', 'Condado de Zagreb', 512),
(3939, 'pt_BR', 'Condado de Krapina-Zagorje', 513),
(3940, 'pt_BR', 'Condado de Sisak-Moslavina', 514),
(3941, 'pt_BR', 'Condado de Karlovac', 515),
(3942, 'pt_BR', 'Concelho de Varaždin', 516),
(3943, 'pt_BR', 'Condado de Koprivnica-Križevci', 517),
(3944, 'pt_BR', 'Condado de Bjelovar-Bilogora', 518),
(3945, 'pt_BR', 'Condado de Primorje-Gorski kotar', 519),
(3946, 'pt_BR', 'Condado de Lika-Senj', 520),
(3947, 'pt_BR', 'Condado de Virovitica-Podravina', 521),
(3948, 'pt_BR', 'Condado de Požega-Slavonia', 522),
(3949, 'pt_BR', 'Condado de Brod-Posavina', 523),
(3950, 'pt_BR', 'Condado de Zadar', 524),
(3951, 'pt_BR', 'Condado de Osijek-Baranja', 525),
(3952, 'pt_BR', 'Condado de Šibenik-Knin', 526),
(3953, 'pt_BR', 'Condado de Vukovar-Srijem', 527),
(3954, 'pt_BR', 'Condado de Split-Dalmácia', 528),
(3955, 'pt_BR', 'Condado de Ístria', 529),
(3956, 'pt_BR', 'Condado de Dubrovnik-Neretva', 530),
(3957, 'pt_BR', 'Međimurska županija', 531),
(3958, 'pt_BR', 'Grad Zagreb', 532),
(3959, 'pt_BR', 'Ilhas Andaman e Nicobar', 533),
(3960, 'pt_BR', 'Andhra Pradesh', 534),
(3961, 'pt_BR', 'Arunachal Pradesh', 535),
(3962, 'pt_BR', 'Assam', 536),
(3963, 'pt_BR', 'Bihar', 537),
(3964, 'pt_BR', 'Chandigarh', 538),
(3965, 'pt_BR', 'Chhattisgarh', 539),
(3966, 'pt_BR', 'Dadra e Nagar Haveli', 540),
(3967, 'pt_BR', 'Daman e Diu', 541),
(3968, 'pt_BR', 'Delhi', 542),
(3969, 'pt_BR', 'Goa', 543),
(3970, 'pt_BR', 'Gujarat', 544),
(3971, 'pt_BR', 'Haryana', 545),
(3972, 'pt_BR', 'Himachal Pradesh', 546),
(3973, 'pt_BR', 'Jammu e Caxemira', 547),
(3974, 'pt_BR', 'Jharkhand', 548),
(3975, 'pt_BR', 'Karnataka', 549),
(3976, 'pt_BR', 'Kerala', 550),
(3977, 'pt_BR', 'Lakshadweep', 551),
(3978, 'pt_BR', 'Madhya Pradesh', 552),
(3979, 'pt_BR', 'Maharashtra', 553),
(3980, 'pt_BR', 'Manipur', 554),
(3981, 'pt_BR', 'Meghalaya', 555),
(3982, 'pt_BR', 'Mizoram', 556),
(3983, 'pt_BR', 'Nagaland', 557),
(3984, 'pt_BR', 'Odisha', 558),
(3985, 'pt_BR', 'Puducherry', 559),
(3986, 'pt_BR', 'Punjab', 560),
(3987, 'pt_BR', 'Rajasthan', 561),
(3988, 'pt_BR', 'Sikkim', 562),
(3989, 'pt_BR', 'Tamil Nadu', 563),
(3990, 'pt_BR', 'Telangana', 564),
(3991, 'pt_BR', 'Tripura', 565),
(3992, 'pt_BR', 'Uttar Pradesh', 566),
(3993, 'pt_BR', 'Uttarakhand', 567),
(3994, 'pt_BR', 'Bengala Ocidental', 568);

-- --------------------------------------------------------

--
-- Estrutura da tabela `country_translations`
--

CREATE TABLE `country_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `country_translations`
--

INSERT INTO `country_translations` (`id`, `locale`, `name`, `country_id`) VALUES
(766, 'ar', 'أفغانستان', 1),
(767, 'ar', 'جزر آلاند', 2),
(768, 'ar', 'ألبانيا', 3),
(769, 'ar', 'الجزائر', 4),
(770, 'ar', 'ساموا الأمريكية', 5),
(771, 'ar', 'أندورا', 6),
(772, 'ar', 'أنغولا', 7),
(773, 'ar', 'أنغيلا', 8),
(774, 'ar', 'القارة القطبية الجنوبية', 9),
(775, 'ar', 'أنتيغوا وبربودا', 10),
(776, 'ar', 'الأرجنتين', 11),
(777, 'ar', 'أرمينيا', 12),
(778, 'ar', 'أروبا', 13),
(779, 'ar', 'جزيرة الصعود', 14),
(780, 'ar', 'أستراليا', 15),
(781, 'ar', 'النمسا', 16),
(782, 'ar', 'أذربيجان', 17),
(783, 'ar', 'الباهاما', 18),
(784, 'ar', 'البحرين', 19),
(785, 'ar', 'بنغلاديش', 20),
(786, 'ar', 'بربادوس', 21),
(787, 'ar', 'روسيا البيضاء', 22),
(788, 'ar', 'بلجيكا', 23),
(789, 'ar', 'بليز', 24),
(790, 'ar', 'بنين', 25),
(791, 'ar', 'برمودا', 26),
(792, 'ar', 'بوتان', 27),
(793, 'ar', 'بوليفيا', 28),
(794, 'ar', 'البوسنة والهرسك', 29),
(795, 'ar', 'بوتسوانا', 30),
(796, 'ar', 'البرازيل', 31),
(797, 'ar', 'إقليم المحيط البريطاني الهندي', 32),
(798, 'ar', 'جزر فيرجن البريطانية', 33),
(799, 'ar', 'بروناي', 34),
(800, 'ar', 'بلغاريا', 35),
(801, 'ar', 'بوركينا فاسو', 36),
(802, 'ar', 'بوروندي', 37),
(803, 'ar', 'كمبوديا', 38),
(804, 'ar', 'الكاميرون', 39),
(805, 'ar', 'كندا', 40),
(806, 'ar', 'جزر الكناري', 41),
(807, 'ar', 'الرأس الأخضر', 42),
(808, 'ar', 'الكاريبي هولندا', 43),
(809, 'ar', 'جزر كايمان', 44),
(810, 'ar', 'جمهورية افريقيا الوسطى', 45),
(811, 'ar', 'سبتة ومليلية', 46),
(812, 'ar', 'تشاد', 47),
(813, 'ar', 'تشيلي', 48),
(814, 'ar', 'الصين', 49),
(815, 'ar', 'جزيرة الكريسماس', 50),
(816, 'ar', 'جزر كوكوس (كيلينغ)', 51),
(817, 'ar', 'كولومبيا', 52),
(818, 'ar', 'جزر القمر', 53),
(819, 'ar', 'الكونغو - برازافيل', 54),
(820, 'ar', 'الكونغو - كينشاسا', 55),
(821, 'ar', 'جزر كوك', 56),
(822, 'ar', 'كوستاريكا', 57),
(823, 'ar', 'ساحل العاج', 58),
(824, 'ar', 'كرواتيا', 59),
(825, 'ar', 'كوبا', 60),
(826, 'ar', 'كوراساو', 61),
(827, 'ar', 'قبرص', 62),
(828, 'ar', 'التشيك', 63),
(829, 'ar', 'الدنمارك', 64),
(830, 'ar', 'دييغو غارسيا', 65),
(831, 'ar', 'جيبوتي', 66),
(832, 'ar', 'دومينيكا', 67),
(833, 'ar', 'جمهورية الدومنيكان', 68),
(834, 'ar', 'الإكوادور', 69),
(835, 'ar', 'مصر', 70),
(836, 'ar', 'السلفادور', 71),
(837, 'ar', 'غينيا الإستوائية', 72),
(838, 'ar', 'إريتريا', 73),
(839, 'ar', 'استونيا', 74),
(840, 'ar', 'أثيوبيا', 75),
(841, 'ar', 'منطقة اليورو', 76),
(842, 'ar', 'جزر فوكلاند', 77),
(843, 'ar', 'جزر فاروس', 78),
(844, 'ar', 'فيجي', 79),
(845, 'ar', 'فنلندا', 80),
(846, 'ar', 'فرنسا', 81),
(847, 'ar', 'غيانا الفرنسية', 82),
(848, 'ar', 'بولينيزيا الفرنسية', 83),
(849, 'ar', 'المناطق الجنوبية لفرنسا', 84),
(850, 'ar', 'الغابون', 85),
(851, 'ar', 'غامبيا', 86),
(852, 'ar', 'جورجيا', 87),
(853, 'ar', 'ألمانيا', 88),
(854, 'ar', 'غانا', 89),
(855, 'ar', 'جبل طارق', 90),
(856, 'ar', 'اليونان', 91),
(857, 'ar', 'الأرض الخضراء', 92),
(858, 'ar', 'غرينادا', 93),
(859, 'ar', 'جوادلوب', 94),
(860, 'ar', 'غوام', 95),
(861, 'ar', 'غواتيمالا', 96),
(862, 'ar', 'غيرنسي', 97),
(863, 'ar', 'غينيا', 98),
(864, 'ar', 'غينيا بيساو', 99),
(865, 'ar', 'غيانا', 100),
(866, 'ar', 'هايتي', 101),
(867, 'ar', 'هندوراس', 102),
(868, 'ar', 'هونج كونج SAR الصين', 103),
(869, 'ar', 'هنغاريا', 104),
(870, 'ar', 'أيسلندا', 105),
(871, 'ar', 'الهند', 106),
(872, 'ar', 'إندونيسيا', 107),
(873, 'ar', 'إيران', 108),
(874, 'ar', 'العراق', 109),
(875, 'ar', 'أيرلندا', 110),
(876, 'ar', 'جزيرة آيل أوف مان', 111),
(877, 'ar', 'إسرائيل', 112),
(878, 'ar', 'إيطاليا', 113),
(879, 'ar', 'جامايكا', 114),
(880, 'ar', 'اليابان', 115),
(881, 'ar', 'جيرسي', 116),
(882, 'ar', 'الأردن', 117),
(883, 'ar', 'كازاخستان', 118),
(884, 'ar', 'كينيا', 119),
(885, 'ar', 'كيريباس', 120),
(886, 'ar', 'كوسوفو', 121),
(887, 'ar', 'الكويت', 122),
(888, 'ar', 'قرغيزستان', 123),
(889, 'ar', 'لاوس', 124),
(890, 'ar', 'لاتفيا', 125),
(891, 'ar', 'لبنان', 126),
(892, 'ar', 'ليسوتو', 127),
(893, 'ar', 'ليبيريا', 128),
(894, 'ar', 'ليبيا', 129),
(895, 'ar', 'ليختنشتاين', 130),
(896, 'ar', 'ليتوانيا', 131),
(897, 'ar', 'لوكسمبورغ', 132),
(898, 'ar', 'ماكاو SAR الصين', 133),
(899, 'ar', 'مقدونيا', 134),
(900, 'ar', 'مدغشقر', 135),
(901, 'ar', 'مالاوي', 136),
(902, 'ar', 'ماليزيا', 137),
(903, 'ar', 'جزر المالديف', 138),
(904, 'ar', 'مالي', 139),
(905, 'ar', 'مالطا', 140),
(906, 'ar', 'جزر مارشال', 141),
(907, 'ar', 'مارتينيك', 142),
(908, 'ar', 'موريتانيا', 143),
(909, 'ar', 'موريشيوس', 144),
(910, 'ar', 'ضائع', 145),
(911, 'ar', 'المكسيك', 146),
(912, 'ar', 'ميكرونيزيا', 147),
(913, 'ar', 'مولدوفا', 148),
(914, 'ar', 'موناكو', 149),
(915, 'ar', 'منغوليا', 150),
(916, 'ar', 'الجبل الأسود', 151),
(917, 'ar', 'مونتسيرات', 152),
(918, 'ar', 'المغرب', 153),
(919, 'ar', 'موزمبيق', 154),
(920, 'ar', 'ميانمار (بورما)', 155),
(921, 'ar', 'ناميبيا', 156),
(922, 'ar', 'ناورو', 157),
(923, 'ar', 'نيبال', 158),
(924, 'ar', 'نيبال', 159),
(925, 'ar', 'كاليدونيا الجديدة', 160),
(926, 'ar', 'نيوزيلاندا', 161),
(927, 'ar', 'نيكاراغوا', 162),
(928, 'ar', 'النيجر', 163),
(929, 'ar', 'نيجيريا', 164),
(930, 'ar', 'نيوي', 165),
(931, 'ar', 'جزيرة نورفولك', 166),
(932, 'ar', 'كوريا الشماليه', 167),
(933, 'ar', 'جزر مريانا الشمالية', 168),
(934, 'ar', 'النرويج', 169),
(935, 'ar', 'سلطنة عمان', 170),
(936, 'ar', 'باكستان', 171),
(937, 'ar', 'بالاو', 172),
(938, 'ar', 'الاراضي الفلسطينية', 173),
(939, 'ar', 'بناما', 174),
(940, 'ar', 'بابوا غينيا الجديدة', 175),
(941, 'ar', 'باراغواي', 176),
(942, 'ar', 'بيرو', 177),
(943, 'ar', 'الفلبين', 178),
(944, 'ar', 'جزر بيتكيرن', 179),
(945, 'ar', 'بولندا', 180),
(946, 'ar', 'البرتغال', 181),
(947, 'ar', 'بورتوريكو', 182),
(948, 'ar', 'دولة قطر', 183),
(949, 'ar', 'جمع شمل', 184),
(950, 'ar', 'رومانيا', 185),
(951, 'ar', 'روسيا', 186),
(952, 'ar', 'رواندا', 187),
(953, 'ar', 'ساموا', 188),
(954, 'ar', 'سان مارينو', 189),
(955, 'ar', 'سانت كيتس ونيفيس', 190),
(956, 'ar', 'المملكة العربية السعودية', 191),
(957, 'ar', 'السنغال', 192),
(958, 'ar', 'صربيا', 193),
(959, 'ar', 'سيشيل', 194),
(960, 'ar', 'سيراليون', 195),
(961, 'ar', 'سنغافورة', 196),
(962, 'ar', 'سينت مارتن', 197),
(963, 'ar', 'سلوفاكيا', 198),
(964, 'ar', 'سلوفينيا', 199),
(965, 'ar', 'جزر سليمان', 200),
(966, 'ar', 'الصومال', 201),
(967, 'ar', 'جنوب أفريقيا', 202),
(968, 'ar', 'جورجيا الجنوبية وجزر ساندويتش الجنوبية', 203),
(969, 'ar', 'كوريا الجنوبية', 204),
(970, 'ar', 'جنوب السودان', 205),
(971, 'ar', 'إسبانيا', 206),
(972, 'ar', 'سيريلانكا', 207),
(973, 'ar', 'سانت بارتيليمي', 208),
(974, 'ar', 'سانت هيلانة', 209),
(975, 'ar', 'سانت كيتس ونيفيس', 210),
(976, 'ar', 'شارع لوسيا', 211),
(977, 'ar', 'سانت مارتن', 212),
(978, 'ar', 'سانت بيير وميكلون', 213),
(979, 'ar', 'سانت فنسنت وجزر غرينادين', 214),
(980, 'ar', 'السودان', 215),
(981, 'ar', 'سورينام', 216),
(982, 'ar', 'سفالبارد وجان ماين', 217),
(983, 'ar', 'سوازيلاند', 218),
(984, 'ar', 'السويد', 219),
(985, 'ar', 'سويسرا', 220),
(986, 'ar', 'سوريا', 221),
(987, 'ar', 'تايوان', 222),
(988, 'ar', 'طاجيكستان', 223),
(989, 'ar', 'تنزانيا', 224),
(990, 'ar', 'تايلاند', 225),
(991, 'ar', 'تيمور', 226),
(992, 'ar', 'توجو', 227),
(993, 'ar', 'توكيلاو', 228),
(994, 'ar', 'تونغا', 229),
(995, 'ar', 'ترينيداد وتوباغو', 230),
(996, 'ar', 'تريستان دا كونها', 231),
(997, 'ar', 'تونس', 232),
(998, 'ar', 'ديك رومي', 233),
(999, 'ar', 'تركمانستان', 234),
(1000, 'ar', 'جزر تركس وكايكوس', 235),
(1001, 'ar', 'توفالو', 236),
(1002, 'ar', 'جزر الولايات المتحدة البعيدة', 237),
(1003, 'ar', 'جزر فيرجن الأمريكية', 238),
(1004, 'ar', 'أوغندا', 239),
(1005, 'ar', 'أوكرانيا', 240),
(1006, 'ar', 'الإمارات العربية المتحدة', 241),
(1007, 'ar', 'المملكة المتحدة', 242),
(1008, 'ar', 'الأمم المتحدة', 243),
(1009, 'ar', 'الولايات المتحدة الأمريكية', 244),
(1010, 'ar', 'أوروغواي', 245),
(1011, 'ar', 'أوزبكستان', 246),
(1012, 'ar', 'فانواتو', 247),
(1013, 'ar', 'مدينة الفاتيكان', 248),
(1014, 'ar', 'فنزويلا', 249),
(1015, 'ar', 'فيتنام', 250),
(1016, 'ar', 'واليس وفوتونا', 251),
(1017, 'ar', 'الصحراء الغربية', 252),
(1018, 'ar', 'اليمن', 253),
(1019, 'ar', 'زامبيا', 254),
(1020, 'ar', 'زيمبابوي', 255),
(1021, 'es', 'Afganistán', 1),
(1022, 'es', 'Islas Åland', 2),
(1023, 'es', 'Albania', 3),
(1024, 'es', 'Argelia', 4),
(1025, 'es', 'Samoa Americana', 5),
(1026, 'es', 'Andorra', 6),
(1027, 'es', 'Angola', 7),
(1028, 'es', 'Anguila', 8),
(1029, 'es', 'Antártida', 9),
(1030, 'es', 'Antigua y Barbuda', 10),
(1031, 'es', 'Argentina', 11),
(1032, 'es', 'Armenia', 12),
(1033, 'es', 'Aruba', 13),
(1034, 'es', 'Isla Ascensión', 14),
(1035, 'es', 'Australia', 15),
(1036, 'es', 'Austria', 16),
(1037, 'es', 'Azerbaiyán', 17),
(1038, 'es', 'Bahamas', 18),
(1039, 'es', 'Bahrein', 19),
(1040, 'es', 'Bangladesh', 20),
(1041, 'es', 'Barbados', 21),
(1042, 'es', 'Bielorrusia', 22),
(1043, 'es', 'Bélgica', 23),
(1044, 'es', 'Belice', 24),
(1045, 'es', 'Benín', 25),
(1046, 'es', 'Islas Bermudas', 26),
(1047, 'es', 'Bhután', 27),
(1048, 'es', 'Bolivia', 28),
(1049, 'es', 'Bosnia y Herzegovina', 29),
(1050, 'es', 'Botsuana', 30),
(1051, 'es', 'Brasil', 31),
(1052, 'es', 'Territorio Británico del Océano índico', 32),
(1053, 'es', 'Islas Vírgenes Británicas', 33),
(1054, 'es', 'Brunéi', 34),
(1055, 'es', 'Bulgaria', 35),
(1056, 'es', 'Burkina Faso', 36),
(1057, 'es', 'Burundi', 37),
(1058, 'es', 'Camboya', 38),
(1059, 'es', 'Camerún', 39),
(1060, 'es', 'Canadá', 40),
(1061, 'es', 'Islas Canarias', 41),
(1062, 'es', 'Cabo Verde', 42),
(1063, 'es', 'Caribe Neerlandés', 43),
(1064, 'es', 'Islas Caimán', 44),
(1065, 'es', 'República Centroafricana', 45),
(1066, 'es', 'Ceuta y Melilla', 46),
(1067, 'es', 'Chad', 47),
(1068, 'es', 'Chile', 48),
(1069, 'es', 'China', 49),
(1070, 'es', 'Isla de Navidad', 50),
(1071, 'es', 'Islas Cocos', 51),
(1072, 'es', 'Colombia', 52),
(1073, 'es', 'Comoras', 53),
(1074, 'es', 'República del Congo', 54),
(1075, 'es', 'República Democrática del Congo', 55),
(1076, 'es', 'Islas Cook', 56),
(1077, 'es', 'Costa Rica', 57),
(1078, 'es', 'Costa de Marfil', 58),
(1079, 'es', 'Croacia', 59),
(1080, 'es', 'Cuba', 60),
(1081, 'es', 'Curazao', 61),
(1082, 'es', 'Chipre', 62),
(1083, 'es', 'República Checa', 63),
(1084, 'es', 'Dinamarca', 64),
(1085, 'es', 'Diego García', 65),
(1086, 'es', 'Yibuti', 66),
(1087, 'es', 'Dominica', 67),
(1088, 'es', 'República Dominicana', 68),
(1089, 'es', 'Ecuador', 69),
(1090, 'es', 'Egipto', 70),
(1091, 'es', 'El Salvador', 71),
(1092, 'es', 'Guinea Ecuatorial', 72),
(1093, 'es', 'Eritrea', 73),
(1094, 'es', 'Estonia', 74),
(1095, 'es', 'Etiopía', 75),
(1096, 'es', 'Europa', 76),
(1097, 'es', 'Islas Malvinas', 77),
(1098, 'es', 'Islas Feroe', 78),
(1099, 'es', 'Fiyi', 79),
(1100, 'es', 'Finlandia', 80),
(1101, 'es', 'Francia', 81),
(1102, 'es', 'Guayana Francesa', 82),
(1103, 'es', 'Polinesia Francesa', 83),
(1104, 'es', 'Territorios Australes y Antárticas Franceses', 84),
(1105, 'es', 'Gabón', 85),
(1106, 'es', 'Gambia', 86),
(1107, 'es', 'Georgia', 87),
(1108, 'es', 'Alemania', 88),
(1109, 'es', 'Ghana', 89),
(1110, 'es', 'Gibraltar', 90),
(1111, 'es', 'Grecia', 91),
(1112, 'es', 'Groenlandia', 92),
(1113, 'es', 'Granada', 93),
(1114, 'es', 'Guadalupe', 94),
(1115, 'es', 'Guam', 95),
(1116, 'es', 'Guatemala', 96),
(1117, 'es', 'Guernsey', 97),
(1118, 'es', 'Guinea', 98),
(1119, 'es', 'Guinea-Bisáu', 99),
(1120, 'es', 'Guyana', 100),
(1121, 'es', 'Haití', 101),
(1122, 'es', 'Honduras', 102),
(1123, 'es', 'Hong Kong', 103),
(1124, 'es', 'Hungría', 104),
(1125, 'es', 'Islandia', 105),
(1126, 'es', 'India', 106),
(1127, 'es', 'Indonesia', 107),
(1128, 'es', 'Irán', 108),
(1129, 'es', 'Irak', 109),
(1130, 'es', 'Irlanda', 110),
(1131, 'es', 'Isla de Man', 111),
(1132, 'es', 'Israel', 112),
(1133, 'es', 'Italia', 113),
(1134, 'es', 'Jamaica', 114),
(1135, 'es', 'Japón', 115),
(1136, 'es', 'Jersey', 116),
(1137, 'es', 'Jordania', 117),
(1138, 'es', 'Kazajistán', 118),
(1139, 'es', 'Kenia', 119),
(1140, 'es', 'Kiribati', 120),
(1141, 'es', 'Kosovo', 121),
(1142, 'es', 'Kuwait', 122),
(1143, 'es', 'Kirguistán', 123),
(1144, 'es', 'Laos', 124),
(1145, 'es', 'Letonia', 125),
(1146, 'es', 'Líbano', 126),
(1147, 'es', 'Lesoto', 127),
(1148, 'es', 'Liberia', 128),
(1149, 'es', 'Libia', 129),
(1150, 'es', 'Liechtenstein', 130),
(1151, 'es', 'Lituania', 131),
(1152, 'es', 'Luxemburgo', 132),
(1153, 'es', 'Macao', 133),
(1154, 'es', 'Macedonia', 134),
(1155, 'es', 'Madagascar', 135),
(1156, 'es', 'Malaui', 136),
(1157, 'es', 'Malasia', 137),
(1158, 'es', 'Maldivas', 138),
(1159, 'es', 'Malí', 139),
(1160, 'es', 'Malta', 140),
(1161, 'es', 'Islas Marshall', 141),
(1162, 'es', 'Martinica', 142),
(1163, 'es', 'Mauritania', 143),
(1164, 'es', 'Mauricio', 144),
(1165, 'es', 'Mayotte', 145),
(1166, 'es', 'México', 146),
(1167, 'es', 'Micronesia', 147),
(1168, 'es', 'Moldavia', 148),
(1169, 'es', 'Mónaco', 149),
(1170, 'es', 'Mongolia', 150),
(1171, 'es', 'Montenegro', 151),
(1172, 'es', 'Montserrat', 152),
(1173, 'es', 'Marruecos', 153),
(1174, 'es', 'Mozambique', 154),
(1175, 'es', 'Birmania', 155),
(1176, 'es', 'Namibia', 156),
(1177, 'es', 'Nauru', 157),
(1178, 'es', 'Nepal', 158),
(1179, 'es', 'Holanda', 159),
(1180, 'es', 'Nueva Caledonia', 160),
(1181, 'es', 'Nueva Zelanda', 161),
(1182, 'es', 'Nicaragua', 162),
(1183, 'es', 'Níger', 163),
(1184, 'es', 'Nigeria', 164),
(1185, 'es', 'Niue', 165),
(1186, 'es', 'Isla Norfolk', 166),
(1187, 'es', 'Corea del Norte', 167),
(1188, 'es', 'Islas Marianas del Norte', 168),
(1189, 'es', 'Noruega', 169),
(1190, 'es', 'Omán', 170),
(1191, 'es', 'Pakistán', 171),
(1192, 'es', 'Palaos', 172),
(1193, 'es', 'Palestina', 173),
(1194, 'es', 'Panamá', 174),
(1195, 'es', 'Papúa Nueva Guinea', 175),
(1196, 'es', 'Paraguay', 176),
(1197, 'es', 'Perú', 177),
(1198, 'es', 'Filipinas', 178),
(1199, 'es', 'Islas Pitcairn', 179),
(1200, 'es', 'Polonia', 180),
(1201, 'es', 'Portugal', 181),
(1202, 'es', 'Puerto Rico', 182),
(1203, 'es', 'Catar', 183),
(1204, 'es', 'Reunión', 184),
(1205, 'es', 'Rumania', 185),
(1206, 'es', 'Rusia', 186),
(1207, 'es', 'Ruanda', 187),
(1208, 'es', 'Samoa', 188),
(1209, 'es', 'San Marino', 189),
(1210, 'es', 'Santo Tomé y Príncipe', 190),
(1211, 'es', 'Arabia Saudita', 191),
(1212, 'es', 'Senegal', 192),
(1213, 'es', 'Serbia', 193),
(1214, 'es', 'Seychelles', 194),
(1215, 'es', 'Sierra Leona', 195),
(1216, 'es', 'Singapur', 196),
(1217, 'es', 'San Martín', 197),
(1218, 'es', 'Eslovaquia', 198),
(1219, 'es', 'Eslovenia', 199),
(1220, 'es', 'Islas Salomón', 200),
(1221, 'es', 'Somalia', 201),
(1222, 'es', 'Sudáfrica', 202),
(1223, 'es', 'Islas Georgias del Sur y Sandwich del Sur', 203),
(1224, 'es', 'Corea del Sur', 204),
(1225, 'es', 'Sudán del Sur', 205),
(1226, 'es', 'España', 206),
(1227, 'es', 'Sri Lanka', 207),
(1228, 'es', 'San Bartolomé', 208),
(1229, 'es', 'Santa Elena', 209),
(1230, 'es', 'San Cristóbal y Nieves', 210),
(1231, 'es', 'Santa Lucía', 211),
(1232, 'es', 'San Martín', 212),
(1233, 'es', 'San Pedro y Miquelón', 213),
(1234, 'es', 'San Vicente y las Granadinas', 214),
(1235, 'es', 'Sudán', 215),
(1236, 'es', 'Surinam', 216),
(1237, 'es', 'Svalbard y Jan Mayen', 217),
(1238, 'es', 'Suazilandia', 218),
(1239, 'es', 'Suecia', 219),
(1240, 'es', 'Suiza', 220),
(1241, 'es', 'Siri', 221),
(1242, 'es', 'Taiwán', 222),
(1243, 'es', 'Tayikistán', 223),
(1244, 'es', 'Tanzania', 224),
(1245, 'es', 'Tailandia', 225),
(1246, 'es', 'Timor Oriental', 226),
(1247, 'es', 'Togo', 227),
(1248, 'es', 'Tokelau', 228),
(1249, 'es', 'Tonga', 229),
(1250, 'es', 'Trinidad y Tobago', 230),
(1251, 'es', 'Tristán de Acuña', 231),
(1252, 'es', 'Túnez', 232),
(1253, 'es', 'Turquía', 233),
(1254, 'es', 'Turkmenistán', 234),
(1255, 'es', 'Islas Turcas y Caicos', 235),
(1256, 'es', 'Tuvalu', 236),
(1257, 'es', 'Islas Ultramarinas Menores de los Estados Unidos', 237),
(1258, 'es', 'Islas Vírgenes de los Estados Unidos', 238),
(1259, 'es', 'Uganda', 239),
(1260, 'es', 'Ucrania', 240),
(1261, 'es', 'Emiratos árabes Unidos', 241),
(1262, 'es', 'Reino Unido', 242),
(1263, 'es', 'Naciones Unidas', 243),
(1264, 'es', 'Estados Unidos', 244),
(1265, 'es', 'Uruguay', 245),
(1266, 'es', 'Uzbekistán', 246),
(1267, 'es', 'Vanuatu', 247),
(1268, 'es', 'Ciudad del Vaticano', 248),
(1269, 'es', 'Venezuela', 249),
(1270, 'es', 'Vietnam', 250),
(1271, 'es', 'Wallis y Futuna', 251),
(1272, 'es', 'Sahara Occidental', 252),
(1273, 'es', 'Yemen', 253),
(1274, 'es', 'Zambia', 254),
(1275, 'es', 'Zimbabue', 255),
(1276, 'fa', 'افغانستان', 1),
(1277, 'fa', 'جزایر الند', 2),
(1278, 'fa', 'آلبانی', 3),
(1279, 'fa', 'الجزایر', 4),
(1280, 'fa', 'ساموآ آمریکایی', 5),
(1281, 'fa', 'آندورا', 6),
(1282, 'fa', 'آنگولا', 7),
(1283, 'fa', 'آنگولا', 8),
(1284, 'fa', 'جنوبگان', 9),
(1285, 'fa', 'آنتیگوا و باربودا', 10),
(1286, 'fa', 'آرژانتین', 11),
(1287, 'fa', 'ارمنستان', 12),
(1288, 'fa', 'آروبا', 13),
(1289, 'fa', 'جزیره صعود', 14),
(1290, 'fa', 'استرالیا', 15),
(1291, 'fa', 'اتریش', 16),
(1292, 'fa', 'آذربایجان', 17),
(1293, 'fa', 'باهاما', 18),
(1294, 'fa', 'بحرین', 19),
(1295, 'fa', 'بنگلادش', 20),
(1296, 'fa', 'باربادوس', 21),
(1297, 'fa', 'بلاروس', 22),
(1298, 'fa', 'بلژیک', 23),
(1299, 'fa', 'بلژیک', 24),
(1300, 'fa', 'بنین', 25),
(1301, 'fa', 'برمودا', 26),
(1302, 'fa', 'بوتان', 27),
(1303, 'fa', 'بولیوی', 28),
(1304, 'fa', 'بوسنی و هرزگوین', 29),
(1305, 'fa', 'بوتسوانا', 30),
(1306, 'fa', 'برزیل', 31),
(1307, 'fa', 'قلمرو اقیانوس هند انگلیس', 32),
(1308, 'fa', 'جزایر ویرجین انگلیس', 33),
(1309, 'fa', 'برونئی', 34),
(1310, 'fa', 'بلغارستان', 35),
(1311, 'fa', 'بورکینا فاسو', 36),
(1312, 'fa', 'بوروندی', 37),
(1313, 'fa', 'کامبوج', 38),
(1314, 'fa', 'کامرون', 39),
(1315, 'fa', 'کانادا', 40),
(1316, 'fa', 'جزایر قناری', 41),
(1317, 'fa', 'کیپ ورد', 42),
(1318, 'fa', 'کارائیب هلند', 43),
(1319, 'fa', 'Cayman Islands', 44),
(1320, 'fa', 'جمهوری آفریقای مرکزی', 45),
(1321, 'fa', 'سوتا و ملیلا', 46),
(1322, 'fa', 'چاد', 47),
(1323, 'fa', 'شیلی', 48),
(1324, 'fa', 'چین', 49),
(1325, 'fa', 'جزیره کریسمس', 50),
(1326, 'fa', 'جزایر کوکو (Keeling)', 51),
(1327, 'fa', 'کلمبیا', 52),
(1328, 'fa', 'کومور', 53),
(1329, 'fa', 'کنگو - برزاویل', 54),
(1330, 'fa', 'کنگو - کینشاسا', 55),
(1331, 'fa', 'جزایر کوک', 56),
(1332, 'fa', 'کاستاریکا', 57),
(1333, 'fa', 'ساحل عاج', 58),
(1334, 'fa', 'کرواسی', 59),
(1335, 'fa', 'کوبا', 60),
(1336, 'fa', 'کوراسائو', 61),
(1337, 'fa', 'قبرس', 62),
(1338, 'fa', 'چک', 63),
(1339, 'fa', 'دانمارک', 64),
(1340, 'fa', 'دیگو گارسیا', 65),
(1341, 'fa', 'جیبوتی', 66),
(1342, 'fa', 'دومینیکا', 67),
(1343, 'fa', 'جمهوری دومینیکن', 68),
(1344, 'fa', 'اکوادور', 69),
(1345, 'fa', 'مصر', 70),
(1346, 'fa', 'السالوادور', 71),
(1347, 'fa', 'گینه استوایی', 72),
(1348, 'fa', 'اریتره', 73),
(1349, 'fa', 'استونی', 74),
(1350, 'fa', 'اتیوپی', 75),
(1351, 'fa', 'منطقه یورو', 76),
(1352, 'fa', 'جزایر فالکلند', 77),
(1353, 'fa', 'جزایر فارو', 78),
(1354, 'fa', 'فیجی', 79),
(1355, 'fa', 'فنلاند', 80),
(1356, 'fa', 'فرانسه', 81),
(1357, 'fa', 'گویان فرانسه', 82),
(1358, 'fa', 'پلی‌نزی فرانسه', 83),
(1359, 'fa', 'سرزمین های جنوبی فرانسه', 84),
(1360, 'fa', 'گابن', 85),
(1361, 'fa', 'گامبیا', 86),
(1362, 'fa', 'جورجیا', 87),
(1363, 'fa', 'آلمان', 88),
(1364, 'fa', 'غنا', 89),
(1365, 'fa', 'جبل الطارق', 90),
(1366, 'fa', 'یونان', 91),
(1367, 'fa', 'گرینلند', 92),
(1368, 'fa', 'گرنادا', 93),
(1369, 'fa', 'گوادلوپ', 94),
(1370, 'fa', 'گوام', 95),
(1371, 'fa', 'گواتمالا', 96),
(1372, 'fa', 'گورنسی', 97),
(1373, 'fa', 'گینه', 98),
(1374, 'fa', 'گینه بیسائو', 99),
(1375, 'fa', 'گویان', 100),
(1376, 'fa', 'هائیتی', 101),
(1377, 'fa', 'هندوراس', 102),
(1378, 'fa', 'هنگ کنگ SAR چین', 103),
(1379, 'fa', 'مجارستان', 104),
(1380, 'fa', 'ایسلند', 105),
(1381, 'fa', 'هند', 106),
(1382, 'fa', 'اندونزی', 107),
(1383, 'fa', 'ایران', 108),
(1384, 'fa', 'عراق', 109),
(1385, 'fa', 'ایرلند', 110),
(1386, 'fa', 'جزیره من', 111),
(1387, 'fa', 'اسرائيل', 112),
(1388, 'fa', 'ایتالیا', 113),
(1389, 'fa', 'جامائیکا', 114),
(1390, 'fa', 'ژاپن', 115),
(1391, 'fa', 'پیراهن ورزشی', 116),
(1392, 'fa', 'اردن', 117),
(1393, 'fa', 'قزاقستان', 118),
(1394, 'fa', 'کنیا', 119),
(1395, 'fa', 'کیریباتی', 120),
(1396, 'fa', 'کوزوو', 121),
(1397, 'fa', 'کویت', 122),
(1398, 'fa', 'قرقیزستان', 123),
(1399, 'fa', 'لائوس', 124),
(1400, 'fa', 'لتونی', 125),
(1401, 'fa', 'لبنان', 126),
(1402, 'fa', 'لسوتو', 127),
(1403, 'fa', 'لیبریا', 128),
(1404, 'fa', 'لیبی', 129),
(1405, 'fa', 'لیختن اشتاین', 130),
(1406, 'fa', 'لیتوانی', 131),
(1407, 'fa', 'لوکزامبورگ', 132),
(1408, 'fa', 'ماکائو SAR چین', 133),
(1409, 'fa', 'مقدونیه', 134),
(1410, 'fa', 'ماداگاسکار', 135),
(1411, 'fa', 'مالاوی', 136),
(1412, 'fa', 'مالزی', 137),
(1413, 'fa', 'مالدیو', 138),
(1414, 'fa', 'مالی', 139),
(1415, 'fa', 'مالت', 140),
(1416, 'fa', 'جزایر مارشال', 141),
(1417, 'fa', 'مارتینیک', 142),
(1418, 'fa', 'موریتانی', 143),
(1419, 'fa', 'موریس', 144),
(1420, 'fa', 'گمشده', 145),
(1421, 'fa', 'مکزیک', 146),
(1422, 'fa', 'میکرونزی', 147),
(1423, 'fa', 'مولداوی', 148),
(1424, 'fa', 'موناکو', 149),
(1425, 'fa', 'مغولستان', 150),
(1426, 'fa', 'مونته نگرو', 151),
(1427, 'fa', 'مونتسرات', 152),
(1428, 'fa', 'مراکش', 153),
(1429, 'fa', 'موزامبیک', 154),
(1430, 'fa', 'میانمار (برمه)', 155),
(1431, 'fa', 'ناميبيا', 156),
(1432, 'fa', 'نائورو', 157),
(1433, 'fa', 'نپال', 158),
(1434, 'fa', 'هلند', 159),
(1435, 'fa', 'کالدونیای جدید', 160),
(1436, 'fa', 'نیوزلند', 161),
(1437, 'fa', 'نیکاراگوئه', 162),
(1438, 'fa', 'نیجر', 163),
(1439, 'fa', 'نیجریه', 164),
(1440, 'fa', 'نیو', 165),
(1441, 'fa', 'جزیره نورفولک', 166),
(1442, 'fa', 'کره شمالی', 167),
(1443, 'fa', 'جزایر ماریانای شمالی', 168),
(1444, 'fa', 'نروژ', 169),
(1445, 'fa', 'عمان', 170),
(1446, 'fa', 'پاکستان', 171),
(1447, 'fa', 'پالائو', 172),
(1448, 'fa', 'سرزمین های فلسطینی', 173),
(1449, 'fa', 'پاناما', 174),
(1450, 'fa', 'پاپوا گینه نو', 175),
(1451, 'fa', 'پاراگوئه', 176),
(1452, 'fa', 'پرو', 177),
(1453, 'fa', 'فیلیپین', 178),
(1454, 'fa', 'جزایر پیکریرن', 179),
(1455, 'fa', 'لهستان', 180),
(1456, 'fa', 'کشور پرتغال', 181),
(1457, 'fa', 'پورتوریکو', 182),
(1458, 'fa', 'قطر', 183),
(1459, 'fa', 'تجدید دیدار', 184),
(1460, 'fa', 'رومانی', 185),
(1461, 'fa', 'روسیه', 186),
(1462, 'fa', 'رواندا', 187),
(1463, 'fa', 'ساموآ', 188),
(1464, 'fa', 'سان مارینو', 189),
(1465, 'fa', 'سنت کیتس و نوویس', 190),
(1466, 'fa', 'عربستان سعودی', 191),
(1467, 'fa', 'سنگال', 192),
(1468, 'fa', 'صربستان', 193),
(1469, 'fa', 'سیشل', 194),
(1470, 'fa', 'سیرالئون', 195),
(1471, 'fa', 'سنگاپور', 196),
(1472, 'fa', 'سینت ماارتن', 197),
(1473, 'fa', 'اسلواکی', 198),
(1474, 'fa', 'اسلوونی', 199),
(1475, 'fa', 'جزایر سلیمان', 200),
(1476, 'fa', 'سومالی', 201),
(1477, 'fa', 'آفریقای جنوبی', 202),
(1478, 'fa', 'جزایر جورجیا جنوبی و جزایر ساندویچ جنوبی', 203),
(1479, 'fa', 'کره جنوبی', 204),
(1480, 'fa', 'سودان جنوبی', 205),
(1481, 'fa', 'اسپانیا', 206),
(1482, 'fa', 'سری لانکا', 207),
(1483, 'fa', 'سنت بارتلی', 208),
(1484, 'fa', 'سنت هلنا', 209),
(1485, 'fa', 'سنت کیتز و نوویس', 210),
(1486, 'fa', 'سنت لوسیا', 211),
(1487, 'fa', 'سنت مارتین', 212),
(1488, 'fa', 'سنت پیر و میکلون', 213),
(1489, 'fa', 'سنت وینسنت و گرنادینها', 214),
(1490, 'fa', 'سودان', 215),
(1491, 'fa', 'سورینام', 216),
(1492, 'fa', 'اسوالبارد و جان ماین', 217),
(1493, 'fa', 'سوازیلند', 218),
(1494, 'fa', 'سوئد', 219),
(1495, 'fa', 'سوئیس', 220),
(1496, 'fa', 'سوریه', 221),
(1497, 'fa', 'تایوان', 222),
(1498, 'fa', 'تاجیکستان', 223),
(1499, 'fa', 'تانزانیا', 224),
(1500, 'fa', 'تایلند', 225),
(1501, 'fa', 'تیمور-لست', 226),
(1502, 'fa', 'رفتن', 227),
(1503, 'fa', 'توکلو', 228),
(1504, 'fa', 'تونگا', 229),
(1505, 'fa', 'ترینیداد و توباگو', 230),
(1506, 'fa', 'تریستان دا کانونا', 231),
(1507, 'fa', 'تونس', 232),
(1508, 'fa', 'بوقلمون', 233),
(1509, 'fa', 'ترکمنستان', 234),
(1510, 'fa', 'جزایر تورکس و کایکوس', 235),
(1511, 'fa', 'تووالو', 236),
(1512, 'fa', 'جزایر دور افتاده ایالات متحده آمریکا', 237),
(1513, 'fa', 'جزایر ویرجین ایالات متحده', 238),
(1514, 'fa', 'اوگاندا', 239),
(1515, 'fa', 'اوکراین', 240),
(1516, 'fa', 'امارات متحده عربی', 241),
(1517, 'fa', 'انگلستان', 242),
(1518, 'fa', 'سازمان ملل', 243),
(1519, 'fa', 'ایالات متحده', 244),
(1520, 'fa', 'اروگوئه', 245),
(1521, 'fa', 'ازبکستان', 246),
(1522, 'fa', 'وانواتو', 247),
(1523, 'fa', 'شهر واتیکان', 248),
(1524, 'fa', 'ونزوئلا', 249),
(1525, 'fa', 'ویتنام', 250),
(1526, 'fa', 'والیس و فوتونا', 251),
(1527, 'fa', 'صحرای غربی', 252),
(1528, 'fa', 'یمن', 253),
(1529, 'fa', 'زامبیا', 254),
(1530, 'fa', 'زیمبابوه', 255),
(1531, 'pt_BR', 'Afeganistão', 1),
(1532, 'pt_BR', 'Ilhas Åland', 2),
(1533, 'pt_BR', 'Albânia', 3),
(1534, 'pt_BR', 'Argélia', 4),
(1535, 'pt_BR', 'Samoa Americana', 5),
(1536, 'pt_BR', 'Andorra', 6),
(1537, 'pt_BR', 'Angola', 7),
(1538, 'pt_BR', 'Angola', 8),
(1539, 'pt_BR', 'Antártico', 9),
(1540, 'pt_BR', 'Antígua e Barbuda', 10),
(1541, 'pt_BR', 'Argentina', 11),
(1542, 'pt_BR', 'Armênia', 12),
(1543, 'pt_BR', 'Aruba', 13),
(1544, 'pt_BR', 'Ilha de escalada', 14),
(1545, 'pt_BR', 'Austrália', 15),
(1546, 'pt_BR', 'Áustria', 16),
(1547, 'pt_BR', 'Azerbaijão', 17),
(1548, 'pt_BR', 'Bahamas', 18),
(1549, 'pt_BR', 'Bahrain', 19),
(1550, 'pt_BR', 'Bangladesh', 20),
(1551, 'pt_BR', 'Barbados', 21),
(1552, 'pt_BR', 'Bielorrússia', 22),
(1553, 'pt_BR', 'Bélgica', 23),
(1554, 'pt_BR', 'Bélgica', 24),
(1555, 'pt_BR', 'Benin', 25),
(1556, 'pt_BR', 'Bermuda', 26),
(1557, 'pt_BR', 'Butão', 27),
(1558, 'pt_BR', 'Bolívia', 28),
(1559, 'pt_BR', 'Bósnia e Herzegovina', 29),
(1560, 'pt_BR', 'Botsuana', 30),
(1561, 'pt_BR', 'Brasil', 31),
(1562, 'pt_BR', 'Território Britânico do Oceano Índico', 32),
(1563, 'pt_BR', 'Ilhas Virgens Britânicas', 33),
(1564, 'pt_BR', 'Brunei', 34),
(1565, 'pt_BR', 'Bulgária', 35),
(1566, 'pt_BR', 'Burkina Faso', 36),
(1567, 'pt_BR', 'Burundi', 37),
(1568, 'pt_BR', 'Camboja', 38),
(1569, 'pt_BR', 'Camarões', 39),
(1570, 'pt_BR', 'Canadá', 40),
(1571, 'pt_BR', 'Ilhas Canárias', 41),
(1572, 'pt_BR', 'Cabo Verde', 42),
(1573, 'pt_BR', 'Holanda do Caribe', 43),
(1574, 'pt_BR', 'Ilhas Cayman', 44),
(1575, 'pt_BR', 'República Centro-Africana', 45),
(1576, 'pt_BR', 'Ceuta e Melilla', 46),
(1577, 'pt_BR', 'Chade', 47),
(1578, 'pt_BR', 'Chile', 48),
(1579, 'pt_BR', 'China', 49),
(1580, 'pt_BR', 'Ilha Christmas', 50),
(1581, 'pt_BR', 'Ilhas Cocos (Keeling)', 51),
(1582, 'pt_BR', 'Colômbia', 52),
(1583, 'pt_BR', 'Comores', 53),
(1584, 'pt_BR', 'Congo - Brazzaville', 54),
(1585, 'pt_BR', 'Congo - Kinshasa', 55),
(1586, 'pt_BR', 'Ilhas Cook', 56),
(1587, 'pt_BR', 'Costa Rica', 57),
(1588, 'pt_BR', 'Costa do Marfim', 58),
(1589, 'pt_BR', 'Croácia', 59),
(1590, 'pt_BR', 'Cuba', 60),
(1591, 'pt_BR', 'Curaçao', 61),
(1592, 'pt_BR', 'Chipre', 62),
(1593, 'pt_BR', 'Czechia', 63),
(1594, 'pt_BR', 'Dinamarca', 64),
(1595, 'pt_BR', 'Diego Garcia', 65),
(1596, 'pt_BR', 'Djibuti', 66),
(1597, 'pt_BR', 'Dominica', 67),
(1598, 'pt_BR', 'República Dominicana', 68),
(1599, 'pt_BR', 'Equador', 69),
(1600, 'pt_BR', 'Egito', 70),
(1601, 'pt_BR', 'El Salvador', 71),
(1602, 'pt_BR', 'Guiné Equatorial', 72),
(1603, 'pt_BR', 'Eritreia', 73),
(1604, 'pt_BR', 'Estônia', 74),
(1605, 'pt_BR', 'Etiópia', 75),
(1606, 'pt_BR', 'Zona Euro', 76),
(1607, 'pt_BR', 'Ilhas Malvinas', 77),
(1608, 'pt_BR', 'Ilhas Faroe', 78),
(1609, 'pt_BR', 'Fiji', 79),
(1610, 'pt_BR', 'Finlândia', 80),
(1611, 'pt_BR', 'França', 81),
(1612, 'pt_BR', 'Guiana Francesa', 82),
(1613, 'pt_BR', 'Polinésia Francesa', 83),
(1614, 'pt_BR', 'Territórios Franceses do Sul', 84),
(1615, 'pt_BR', 'Gabão', 85),
(1616, 'pt_BR', 'Gâmbia', 86),
(1617, 'pt_BR', 'Geórgia', 87),
(1618, 'pt_BR', 'Alemanha', 88),
(1619, 'pt_BR', 'Gana', 89),
(1620, 'pt_BR', 'Gibraltar', 90),
(1621, 'pt_BR', 'Grécia', 91),
(1622, 'pt_BR', 'Gronelândia', 92),
(1623, 'pt_BR', 'Granada', 93),
(1624, 'pt_BR', 'Guadalupe', 94),
(1625, 'pt_BR', 'Guam', 95),
(1626, 'pt_BR', 'Guatemala', 96),
(1627, 'pt_BR', 'Guernsey', 97),
(1628, 'pt_BR', 'Guiné', 98),
(1629, 'pt_BR', 'Guiné-Bissau', 99),
(1630, 'pt_BR', 'Guiana', 100),
(1631, 'pt_BR', 'Haiti', 101),
(1632, 'pt_BR', 'Honduras', 102),
(1633, 'pt_BR', 'Região Administrativa Especial de Hong Kong, China', 103),
(1634, 'pt_BR', 'Hungria', 104),
(1635, 'pt_BR', 'Islândia', 105),
(1636, 'pt_BR', 'Índia', 106),
(1637, 'pt_BR', 'Indonésia', 107),
(1638, 'pt_BR', 'Irã', 108),
(1639, 'pt_BR', 'Iraque', 109),
(1640, 'pt_BR', 'Irlanda', 110),
(1641, 'pt_BR', 'Ilha de Man', 111),
(1642, 'pt_BR', 'Israel', 112),
(1643, 'pt_BR', 'Itália', 113),
(1644, 'pt_BR', 'Jamaica', 114),
(1645, 'pt_BR', 'Japão', 115),
(1646, 'pt_BR', 'Jersey', 116),
(1647, 'pt_BR', 'Jordânia', 117),
(1648, 'pt_BR', 'Cazaquistão', 118),
(1649, 'pt_BR', 'Quênia', 119),
(1650, 'pt_BR', 'Quiribati', 120),
(1651, 'pt_BR', 'Kosovo', 121),
(1652, 'pt_BR', 'Kuwait', 122),
(1653, 'pt_BR', 'Quirguistão', 123),
(1654, 'pt_BR', 'Laos', 124),
(1655, 'pt_BR', 'Letônia', 125),
(1656, 'pt_BR', 'Líbano', 126),
(1657, 'pt_BR', 'Lesoto', 127),
(1658, 'pt_BR', 'Libéria', 128),
(1659, 'pt_BR', 'Líbia', 129),
(1660, 'pt_BR', 'Liechtenstein', 130),
(1661, 'pt_BR', 'Lituânia', 131),
(1662, 'pt_BR', 'Luxemburgo', 132),
(1663, 'pt_BR', 'Macau SAR China', 133),
(1664, 'pt_BR', 'Macedônia', 134),
(1665, 'pt_BR', 'Madagascar', 135),
(1666, 'pt_BR', 'Malawi', 136),
(1667, 'pt_BR', 'Malásia', 137),
(1668, 'pt_BR', 'Maldivas', 138),
(1669, 'pt_BR', 'Mali', 139),
(1670, 'pt_BR', 'Malta', 140),
(1671, 'pt_BR', 'Ilhas Marshall', 141),
(1672, 'pt_BR', 'Martinica', 142),
(1673, 'pt_BR', 'Mauritânia', 143),
(1674, 'pt_BR', 'Maurício', 144),
(1675, 'pt_BR', 'Maiote', 145),
(1676, 'pt_BR', 'México', 146),
(1677, 'pt_BR', 'Micronésia', 147),
(1678, 'pt_BR', 'Moldávia', 148),
(1679, 'pt_BR', 'Mônaco', 149),
(1680, 'pt_BR', 'Mongólia', 150),
(1681, 'pt_BR', 'Montenegro', 151),
(1682, 'pt_BR', 'Montserrat', 152),
(1683, 'pt_BR', 'Marrocos', 153),
(1684, 'pt_BR', 'Moçambique', 154),
(1685, 'pt_BR', 'Mianmar (Birmânia)', 155),
(1686, 'pt_BR', 'Namíbia', 156),
(1687, 'pt_BR', 'Nauru', 157),
(1688, 'pt_BR', 'Nepal', 158),
(1689, 'pt_BR', 'Holanda', 159),
(1690, 'pt_BR', 'Nova Caledônia', 160),
(1691, 'pt_BR', 'Nova Zelândia', 161),
(1692, 'pt_BR', 'Nicarágua', 162),
(1693, 'pt_BR', 'Níger', 163),
(1694, 'pt_BR', 'Nigéria', 164),
(1695, 'pt_BR', 'Niue', 165),
(1696, 'pt_BR', 'Ilha Norfolk', 166),
(1697, 'pt_BR', 'Coréia do Norte', 167),
(1698, 'pt_BR', 'Ilhas Marianas do Norte', 168),
(1699, 'pt_BR', 'Noruega', 169),
(1700, 'pt_BR', 'Omã', 170),
(1701, 'pt_BR', 'Paquistão', 171),
(1702, 'pt_BR', 'Palau', 172),
(1703, 'pt_BR', 'Territórios Palestinos', 173),
(1704, 'pt_BR', 'Panamá', 174),
(1705, 'pt_BR', 'Papua Nova Guiné', 175),
(1706, 'pt_BR', 'Paraguai', 176),
(1707, 'pt_BR', 'Peru', 177),
(1708, 'pt_BR', 'Filipinas', 178),
(1709, 'pt_BR', 'Ilhas Pitcairn', 179),
(1710, 'pt_BR', 'Polônia', 180),
(1711, 'pt_BR', 'Portugal', 181),
(1712, 'pt_BR', 'Porto Rico', 182),
(1713, 'pt_BR', 'Catar', 183),
(1714, 'pt_BR', 'Reunião', 184),
(1715, 'pt_BR', 'Romênia', 185),
(1716, 'pt_BR', 'Rússia', 186),
(1717, 'pt_BR', 'Ruanda', 187),
(1718, 'pt_BR', 'Samoa', 188),
(1719, 'pt_BR', 'São Marinho', 189),
(1720, 'pt_BR', 'São Cristóvão e Nevis', 190),
(1721, 'pt_BR', 'Arábia Saudita', 191),
(1722, 'pt_BR', 'Senegal', 192),
(1723, 'pt_BR', 'Sérvia', 193),
(1724, 'pt_BR', 'Seychelles', 194),
(1725, 'pt_BR', 'Serra Leoa', 195),
(1726, 'pt_BR', 'Cingapura', 196),
(1727, 'pt_BR', 'São Martinho', 197),
(1728, 'pt_BR', 'Eslováquia', 198),
(1729, 'pt_BR', 'Eslovênia', 199),
(1730, 'pt_BR', 'Ilhas Salomão', 200),
(1731, 'pt_BR', 'Somália', 201),
(1732, 'pt_BR', 'África do Sul', 202),
(1733, 'pt_BR', 'Ilhas Geórgia do Sul e Sandwich do Sul', 203),
(1734, 'pt_BR', 'Coréia do Sul', 204),
(1735, 'pt_BR', 'Sudão do Sul', 205),
(1736, 'pt_BR', 'Espanha', 206),
(1737, 'pt_BR', 'Sri Lanka', 207),
(1738, 'pt_BR', 'São Bartolomeu', 208),
(1739, 'pt_BR', 'Santa Helena', 209),
(1740, 'pt_BR', 'São Cristóvão e Nevis', 210),
(1741, 'pt_BR', 'Santa Lúcia', 211),
(1742, 'pt_BR', 'São Martinho', 212),
(1743, 'pt_BR', 'São Pedro e Miquelon', 213),
(1744, 'pt_BR', 'São Vicente e Granadinas', 214),
(1745, 'pt_BR', 'Sudão', 215),
(1746, 'pt_BR', 'Suriname', 216),
(1747, 'pt_BR', 'Svalbard e Jan Mayen', 217),
(1748, 'pt_BR', 'Suazilândia', 218),
(1749, 'pt_BR', 'Suécia', 219),
(1750, 'pt_BR', 'Suíça', 220),
(1751, 'pt_BR', 'Síria', 221),
(1752, 'pt_BR', 'Taiwan', 222),
(1753, 'pt_BR', 'Tajiquistão', 223),
(1754, 'pt_BR', 'Tanzânia', 224),
(1755, 'pt_BR', 'Tailândia', 225),
(1756, 'pt_BR', 'Timor-Leste', 226),
(1757, 'pt_BR', 'Togo', 227),
(1758, 'pt_BR', 'Tokelau', 228),
(1759, 'pt_BR', 'Tonga', 229),
(1760, 'pt_BR', 'Trinidad e Tobago', 230),
(1761, 'pt_BR', 'Tristan da Cunha', 231),
(1762, 'pt_BR', 'Tunísia', 232),
(1763, 'pt_BR', 'Turquia', 233),
(1764, 'pt_BR', 'Turquemenistão', 234),
(1765, 'pt_BR', 'Ilhas Turks e Caicos', 235),
(1766, 'pt_BR', 'Tuvalu', 236),
(1767, 'pt_BR', 'Ilhas periféricas dos EUA', 237),
(1768, 'pt_BR', 'Ilhas Virgens dos EUA', 238),
(1769, 'pt_BR', 'Uganda', 239),
(1770, 'pt_BR', 'Ucrânia', 240),
(1771, 'pt_BR', 'Emirados Árabes Unidos', 241),
(1772, 'pt_BR', 'Reino Unido', 242),
(1773, 'pt_BR', 'Nações Unidas', 243),
(1774, 'pt_BR', 'Estados Unidos', 244),
(1775, 'pt_BR', 'Uruguai', 245),
(1776, 'pt_BR', 'Uzbequistão', 246),
(1777, 'pt_BR', 'Vanuatu', 247),
(1778, 'pt_BR', 'Cidade do Vaticano', 248),
(1779, 'pt_BR', 'Venezuela', 249),
(1780, 'pt_BR', 'Vietnã', 250),
(1781, 'pt_BR', 'Wallis e Futuna', 251),
(1782, 'pt_BR', 'Saara Ocidental', 252),
(1783, 'pt_BR', 'Iêmen', 253),
(1784, 'pt_BR', 'Zâmbia', 254),
(1785, 'pt_BR', 'Zimbábue', 255);

-- --------------------------------------------------------

--
-- Estrutura da tabela `currencies`
--

CREATE TABLE `currencies` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `symbol` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `currencies`
--

INSERT INTO `currencies` (`id`, `code`, `name`, `created_at`, `updated_at`, `symbol`) VALUES
(1, 'USD', 'US Dollar', NULL, NULL, '$'),
(2, 'EUR', 'Euro', NULL, NULL, '€'),
(3, 'BRL', 'Real Brasileiro', '2021-04-07 10:35:48', '2021-04-07 10:35:48', 'R$');

-- --------------------------------------------------------

--
-- Estrutura da tabela `currency_exchange_rates`
--

CREATE TABLE `currency_exchange_rates` (
  `id` int(10) UNSIGNED NOT NULL,
  `rate` decimal(24,12) NOT NULL,
  `target_currency` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `currency_exchange_rates`
--

INSERT INTO `currency_exchange_rates` (`id`, `rate`, `target_currency`, `created_at`, `updated_at`) VALUES
(1, '0.200000000000', 1, '2021-04-07 14:23:28', '2021-04-07 14:29:08');

-- --------------------------------------------------------

--
-- Estrutura da tabela `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `api_token` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_group_id` int(10) UNSIGNED DEFAULT NULL,
  `subscribed_to_news_letter` tinyint(1) NOT NULL DEFAULT 0,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_verified` tinyint(1) NOT NULL DEFAULT 0,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `customers`
--

INSERT INTO `customers` (`id`, `first_name`, `last_name`, `gender`, `date_of_birth`, `email`, `status`, `password`, `api_token`, `customer_group_id`, `subscribed_to_news_letter`, `remember_token`, `created_at`, `updated_at`, `is_verified`, `token`, `notes`, `phone`) VALUES
(1, 'Erich', 'Martter', NULL, NULL, 'user@example.com', 1, '$2y$10$BvOAeMdWY0nRrk3LphqAieHL2oAwVOsJxM/M075hPJlyOxSifHlbG', '0xNHwUJGF92fHoLXQh8qgfKNSa78OWXNed18ZIOgAZJv25lEHiOKZwChTo6yXdRfMZccaAb9iCtM3txz', 2, 0, NULL, '2021-04-07 19:00:53', '2021-04-07 19:00:53', 1, '025fe0331a8ceb8e80071faa8ee62829', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `customer_groups`
--

CREATE TABLE `customer_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_user_defined` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `customer_groups`
--

INSERT INTO `customer_groups` (`id`, `name`, `is_user_defined`, `created_at`, `updated_at`, `code`) VALUES
(1, 'Guest', 0, NULL, NULL, 'guest'),
(2, 'General', 0, NULL, NULL, 'general'),
(3, 'Wholesale', 0, NULL, NULL, 'wholesale');

-- --------------------------------------------------------

--
-- Estrutura da tabela `customer_password_resets`
--

CREATE TABLE `customer_password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `customer_social_accounts`
--

CREATE TABLE `customer_social_accounts` (
  `id` int(10) UNSIGNED NOT NULL,
  `provider_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `downloadable_link_purchased`
--

CREATE TABLE `downloadable_link_purchased` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `download_bought` int(11) NOT NULL DEFAULT 0,
  `download_used` int(11) NOT NULL DEFAULT 0,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `order_item_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `download_canceled` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `inventory_sources`
--

CREATE TABLE `inventory_sources` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_fax` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `street` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `postcode` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `priority` int(11) NOT NULL DEFAULT 0,
  `latitude` decimal(10,5) DEFAULT NULL,
  `longitude` decimal(10,5) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `inventory_sources`
--

INSERT INTO `inventory_sources` (`id`, `code`, `name`, `description`, `contact_name`, `contact_email`, `contact_number`, `contact_fax`, `country`, `state`, `city`, `street`, `postcode`, `priority`, `latitude`, `longitude`, `status`, `created_at`, `updated_at`) VALUES
(1, 'default', 'Default', NULL, 'Detroit Warehouse', 'warehouse@example.com', '1234567899', NULL, 'US', 'MI', 'Detroit', '12th Street', '48127', 0, NULL, NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `invoices`
--

CREATE TABLE `invoices` (
  `id` int(10) UNSIGNED NOT NULL,
  `increment_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_sent` tinyint(1) NOT NULL DEFAULT 0,
  `total_qty` int(11) DEFAULT NULL,
  `base_currency_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `channel_currency_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_currency_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_total` decimal(12,4) DEFAULT 0.0000,
  `base_sub_total` decimal(12,4) DEFAULT 0.0000,
  `grand_total` decimal(12,4) DEFAULT 0.0000,
  `base_grand_total` decimal(12,4) DEFAULT 0.0000,
  `shipping_amount` decimal(12,4) DEFAULT 0.0000,
  `base_shipping_amount` decimal(12,4) DEFAULT 0.0000,
  `tax_amount` decimal(12,4) DEFAULT 0.0000,
  `base_tax_amount` decimal(12,4) DEFAULT 0.0000,
  `discount_amount` decimal(12,4) DEFAULT 0.0000,
  `base_discount_amount` decimal(12,4) DEFAULT 0.0000,
  `order_id` int(10) UNSIGNED DEFAULT NULL,
  `order_address_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `transaction_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `invoices`
--

INSERT INTO `invoices` (`id`, `increment_id`, `state`, `email_sent`, `total_qty`, `base_currency_code`, `channel_currency_code`, `order_currency_code`, `sub_total`, `base_sub_total`, `grand_total`, `base_grand_total`, `shipping_amount`, `base_shipping_amount`, `tax_amount`, `base_tax_amount`, `discount_amount`, `base_discount_amount`, `order_id`, `order_address_id`, `created_at`, `updated_at`, `transaction_id`) VALUES
(1, NULL, 'paid', 0, 1, 'BRL', 'BRL', 'USD', '3.0000', '15.0000', '3.0000', '15.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', 3, 13, '2021-04-07 14:36:28', '2021-04-07 14:36:29', NULL),
(2, NULL, 'paid', 0, 1, 'BRL', 'BRL', 'BRL', '15.0000', '15.0000', '15.0000', '15.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', 2, 9, '2021-04-07 14:37:09', '2021-04-07 14:37:10', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `invoice_items`
--

CREATE TABLE `invoice_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sku` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `price` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `base_price` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `total` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `base_total` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `tax_amount` decimal(12,4) DEFAULT 0.0000,
  `base_tax_amount` decimal(12,4) DEFAULT 0.0000,
  `product_id` int(10) UNSIGNED DEFAULT NULL,
  `product_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_item_id` int(10) UNSIGNED DEFAULT NULL,
  `invoice_id` int(10) UNSIGNED DEFAULT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `additional` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`additional`)),
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `discount_percent` decimal(12,4) DEFAULT 0.0000,
  `discount_amount` decimal(12,4) DEFAULT 0.0000,
  `base_discount_amount` decimal(12,4) DEFAULT 0.0000
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `invoice_items`
--

INSERT INTO `invoice_items` (`id`, `name`, `description`, `sku`, `qty`, `price`, `base_price`, `total`, `base_total`, `tax_amount`, `base_tax_amount`, `product_id`, `product_type`, `order_item_id`, `invoice_id`, `parent_id`, `additional`, `created_at`, `updated_at`, `discount_percent`, `discount_amount`, `base_discount_amount`) VALUES
(1, 'Tony Hawks XBOX', NULL, 'tony-hawks-xbox', 1, '3.0000', '15.0000', '3.0000', '15.0000', '0.0000', '0.0000', 3, 'Webkul\\Product\\Models\\Product', 5, 1, NULL, '{\"is_buy_now\":\"0\",\"_token\":\"98Z1uckkOpR6qzyPaYaumIWh1ugP3kSLSpwvoO7H\",\"product_id\":\"3\",\"quantity\":\"1\",\"selected_configurable_option\":\"4\",\"super_attribute\":{\"29\":\"14\"},\"attributes\":{\"tipo_conta\":{\"attribute_name\":\"Tipo Conta\",\"option_id\":14,\"option_label\":\"Prim\\u00e1ria\"}},\"locale\":\"pt_BR\"}', '2021-04-07 14:36:29', '2021-04-07 14:36:29', '0.0000', '0.0000', '0.0000'),
(2, 'Primária', NULL, 'tony-hawks-xbox-variant-14', 1, '1.0000', '0.0000', '1.0000', '0.0000', '0.0000', '0.0000', 4, 'Webkul\\Product\\Models\\Product', 6, 1, 1, '{\"product_id\":4,\"parent_id\":3,\"locale\":\"pt_BR\"}', '2021-04-07 14:36:29', '2021-04-07 14:36:29', '0.0000', '0.0000', '0.0000'),
(3, 'Tony Hawks XBOX', NULL, 'tony-hawks-xbox', 1, '15.0000', '15.0000', '15.0000', '15.0000', '0.0000', '0.0000', 3, 'Webkul\\Product\\Models\\Product', 3, 2, NULL, '{\"is_buy_now\":\"0\",\"_token\":\"98Z1uckkOpR6qzyPaYaumIWh1ugP3kSLSpwvoO7H\",\"product_id\":\"3\",\"quantity\":\"1\",\"selected_configurable_option\":\"4\",\"super_attribute\":{\"29\":\"14\"},\"attributes\":{\"tipo_conta\":{\"attribute_name\":\"Tipo Conta\",\"option_id\":14,\"option_label\":\"Prim\\u00e1ria\"}},\"locale\":\"pt_BR\"}', '2021-04-07 14:37:09', '2021-04-07 14:37:09', '0.0000', '0.0000', '0.0000'),
(4, 'Primária', NULL, 'tony-hawks-xbox-variant-14', 1, '1.0000', '0.0000', '1.0000', '0.0000', '0.0000', '0.0000', 4, 'Webkul\\Product\\Models\\Product', 4, 2, 3, '{\"product_id\":4,\"parent_id\":3,\"locale\":\"pt_BR\"}', '2021-04-07 14:37:09', '2021-04-07 14:37:09', '0.0000', '0.0000', '0.0000');

-- --------------------------------------------------------

--
-- Estrutura da tabela `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `locales`
--

CREATE TABLE `locales` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `direction` enum('ltr','rtl') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'ltr',
  `locale_image` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `locales`
--

INSERT INTO `locales` (`id`, `code`, `name`, `created_at`, `updated_at`, `direction`, `locale_image`) VALUES
(1, 'en', 'English', NULL, NULL, 'ltr', NULL),
(2, 'fr', 'French', NULL, NULL, 'ltr', NULL),
(3, 'nl', 'Dutch', NULL, NULL, 'ltr', NULL),
(4, 'tr', 'Türkçe', NULL, NULL, 'ltr', NULL),
(5, 'es', 'Español', NULL, NULL, 'ltr', NULL),
(6, 'pt_BR', 'Português Brasil', '2021-04-07 10:35:16', '2021-04-07 10:35:16', 'ltr', 'velocity/locale_image/6/3NB0uG1mc2ZNoA5nAYYMkWbp7Tl0AvXJVNNIezFn.png');

-- --------------------------------------------------------

--
-- Estrutura da tabela `marketing_campaigns`
--

CREATE TABLE `marketing_campaigns` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mail_to` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spooling` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `channel_id` int(10) UNSIGNED DEFAULT NULL,
  `customer_group_id` int(10) UNSIGNED DEFAULT NULL,
  `marketing_template_id` int(10) UNSIGNED DEFAULT NULL,
  `marketing_event_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `marketing_events`
--

CREATE TABLE `marketing_events` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `marketing_events`
--

INSERT INTO `marketing_events` (`id`, `name`, `description`, `date`, `created_at`, `updated_at`) VALUES
(1, 'Birthday', 'Birthday', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `marketing_templates`
--

CREATE TABLE `marketing_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_admin_password_resets_table', 1),
(3, '2014_10_12_100000_create_password_resets_table', 1),
(4, '2018_06_12_111907_create_admins_table', 1),
(5, '2018_06_13_055341_create_roles_table', 1),
(6, '2018_07_05_130148_create_attributes_table', 1),
(7, '2018_07_05_132854_create_attribute_translations_table', 1),
(8, '2018_07_05_135150_create_attribute_families_table', 1),
(9, '2018_07_05_135152_create_attribute_groups_table', 1),
(10, '2018_07_05_140832_create_attribute_options_table', 1),
(11, '2018_07_05_140856_create_attribute_option_translations_table', 1),
(12, '2018_07_05_142820_create_categories_table', 1),
(13, '2018_07_10_055143_create_locales_table', 1),
(14, '2018_07_20_054426_create_countries_table', 1),
(15, '2018_07_20_054502_create_currencies_table', 1),
(16, '2018_07_20_054542_create_currency_exchange_rates_table', 1),
(17, '2018_07_20_064849_create_channels_table', 1),
(18, '2018_07_21_142836_create_category_translations_table', 1),
(19, '2018_07_23_110040_create_inventory_sources_table', 1),
(20, '2018_07_24_082635_create_customer_groups_table', 1),
(21, '2018_07_24_082930_create_customers_table', 1),
(22, '2018_07_24_083025_create_customer_addresses_table', 1),
(23, '2018_07_27_065727_create_products_table', 1),
(24, '2018_07_27_070011_create_product_attribute_values_table', 1),
(25, '2018_07_27_092623_create_product_reviews_table', 1),
(26, '2018_07_27_113941_create_product_images_table', 1),
(27, '2018_07_27_113956_create_product_inventories_table', 1),
(28, '2018_08_03_114203_create_sliders_table', 1),
(29, '2018_08_30_064755_create_tax_categories_table', 1),
(30, '2018_08_30_065042_create_tax_rates_table', 1),
(31, '2018_08_30_065840_create_tax_mappings_table', 1),
(32, '2018_09_05_150444_create_cart_table', 1),
(33, '2018_09_05_150915_create_cart_items_table', 1),
(34, '2018_09_11_064045_customer_password_resets', 1),
(35, '2018_09_19_092845_create_cart_address', 1),
(36, '2018_09_19_093453_create_cart_payment', 1),
(37, '2018_09_19_093508_create_cart_shipping_rates_table', 1),
(38, '2018_09_20_060658_create_core_config_table', 1),
(39, '2018_09_27_113154_create_orders_table', 1),
(40, '2018_09_27_113207_create_order_items_table', 1),
(41, '2018_09_27_113405_create_order_address_table', 1),
(42, '2018_09_27_115022_create_shipments_table', 1),
(43, '2018_09_27_115029_create_shipment_items_table', 1),
(44, '2018_09_27_115135_create_invoices_table', 1),
(45, '2018_09_27_115144_create_invoice_items_table', 1),
(46, '2018_10_01_095504_create_order_payment_table', 1),
(47, '2018_10_03_025230_create_wishlist_table', 1),
(48, '2018_10_12_101803_create_country_translations_table', 1),
(49, '2018_10_12_101913_create_country_states_table', 1),
(50, '2018_10_12_101923_create_country_state_translations_table', 1),
(51, '2018_11_15_153257_alter_order_table', 1),
(52, '2018_11_15_163729_alter_invoice_table', 1),
(53, '2018_11_16_173504_create_subscribers_list_table', 1),
(54, '2018_11_17_165758_add_is_verified_column_in_customers_table', 1),
(55, '2018_11_21_144411_create_cart_item_inventories_table', 1),
(56, '2018_11_26_110500_change_gender_column_in_customers_table', 1),
(57, '2018_11_27_174449_change_content_column_in_sliders_table', 1),
(58, '2018_12_05_132625_drop_foreign_key_core_config_table', 1),
(59, '2018_12_05_132629_alter_core_config_table', 1),
(60, '2018_12_06_185202_create_product_flat_table', 1),
(61, '2018_12_21_101307_alter_channels_table', 1),
(62, '2018_12_24_123812_create_channel_inventory_sources_table', 1),
(63, '2018_12_24_184402_alter_shipments_table', 1),
(64, '2018_12_26_165327_create_product_ordered_inventories_table', 1),
(65, '2018_12_31_161114_alter_channels_category_table', 1),
(66, '2019_01_11_122452_add_vendor_id_column_in_product_inventories_table', 1),
(67, '2019_01_25_124522_add_updated_at_column_in_product_flat_table', 1),
(68, '2019_01_29_123053_add_min_price_and_max_price_column_in_product_flat_table', 1),
(69, '2019_01_31_164117_update_value_column_type_to_text_in_core_config_table', 1),
(70, '2019_02_21_145238_alter_product_reviews_table', 1),
(71, '2019_02_21_152709_add_swatch_type_column_in_attributes_table', 1),
(72, '2019_02_21_153035_alter_customer_id_in_product_reviews_table', 1),
(73, '2019_02_21_153851_add_swatch_value_columns_in_attribute_options_table', 1),
(74, '2019_03_15_123337_add_display_mode_column_in_categories_table', 1),
(75, '2019_03_28_103658_add_notes_column_in_customers_table', 1),
(76, '2019_04_24_155820_alter_product_flat_table', 1),
(77, '2019_05_13_024320_remove_tables', 1),
(78, '2019_05_13_024321_create_cart_rules_table', 1),
(79, '2019_05_13_024322_create_cart_rule_channels_table', 1),
(80, '2019_05_13_024323_create_cart_rule_customer_groups_table', 1),
(81, '2019_05_13_024324_create_cart_rule_translations_table', 1),
(82, '2019_05_13_024325_create_cart_rule_customers_table', 1),
(83, '2019_05_13_024326_create_cart_rule_coupons_table', 1),
(84, '2019_05_13_024327_create_cart_rule_coupon_usage_table', 1),
(85, '2019_05_22_165833_update_zipcode_column_type_to_varchar_in_cart_address_table', 1),
(86, '2019_05_23_113407_add_remaining_column_in_product_flat_table', 1),
(87, '2019_05_23_155520_add_discount_columns_in_invoice_items_table', 1),
(88, '2019_05_23_184029_rename_discount_columns_in_cart_table', 1),
(89, '2019_06_04_114009_add_phone_column_in_customers_table', 1),
(90, '2019_06_06_195905_update_custom_price_to_nullable_in_cart_items', 1),
(91, '2019_06_15_183412_add_code_column_in_customer_groups_table', 1),
(92, '2019_06_17_180258_create_product_downloadable_samples_table', 1),
(93, '2019_06_17_180314_create_product_downloadable_sample_translations_table', 1),
(94, '2019_06_17_180325_create_product_downloadable_links_table', 1),
(95, '2019_06_17_180346_create_product_downloadable_link_translations_table', 1),
(96, '2019_06_19_162817_remove_unique_in_phone_column_in_customers_table', 1),
(97, '2019_06_21_130512_update_weight_column_deafult_value_in_cart_items_table', 1),
(98, '2019_06_21_202249_create_downloadable_link_purchased_table', 1),
(99, '2019_07_02_180307_create_booking_products_table', 1),
(100, '2019_07_05_114157_add_symbol_column_in_currencies_table', 1),
(101, '2019_07_05_154415_create_booking_product_default_slots_table', 1),
(102, '2019_07_05_154429_create_booking_product_appointment_slots_table', 1),
(103, '2019_07_05_154440_create_booking_product_event_tickets_table', 1),
(104, '2019_07_05_154451_create_booking_product_rental_slots_table', 1),
(105, '2019_07_05_154502_create_booking_product_table_slots_table', 1),
(106, '2019_07_11_151210_add_locale_id_in_category_translations', 1),
(107, '2019_07_23_033128_alter_locales_table', 1),
(108, '2019_07_23_174708_create_velocity_contents_table', 1),
(109, '2019_07_23_175212_create_velocity_contents_translations_table', 1),
(110, '2019_07_29_142734_add_use_in_flat_column_in_attributes_table', 1),
(111, '2019_07_30_153530_create_cms_pages_table', 1),
(112, '2019_07_31_143339_create_category_filterable_attributes_table', 1),
(113, '2019_08_02_105320_create_product_grouped_products_table', 1),
(114, '2019_08_12_184925_add_additional_cloumn_in_wishlist_table', 1),
(115, '2019_08_20_170510_create_product_bundle_options_table', 1),
(116, '2019_08_20_170520_create_product_bundle_option_translations_table', 1),
(117, '2019_08_20_170528_create_product_bundle_option_products_table', 1),
(118, '2019_08_21_123707_add_seo_column_in_channels_table', 1),
(119, '2019_09_11_184511_create_refunds_table', 1),
(120, '2019_09_11_184519_create_refund_items_table', 1),
(121, '2019_09_26_163950_remove_channel_id_from_customers_table', 1),
(122, '2019_10_03_105451_change_rate_column_in_currency_exchange_rates_table', 1),
(123, '2019_10_21_105136_order_brands', 1),
(124, '2019_10_24_173358_change_postcode_column_type_in_order_address_table', 1),
(125, '2019_10_24_173437_change_postcode_column_type_in_cart_address_table', 1),
(126, '2019_10_24_173507_change_postcode_column_type_in_customer_addresses_table', 1),
(127, '2019_11_21_194541_add_column_url_path_to_category_translations', 1),
(128, '2019_11_21_194608_add_stored_function_to_get_url_path_of_category', 1),
(129, '2019_11_21_194627_add_trigger_to_category_translations', 1),
(130, '2019_11_21_194648_add_url_path_to_existing_category_translations', 1),
(131, '2019_11_21_194703_add_trigger_to_categories', 1),
(132, '2019_11_25_171136_add_applied_cart_rule_ids_column_in_cart_table', 1),
(133, '2019_11_25_171208_add_applied_cart_rule_ids_column_in_cart_items_table', 1),
(134, '2019_11_30_124437_add_applied_cart_rule_ids_column_in_orders_table', 1),
(135, '2019_11_30_165644_add_discount_columns_in_cart_shipping_rates_table', 1),
(136, '2019_12_03_175253_create_remove_catalog_rule_tables', 1),
(137, '2019_12_03_184613_create_catalog_rules_table', 1),
(138, '2019_12_03_184651_create_catalog_rule_channels_table', 1),
(139, '2019_12_03_184732_create_catalog_rule_customer_groups_table', 1),
(140, '2019_12_06_101110_create_catalog_rule_products_table', 1),
(141, '2019_12_06_110507_create_catalog_rule_product_prices_table', 1),
(142, '2019_12_30_155256_create_velocity_meta_data', 1),
(143, '2020_01_02_201029_add_api_token_columns', 1),
(144, '2020_01_06_173505_alter_trigger_category_translations', 1),
(145, '2020_01_06_173524_alter_stored_function_url_path_category', 1),
(146, '2020_01_06_195305_alter_trigger_on_categories', 1),
(147, '2020_01_09_154851_add_shipping_discount_columns_in_orders_table', 1),
(148, '2020_01_09_202815_add_inventory_source_name_column_in_shipments_table', 1),
(149, '2020_01_10_122226_update_velocity_meta_data', 1),
(150, '2020_01_10_151902_customer_address_improvements', 1),
(151, '2020_01_13_131431_alter_float_value_column_type_in_product_attribute_values_table', 1),
(152, '2020_01_13_155803_add_velocity_locale_icon', 1),
(153, '2020_01_13_192149_add_category_velocity_meta_data', 1),
(154, '2020_01_14_191854_create_cms_page_translations_table', 1),
(155, '2020_01_14_192206_remove_columns_from_cms_pages_table', 1),
(156, '2020_01_15_130209_create_cms_page_channels_table', 1),
(157, '2020_01_15_145637_add_product_policy', 1),
(158, '2020_01_15_152121_add_banner_link', 1),
(159, '2020_01_28_102422_add_new_column_and_rename_name_column_in_customer_addresses_table', 1),
(160, '2020_01_29_124748_alter_name_column_in_country_state_translations_table', 1),
(161, '2020_02_18_165639_create_bookings_table', 1),
(162, '2020_02_21_121201_create_booking_product_event_ticket_translations_table', 1),
(163, '2020_02_24_190025_add_is_comparable_column_in_attributes_table', 1),
(164, '2020_02_25_181902_propagate_company_name', 1),
(165, '2020_02_26_163908_change_column_type_in_cart_rules_table', 1),
(166, '2020_02_28_105104_fix_order_columns', 1),
(167, '2020_02_28_111958_create_customer_compare_products_table', 1),
(168, '2020_03_23_201431_alter_booking_products_table', 1),
(169, '2020_04_13_224524_add_locale_in_sliders_table', 1),
(170, '2020_04_16_130351_remove_channel_from_tax_category', 1),
(171, '2020_04_16_185147_add_table_addresses', 1),
(172, '2020_05_06_171638_create_order_comments_table', 1),
(173, '2020_05_21_171500_create_product_customer_group_prices_table', 1),
(174, '2020_06_08_161708_add_sale_prices_to_booking_product_event_tickets', 1),
(175, '2020_06_10_201453_add_locale_velocity_meta_data', 1),
(176, '2020_06_25_162154_create_customer_social_accounts_table', 1),
(177, '2020_06_25_162340_change_email_password_columns_in_customers_table', 1),
(178, '2020_06_30_163510_remove_unique_name_in_tax_categories_table', 1),
(179, '2020_07_31_142021_update_cms_page_translations_table_field_html_content', 1),
(180, '2020_08_01_132239_add_header_content_count_velocity_meta_data_table', 1),
(181, '2020_08_12_114128_removing_foriegn_key', 1),
(182, '2020_08_17_104228_add_channel_to_velocity_meta_data_table', 1),
(183, '2020_09_07_120413_add_unique_index_to_increment_id_in_orders_table', 1),
(184, '2020_09_07_195157_add_additional_to_category', 1),
(185, '2020_11_10_174816_add_product_number_column_in_product_flat_table', 1),
(186, '2020_11_19_112228_create_product_videos_table', 1),
(187, '2020_11_20_105353_add_columns_in_channels_table', 1),
(188, '2020_11_26_141455_create_marketing_templates_table', 1),
(189, '2020_11_26_150534_create_marketing_events_table', 1),
(190, '2020_11_26_150644_create_marketing_campaigns_table', 1),
(191, '2020_12_18_122826_add_is_tax_calculation_column_to_cart_shipping_rates_table', 1),
(192, '2020_12_21_000200_create_channel_translations_table', 1),
(193, '2020_12_21_140151_remove_columns_from_channels_table', 1),
(194, '2020_12_24_131004_add_customer_id_column_in_subscribers_list_table', 1),
(195, '2020_12_27_121950_create_jobs_table', 1),
(196, '2021_02_03_104907_add_adittional_data_to_order_payment_table', 1),
(197, '2021_02_04_150033_add_download_canceled_column_in_downloadable_link_purchased_table', 1),
(198, '2021_03_11_212124_create_order_transactions_table', 2),
(199, '2021_03_19_184538_add_expired_at_and_sort_order_column_in_sliders_table', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `increment_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `channel_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_guest` tinyint(1) DEFAULT NULL,
  `customer_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_first_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_company_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `customer_vat_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_method` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coupon_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_gift` tinyint(1) NOT NULL DEFAULT 0,
  `total_item_count` int(11) DEFAULT NULL,
  `total_qty_ordered` int(11) DEFAULT NULL,
  `base_currency_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `channel_currency_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_currency_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `grand_total` decimal(12,4) DEFAULT 0.0000,
  `base_grand_total` decimal(12,4) DEFAULT 0.0000,
  `grand_total_invoiced` decimal(12,4) DEFAULT 0.0000,
  `base_grand_total_invoiced` decimal(12,4) DEFAULT 0.0000,
  `grand_total_refunded` decimal(12,4) DEFAULT 0.0000,
  `base_grand_total_refunded` decimal(12,4) DEFAULT 0.0000,
  `sub_total` decimal(12,4) DEFAULT 0.0000,
  `base_sub_total` decimal(12,4) DEFAULT 0.0000,
  `sub_total_invoiced` decimal(12,4) DEFAULT 0.0000,
  `base_sub_total_invoiced` decimal(12,4) DEFAULT 0.0000,
  `sub_total_refunded` decimal(12,4) DEFAULT 0.0000,
  `base_sub_total_refunded` decimal(12,4) DEFAULT 0.0000,
  `discount_percent` decimal(12,4) DEFAULT 0.0000,
  `discount_amount` decimal(12,4) DEFAULT 0.0000,
  `base_discount_amount` decimal(12,4) DEFAULT 0.0000,
  `discount_invoiced` decimal(12,4) DEFAULT 0.0000,
  `base_discount_invoiced` decimal(12,4) DEFAULT 0.0000,
  `discount_refunded` decimal(12,4) DEFAULT 0.0000,
  `base_discount_refunded` decimal(12,4) DEFAULT 0.0000,
  `tax_amount` decimal(12,4) DEFAULT 0.0000,
  `base_tax_amount` decimal(12,4) DEFAULT 0.0000,
  `tax_amount_invoiced` decimal(12,4) DEFAULT 0.0000,
  `base_tax_amount_invoiced` decimal(12,4) DEFAULT 0.0000,
  `tax_amount_refunded` decimal(12,4) DEFAULT 0.0000,
  `base_tax_amount_refunded` decimal(12,4) DEFAULT 0.0000,
  `shipping_amount` decimal(12,4) DEFAULT 0.0000,
  `base_shipping_amount` decimal(12,4) DEFAULT 0.0000,
  `shipping_invoiced` decimal(12,4) DEFAULT 0.0000,
  `base_shipping_invoiced` decimal(12,4) DEFAULT 0.0000,
  `shipping_refunded` decimal(12,4) DEFAULT 0.0000,
  `base_shipping_refunded` decimal(12,4) DEFAULT 0.0000,
  `customer_id` int(10) UNSIGNED DEFAULT NULL,
  `customer_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `channel_id` int(10) UNSIGNED DEFAULT NULL,
  `channel_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `cart_id` int(11) DEFAULT NULL,
  `applied_cart_rule_ids` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_discount_amount` decimal(12,4) DEFAULT 0.0000,
  `base_shipping_discount_amount` decimal(12,4) DEFAULT 0.0000
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `orders`
--

INSERT INTO `orders` (`id`, `increment_id`, `status`, `channel_name`, `is_guest`, `customer_email`, `customer_first_name`, `customer_last_name`, `customer_company_name`, `customer_vat_id`, `shipping_method`, `shipping_title`, `shipping_description`, `coupon_code`, `is_gift`, `total_item_count`, `total_qty_ordered`, `base_currency_code`, `channel_currency_code`, `order_currency_code`, `grand_total`, `base_grand_total`, `grand_total_invoiced`, `base_grand_total_invoiced`, `grand_total_refunded`, `base_grand_total_refunded`, `sub_total`, `base_sub_total`, `sub_total_invoiced`, `base_sub_total_invoiced`, `sub_total_refunded`, `base_sub_total_refunded`, `discount_percent`, `discount_amount`, `base_discount_amount`, `discount_invoiced`, `base_discount_invoiced`, `discount_refunded`, `base_discount_refunded`, `tax_amount`, `base_tax_amount`, `tax_amount_invoiced`, `base_tax_amount_invoiced`, `tax_amount_refunded`, `base_tax_amount_refunded`, `shipping_amount`, `base_shipping_amount`, `shipping_invoiced`, `base_shipping_invoiced`, `shipping_refunded`, `base_shipping_refunded`, `customer_id`, `customer_type`, `channel_id`, `channel_type`, `created_at`, `updated_at`, `cart_id`, `applied_cart_rule_ids`, `shipping_discount_amount`, `base_shipping_discount_amount`) VALUES
(1, '1', 'canceled', 'Onclick', 1, 'erichmartter@gmail.com', 'Erich', 'Martter', NULL, NULL, 'flatrate_flatrate', 'Flat Rate - Flat Rate', 'Flat Rate Shipping', NULL, 0, 1, 1, 'BRL', 'BRL', 'BRL', '25.0000', '25.0000', '0.0000', '0.0000', '0.0000', '0.0000', '15.0000', '15.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '10.0000', '10.0000', '0.0000', '0.0000', '0.0000', '0.0000', NULL, NULL, 1, 'Webkul\\Core\\Models\\Channel', '2021-04-07 14:08:34', '2021-04-07 14:09:59', 3, '', '0.0000', '0.0000'),
(2, '2', 'completed', 'Onclick', 0, 'user@example.com', 'Erich', 'Martter', NULL, NULL, 'free_free', 'Free Shipping - Free Shipping', 'Free Shipping', NULL, 0, 1, 1, 'BRL', 'BRL', 'BRL', '15.0000', '15.0000', '15.0000', '15.0000', '0.0000', '0.0000', '15.0000', '15.0000', '15.0000', '15.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', 1, 'Webkul\\Customer\\Models\\Customer', 1, 'Webkul\\Core\\Models\\Channel', '2021-04-07 14:13:15', '2021-04-07 14:39:56', 2, '', '0.0000', '0.0000'),
(3, '3', 'closed', 'Onclick', 0, 'user@example.com', 'Erich', 'Martter', NULL, NULL, 'free_free', 'Free Shipping - Free Shipping', 'Free Shipping', NULL, 0, 1, 1, 'BRL', 'BRL', 'USD', '3.0000', '15.0000', '3.0000', '15.0000', '3.0000', '15.0000', '3.0000', '15.0000', '3.0000', '15.0000', '3.0000', '15.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', 1, 'Webkul\\Customer\\Models\\Customer', 1, 'Webkul\\Core\\Models\\Channel', '2021-04-07 14:34:08', '2021-04-07 14:36:48', 4, '', '0.0000', '0.0000');

-- --------------------------------------------------------

--
-- Estrutura da tabela `order_brands`
--

CREATE TABLE `order_brands` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED DEFAULT NULL,
  `order_item_id` int(10) UNSIGNED DEFAULT NULL,
  `product_id` int(10) UNSIGNED DEFAULT NULL,
  `brand` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `order_brands`
--

INSERT INTO `order_brands` (`id`, `order_id`, `order_item_id`, `product_id`, `brand`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 3, NULL, '2021-04-07 14:08:34', '2021-04-07 14:08:34'),
(2, 2, 3, 3, NULL, '2021-04-07 14:13:16', '2021-04-07 14:13:16'),
(3, 3, 5, 3, NULL, '2021-04-07 14:34:08', '2021-04-07 14:34:08');

-- --------------------------------------------------------

--
-- Estrutura da tabela `order_comments`
--

CREATE TABLE `order_comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_notified` tinyint(1) NOT NULL DEFAULT 0,
  `order_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `order_items`
--

CREATE TABLE `order_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `sku` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coupon_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `weight` decimal(12,4) DEFAULT 0.0000,
  `total_weight` decimal(12,4) DEFAULT 0.0000,
  `qty_ordered` int(11) DEFAULT 0,
  `qty_shipped` int(11) DEFAULT 0,
  `qty_invoiced` int(11) DEFAULT 0,
  `qty_canceled` int(11) DEFAULT 0,
  `qty_refunded` int(11) DEFAULT 0,
  `price` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `base_price` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `total` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `base_total` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `total_invoiced` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `base_total_invoiced` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `amount_refunded` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `base_amount_refunded` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `discount_percent` decimal(12,4) DEFAULT 0.0000,
  `discount_amount` decimal(12,4) DEFAULT 0.0000,
  `base_discount_amount` decimal(12,4) DEFAULT 0.0000,
  `discount_invoiced` decimal(12,4) DEFAULT 0.0000,
  `base_discount_invoiced` decimal(12,4) DEFAULT 0.0000,
  `discount_refunded` decimal(12,4) DEFAULT 0.0000,
  `base_discount_refunded` decimal(12,4) DEFAULT 0.0000,
  `tax_percent` decimal(12,4) DEFAULT 0.0000,
  `tax_amount` decimal(12,4) DEFAULT 0.0000,
  `base_tax_amount` decimal(12,4) DEFAULT 0.0000,
  `tax_amount_invoiced` decimal(12,4) DEFAULT 0.0000,
  `base_tax_amount_invoiced` decimal(12,4) DEFAULT 0.0000,
  `tax_amount_refunded` decimal(12,4) DEFAULT 0.0000,
  `base_tax_amount_refunded` decimal(12,4) DEFAULT 0.0000,
  `product_id` int(10) UNSIGNED DEFAULT NULL,
  `product_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_id` int(10) UNSIGNED DEFAULT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `additional` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`additional`)),
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `order_items`
--

INSERT INTO `order_items` (`id`, `sku`, `type`, `name`, `coupon_code`, `weight`, `total_weight`, `qty_ordered`, `qty_shipped`, `qty_invoiced`, `qty_canceled`, `qty_refunded`, `price`, `base_price`, `total`, `base_total`, `total_invoiced`, `base_total_invoiced`, `amount_refunded`, `base_amount_refunded`, `discount_percent`, `discount_amount`, `base_discount_amount`, `discount_invoiced`, `base_discount_invoiced`, `discount_refunded`, `base_discount_refunded`, `tax_percent`, `tax_amount`, `base_tax_amount`, `tax_amount_invoiced`, `base_tax_amount_invoiced`, `tax_amount_refunded`, `base_tax_amount_refunded`, `product_id`, `product_type`, `order_id`, `parent_id`, `additional`, `created_at`, `updated_at`) VALUES
(1, 'tony-hawks-xbox', 'game', 'Tony Hawks XBOX', NULL, '0.0000', '0.0000', 1, 0, 0, 1, 0, '15.0000', '15.0000', '15.0000', '15.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', 3, 'Webkul\\Product\\Models\\Product', 1, NULL, '{\"is_buy_now\":\"0\",\"_token\":\"98Z1uckkOpR6qzyPaYaumIWh1ugP3kSLSpwvoO7H\",\"product_id\":\"3\",\"quantity\":\"1\",\"selected_configurable_option\":\"4\",\"super_attribute\":{\"29\":\"14\"},\"attributes\":{\"tipo_conta\":{\"attribute_name\":\"Tipo Conta\",\"option_id\":14,\"option_label\":\"Prim\\u00e1ria\"}},\"locale\":\"pt_BR\"}', '2021-04-07 14:08:34', '2021-04-07 14:09:59'),
(2, 'tony-hawks-xbox-variant-14', 'virtual', 'Primária', NULL, '0.0000', '0.0000', 0, 0, 0, 0, 0, '1.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', 4, 'Webkul\\Product\\Models\\Product', 1, 1, '{\"product_id\":4,\"parent_id\":3,\"locale\":\"pt_BR\"}', '2021-04-07 14:08:34', '2021-04-07 14:08:34'),
(3, 'tony-hawks-xbox', 'game', 'Tony Hawks XBOX', NULL, '0.0000', '0.0000', 1, 1, 1, 0, 0, '15.0000', '15.0000', '15.0000', '15.0000', '15.0000', '15.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', 3, 'Webkul\\Product\\Models\\Product', 2, NULL, '{\"is_buy_now\":\"0\",\"_token\":\"98Z1uckkOpR6qzyPaYaumIWh1ugP3kSLSpwvoO7H\",\"product_id\":\"3\",\"quantity\":\"1\",\"selected_configurable_option\":\"4\",\"super_attribute\":{\"29\":\"14\"},\"attributes\":{\"tipo_conta\":{\"attribute_name\":\"Tipo Conta\",\"option_id\":14,\"option_label\":\"Prim\\u00e1ria\"}},\"locale\":\"pt_BR\"}', '2021-04-07 14:13:15', '2021-04-07 14:39:56'),
(4, 'tony-hawks-xbox-variant-14', 'virtual', 'Primária', NULL, '0.0000', '0.0000', 0, 1, 1, 0, 0, '1.0000', '0.0000', '0.0000', '0.0000', '1.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', 4, 'Webkul\\Product\\Models\\Product', 2, 3, '{\"product_id\":4,\"parent_id\":3,\"locale\":\"pt_BR\"}', '2021-04-07 14:13:15', '2021-04-07 14:39:56'),
(5, 'tony-hawks-xbox', 'game', 'Tony Hawks XBOX', NULL, '0.0000', '0.0000', 1, 0, 1, 0, 1, '3.0000', '15.0000', '3.0000', '15.0000', '3.0000', '15.0000', '3.0000', '15.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', 3, 'Webkul\\Product\\Models\\Product', 3, NULL, '{\"is_buy_now\":\"0\",\"_token\":\"98Z1uckkOpR6qzyPaYaumIWh1ugP3kSLSpwvoO7H\",\"product_id\":\"3\",\"quantity\":\"1\",\"selected_configurable_option\":\"4\",\"super_attribute\":{\"29\":\"14\"},\"attributes\":{\"tipo_conta\":{\"attribute_name\":\"Tipo Conta\",\"option_id\":14,\"option_label\":\"Prim\\u00e1ria\"}},\"locale\":\"pt_BR\"}', '2021-04-07 14:34:08', '2021-04-07 14:36:48'),
(6, 'tony-hawks-xbox-variant-14', 'virtual', 'Primária', NULL, '0.0000', '0.0000', 0, 0, 1, 0, 1, '1.0000', '0.0000', '0.0000', '0.0000', '1.0000', '0.0000', '1.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', 4, 'Webkul\\Product\\Models\\Product', 3, 5, '{\"product_id\":4,\"parent_id\":3,\"locale\":\"pt_BR\"}', '2021-04-07 14:34:08', '2021-04-07 14:36:48');

-- --------------------------------------------------------

--
-- Estrutura da tabela `order_payment`
--

CREATE TABLE `order_payment` (
  `id` int(10) UNSIGNED NOT NULL,
  `method` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_id` int(10) UNSIGNED DEFAULT NULL,
  `additional` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`additional`)),
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `order_payment`
--

INSERT INTO `order_payment` (`id`, `method`, `method_title`, `order_id`, `additional`, `created_at`, `updated_at`) VALUES
(1, 'cashondelivery', NULL, 1, NULL, '2021-04-07 14:08:34', '2021-04-07 14:08:34'),
(2, 'moneytransfer', NULL, 2, NULL, '2021-04-07 14:13:15', '2021-04-07 14:13:15'),
(3, 'moneytransfer', NULL, 3, NULL, '2021-04-07 14:34:08', '2021-04-07 14:34:08');

-- --------------------------------------------------------

--
-- Estrutura da tabela `order_transactions`
--

CREATE TABLE `order_transactions` (
  `id` int(10) UNSIGNED NOT NULL,
  `transaction_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_method` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`data`)),
  `invoice_id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `sku` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `attribute_family_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `products`
--

INSERT INTO `products` (`id`, `sku`, `type`, `created_at`, `updated_at`, `parent_id`, `attribute_family_id`) VALUES
(1, 'tony-hawks-ps5', 'virtual', '2021-04-07 10:42:24', '2021-04-07 10:42:24', NULL, 1),
(3, 'tony-hawks-xbox', 'game', '2021-04-07 14:04:35', '2021-04-07 14:04:35', NULL, 3),
(4, 'tony-hawks-xbox-variant-14', 'virtual', '2021-04-07 14:04:35', '2021-04-07 14:04:35', 3, 3),
(5, 'tony-hawks-xbox-variant-15', 'virtual', '2021-04-07 14:04:35', '2021-04-07 14:04:35', 3, 3);

-- --------------------------------------------------------

--
-- Estrutura da tabela `product_attribute_values`
--

CREATE TABLE `product_attribute_values` (
  `id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `channel` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text_value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `boolean_value` tinyint(1) DEFAULT NULL,
  `integer_value` int(11) DEFAULT NULL,
  `float_value` decimal(12,4) DEFAULT NULL,
  `datetime_value` datetime DEFAULT NULL,
  `date_value` date DEFAULT NULL,
  `json_value` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`json_value`)),
  `product_id` int(10) UNSIGNED NOT NULL,
  `attribute_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `product_attribute_values`
--

INSERT INTO `product_attribute_values` (`id`, `locale`, `channel`, `text_value`, `boolean_value`, `integer_value`, `float_value`, `datetime_value`, `date_value`, `json_value`, `product_id`, `attribute_id`) VALUES
(1, 'pt_BR', 'default', '<p>Curta</p>', NULL, NULL, NULL, NULL, NULL, NULL, 1, 9),
(2, 'pt_BR', 'default', '<p>Longa</p>', NULL, NULL, NULL, NULL, NULL, NULL, 1, 10),
(3, NULL, NULL, 'tony-hawks-ps5', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1),
(4, 'pt_BR', 'default', 'Tony Hawks PS5', NULL, NULL, NULL, NULL, NULL, NULL, 1, 2),
(5, NULL, NULL, 'tony-hawks-ps5', NULL, NULL, NULL, NULL, NULL, NULL, 1, 3),
(6, NULL, 'default', NULL, NULL, 0, NULL, NULL, NULL, NULL, 1, 4),
(7, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, 5),
(8, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, 6),
(9, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, 7),
(10, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 1, 8),
(11, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, 1, 23),
(12, NULL, NULL, NULL, NULL, 6, NULL, NULL, NULL, NULL, 1, 24),
(13, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 1, 26),
(14, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 1, 27),
(15, 'pt_BR', 'default', '', NULL, NULL, NULL, NULL, NULL, NULL, 1, 16),
(16, 'pt_BR', 'default', '', NULL, NULL, NULL, NULL, NULL, NULL, 1, 17),
(17, 'pt_BR', 'default', '', NULL, NULL, NULL, NULL, NULL, NULL, 1, 18),
(18, NULL, NULL, NULL, NULL, NULL, '15.0000', NULL, NULL, NULL, 1, 11),
(19, NULL, 'default', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 12),
(20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 13),
(21, NULL, 'default', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 14),
(22, NULL, 'default', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 15),
(23, NULL, NULL, 'tony-hawks-xbox-variant-14', NULL, NULL, NULL, NULL, NULL, NULL, 4, 1),
(24, 'en', 'default', 'First', NULL, NULL, NULL, NULL, NULL, NULL, 4, 2),
(25, 'fr', 'default', '', NULL, NULL, NULL, NULL, NULL, NULL, 4, 2),
(26, 'nl', 'default', '', NULL, NULL, NULL, NULL, NULL, NULL, 4, 2),
(27, 'tr', 'default', '', NULL, NULL, NULL, NULL, NULL, NULL, 4, 2),
(28, 'es', 'default', '', NULL, NULL, NULL, NULL, NULL, NULL, 4, 2),
(29, 'pt_BR', 'default', 'Primária', NULL, NULL, NULL, NULL, NULL, NULL, 4, 2),
(30, NULL, NULL, NULL, NULL, NULL, '15.0000', NULL, NULL, NULL, 4, 11),
(31, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, 4, 22),
(32, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 4, 8),
(33, NULL, NULL, NULL, NULL, 14, NULL, NULL, NULL, NULL, 4, 29),
(34, NULL, NULL, 'tony-hawks-xbox-variant-15', NULL, NULL, NULL, NULL, NULL, NULL, 5, 1),
(35, 'en', 'default', 'Second', NULL, NULL, NULL, NULL, NULL, NULL, 5, 2),
(36, 'fr', 'default', '', NULL, NULL, NULL, NULL, NULL, NULL, 5, 2),
(37, 'nl', 'default', '', NULL, NULL, NULL, NULL, NULL, NULL, 5, 2),
(38, 'tr', 'default', '', NULL, NULL, NULL, NULL, NULL, NULL, 5, 2),
(39, 'es', 'default', '', NULL, NULL, NULL, NULL, NULL, NULL, 5, 2),
(40, 'pt_BR', 'default', 'Secundária', NULL, NULL, NULL, NULL, NULL, NULL, 5, 2),
(41, NULL, NULL, NULL, NULL, NULL, '10.0000', NULL, NULL, NULL, 5, 11),
(42, NULL, NULL, '0', NULL, NULL, NULL, NULL, NULL, NULL, 5, 22),
(43, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 5, 8),
(44, NULL, NULL, NULL, NULL, 15, NULL, NULL, NULL, NULL, 5, 29),
(45, 'pt_BR', 'default', '<p>dsa</p>', NULL, NULL, NULL, NULL, NULL, NULL, 3, 9),
(46, 'pt_BR', 'default', '<p>sd</p>', NULL, NULL, NULL, NULL, NULL, NULL, 3, 10),
(47, NULL, NULL, 'tony-hawks-xbox', NULL, NULL, NULL, NULL, NULL, NULL, 3, 1),
(48, 'pt_BR', 'default', 'Tony Hawks XBOX', NULL, NULL, NULL, NULL, NULL, NULL, 3, 2),
(49, NULL, NULL, 'tony-hawks-xbox-game', NULL, NULL, NULL, NULL, NULL, NULL, 3, 3),
(50, NULL, 'default', NULL, NULL, 0, NULL, NULL, NULL, NULL, 3, 4),
(51, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 3, 5),
(52, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 3, 6),
(53, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 3, 7),
(54, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 3, 8),
(55, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 3, 26),
(56, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 3, 27),
(57, 'pt_BR', 'default', '', NULL, NULL, NULL, NULL, NULL, NULL, 3, 16),
(58, 'pt_BR', 'default', '', NULL, NULL, NULL, NULL, NULL, NULL, 3, 17),
(59, 'pt_BR', 'default', '', NULL, NULL, NULL, NULL, NULL, NULL, 3, 18),
(60, 'en', 'default', '<p>English Short</p>', NULL, NULL, NULL, NULL, NULL, NULL, 3, 9),
(61, 'en', 'default', '<p>English lond desc</p>', NULL, NULL, NULL, NULL, NULL, NULL, 3, 10),
(62, 'en', 'default', 'Tony Hawks XBOX Game', NULL, NULL, NULL, NULL, NULL, NULL, 3, 2),
(63, 'en', 'default', 'English Short', NULL, NULL, NULL, NULL, NULL, NULL, 3, 16),
(64, 'en', 'default', 'English lond desc', NULL, NULL, NULL, NULL, NULL, NULL, 3, 17),
(65, 'en', 'default', '', NULL, NULL, NULL, NULL, NULL, NULL, 3, 18);

-- --------------------------------------------------------

--
-- Estrutura da tabela `product_bundle_options`
--

CREATE TABLE `product_bundle_options` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_required` tinyint(1) NOT NULL DEFAULT 1,
  `sort_order` int(11) NOT NULL DEFAULT 0,
  `product_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `product_bundle_option_products`
--

CREATE TABLE `product_bundle_option_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `qty` int(11) NOT NULL DEFAULT 0,
  `is_user_defined` tinyint(1) NOT NULL DEFAULT 1,
  `is_default` tinyint(1) NOT NULL DEFAULT 0,
  `sort_order` int(11) NOT NULL DEFAULT 0,
  `product_bundle_option_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `product_bundle_option_translations`
--

CREATE TABLE `product_bundle_option_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_bundle_option_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `product_categories`
--

CREATE TABLE `product_categories` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `product_cross_sells`
--

CREATE TABLE `product_cross_sells` (
  `parent_id` int(10) UNSIGNED NOT NULL,
  `child_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `product_customer_group_prices`
--

CREATE TABLE `product_customer_group_prices` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `qty` int(11) NOT NULL DEFAULT 0,
  `value_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `product_id` int(10) UNSIGNED NOT NULL,
  `customer_group_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `product_downloadable_links`
--

CREATE TABLE `product_downloadable_links` (
  `id` int(10) UNSIGNED NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `sample_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sample_file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sample_file_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sample_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `downloads` int(11) NOT NULL DEFAULT 0,
  `sort_order` int(11) DEFAULT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `product_downloadable_link_translations`
--

CREATE TABLE `product_downloadable_link_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_downloadable_link_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `product_downloadable_samples`
--

CREATE TABLE `product_downloadable_samples` (
  `id` int(10) UNSIGNED NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `file_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `product_downloadable_sample_translations`
--

CREATE TABLE `product_downloadable_sample_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_downloadable_sample_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `product_flat`
--

CREATE TABLE `product_flat` (
  `id` int(10) UNSIGNED NOT NULL,
  `sku` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_number` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url_key` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `new` tinyint(1) DEFAULT NULL,
  `featured` tinyint(1) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `thumbnail` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` decimal(12,4) DEFAULT NULL,
  `cost` decimal(12,4) DEFAULT NULL,
  `special_price` decimal(12,4) DEFAULT NULL,
  `special_price_from` date DEFAULT NULL,
  `special_price_to` date DEFAULT NULL,
  `weight` decimal(12,4) DEFAULT NULL,
  `color` int(11) DEFAULT NULL,
  `color_label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `size_label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `channel` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `visible_individually` tinyint(1) DEFAULT NULL,
  `min_price` decimal(12,4) DEFAULT NULL,
  `max_price` decimal(12,4) DEFAULT NULL,
  `short_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_title` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `width` decimal(12,4) DEFAULT NULL,
  `height` decimal(12,4) DEFAULT NULL,
  `depth` decimal(12,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `product_flat`
--

INSERT INTO `product_flat` (`id`, `sku`, `product_number`, `name`, `description`, `url_key`, `new`, `featured`, `status`, `thumbnail`, `price`, `cost`, `special_price`, `special_price_from`, `special_price_to`, `weight`, `color`, `color_label`, `size`, `size_label`, `created_at`, `locale`, `channel`, `product_id`, `updated_at`, `parent_id`, `visible_individually`, `min_price`, `max_price`, `short_description`, `meta_title`, `meta_keywords`, `meta_description`, `width`, `height`, `depth`) VALUES
(1, 'tony-hawks-ps5', '', NULL, NULL, 'tony-hawks-ps5', 1, 1, 1, NULL, '15.0000', NULL, NULL, NULL, NULL, NULL, 1, 'Red', 6, 'S', '2021-04-07 07:42:24', 'en', 'default', 1, '2021-04-07 07:42:24', NULL, 1, '15.0000', '15.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'tony-hawks-ps5', '', 'Tony Hawks PS5', '<p>Longa</p>', 'tony-hawks-ps5', 1, 1, 1, NULL, '15.0000', NULL, NULL, NULL, NULL, NULL, 1, 'Red', 6, 'S', '2021-04-07 07:42:24', 'pt_BR', 'default', 1, '2021-04-07 07:42:24', NULL, 1, '15.0000', '15.0000', '<p>Curta</p>', '', '', '', NULL, NULL, NULL),
(5, 'tony-hawks-xbox', '', 'Tony Hawks XBOX Game', '<p>English lond desc</p>', 'tony-hawks-xbox-game', 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-07 11:04:35', 'en', 'default', 3, '2021-04-07 11:04:35', NULL, 1, '6.0000', '9.0000', '<p>English Short</p>', 'English Short', 'English lond desc', '', NULL, NULL, NULL),
(6, 'tony-hawks-xbox', '', 'Tony Hawks XBOX', '<p>sd</p>', 'tony-hawks-xbox-game', 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-04-07 11:04:35', 'pt_BR', 'default', 3, '2021-04-07 11:04:35', NULL, 1, '6.0000', '9.0000', '<p>dsa</p>', '', '', '', NULL, NULL, NULL),
(7, 'tony-hawks-xbox-variant-14', NULL, 'First', NULL, NULL, NULL, NULL, 1, NULL, '15.0000', NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL, '2021-04-07 11:04:35', 'en', 'default', 4, '2021-04-07 11:04:35', 5, NULL, '15.0000', '15.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 'tony-hawks-xbox-variant-14', NULL, 'Primária', NULL, NULL, NULL, NULL, 1, NULL, '15.0000', NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL, '2021-04-07 11:04:35', 'pt_BR', 'default', 4, '2021-04-07 11:04:35', 6, NULL, '15.0000', '15.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 'tony-hawks-xbox-variant-15', NULL, 'Second', NULL, NULL, NULL, NULL, 1, NULL, '10.0000', NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL, '2021-04-07 11:04:35', 'en', 'default', 5, '2021-04-07 11:04:35', 5, NULL, '10.0000', '10.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 'tony-hawks-xbox-variant-15', NULL, 'Secundária', NULL, NULL, NULL, NULL, 1, NULL, '10.0000', NULL, NULL, NULL, NULL, '0.0000', NULL, NULL, NULL, NULL, '2021-04-07 11:04:35', 'pt_BR', 'default', 5, '2021-04-07 11:04:35', 6, NULL, '10.0000', '10.0000', NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `product_grouped_products`
--

CREATE TABLE `product_grouped_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `qty` int(11) NOT NULL DEFAULT 0,
  `sort_order` int(11) NOT NULL DEFAULT 0,
  `product_id` int(10) UNSIGNED NOT NULL,
  `associated_product_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `product_images`
--

CREATE TABLE `product_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `product_images`
--

INSERT INTO `product_images` (`id`, `type`, `path`, `product_id`) VALUES
(1, NULL, 'product/1/BzEjHb4w2qWjOm7Q3qjAslZ7RtsORd8hWoELmT3T.jpg', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `product_inventories`
--

CREATE TABLE `product_inventories` (
  `id` int(10) UNSIGNED NOT NULL,
  `qty` int(11) NOT NULL DEFAULT 0,
  `product_id` int(10) UNSIGNED NOT NULL,
  `inventory_source_id` int(10) UNSIGNED NOT NULL,
  `vendor_id` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `product_inventories`
--

INSERT INTO `product_inventories` (`id`, `qty`, `product_id`, `inventory_source_id`, `vendor_id`) VALUES
(1, 2, 1, 1, 0),
(2, 0, 4, 1, 0),
(3, 2, 5, 1, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `product_ordered_inventories`
--

CREATE TABLE `product_ordered_inventories` (
  `id` int(10) UNSIGNED NOT NULL,
  `qty` int(11) NOT NULL DEFAULT 0,
  `product_id` int(10) UNSIGNED NOT NULL,
  `channel_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `product_ordered_inventories`
--

INSERT INTO `product_ordered_inventories` (`id`, `qty`, `product_id`, `channel_id`) VALUES
(1, 0, 4, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `product_relations`
--

CREATE TABLE `product_relations` (
  `parent_id` int(10) UNSIGNED NOT NULL,
  `child_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `product_reviews`
--

CREATE TABLE `product_reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rating` int(11) NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `product_super_attributes`
--

CREATE TABLE `product_super_attributes` (
  `product_id` int(10) UNSIGNED NOT NULL,
  `attribute_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `product_super_attributes`
--

INSERT INTO `product_super_attributes` (`product_id`, `attribute_id`) VALUES
(3, 29);

-- --------------------------------------------------------

--
-- Estrutura da tabela `product_up_sells`
--

CREATE TABLE `product_up_sells` (
  `parent_id` int(10) UNSIGNED NOT NULL,
  `child_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `product_videos`
--

CREATE TABLE `product_videos` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `refunds`
--

CREATE TABLE `refunds` (
  `id` int(10) UNSIGNED NOT NULL,
  `increment_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_sent` tinyint(1) NOT NULL DEFAULT 0,
  `total_qty` int(11) DEFAULT NULL,
  `base_currency_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `channel_currency_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_currency_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `adjustment_refund` decimal(12,4) DEFAULT 0.0000,
  `base_adjustment_refund` decimal(12,4) DEFAULT 0.0000,
  `adjustment_fee` decimal(12,4) DEFAULT 0.0000,
  `base_adjustment_fee` decimal(12,4) DEFAULT 0.0000,
  `sub_total` decimal(12,4) DEFAULT 0.0000,
  `base_sub_total` decimal(12,4) DEFAULT 0.0000,
  `grand_total` decimal(12,4) DEFAULT 0.0000,
  `base_grand_total` decimal(12,4) DEFAULT 0.0000,
  `shipping_amount` decimal(12,4) DEFAULT 0.0000,
  `base_shipping_amount` decimal(12,4) DEFAULT 0.0000,
  `tax_amount` decimal(12,4) DEFAULT 0.0000,
  `base_tax_amount` decimal(12,4) DEFAULT 0.0000,
  `discount_percent` decimal(12,4) DEFAULT 0.0000,
  `discount_amount` decimal(12,4) DEFAULT 0.0000,
  `base_discount_amount` decimal(12,4) DEFAULT 0.0000,
  `order_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `refunds`
--

INSERT INTO `refunds` (`id`, `increment_id`, `state`, `email_sent`, `total_qty`, `base_currency_code`, `channel_currency_code`, `order_currency_code`, `adjustment_refund`, `base_adjustment_refund`, `adjustment_fee`, `base_adjustment_fee`, `sub_total`, `base_sub_total`, `grand_total`, `base_grand_total`, `shipping_amount`, `base_shipping_amount`, `tax_amount`, `base_tax_amount`, `discount_percent`, `discount_amount`, `base_discount_amount`, `order_id`, `created_at`, `updated_at`) VALUES
(1, NULL, 'refunded', 0, 1, 'BRL', 'BRL', 'USD', '0.0000', '0.0000', '0.0000', '0.0000', '3.0000', '15.0000', '3.0000', '15.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', 3, '2021-04-07 14:36:48', '2021-04-07 14:36:48');

-- --------------------------------------------------------

--
-- Estrutura da tabela `refund_items`
--

CREATE TABLE `refund_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sku` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `price` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `base_price` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `total` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `base_total` decimal(12,4) NOT NULL DEFAULT 0.0000,
  `tax_amount` decimal(12,4) DEFAULT 0.0000,
  `base_tax_amount` decimal(12,4) DEFAULT 0.0000,
  `discount_percent` decimal(12,4) DEFAULT 0.0000,
  `discount_amount` decimal(12,4) DEFAULT 0.0000,
  `base_discount_amount` decimal(12,4) DEFAULT 0.0000,
  `product_id` int(10) UNSIGNED DEFAULT NULL,
  `product_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_item_id` int(10) UNSIGNED DEFAULT NULL,
  `refund_id` int(10) UNSIGNED DEFAULT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `additional` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`additional`)),
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `refund_items`
--

INSERT INTO `refund_items` (`id`, `name`, `description`, `sku`, `qty`, `price`, `base_price`, `total`, `base_total`, `tax_amount`, `base_tax_amount`, `discount_percent`, `discount_amount`, `base_discount_amount`, `product_id`, `product_type`, `order_item_id`, `refund_id`, `parent_id`, `additional`, `created_at`, `updated_at`) VALUES
(1, 'Tony Hawks XBOX', NULL, 'tony-hawks-xbox', 1, '3.0000', '15.0000', '3.0000', '15.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', 3, 'Webkul\\Product\\Models\\Product', 5, 1, NULL, '{\"is_buy_now\":\"0\",\"_token\":\"98Z1uckkOpR6qzyPaYaumIWh1ugP3kSLSpwvoO7H\",\"product_id\":\"3\",\"quantity\":\"1\",\"selected_configurable_option\":\"4\",\"super_attribute\":{\"29\":\"14\"},\"attributes\":{\"tipo_conta\":{\"attribute_name\":\"Tipo Conta\",\"option_id\":14,\"option_label\":\"Prim\\u00e1ria\"}},\"locale\":\"pt_BR\"}', '2021-04-07 14:36:48', '2021-04-07 14:36:48'),
(2, 'Primária', NULL, 'tony-hawks-xbox-variant-14', 1, '1.0000', '0.0000', '1.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', '0.0000', 4, 'Webkul\\Product\\Models\\Product', 6, 1, 1, '{\"product_id\":4,\"parent_id\":3,\"locale\":\"pt_BR\"}', '2021-04-07 14:36:48', '2021-04-07 14:36:48');

-- --------------------------------------------------------

--
-- Estrutura da tabela `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permission_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permissions` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`permissions`)),
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `roles`
--

INSERT INTO `roles` (`id`, `name`, `description`, `permission_type`, `permissions`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 'Administrator role', 'all', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `shipments`
--

CREATE TABLE `shipments` (
  `id` int(10) UNSIGNED NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_qty` int(11) DEFAULT NULL,
  `total_weight` int(11) DEFAULT NULL,
  `carrier_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `carrier_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `track_number` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_sent` tinyint(1) NOT NULL DEFAULT 0,
  `customer_id` int(10) UNSIGNED DEFAULT NULL,
  `customer_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `order_address_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `inventory_source_id` int(10) UNSIGNED DEFAULT NULL,
  `inventory_source_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `shipments`
--

INSERT INTO `shipments` (`id`, `status`, `total_qty`, `total_weight`, `carrier_code`, `carrier_title`, `track_number`, `email_sent`, `customer_id`, `customer_type`, `order_id`, `order_address_id`, `created_at`, `updated_at`, `inventory_source_id`, `inventory_source_name`) VALUES
(1, NULL, 1, NULL, NULL, '', '', 0, 1, 'Webkul\\Customer\\Models\\Customer', 2, 8, '2021-04-07 14:39:55', '2021-04-07 14:39:56', 1, 'Default');

-- --------------------------------------------------------

--
-- Estrutura da tabela `shipment_items`
--

CREATE TABLE `shipment_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sku` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `price` decimal(12,4) DEFAULT 0.0000,
  `base_price` decimal(12,4) DEFAULT 0.0000,
  `total` decimal(12,4) DEFAULT 0.0000,
  `base_total` decimal(12,4) DEFAULT 0.0000,
  `product_id` int(10) UNSIGNED DEFAULT NULL,
  `product_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_item_id` int(10) UNSIGNED DEFAULT NULL,
  `shipment_id` int(10) UNSIGNED NOT NULL,
  `additional` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`additional`)),
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `shipment_items`
--

INSERT INTO `shipment_items` (`id`, `name`, `description`, `sku`, `qty`, `weight`, `price`, `base_price`, `total`, `base_total`, `product_id`, `product_type`, `order_item_id`, `shipment_id`, `additional`, `created_at`, `updated_at`) VALUES
(1, 'Tony Hawks XBOX', NULL, 'tony-hawks-xbox-variant-14', 1, 0, '15.0000', '15.0000', '15.0000', '15.0000', 3, 'Webkul\\Product\\Models\\Product', 3, 1, '{\"is_buy_now\":\"0\",\"_token\":\"98Z1uckkOpR6qzyPaYaumIWh1ugP3kSLSpwvoO7H\",\"product_id\":\"3\",\"quantity\":\"1\",\"selected_configurable_option\":\"4\",\"super_attribute\":{\"29\":\"14\"},\"attributes\":{\"tipo_conta\":{\"attribute_name\":\"Tipo Conta\",\"option_id\":14,\"option_label\":\"Prim\\u00e1ria\"}},\"locale\":\"pt_BR\"}', '2021-04-07 14:39:55', '2021-04-07 14:39:55');

-- --------------------------------------------------------

--
-- Estrutura da tabela `sliders`
--

CREATE TABLE `sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `channel_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `slider_path` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expired_at` date DEFAULT NULL,
  `sort_order` int(10) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `subscribers_list`
--

CREATE TABLE `subscribers_list` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_subscribed` tinyint(1) NOT NULL DEFAULT 0,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `channel_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `customer_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tax_categories`
--

CREATE TABLE `tax_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tax_categories_tax_rates`
--

CREATE TABLE `tax_categories_tax_rates` (
  `id` int(10) UNSIGNED NOT NULL,
  `tax_category_id` int(10) UNSIGNED NOT NULL,
  `tax_rate_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tax_rates`
--

CREATE TABLE `tax_rates` (
  `id` int(10) UNSIGNED NOT NULL,
  `identifier` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_zip` tinyint(1) NOT NULL DEFAULT 0,
  `zip_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_from` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip_to` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tax_rate` decimal(12,4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `velocity_contents`
--

CREATE TABLE `velocity_contents` (
  `id` int(10) UNSIGNED NOT NULL,
  `content_type` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` int(10) UNSIGNED DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `velocity_contents_translations`
--

CREATE TABLE `velocity_contents_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `content_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_title` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `custom_heading` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `page_link` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link_target` tinyint(1) NOT NULL DEFAULT 0,
  `catalog_type` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `products` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Estrutura da tabela `velocity_customer_compare_products`
--

CREATE TABLE `velocity_customer_compare_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_flat_id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `velocity_customer_compare_products`
--

INSERT INTO `velocity_customer_compare_products` (`id`, `product_flat_id`, `customer_id`, `created_at`, `updated_at`) VALUES
(2, 2, 1, '2021-04-07 14:16:44', '2021-04-07 14:16:44'),
(3, 6, 1, '2021-04-07 14:16:47', '2021-04-07 14:16:47');

-- --------------------------------------------------------

--
-- Estrutura da tabela `velocity_meta_data`
--

CREATE TABLE `velocity_meta_data` (
  `id` int(10) UNSIGNED NOT NULL,
  `home_page_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `footer_left_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `footer_middle_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slider` tinyint(1) NOT NULL DEFAULT 0,
  `advertisement` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`advertisement`)),
  `sidebar_category_count` int(11) NOT NULL DEFAULT 9,
  `featured_product_count` int(11) NOT NULL DEFAULT 10,
  `new_products_count` int(11) NOT NULL DEFAULT 10,
  `subscription_bar_content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `product_view_images` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`product_view_images`)),
  `product_policy` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `locale` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `channel` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `header_content_count` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Extraindo dados da tabela `velocity_meta_data`
--

INSERT INTO `velocity_meta_data` (`id`, `home_page_content`, `footer_left_content`, `footer_middle_content`, `slider`, `advertisement`, `sidebar_category_count`, `featured_product_count`, `new_products_count`, `subscription_bar_content`, `created_at`, `updated_at`, `product_view_images`, `product_policy`, `locale`, `channel`, `header_content_count`) VALUES
(1, '<p>@include(\'shop::home.advertisements.advertisement-four\')@include(\'shop::home.featured-products\') @include(\'shop::home.product-policy\') @include(\'shop::home.advertisements.advertisement-three\') @include(\'shop::home.new-products\') @include(\'shop::home.advertisements.advertisement-two\')</p>', '<p>We love to craft softwares and solve the real world problems with the binaries. We are highly committed to our goals. We invest our resources to create world class easy to use softwares and applications for the enterprise business with the top notch, on the edge technology expertise.</p>', '<div class=\"col-lg-6 col-md-12 col-sm-12 no-padding\"><ul type=\"none\"><li><a href=\"{!! url(\'page/about-us\') !!}\">About Us</a></li><li><a href=\"{!! url(\'page/cutomer-service\') !!}\">Customer Service</a></li><li><a href=\"{!! url(\'page/whats-new\') !!}\">What&rsquo;s New</a></li><li><a href=\"{!! url(\'page/contact-us\') !!}\">Contact Us </a></li></ul></div><div class=\"col-lg-6 col-md-12 col-sm-12 no-padding\"><ul type=\"none\"><li><a href=\"{!! url(\'page/return-policy\') !!}\"> Order and Returns </a></li><li><a href=\"{!! url(\'page/payment-policy\') !!}\"> Payment Policy </a></li><li><a href=\"{!! url(\'page/shipping-policy\') !!}\"> Shipping Policy</a></li><li><a href=\"{!! url(\'page/privacy-policy\') !!}\"> Privacy and Cookies Policy </a></li></ul></div>', 1, NULL, 9, 10, 10, '<div class=\"social-icons col-lg-6\"><a href=\"https://webkul.com\" target=\"_blank\" class=\"unset\" rel=\"noopener noreferrer\"><i class=\"fs24 within-circle rango-facebook\" title=\"facebook\"></i> </a> <a href=\"https://webkul.com\" target=\"_blank\" class=\"unset\" rel=\"noopener noreferrer\"><i class=\"fs24 within-circle rango-twitter\" title=\"twitter\"></i> </a> <a href=\"https://webkul.com\" target=\"_blank\" class=\"unset\" rel=\"noopener noreferrer\"><i class=\"fs24 within-circle rango-linked-in\" title=\"linkedin\"></i> </a> <a href=\"https://webkul.com\" target=\"_blank\" class=\"unset\" rel=\"noopener noreferrer\"><i class=\"fs24 within-circle rango-pintrest\" title=\"Pinterest\"></i> </a> <a href=\"https://webkul.com\" target=\"_blank\" class=\"unset\" rel=\"noopener noreferrer\"><i class=\"fs24 within-circle rango-youtube\" title=\"Youtube\"></i> </a> <a href=\"https://webkul.com\" target=\"_blank\" class=\"unset\" rel=\"noopener noreferrer\"><i class=\"fs24 within-circle rango-instagram\" title=\"instagram\"></i></a></div>', NULL, NULL, NULL, '<div class=\"row col-12 remove-padding-margin\"><div class=\"col-lg-4 col-sm-12 product-policy-wrapper\"><div class=\"card\"><div class=\"policy\"><div class=\"left\"><i class=\"rango-van-ship fs40\"></i></div> <div class=\"right\"><span class=\"font-setting fs20\">Free Shipping on Order $20 or More</span></div></div></div></div> <div class=\"col-lg-4 col-sm-12 product-policy-wrapper\"><div class=\"card\"><div class=\"policy\"><div class=\"left\"><i class=\"rango-exchnage fs40\"></i></div> <div class=\"right\"><span class=\"font-setting fs20\">Product Replace &amp; Return Available </span></div></div></div></div> <div class=\"col-lg-4 col-sm-12 product-policy-wrapper\"><div class=\"card\"><div class=\"policy\"><div class=\"left\"><i class=\"rango-exchnage fs40\"></i></div> <div class=\"right\"><span class=\"font-setting fs20\">Product Exchange and EMI Available </span></div></div></div></div></div>', 'en', 'default', '5');

-- --------------------------------------------------------

--
-- Estrutura da tabela `wishlist`
--

CREATE TABLE `wishlist` (
  `id` int(10) UNSIGNED NOT NULL,
  `channel_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `item_options` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`item_options`)),
  `moved_to_cart` date DEFAULT NULL,
  `shared` tinyint(1) DEFAULT NULL,
  `time_of_moving` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `additional` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`additional`))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `addresses`
--
ALTER TABLE `addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `addresses_customer_id_foreign` (`customer_id`),
  ADD KEY `addresses_cart_id_foreign` (`cart_id`),
  ADD KEY `addresses_order_id_foreign` (`order_id`);

--
-- Índices para tabela `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`),
  ADD UNIQUE KEY `admins_api_token_unique` (`api_token`);

--
-- Índices para tabela `admin_password_resets`
--
ALTER TABLE `admin_password_resets`
  ADD KEY `admin_password_resets_email_index` (`email`);

--
-- Índices para tabela `attributes`
--
ALTER TABLE `attributes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `attributes_code_unique` (`code`);

--
-- Índices para tabela `attribute_families`
--
ALTER TABLE `attribute_families`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `attribute_groups`
--
ALTER TABLE `attribute_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `attribute_groups_attribute_family_id_name_unique` (`attribute_family_id`,`name`);

--
-- Índices para tabela `attribute_group_mappings`
--
ALTER TABLE `attribute_group_mappings`
  ADD PRIMARY KEY (`attribute_id`,`attribute_group_id`),
  ADD KEY `attribute_group_mappings_attribute_group_id_foreign` (`attribute_group_id`);

--
-- Índices para tabela `attribute_options`
--
ALTER TABLE `attribute_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `attribute_options_attribute_id_foreign` (`attribute_id`);

--
-- Índices para tabela `attribute_option_translations`
--
ALTER TABLE `attribute_option_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `attribute_option_translations_attribute_option_id_locale_unique` (`attribute_option_id`,`locale`);

--
-- Índices para tabela `attribute_translations`
--
ALTER TABLE `attribute_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `attribute_translations_attribute_id_locale_unique` (`attribute_id`,`locale`);

--
-- Índices para tabela `bookings`
--
ALTER TABLE `bookings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bookings_order_id_foreign` (`order_id`),
  ADD KEY `bookings_product_id_foreign` (`product_id`);

--
-- Índices para tabela `booking_products`
--
ALTER TABLE `booking_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `booking_products_product_id_foreign` (`product_id`);

--
-- Índices para tabela `booking_product_appointment_slots`
--
ALTER TABLE `booking_product_appointment_slots`
  ADD PRIMARY KEY (`id`),
  ADD KEY `booking_product_appointment_slots_booking_product_id_foreign` (`booking_product_id`);

--
-- Índices para tabela `booking_product_default_slots`
--
ALTER TABLE `booking_product_default_slots`
  ADD PRIMARY KEY (`id`),
  ADD KEY `booking_product_default_slots_booking_product_id_foreign` (`booking_product_id`);

--
-- Índices para tabela `booking_product_event_tickets`
--
ALTER TABLE `booking_product_event_tickets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `booking_product_event_tickets_booking_product_id_foreign` (`booking_product_id`);

--
-- Índices para tabela `booking_product_event_ticket_translations`
--
ALTER TABLE `booking_product_event_ticket_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `booking_product_event_ticket_translations_locale_unique` (`booking_product_event_ticket_id`,`locale`);

--
-- Índices para tabela `booking_product_rental_slots`
--
ALTER TABLE `booking_product_rental_slots`
  ADD PRIMARY KEY (`id`),
  ADD KEY `booking_product_rental_slots_booking_product_id_foreign` (`booking_product_id`);

--
-- Índices para tabela `booking_product_table_slots`
--
ALTER TABLE `booking_product_table_slots`
  ADD PRIMARY KEY (`id`),
  ADD KEY `booking_product_table_slots_booking_product_id_foreign` (`booking_product_id`);

--
-- Índices para tabela `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cart_customer_id_foreign` (`customer_id`),
  ADD KEY `cart_channel_id_foreign` (`channel_id`);

--
-- Índices para tabela `cart_items`
--
ALTER TABLE `cart_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cart_items_product_id_foreign` (`product_id`),
  ADD KEY `cart_items_cart_id_foreign` (`cart_id`),
  ADD KEY `cart_items_tax_category_id_foreign` (`tax_category_id`),
  ADD KEY `cart_items_parent_id_foreign` (`parent_id`);

--
-- Índices para tabela `cart_item_inventories`
--
ALTER TABLE `cart_item_inventories`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `cart_payment`
--
ALTER TABLE `cart_payment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cart_payment_cart_id_foreign` (`cart_id`);

--
-- Índices para tabela `cart_rules`
--
ALTER TABLE `cart_rules`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `cart_rule_channels`
--
ALTER TABLE `cart_rule_channels`
  ADD PRIMARY KEY (`cart_rule_id`,`channel_id`),
  ADD KEY `cart_rule_channels_channel_id_foreign` (`channel_id`);

--
-- Índices para tabela `cart_rule_coupons`
--
ALTER TABLE `cart_rule_coupons`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cart_rule_coupons_cart_rule_id_foreign` (`cart_rule_id`);

--
-- Índices para tabela `cart_rule_coupon_usage`
--
ALTER TABLE `cart_rule_coupon_usage`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cart_rule_coupon_usage_cart_rule_coupon_id_foreign` (`cart_rule_coupon_id`),
  ADD KEY `cart_rule_coupon_usage_customer_id_foreign` (`customer_id`);

--
-- Índices para tabela `cart_rule_customers`
--
ALTER TABLE `cart_rule_customers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cart_rule_customers_cart_rule_id_foreign` (`cart_rule_id`),
  ADD KEY `cart_rule_customers_customer_id_foreign` (`customer_id`);

--
-- Índices para tabela `cart_rule_customer_groups`
--
ALTER TABLE `cart_rule_customer_groups`
  ADD PRIMARY KEY (`cart_rule_id`,`customer_group_id`),
  ADD KEY `cart_rule_customer_groups_customer_group_id_foreign` (`customer_group_id`);

--
-- Índices para tabela `cart_rule_translations`
--
ALTER TABLE `cart_rule_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cart_rule_translations_cart_rule_id_locale_unique` (`cart_rule_id`,`locale`);

--
-- Índices para tabela `cart_shipping_rates`
--
ALTER TABLE `cart_shipping_rates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cart_shipping_rates_cart_address_id_foreign` (`cart_address_id`);

--
-- Índices para tabela `catalog_rules`
--
ALTER TABLE `catalog_rules`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `catalog_rule_channels`
--
ALTER TABLE `catalog_rule_channels`
  ADD PRIMARY KEY (`catalog_rule_id`,`channel_id`),
  ADD KEY `catalog_rule_channels_channel_id_foreign` (`channel_id`);

--
-- Índices para tabela `catalog_rule_customer_groups`
--
ALTER TABLE `catalog_rule_customer_groups`
  ADD PRIMARY KEY (`catalog_rule_id`,`customer_group_id`),
  ADD KEY `catalog_rule_customer_groups_customer_group_id_foreign` (`customer_group_id`);

--
-- Índices para tabela `catalog_rule_products`
--
ALTER TABLE `catalog_rule_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `catalog_rule_products_product_id_foreign` (`product_id`),
  ADD KEY `catalog_rule_products_customer_group_id_foreign` (`customer_group_id`),
  ADD KEY `catalog_rule_products_catalog_rule_id_foreign` (`catalog_rule_id`),
  ADD KEY `catalog_rule_products_channel_id_foreign` (`channel_id`);

--
-- Índices para tabela `catalog_rule_product_prices`
--
ALTER TABLE `catalog_rule_product_prices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `catalog_rule_product_prices_product_id_foreign` (`product_id`),
  ADD KEY `catalog_rule_product_prices_customer_group_id_foreign` (`customer_group_id`),
  ADD KEY `catalog_rule_product_prices_catalog_rule_id_foreign` (`catalog_rule_id`),
  ADD KEY `catalog_rule_product_prices_channel_id_foreign` (`channel_id`);

--
-- Índices para tabela `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categories__lft__rgt_parent_id_index` (`_lft`,`_rgt`,`parent_id`);

--
-- Índices para tabela `category_filterable_attributes`
--
ALTER TABLE `category_filterable_attributes`
  ADD KEY `category_filterable_attributes_category_id_foreign` (`category_id`),
  ADD KEY `category_filterable_attributes_attribute_id_foreign` (`attribute_id`);

--
-- Índices para tabela `category_translations`
--
ALTER TABLE `category_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `category_translations_category_id_slug_locale_unique` (`category_id`,`slug`,`locale`),
  ADD KEY `category_translations_locale_id_foreign` (`locale_id`);

--
-- Índices para tabela `channels`
--
ALTER TABLE `channels`
  ADD PRIMARY KEY (`id`),
  ADD KEY `channels_default_locale_id_foreign` (`default_locale_id`),
  ADD KEY `channels_base_currency_id_foreign` (`base_currency_id`),
  ADD KEY `channels_root_category_id_foreign` (`root_category_id`);

--
-- Índices para tabela `channel_currencies`
--
ALTER TABLE `channel_currencies`
  ADD PRIMARY KEY (`channel_id`,`currency_id`),
  ADD KEY `channel_currencies_currency_id_foreign` (`currency_id`);

--
-- Índices para tabela `channel_inventory_sources`
--
ALTER TABLE `channel_inventory_sources`
  ADD UNIQUE KEY `channel_inventory_sources_channel_id_inventory_source_id_unique` (`channel_id`,`inventory_source_id`),
  ADD KEY `channel_inventory_sources_inventory_source_id_foreign` (`inventory_source_id`);

--
-- Índices para tabela `channel_locales`
--
ALTER TABLE `channel_locales`
  ADD PRIMARY KEY (`channel_id`,`locale_id`),
  ADD KEY `channel_locales_locale_id_foreign` (`locale_id`);

--
-- Índices para tabela `channel_translations`
--
ALTER TABLE `channel_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `channel_translations_channel_id_locale_unique` (`channel_id`,`locale`),
  ADD KEY `channel_translations_locale_index` (`locale`);

--
-- Índices para tabela `cms_pages`
--
ALTER TABLE `cms_pages`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `cms_page_channels`
--
ALTER TABLE `cms_page_channels`
  ADD UNIQUE KEY `cms_page_channels_cms_page_id_channel_id_unique` (`cms_page_id`,`channel_id`),
  ADD KEY `cms_page_channels_channel_id_foreign` (`channel_id`);

--
-- Índices para tabela `cms_page_translations`
--
ALTER TABLE `cms_page_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cms_page_translations_cms_page_id_url_key_locale_unique` (`cms_page_id`,`url_key`,`locale`);

--
-- Índices para tabela `core_config`
--
ALTER TABLE `core_config`
  ADD PRIMARY KEY (`id`),
  ADD KEY `core_config_channel_id_foreign` (`channel_code`);

--
-- Índices para tabela `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `country_states`
--
ALTER TABLE `country_states`
  ADD PRIMARY KEY (`id`),
  ADD KEY `country_states_country_id_foreign` (`country_id`);

--
-- Índices para tabela `country_state_translations`
--
ALTER TABLE `country_state_translations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `country_state_translations_country_state_id_foreign` (`country_state_id`);

--
-- Índices para tabela `country_translations`
--
ALTER TABLE `country_translations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `country_translations_country_id_foreign` (`country_id`);

--
-- Índices para tabela `currencies`
--
ALTER TABLE `currencies`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `currency_exchange_rates`
--
ALTER TABLE `currency_exchange_rates`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `currency_exchange_rates_target_currency_unique` (`target_currency`);

--
-- Índices para tabela `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `customers_email_unique` (`email`),
  ADD UNIQUE KEY `customers_api_token_unique` (`api_token`),
  ADD KEY `customers_customer_group_id_foreign` (`customer_group_id`);

--
-- Índices para tabela `customer_groups`
--
ALTER TABLE `customer_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `customer_groups_code_unique` (`code`);

--
-- Índices para tabela `customer_password_resets`
--
ALTER TABLE `customer_password_resets`
  ADD KEY `customer_password_resets_email_index` (`email`);

--
-- Índices para tabela `customer_social_accounts`
--
ALTER TABLE `customer_social_accounts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `customer_social_accounts_provider_id_unique` (`provider_id`),
  ADD KEY `customer_social_accounts_customer_id_foreign` (`customer_id`);

--
-- Índices para tabela `downloadable_link_purchased`
--
ALTER TABLE `downloadable_link_purchased`
  ADD PRIMARY KEY (`id`),
  ADD KEY `downloadable_link_purchased_customer_id_foreign` (`customer_id`),
  ADD KEY `downloadable_link_purchased_order_id_foreign` (`order_id`),
  ADD KEY `downloadable_link_purchased_order_item_id_foreign` (`order_item_id`);

--
-- Índices para tabela `inventory_sources`
--
ALTER TABLE `inventory_sources`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `inventory_sources_code_unique` (`code`);

--
-- Índices para tabela `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoices_order_id_foreign` (`order_id`),
  ADD KEY `invoices_order_address_id_foreign` (`order_address_id`);

--
-- Índices para tabela `invoice_items`
--
ALTER TABLE `invoice_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `invoice_items_invoice_id_foreign` (`invoice_id`),
  ADD KEY `invoice_items_parent_id_foreign` (`parent_id`);

--
-- Índices para tabela `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Índices para tabela `locales`
--
ALTER TABLE `locales`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `locales_code_unique` (`code`);

--
-- Índices para tabela `marketing_campaigns`
--
ALTER TABLE `marketing_campaigns`
  ADD PRIMARY KEY (`id`),
  ADD KEY `marketing_campaigns_channel_id_foreign` (`channel_id`),
  ADD KEY `marketing_campaigns_customer_group_id_foreign` (`customer_group_id`),
  ADD KEY `marketing_campaigns_marketing_template_id_foreign` (`marketing_template_id`),
  ADD KEY `marketing_campaigns_marketing_event_id_foreign` (`marketing_event_id`);

--
-- Índices para tabela `marketing_events`
--
ALTER TABLE `marketing_events`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `marketing_templates`
--
ALTER TABLE `marketing_templates`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `orders_increment_id_unique` (`increment_id`),
  ADD KEY `orders_customer_id_foreign` (`customer_id`),
  ADD KEY `orders_channel_id_foreign` (`channel_id`);

--
-- Índices para tabela `order_brands`
--
ALTER TABLE `order_brands`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_brands_order_id_foreign` (`order_id`),
  ADD KEY `order_brands_order_item_id_foreign` (`order_item_id`),
  ADD KEY `order_brands_product_id_foreign` (`product_id`),
  ADD KEY `order_brands_brand_foreign` (`brand`);

--
-- Índices para tabela `order_comments`
--
ALTER TABLE `order_comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_comments_order_id_foreign` (`order_id`);

--
-- Índices para tabela `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_items_order_id_foreign` (`order_id`),
  ADD KEY `order_items_parent_id_foreign` (`parent_id`);

--
-- Índices para tabela `order_payment`
--
ALTER TABLE `order_payment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_payment_order_id_foreign` (`order_id`);

--
-- Índices para tabela `order_transactions`
--
ALTER TABLE `order_transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_transactions_order_id_foreign` (`order_id`);

--
-- Índices para tabela `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Índices para tabela `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `products_sku_unique` (`sku`),
  ADD KEY `products_attribute_family_id_foreign` (`attribute_family_id`),
  ADD KEY `products_parent_id_foreign` (`parent_id`);

--
-- Índices para tabela `product_attribute_values`
--
ALTER TABLE `product_attribute_values`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `chanel_locale_attribute_value_index_unique` (`channel`,`locale`,`attribute_id`,`product_id`),
  ADD KEY `product_attribute_values_product_id_foreign` (`product_id`),
  ADD KEY `product_attribute_values_attribute_id_foreign` (`attribute_id`);

--
-- Índices para tabela `product_bundle_options`
--
ALTER TABLE `product_bundle_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_bundle_options_product_id_foreign` (`product_id`);

--
-- Índices para tabela `product_bundle_option_products`
--
ALTER TABLE `product_bundle_option_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_bundle_option_products_product_bundle_option_id_foreign` (`product_bundle_option_id`),
  ADD KEY `product_bundle_option_products_product_id_foreign` (`product_id`);

--
-- Índices para tabela `product_bundle_option_translations`
--
ALTER TABLE `product_bundle_option_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `product_bundle_option_translations_option_id_locale_unique` (`product_bundle_option_id`,`locale`);

--
-- Índices para tabela `product_categories`
--
ALTER TABLE `product_categories`
  ADD KEY `product_categories_product_id_foreign` (`product_id`),
  ADD KEY `product_categories_category_id_foreign` (`category_id`);

--
-- Índices para tabela `product_cross_sells`
--
ALTER TABLE `product_cross_sells`
  ADD KEY `product_cross_sells_parent_id_foreign` (`parent_id`),
  ADD KEY `product_cross_sells_child_id_foreign` (`child_id`);

--
-- Índices para tabela `product_customer_group_prices`
--
ALTER TABLE `product_customer_group_prices`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_customer_group_prices_product_id_foreign` (`product_id`),
  ADD KEY `product_customer_group_prices_customer_group_id_foreign` (`customer_group_id`);

--
-- Índices para tabela `product_downloadable_links`
--
ALTER TABLE `product_downloadable_links`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_downloadable_links_product_id_foreign` (`product_id`);

--
-- Índices para tabela `product_downloadable_link_translations`
--
ALTER TABLE `product_downloadable_link_translations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `link_translations_link_id_foreign` (`product_downloadable_link_id`);

--
-- Índices para tabela `product_downloadable_samples`
--
ALTER TABLE `product_downloadable_samples`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_downloadable_samples_product_id_foreign` (`product_id`);

--
-- Índices para tabela `product_downloadable_sample_translations`
--
ALTER TABLE `product_downloadable_sample_translations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sample_translations_sample_id_foreign` (`product_downloadable_sample_id`);

--
-- Índices para tabela `product_flat`
--
ALTER TABLE `product_flat`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `product_flat_unique_index` (`product_id`,`channel`,`locale`),
  ADD KEY `product_flat_parent_id_foreign` (`parent_id`);

--
-- Índices para tabela `product_grouped_products`
--
ALTER TABLE `product_grouped_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_grouped_products_product_id_foreign` (`product_id`),
  ADD KEY `product_grouped_products_associated_product_id_foreign` (`associated_product_id`);

--
-- Índices para tabela `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_images_product_id_foreign` (`product_id`);

--
-- Índices para tabela `product_inventories`
--
ALTER TABLE `product_inventories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `product_source_vendor_index_unique` (`product_id`,`inventory_source_id`,`vendor_id`),
  ADD KEY `product_inventories_inventory_source_id_foreign` (`inventory_source_id`);

--
-- Índices para tabela `product_ordered_inventories`
--
ALTER TABLE `product_ordered_inventories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `product_ordered_inventories_product_id_channel_id_unique` (`product_id`,`channel_id`),
  ADD KEY `product_ordered_inventories_channel_id_foreign` (`channel_id`);

--
-- Índices para tabela `product_relations`
--
ALTER TABLE `product_relations`
  ADD KEY `product_relations_parent_id_foreign` (`parent_id`),
  ADD KEY `product_relations_child_id_foreign` (`child_id`);

--
-- Índices para tabela `product_reviews`
--
ALTER TABLE `product_reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_reviews_product_id_foreign` (`product_id`),
  ADD KEY `product_reviews_customer_id_foreign` (`customer_id`);

--
-- Índices para tabela `product_super_attributes`
--
ALTER TABLE `product_super_attributes`
  ADD KEY `product_super_attributes_product_id_foreign` (`product_id`),
  ADD KEY `product_super_attributes_attribute_id_foreign` (`attribute_id`);

--
-- Índices para tabela `product_up_sells`
--
ALTER TABLE `product_up_sells`
  ADD KEY `product_up_sells_parent_id_foreign` (`parent_id`),
  ADD KEY `product_up_sells_child_id_foreign` (`child_id`);

--
-- Índices para tabela `product_videos`
--
ALTER TABLE `product_videos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_videos_product_id_foreign` (`product_id`);

--
-- Índices para tabela `refunds`
--
ALTER TABLE `refunds`
  ADD PRIMARY KEY (`id`),
  ADD KEY `refunds_order_id_foreign` (`order_id`);

--
-- Índices para tabela `refund_items`
--
ALTER TABLE `refund_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `refund_items_order_item_id_foreign` (`order_item_id`),
  ADD KEY `refund_items_refund_id_foreign` (`refund_id`),
  ADD KEY `refund_items_parent_id_foreign` (`parent_id`);

--
-- Índices para tabela `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `shipments`
--
ALTER TABLE `shipments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shipments_order_id_foreign` (`order_id`),
  ADD KEY `shipments_inventory_source_id_foreign` (`inventory_source_id`),
  ADD KEY `shipments_order_address_id_foreign` (`order_address_id`);

--
-- Índices para tabela `shipment_items`
--
ALTER TABLE `shipment_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shipment_items_shipment_id_foreign` (`shipment_id`);

--
-- Índices para tabela `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sliders_channel_id_foreign` (`channel_id`);

--
-- Índices para tabela `subscribers_list`
--
ALTER TABLE `subscribers_list`
  ADD PRIMARY KEY (`id`),
  ADD KEY `subscribers_list_channel_id_foreign` (`channel_id`),
  ADD KEY `subscribers_list_customer_id_foreign` (`customer_id`);

--
-- Índices para tabela `tax_categories`
--
ALTER TABLE `tax_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tax_categories_code_unique` (`code`);

--
-- Índices para tabela `tax_categories_tax_rates`
--
ALTER TABLE `tax_categories_tax_rates`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tax_map_index_unique` (`tax_category_id`,`tax_rate_id`),
  ADD KEY `tax_categories_tax_rates_tax_rate_id_foreign` (`tax_rate_id`);

--
-- Índices para tabela `tax_rates`
--
ALTER TABLE `tax_rates`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tax_rates_identifier_unique` (`identifier`);

--
-- Índices para tabela `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Índices para tabela `velocity_contents`
--
ALTER TABLE `velocity_contents`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `velocity_contents_translations`
--
ALTER TABLE `velocity_contents_translations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `velocity_contents_translations_content_id_foreign` (`content_id`);

--
-- Índices para tabela `velocity_customer_compare_products`
--
ALTER TABLE `velocity_customer_compare_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `velocity_customer_compare_products_product_flat_id_foreign` (`product_flat_id`),
  ADD KEY `velocity_customer_compare_products_customer_id_foreign` (`customer_id`);

--
-- Índices para tabela `velocity_meta_data`
--
ALTER TABLE `velocity_meta_data`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `wishlist`
--
ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`id`),
  ADD KEY `wishlist_channel_id_foreign` (`channel_id`),
  ADD KEY `wishlist_product_id_foreign` (`product_id`),
  ADD KEY `wishlist_customer_id_foreign` (`customer_id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `addresses`
--
ALTER TABLE `addresses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de tabela `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `attributes`
--
ALTER TABLE `attributes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT de tabela `attribute_families`
--
ALTER TABLE `attribute_families`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `attribute_groups`
--
ALTER TABLE `attribute_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de tabela `attribute_options`
--
ALTER TABLE `attribute_options`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de tabela `attribute_option_translations`
--
ALTER TABLE `attribute_option_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de tabela `attribute_translations`
--
ALTER TABLE `attribute_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT de tabela `bookings`
--
ALTER TABLE `bookings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `booking_products`
--
ALTER TABLE `booking_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `booking_product_appointment_slots`
--
ALTER TABLE `booking_product_appointment_slots`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `booking_product_default_slots`
--
ALTER TABLE `booking_product_default_slots`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `booking_product_event_tickets`
--
ALTER TABLE `booking_product_event_tickets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `booking_product_event_ticket_translations`
--
ALTER TABLE `booking_product_event_ticket_translations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `booking_product_rental_slots`
--
ALTER TABLE `booking_product_rental_slots`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `booking_product_table_slots`
--
ALTER TABLE `booking_product_table_slots`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de tabela `cart_items`
--
ALTER TABLE `cart_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de tabela `cart_item_inventories`
--
ALTER TABLE `cart_item_inventories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `cart_payment`
--
ALTER TABLE `cart_payment`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de tabela `cart_rules`
--
ALTER TABLE `cart_rules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `cart_rule_coupons`
--
ALTER TABLE `cart_rule_coupons`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `cart_rule_coupon_usage`
--
ALTER TABLE `cart_rule_coupon_usage`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `cart_rule_customers`
--
ALTER TABLE `cart_rule_customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `cart_rule_translations`
--
ALTER TABLE `cart_rule_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `cart_shipping_rates`
--
ALTER TABLE `cart_shipping_rates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de tabela `catalog_rules`
--
ALTER TABLE `catalog_rules`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `catalog_rule_products`
--
ALTER TABLE `catalog_rule_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `catalog_rule_product_prices`
--
ALTER TABLE `catalog_rule_product_prices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `category_translations`
--
ALTER TABLE `category_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `channels`
--
ALTER TABLE `channels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `channel_translations`
--
ALTER TABLE `channel_translations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de tabela `cms_pages`
--
ALTER TABLE `cms_pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de tabela `cms_page_translations`
--
ALTER TABLE `cms_page_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT de tabela `core_config`
--
ALTER TABLE `core_config`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;

--
-- AUTO_INCREMENT de tabela `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=256;

--
-- AUTO_INCREMENT de tabela `country_states`
--
ALTER TABLE `country_states`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=587;

--
-- AUTO_INCREMENT de tabela `country_state_translations`
--
ALTER TABLE `country_state_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3995;

--
-- AUTO_INCREMENT de tabela `country_translations`
--
ALTER TABLE `country_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1786;

--
-- AUTO_INCREMENT de tabela `currencies`
--
ALTER TABLE `currencies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `currency_exchange_rates`
--
ALTER TABLE `currency_exchange_rates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `customer_groups`
--
ALTER TABLE `customer_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `customer_social_accounts`
--
ALTER TABLE `customer_social_accounts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `downloadable_link_purchased`
--
ALTER TABLE `downloadable_link_purchased`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `inventory_sources`
--
ALTER TABLE `inventory_sources`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `invoice_items`
--
ALTER TABLE `invoice_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de tabela `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `locales`
--
ALTER TABLE `locales`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de tabela `marketing_campaigns`
--
ALTER TABLE `marketing_campaigns`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `marketing_events`
--
ALTER TABLE `marketing_events`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `marketing_templates`
--
ALTER TABLE `marketing_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=200;

--
-- AUTO_INCREMENT de tabela `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `order_brands`
--
ALTER TABLE `order_brands`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `order_comments`
--
ALTER TABLE `order_comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de tabela `order_payment`
--
ALTER TABLE `order_payment`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `order_transactions`
--
ALTER TABLE `order_transactions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de tabela `product_attribute_values`
--
ALTER TABLE `product_attribute_values`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT de tabela `product_bundle_options`
--
ALTER TABLE `product_bundle_options`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `product_bundle_option_products`
--
ALTER TABLE `product_bundle_option_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `product_bundle_option_translations`
--
ALTER TABLE `product_bundle_option_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `product_customer_group_prices`
--
ALTER TABLE `product_customer_group_prices`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `product_downloadable_links`
--
ALTER TABLE `product_downloadable_links`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `product_downloadable_link_translations`
--
ALTER TABLE `product_downloadable_link_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `product_downloadable_samples`
--
ALTER TABLE `product_downloadable_samples`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `product_downloadable_sample_translations`
--
ALTER TABLE `product_downloadable_sample_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `product_flat`
--
ALTER TABLE `product_flat`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de tabela `product_grouped_products`
--
ALTER TABLE `product_grouped_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `product_inventories`
--
ALTER TABLE `product_inventories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `product_ordered_inventories`
--
ALTER TABLE `product_ordered_inventories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `product_reviews`
--
ALTER TABLE `product_reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `product_videos`
--
ALTER TABLE `product_videos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `refunds`
--
ALTER TABLE `refunds`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `refund_items`
--
ALTER TABLE `refund_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de tabela `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `shipments`
--
ALTER TABLE `shipments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `shipment_items`
--
ALTER TABLE `shipment_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `subscribers_list`
--
ALTER TABLE `subscribers_list`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `tax_categories`
--
ALTER TABLE `tax_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `tax_categories_tax_rates`
--
ALTER TABLE `tax_categories_tax_rates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `tax_rates`
--
ALTER TABLE `tax_rates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `velocity_contents`
--
ALTER TABLE `velocity_contents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `velocity_contents_translations`
--
ALTER TABLE `velocity_contents_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de tabela `velocity_customer_compare_products`
--
ALTER TABLE `velocity_customer_compare_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `velocity_meta_data`
--
ALTER TABLE `velocity_meta_data`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de tabela `wishlist`
--
ALTER TABLE `wishlist`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `addresses`
--
ALTER TABLE `addresses`
  ADD CONSTRAINT `addresses_cart_id_foreign` FOREIGN KEY (`cart_id`) REFERENCES `cart` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `addresses_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `addresses_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `attribute_groups`
--
ALTER TABLE `attribute_groups`
  ADD CONSTRAINT `attribute_groups_attribute_family_id_foreign` FOREIGN KEY (`attribute_family_id`) REFERENCES `attribute_families` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `attribute_group_mappings`
--
ALTER TABLE `attribute_group_mappings`
  ADD CONSTRAINT `attribute_group_mappings_attribute_group_id_foreign` FOREIGN KEY (`attribute_group_id`) REFERENCES `attribute_groups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `attribute_group_mappings_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `attribute_options`
--
ALTER TABLE `attribute_options`
  ADD CONSTRAINT `attribute_options_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `attribute_option_translations`
--
ALTER TABLE `attribute_option_translations`
  ADD CONSTRAINT `attribute_option_translations_attribute_option_id_foreign` FOREIGN KEY (`attribute_option_id`) REFERENCES `attribute_options` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `attribute_translations`
--
ALTER TABLE `attribute_translations`
  ADD CONSTRAINT `attribute_translations_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `bookings`
--
ALTER TABLE `bookings`
  ADD CONSTRAINT `bookings_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `bookings_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE SET NULL;

--
-- Limitadores para a tabela `booking_products`
--
ALTER TABLE `booking_products`
  ADD CONSTRAINT `booking_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `booking_product_appointment_slots`
--
ALTER TABLE `booking_product_appointment_slots`
  ADD CONSTRAINT `booking_product_appointment_slots_booking_product_id_foreign` FOREIGN KEY (`booking_product_id`) REFERENCES `booking_products` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `booking_product_default_slots`
--
ALTER TABLE `booking_product_default_slots`
  ADD CONSTRAINT `booking_product_default_slots_booking_product_id_foreign` FOREIGN KEY (`booking_product_id`) REFERENCES `booking_products` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `booking_product_event_tickets`
--
ALTER TABLE `booking_product_event_tickets`
  ADD CONSTRAINT `booking_product_event_tickets_booking_product_id_foreign` FOREIGN KEY (`booking_product_id`) REFERENCES `booking_products` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `booking_product_event_ticket_translations`
--
ALTER TABLE `booking_product_event_ticket_translations`
  ADD CONSTRAINT `booking_product_event_ticket_translations_locale_foreign` FOREIGN KEY (`booking_product_event_ticket_id`) REFERENCES `booking_product_event_tickets` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `booking_product_rental_slots`
--
ALTER TABLE `booking_product_rental_slots`
  ADD CONSTRAINT `booking_product_rental_slots_booking_product_id_foreign` FOREIGN KEY (`booking_product_id`) REFERENCES `booking_products` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `booking_product_table_slots`
--
ALTER TABLE `booking_product_table_slots`
  ADD CONSTRAINT `booking_product_table_slots_booking_product_id_foreign` FOREIGN KEY (`booking_product_id`) REFERENCES `booking_products` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `cart`
--
ALTER TABLE `cart`
  ADD CONSTRAINT `cart_channel_id_foreign` FOREIGN KEY (`channel_id`) REFERENCES `channels` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cart_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `cart_items`
--
ALTER TABLE `cart_items`
  ADD CONSTRAINT `cart_items_cart_id_foreign` FOREIGN KEY (`cart_id`) REFERENCES `cart` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cart_items_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `cart_items` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cart_items_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cart_items_tax_category_id_foreign` FOREIGN KEY (`tax_category_id`) REFERENCES `tax_categories` (`id`);

--
-- Limitadores para a tabela `cart_payment`
--
ALTER TABLE `cart_payment`
  ADD CONSTRAINT `cart_payment_cart_id_foreign` FOREIGN KEY (`cart_id`) REFERENCES `cart` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `cart_rule_channels`
--
ALTER TABLE `cart_rule_channels`
  ADD CONSTRAINT `cart_rule_channels_cart_rule_id_foreign` FOREIGN KEY (`cart_rule_id`) REFERENCES `cart_rules` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cart_rule_channels_channel_id_foreign` FOREIGN KEY (`channel_id`) REFERENCES `channels` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `cart_rule_coupons`
--
ALTER TABLE `cart_rule_coupons`
  ADD CONSTRAINT `cart_rule_coupons_cart_rule_id_foreign` FOREIGN KEY (`cart_rule_id`) REFERENCES `cart_rules` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `cart_rule_coupon_usage`
--
ALTER TABLE `cart_rule_coupon_usage`
  ADD CONSTRAINT `cart_rule_coupon_usage_cart_rule_coupon_id_foreign` FOREIGN KEY (`cart_rule_coupon_id`) REFERENCES `cart_rule_coupons` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cart_rule_coupon_usage_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `cart_rule_customers`
--
ALTER TABLE `cart_rule_customers`
  ADD CONSTRAINT `cart_rule_customers_cart_rule_id_foreign` FOREIGN KEY (`cart_rule_id`) REFERENCES `cart_rules` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cart_rule_customers_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `cart_rule_customer_groups`
--
ALTER TABLE `cart_rule_customer_groups`
  ADD CONSTRAINT `cart_rule_customer_groups_cart_rule_id_foreign` FOREIGN KEY (`cart_rule_id`) REFERENCES `cart_rules` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cart_rule_customer_groups_customer_group_id_foreign` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_groups` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `cart_rule_translations`
--
ALTER TABLE `cart_rule_translations`
  ADD CONSTRAINT `cart_rule_translations_cart_rule_id_foreign` FOREIGN KEY (`cart_rule_id`) REFERENCES `cart_rules` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `cart_shipping_rates`
--
ALTER TABLE `cart_shipping_rates`
  ADD CONSTRAINT `cart_shipping_rates_cart_address_id_foreign` FOREIGN KEY (`cart_address_id`) REFERENCES `addresses` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `catalog_rule_channels`
--
ALTER TABLE `catalog_rule_channels`
  ADD CONSTRAINT `catalog_rule_channels_catalog_rule_id_foreign` FOREIGN KEY (`catalog_rule_id`) REFERENCES `catalog_rules` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `catalog_rule_channels_channel_id_foreign` FOREIGN KEY (`channel_id`) REFERENCES `channels` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `catalog_rule_customer_groups`
--
ALTER TABLE `catalog_rule_customer_groups`
  ADD CONSTRAINT `catalog_rule_customer_groups_catalog_rule_id_foreign` FOREIGN KEY (`catalog_rule_id`) REFERENCES `catalog_rules` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `catalog_rule_customer_groups_customer_group_id_foreign` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_groups` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `catalog_rule_products`
--
ALTER TABLE `catalog_rule_products`
  ADD CONSTRAINT `catalog_rule_products_catalog_rule_id_foreign` FOREIGN KEY (`catalog_rule_id`) REFERENCES `catalog_rules` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `catalog_rule_products_channel_id_foreign` FOREIGN KEY (`channel_id`) REFERENCES `channels` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `catalog_rule_products_customer_group_id_foreign` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_groups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `catalog_rule_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `catalog_rule_product_prices`
--
ALTER TABLE `catalog_rule_product_prices`
  ADD CONSTRAINT `catalog_rule_product_prices_catalog_rule_id_foreign` FOREIGN KEY (`catalog_rule_id`) REFERENCES `catalog_rules` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `catalog_rule_product_prices_channel_id_foreign` FOREIGN KEY (`channel_id`) REFERENCES `channels` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `catalog_rule_product_prices_customer_group_id_foreign` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_groups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `catalog_rule_product_prices_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `category_filterable_attributes`
--
ALTER TABLE `category_filterable_attributes`
  ADD CONSTRAINT `category_filterable_attributes_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `category_filterable_attributes_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `category_translations`
--
ALTER TABLE `category_translations`
  ADD CONSTRAINT `category_translations_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `category_translations_locale_id_foreign` FOREIGN KEY (`locale_id`) REFERENCES `locales` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `channels`
--
ALTER TABLE `channels`
  ADD CONSTRAINT `channels_base_currency_id_foreign` FOREIGN KEY (`base_currency_id`) REFERENCES `currencies` (`id`),
  ADD CONSTRAINT `channels_default_locale_id_foreign` FOREIGN KEY (`default_locale_id`) REFERENCES `locales` (`id`),
  ADD CONSTRAINT `channels_root_category_id_foreign` FOREIGN KEY (`root_category_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL;

--
-- Limitadores para a tabela `channel_currencies`
--
ALTER TABLE `channel_currencies`
  ADD CONSTRAINT `channel_currencies_channel_id_foreign` FOREIGN KEY (`channel_id`) REFERENCES `channels` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `channel_currencies_currency_id_foreign` FOREIGN KEY (`currency_id`) REFERENCES `currencies` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `channel_inventory_sources`
--
ALTER TABLE `channel_inventory_sources`
  ADD CONSTRAINT `channel_inventory_sources_channel_id_foreign` FOREIGN KEY (`channel_id`) REFERENCES `channels` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `channel_inventory_sources_inventory_source_id_foreign` FOREIGN KEY (`inventory_source_id`) REFERENCES `inventory_sources` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `channel_locales`
--
ALTER TABLE `channel_locales`
  ADD CONSTRAINT `channel_locales_channel_id_foreign` FOREIGN KEY (`channel_id`) REFERENCES `channels` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `channel_locales_locale_id_foreign` FOREIGN KEY (`locale_id`) REFERENCES `locales` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `channel_translations`
--
ALTER TABLE `channel_translations`
  ADD CONSTRAINT `channel_translations_channel_id_foreign` FOREIGN KEY (`channel_id`) REFERENCES `channels` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `cms_page_channels`
--
ALTER TABLE `cms_page_channels`
  ADD CONSTRAINT `cms_page_channels_channel_id_foreign` FOREIGN KEY (`channel_id`) REFERENCES `channels` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cms_page_channels_cms_page_id_foreign` FOREIGN KEY (`cms_page_id`) REFERENCES `cms_pages` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `cms_page_translations`
--
ALTER TABLE `cms_page_translations`
  ADD CONSTRAINT `cms_page_translations_cms_page_id_foreign` FOREIGN KEY (`cms_page_id`) REFERENCES `cms_pages` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `country_states`
--
ALTER TABLE `country_states`
  ADD CONSTRAINT `country_states_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `country_state_translations`
--
ALTER TABLE `country_state_translations`
  ADD CONSTRAINT `country_state_translations_country_state_id_foreign` FOREIGN KEY (`country_state_id`) REFERENCES `country_states` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `country_translations`
--
ALTER TABLE `country_translations`
  ADD CONSTRAINT `country_translations_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES `countries` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `currency_exchange_rates`
--
ALTER TABLE `currency_exchange_rates`
  ADD CONSTRAINT `currency_exchange_rates_target_currency_foreign` FOREIGN KEY (`target_currency`) REFERENCES `currencies` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `customers`
--
ALTER TABLE `customers`
  ADD CONSTRAINT `customers_customer_group_id_foreign` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_groups` (`id`) ON DELETE SET NULL;

--
-- Limitadores para a tabela `customer_social_accounts`
--
ALTER TABLE `customer_social_accounts`
  ADD CONSTRAINT `customer_social_accounts_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `downloadable_link_purchased`
--
ALTER TABLE `downloadable_link_purchased`
  ADD CONSTRAINT `downloadable_link_purchased_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `downloadable_link_purchased_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `downloadable_link_purchased_order_item_id_foreign` FOREIGN KEY (`order_item_id`) REFERENCES `order_items` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `invoices`
--
ALTER TABLE `invoices`
  ADD CONSTRAINT `invoices_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `invoice_items`
--
ALTER TABLE `invoice_items`
  ADD CONSTRAINT `invoice_items_invoice_id_foreign` FOREIGN KEY (`invoice_id`) REFERENCES `invoices` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `invoice_items_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `invoice_items` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `marketing_campaigns`
--
ALTER TABLE `marketing_campaigns`
  ADD CONSTRAINT `marketing_campaigns_channel_id_foreign` FOREIGN KEY (`channel_id`) REFERENCES `channels` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `marketing_campaigns_customer_group_id_foreign` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_groups` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `marketing_campaigns_marketing_event_id_foreign` FOREIGN KEY (`marketing_event_id`) REFERENCES `marketing_events` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `marketing_campaigns_marketing_template_id_foreign` FOREIGN KEY (`marketing_template_id`) REFERENCES `marketing_templates` (`id`) ON DELETE SET NULL;

--
-- Limitadores para a tabela `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_channel_id_foreign` FOREIGN KEY (`channel_id`) REFERENCES `channels` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `orders_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE SET NULL;

--
-- Limitadores para a tabela `order_brands`
--
ALTER TABLE `order_brands`
  ADD CONSTRAINT `order_brands_brand_foreign` FOREIGN KEY (`brand`) REFERENCES `attribute_options` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `order_brands_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `order_brands_order_item_id_foreign` FOREIGN KEY (`order_item_id`) REFERENCES `order_items` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `order_brands_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `order_comments`
--
ALTER TABLE `order_comments`
  ADD CONSTRAINT `order_comments_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `order_items`
--
ALTER TABLE `order_items`
  ADD CONSTRAINT `order_items_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `order_items_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `order_items` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `order_payment`
--
ALTER TABLE `order_payment`
  ADD CONSTRAINT `order_payment_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `order_transactions`
--
ALTER TABLE `order_transactions`
  ADD CONSTRAINT `order_transactions_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_attribute_family_id_foreign` FOREIGN KEY (`attribute_family_id`) REFERENCES `attribute_families` (`id`),
  ADD CONSTRAINT `products_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `product_attribute_values`
--
ALTER TABLE `product_attribute_values`
  ADD CONSTRAINT `product_attribute_values_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_attribute_values_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `product_bundle_options`
--
ALTER TABLE `product_bundle_options`
  ADD CONSTRAINT `product_bundle_options_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `product_bundle_option_products`
--
ALTER TABLE `product_bundle_option_products`
  ADD CONSTRAINT `product_bundle_option_products_product_bundle_option_id_foreign` FOREIGN KEY (`product_bundle_option_id`) REFERENCES `product_bundle_options` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_bundle_option_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `product_bundle_option_translations`
--
ALTER TABLE `product_bundle_option_translations`
  ADD CONSTRAINT `product_bundle_option_translations_option_id_foreign` FOREIGN KEY (`product_bundle_option_id`) REFERENCES `product_bundle_options` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `product_categories`
--
ALTER TABLE `product_categories`
  ADD CONSTRAINT `product_categories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_categories_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `product_cross_sells`
--
ALTER TABLE `product_cross_sells`
  ADD CONSTRAINT `product_cross_sells_child_id_foreign` FOREIGN KEY (`child_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_cross_sells_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `product_customer_group_prices`
--
ALTER TABLE `product_customer_group_prices`
  ADD CONSTRAINT `product_customer_group_prices_customer_group_id_foreign` FOREIGN KEY (`customer_group_id`) REFERENCES `customer_groups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_customer_group_prices_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `product_downloadable_links`
--
ALTER TABLE `product_downloadable_links`
  ADD CONSTRAINT `product_downloadable_links_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `product_downloadable_link_translations`
--
ALTER TABLE `product_downloadable_link_translations`
  ADD CONSTRAINT `link_translations_link_id_foreign` FOREIGN KEY (`product_downloadable_link_id`) REFERENCES `product_downloadable_links` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `product_downloadable_samples`
--
ALTER TABLE `product_downloadable_samples`
  ADD CONSTRAINT `product_downloadable_samples_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `product_downloadable_sample_translations`
--
ALTER TABLE `product_downloadable_sample_translations`
  ADD CONSTRAINT `sample_translations_sample_id_foreign` FOREIGN KEY (`product_downloadable_sample_id`) REFERENCES `product_downloadable_samples` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `product_flat`
--
ALTER TABLE `product_flat`
  ADD CONSTRAINT `product_flat_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `product_flat` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_flat_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `product_grouped_products`
--
ALTER TABLE `product_grouped_products`
  ADD CONSTRAINT `product_grouped_products_associated_product_id_foreign` FOREIGN KEY (`associated_product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_grouped_products_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `product_images`
--
ALTER TABLE `product_images`
  ADD CONSTRAINT `product_images_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `product_inventories`
--
ALTER TABLE `product_inventories`
  ADD CONSTRAINT `product_inventories_inventory_source_id_foreign` FOREIGN KEY (`inventory_source_id`) REFERENCES `inventory_sources` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_inventories_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `product_ordered_inventories`
--
ALTER TABLE `product_ordered_inventories`
  ADD CONSTRAINT `product_ordered_inventories_channel_id_foreign` FOREIGN KEY (`channel_id`) REFERENCES `channels` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_ordered_inventories_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `product_relations`
--
ALTER TABLE `product_relations`
  ADD CONSTRAINT `product_relations_child_id_foreign` FOREIGN KEY (`child_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_relations_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `product_reviews`
--
ALTER TABLE `product_reviews`
  ADD CONSTRAINT `product_reviews_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `product_super_attributes`
--
ALTER TABLE `product_super_attributes`
  ADD CONSTRAINT `product_super_attributes_attribute_id_foreign` FOREIGN KEY (`attribute_id`) REFERENCES `attributes` (`id`),
  ADD CONSTRAINT `product_super_attributes_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `product_up_sells`
--
ALTER TABLE `product_up_sells`
  ADD CONSTRAINT `product_up_sells_child_id_foreign` FOREIGN KEY (`child_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_up_sells_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `product_videos`
--
ALTER TABLE `product_videos`
  ADD CONSTRAINT `product_videos_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `refunds`
--
ALTER TABLE `refunds`
  ADD CONSTRAINT `refunds_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `refund_items`
--
ALTER TABLE `refund_items`
  ADD CONSTRAINT `refund_items_order_item_id_foreign` FOREIGN KEY (`order_item_id`) REFERENCES `order_items` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `refund_items_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `refund_items` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `refund_items_refund_id_foreign` FOREIGN KEY (`refund_id`) REFERENCES `refunds` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `shipments`
--
ALTER TABLE `shipments`
  ADD CONSTRAINT `shipments_inventory_source_id_foreign` FOREIGN KEY (`inventory_source_id`) REFERENCES `inventory_sources` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `shipments_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `shipment_items`
--
ALTER TABLE `shipment_items`
  ADD CONSTRAINT `shipment_items_shipment_id_foreign` FOREIGN KEY (`shipment_id`) REFERENCES `shipments` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `sliders`
--
ALTER TABLE `sliders`
  ADD CONSTRAINT `sliders_channel_id_foreign` FOREIGN KEY (`channel_id`) REFERENCES `channels` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `subscribers_list`
--
ALTER TABLE `subscribers_list`
  ADD CONSTRAINT `subscribers_list_channel_id_foreign` FOREIGN KEY (`channel_id`) REFERENCES `channels` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `subscribers_list_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE SET NULL;

--
-- Limitadores para a tabela `tax_categories_tax_rates`
--
ALTER TABLE `tax_categories_tax_rates`
  ADD CONSTRAINT `tax_categories_tax_rates_tax_category_id_foreign` FOREIGN KEY (`tax_category_id`) REFERENCES `tax_categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tax_categories_tax_rates_tax_rate_id_foreign` FOREIGN KEY (`tax_rate_id`) REFERENCES `tax_rates` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `velocity_contents_translations`
--
ALTER TABLE `velocity_contents_translations`
  ADD CONSTRAINT `velocity_contents_translations_content_id_foreign` FOREIGN KEY (`content_id`) REFERENCES `velocity_contents` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `velocity_customer_compare_products`
--
ALTER TABLE `velocity_customer_compare_products`
  ADD CONSTRAINT `velocity_customer_compare_products_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `velocity_customer_compare_products_product_flat_id_foreign` FOREIGN KEY (`product_flat_id`) REFERENCES `product_flat` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Limitadores para a tabela `wishlist`
--
ALTER TABLE `wishlist`
  ADD CONSTRAINT `wishlist_channel_id_foreign` FOREIGN KEY (`channel_id`) REFERENCES `channels` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `wishlist_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `wishlist_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
